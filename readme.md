I did not create this project but did maintain and expand upon it:

- Added functionality to enable display of alerts within the myGeoTab add-in. 
- Added support for alerting based on reports from the United States National Weather Service. 
