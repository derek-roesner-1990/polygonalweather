import gzip
import ast
import traceback

from tzwhere import tzwhere
import numpy as np
import arrow
import matplotlib
from matplotlib import pyplot
import rasterio
import Nio
import glob
import fiona
import shapely
from shapely import wkb
from shapely import speedups
speedups.enable()
import psycopg2
import requests
import pandas
import redis
import pyproj
from affine import Affine

import data_handler

data_grid_cache = {}
database = redis.StrictRedis(port=26379)

hrrr_crs = ast.literal_eval(data_handler.get_any_data('HRRR:crs'))
transform = data_handler.get_any_data('HRRR:affine')
transform = Affine(*ast.literal_eval(transform))

mrms_grid_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
world_to_mrms_grid_transform = ~mrms_grid_transform

non_hrrr = ['SeamlessHSR_P0_L102_GLL0',
 'GaugeCorrQPE01H_P0_L102_GLL0',
 'LightningProbabilityNext30min_P0_L102_GLL0',
 'GaugeCorrQPE06H_P0_L102_GLL0',
 'RadarOnlyQPE01H_P0_L102_GLL0',
 'RadarOnlyQPE03H_P0_L102_GLL0',
 'POSH_P0_L102_GLL0',
 'PrecipFlag_P0_L102_GLL0',
 'RadarOnlyQPE06H_P0_L102_GLL0',
 'PrecipRate_P0_L102_GLL0',
 'road_temperatures']

hrrr = ['TMP_P0_L103_GLC0',
 'ULWRF_P0_L8_GLC0',
 'VGRD_P0_L103_GLC0',
 'VGTYP_P0_L1_GLC0',
 'VIS_P0_L1_GLC0',
 'WIND_P8_L103_GLC0_max',
 'WIND_P8_L103_GLC0_max1h',
 'REFD_P0_L103_GLC0',
 'RH_P0_L4_GLC0',
 'PWAT_P0_L200_GLC0',
 'LTNG_P0_L10_GLC0',
 'APCP_P8_L1_GLC0_acc',
 'APCP_P8_L1_GLC0_acc1h',
 'CFRZR_P0_L1_GLC0',
 'CICEP_P0_L1_GLC0',
 'CIN_P0_2L108_GLC0',
 'CRAIN_P0_L1_GLC0',
 'CSNOW_P0_L1_GLC0',
 'DSWRF_P0_L1_GLC0']

def cached_layers(variable, arrow_time,interpolated):        
    key = '{}-{}-{}'.format(variable,arrow_time,interpolated)
    if key in data_grid_cache:
        return data_grid_cache[key]
    else:
        data_grid = data_handler.get_layer(variable, arrow_time,latest=False,interpolated=False, verbose=False)
        data_grid_cache[key] = data_grid
        return data_grid

def parse_inrix_string_traffic_no_postgres(current_string):
    timestamp = arrow.get(current_string.split('<')[3].split('"')[-2])
    
    observations = []
    for split in current_string.split('<'):
        try:
            if 'Segment code=' in split:
                code = int(split.split('"')[1])
                speed = int(split.split('"')[5])
                average = int(split.split('"')[7])
                reference = int(split.split('"')[9])
                speed_bucket = int(split.split('"')[13])
                observations.append({'id':code,'speed':speed,'average':average,'reference':reference,'speed_bucket':speed_bucket,'arrow_time':timestamp})
        except:
            continue
    return observations

def get_coordinates(x, y):
    out_proj = pyproj.Proj(hrrr_crs)
    in_proj = pyproj.Proj(init='epsg:4326')
    x,y = x,y
    transformed_x,transformed_y = pyproj.transform(in_proj,out_proj,x,y)
    col_index, row_index = (~transform)*(transformed_x,transformed_y)
    hrrr_row, hrrr_col = (int(round(row_index)),int(round(col_index)))
    
    mrms_col, mrms_row = world_to_mrms_grid_transform*(x,y)
    mrms_row, mrms_col = (int(round(mrms_row)), int(round(mrms_col)))
    
    return {'hrrr':(hrrr_row, hrrr_col),'mrms':(mrms_row, mrms_col)}

def compile_weather_with_traffic(traffic):    
    for incident in traffic:
        try:
            for variable in non_hrrr[0:-1]:
                try:
                    #data_grid = data_handler.get_layer(variable, incident['arrow_time'],latest=False,interpolated=False, verbose=False)
                    data_grid = cached_layers(variable,incident['arrow_time'],interpolated=False)
                    if len(data_grid.shape) > 2:
                        data_grid = data_grid[0,:,:]
                    data_value = data_grid[incident['mrms_coordinates']]
                    incident[variable] = data_value
                except Exception as e:
                    pass
                    #print variable, e
            
            for variable in non_hrrr[-1:]:
                try:
                    #data_grid = data_handler.get_layer(variable, incident['arrow_time'],latest=False,interpolated=False, verbose=False)
                    data_grid = cached_layers(variable,incident['arrow_time'],interpolated=False)
                    if len(data_grid.shape) > 2:
                        data_grid = data_grid[0,:,:]
                    data_value = data_grid[incident['hrrr_coordinates']]
                    incident[variable] = data_value
                except Exception as e:
                    pass
                    #print variable, e
                
            for variable in hrrr:
                try:
                    #data_grid = np.flipud(data_handler.get_layer(variable, incident['arrow_time'],latest=False, verbose=False))
                    data_grid = cached_layers(variable,incident['arrow_time'],interpolated=True)
                    if len(data_grid.shape) > 2:
                        data_grid = data_grid[0,:,:]
                    data_value = data_grid[incident['hrrr_coordinates']]
                    incident[variable] = data_value
                except Exception as e:
                    pass
                    #print variable, e
            
            
        except Exception as e:
            print e
            print incident['arrow_time']

def compile_indices(observations):
    for observation in observations:
        try:
            start_point = ast.literal_eval(database.get('Inrix:XDSegID:{}:endpoints'.format(observation['id'])))[0]
            # start_point being longitude,latitude
            #one_timezone = onetzwhere.tzNameAt(*start_point[::-1])
            #timezoned_timestamp = observation['arrow_time'].to(one_timezone)
            #observation['time_feature'] = timezoned_timestamp.hour*60+timezoned_timestamp.minute
            #observation['start_point'] = start_point

            coordinates = get_coordinates(*start_point)
            observation['mrms_coordinates'] = coordinates['mrms']
            observation['hrrr_coordinates'] = coordinates['hrrr']
        except Exception as e:
            continue
            #traceback.print_exc()

def preprocess_inrix_traffic():
    onetzwhere = tzwhere.tzwhere()

    filepaths = sorted(glob.glob('/home/souellet/inrix_data/current_speed_2017*'))
    observation_count = 0
    for filepath in filepaths[150::30]:
        try:
            with gzip.open(filepath,'r') as opened:
                data_grid_cache.clear()
                
                print arrow.utcnow()
                
                content_string = opened.read()
                observations = parse_inrix_string_traffic_no_postgres(content_string)
                print 'parsed'
                
                compile_indices(observations)
                print 'coordinated'
                
                compile_weather_with_traffic(observations)
                print 'compiled'

                with gzip.open('/home/souellet/Documents/traffic_observations_{}'.format(filepath.split('/')[-1]),'w') as opened:
                    opened.write(str(observations))
                        
                observation_count += 1
                if observation_count > 24:
                    break

        except Exception as e:
            traceback.print_exc()
            print filepath
            continue
    
    
if __name__ == '__main__':
    preprocess_inrix_traffic()