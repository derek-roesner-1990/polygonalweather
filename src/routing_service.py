import re
import json

import requests
import xmltodict

import datetime

address_query_url = "http://nominatim.openstreetmap.org/search?q="
latlon_query_url = "http://nominatim.openstreetmap.org/reverse?"

routing_url = 'http://localhost:8989/route?'

result_format = "&format=json"

pattern = re.compile('lat="(.*)" lon="(.*)".*<time>(.*)</time>')

reference_time = datetime.datetime(1970,1,1)

def parse_gpx(gpx_string):
	lines = gpx_string.split('\n')
	found = False
	for i,line in enumerate(lines):
		if line == '<trk><name>GraphHopper Track</name><trkseg>':
			start = i+1
			found = True
			break
	
	if not found:
		print "Route not found"
		return None

	track = lines[i+1:]
	parsed = []

	for line in track:
		if len(line) < 30:
			break
		lat,lon,time = pattern.findall(line)[0]
		seconds = (datetime.datetime.strptime(time,"%Y-%m-%dT%H:%M:%SZ")-reference_time).total_seconds()
		parsed.append((float(lat),float(lon),seconds))

	return parsed

def get_geocode(address):
	""" Returns geocoding information on either addresses or WGS84 coordinates """

	# Checks for lat lon coordinates
	if re.search('[a-zA-Z]',address):
		split_address = address.split(' ')
		query = "+".join(split_address)
		query = address_query_url+query

	else:
		lat_lon = address.split(',')
		query = 'lat='+lat_lon[0]+'&lon='+lat_lon[1]
		query = latlon_query_url+query
	
	query = query+result_format

	result = json.loads(requests.get(query).content)
	return (result[0]['lat'],result[0]['lon'])

def get_route(points):
	""" Expects a list of two pairs (ordered as lat and lon) minimum as to have a start and end """
	query_string = ""
	for point in points:
		if re.search('[a-zA-Z]',str(point)):
			point = get_geocode(point)
		query_string = query_string + '&point={}%2C{}'.format(*point) 
	query = routing_url+query_string+'&points_encoded=false&type=gpx'
	
	result = requests.get(query).content
	path = parse_gpx(result)
	
	#result = json.loads(requests.get(query).content)
	
	return path


if __name__ == '__main__':
	#get_geocode('981 wellington west, ottawa')
	#get_geocode('47.1234662,-78.453313')
	get_route([(52.187405,-107.072754),(51.0,-102.4)])
	pass