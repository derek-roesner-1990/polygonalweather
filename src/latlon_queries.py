import json

import arrow
import numpy as np
from flask import Flask

import data_handler

application = Flask(__name__)

# Note here: using an inefficient search in coordinate grids to get data location instead of projective geometry, but pyproj isn't installed on the production server and I don't want ot mess it up for a quick demo
# To be fair, it's pretty quick anyway because of numpy
# Also, use flask_socketio's eventlet server eventually, the current one isn't as solid (apparently)

hx,hy = data_handler.get_coordinate_grid('HRRR')
mx,my = data_handler.get_coordinate_grid('MRMS')
gx,gy = data_handler.get_coordinate_grid('GFS')

product_to_data = {
'air_temperature':'TMP_P0_L103_GLC0',
'road_temperature':'road_temperatures',
'windspeed':'WIND_P8_L103_GLC0_max',
'visibility':'VIS_P0_L1_GLC0',
}

product_to_units = {
'air_temperature':'Kelvins',
'road_temperature':'Kelvins',
'windspeed':'meters/seconds',
'visibility':'meters',
}

def fetch_data(data_name,lat,lon,desired_time,x,y,units):
    warning = None
    try:
        if desired_time is None:
            desired_time = arrow.utcnow()    
        else:
            desired_time = arrow.get(desired_time)

        if data_name[0].islower():
            data = data_handler.get_layer(data_name, 'latest')
            desired_time = arrow.utcnow()
            warning = 'If provided, a specific time parameter was ignored as only current values for this product are available'
        else:
            data = data_handler.get_layer(data_name, desired_time, latest=False)
        
        #affine = data.get_any_data('HRRR:affine') #to use for projective geometry
        #crs = data.get_any_data('HRRR:crs')

        lat = float(lat)
        lon = float(lon)

        x_diff = np.abs(x-lon)
        y_diff = np.abs(y-lat)

        search_space = x_diff+y_diff

        if search_space.min() > 0.1:
            return {'error':"Data point is geographically too far from desired coordinates"}

        row, col = np.where(search_space == search_space.min())
        
        row = row[0]
        col = col[0]

        value = round(float(data[row,col]),2)

        return {'warning': warning, 'time': str(desired_time), 'units':units, 'value':value, 'distance_in_degrees_approximate':float(search_space.min())}
    except:
        return {'error':"Either data is missing or the time parameter is out of range"}

@application.route('/latlontime/<product>/<lat>/<lon>/', defaults={'desired_time': None})
@application.route('/latlontime/<product>/<lat>/<lon>/<desired_time>/')
def get_product(product,lat,lon,desired_time):
    data_name = product_to_data[product]
    units = product_to_units[product]
    #Add code to figure out coordinate grids, right now only the HRRR is used
    return json.dumps(fetch_data(data_name,lat,lon,desired_time,hx,hy,units))

@application.route('/what')
def whatis():
    return 'Yup'

if __name__ == "__main__":
    application.run(host='0.0.0.0',port=5354,threaded=False, debug=False)
