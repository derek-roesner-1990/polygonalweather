import ast

import requests

import redis

with open('geoserver_credentials.txt') as opened:
	geoserver_password = opened.read().strip()

def geoserver_trigger(name):
	address = 'https://tiles.weathertelematics.com/geoserver/gwc/rest/seed/wtx:{}.json'.format(name)
	info = "{{'seedRequest': {{'name': 'wtx:{}', 'format': 'image\\/png', 'zoomStart': 0, 'threadCount': 4, 'srs': {{'number': 900913}}, 'zoomStop': 40, 'type': 'truncate'}}}}".format(name)
	
	requests.post(url = address, data= info, auth=('admin',geoserver_password),verify=True, headers={'Content-type':'application/json'})

def clean_cache_local(layer_name):
    cache = redis.StrictRedis(host='127.0.0.1',port=6379)
    
    entries = cache.smembers(layer_name+'-index')
    if len(entries) == 0:
        return

    pipeline = cache.pipeline()
    pipeline.delete(layer_name+'-index')
    pipeline.delete(*entries)
    pipeline.execute()

def clean_cache(layer_name):
    requests.get('http://130.211.132.195:5010/internal/cache_control/{0}/clean_that_layer'.format(layer_name))

def main():
	pass

if __name__ == '__main__':
	main()