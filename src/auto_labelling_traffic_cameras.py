import time
import ast
import traceback
import json

import numpy as np
import arrow
import requests
import psycopg2
import shapely
from shapely import geometry
from shapely import wkb
from shapely import speedups
speedups.enable()

with open('122_credentials.txt','r') as opened:
    credentials = ast.literal_eval(opened.read())

mashape_headers = {"X-Mashape-Key": "4ybIbt7cGLmshyfDJABzUYjgllk9p1bItztjsnohT8ZQ6hsLaZ"}

def get_dry_cameras():
    cursor = psycopg2.connect(**credentials).cursor()
    cursor.execute("select * from traffic_cameras join probabilityzones on ST_Intersects(traffic_cameras.geom,probabilityzones.geom);")
    non_dry = cursor.fetchall()
    cursor.execute("select * from traffic_cameras where ST_Intersects(geom,ST_GeomFromText('POLYGON((-124.0 49.0,-65.0 49.0,-80.0 26.0,-124.0 26.0,-124.0 49.0))',4326));")
    north_american = cursor.fetchall()
    dry = set(north_american)-set(non_dry)
    
    return list(dry)

def get_cameras():
    cursor = psycopg2.connect(**credentials).cursor()
    cursor.execute("select * from traffic_cameras join probabilityzones on ST_Intersects(traffic_cameras.geom,probabilityzones.geom) where probabilityzones.zone_type = 'W';")
    wet_cameras = cursor.fetchall()
    cursor.execute("select * from traffic_cameras join probabilityzones on ST_Intersects(traffic_cameras.geom,probabilityzones.geom) where probabilityzones.zone_type = 'S';")
    snowy_cameras = cursor.fetchall()
    cursor.execute("select * from traffic_cameras join probabilityzones on ST_Intersects(traffic_cameras.geom,probabilityzones.geom) where probabilityzones.zone_type = 'I';")
    icy_cameras = cursor.fetchall()
    cursor.connection.close()

    #new_cameras = [(camera[0], camera[1], camera[2], wkb.loads(camera[3],hex=True)) for camera in cameras]

    return [snowy_cameras,icy_cameras,wet_cameras]

def main():
    cameras = get_cameras()
    how_many = sum(map(lambda x: len(x),cameras))
    print map(lambda x: len(x),cameras)

    base_path = '/home/souellet/Downloads/' 
    paths = ['snowy_images/','icy_images/','wet_images/']

    dry = get_dry_cameras()

    for path,camera_type in zip(paths,cameras):
        current_path = base_path+path
        for camera in camera_type:
            try:
                cam_id = camera[1]
                
                response = requests.get('http://localhost:5055/latest_image/webcamstravel/{}'.format(cam_id))
                timestamp = arrow.get(json.loads(response.content)['timestamp'])
                if (arrow.utcnow().timestamp-timestamp.timestamp) > 3600: # Image has not been updated this past hour
                    continue
                url = camera[2]
                response = requests.get(url)
                with open(current_path+'{}--{}--{}.jpg'.format(arrow.utcnow(),timestamp,cam_id),'w') as opened:
                    opened.write(response.content)
            except:
                traceback.print_exc()
                continue

    print 'Done with bad conditions'

    np.random.shuffle(dry)
    for camera in dry[0:how_many]:
        try:
            current_path = '/home/souellet/Downloads/dry_images/'
            
            cam_id = camera[1]

            response = requests.get('http://localhost:5055/latest_image/webcamstravel/{}'.format(cam_id))
            timestamp = arrow.get(json.loads(response.content)['timestamp'])
            if (arrow.utcnow().timestamp-timestamp.timestamp) > 3600: # Image has not been updated this past hour
                continue
            url = camera[2]
            response = requests.get(url)
            with open(current_path+'{}--{}--{}.jpg'.format(arrow.utcnow(),timestamp,cam_id),'w') as opened:
                opened.write(response.content)
        except:
            traceback.print_exc()
            continue

    print 'Done with dry conditions'

if __name__ == '__main__':
    main()