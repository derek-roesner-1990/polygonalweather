import time
import arrow
import numpy as np
import xgboost
from scipy import ndimage

import postgres_functions

import alarms_and_triggers
import data_handler

import gfs_extra_variables

bst = xgboost.Booster()

bst.load_model('../models/hail_30_features_smaller.xgboost')

def compute_grid(grid):
    shape = grid.shape
    reshaped = grid.reshape(shape[0]*shape[1],shape[2])
    dtest = xgboost.DMatrix(reshaped)
    output = bst.predict(dtest)
    output -= 1 # tone down hail predictions, temporary solution until better model comes along
    output[output<3] = 0 # Small probabilities are too widespread
    output *= 10
    output = output.reshape(shape[0],shape[1])
    return output

def main(fixed_time = None):
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    start_time  = time.time()

    if fixed_time is None:
        current_time = arrow.utcnow()
    else:
        current_time = fixed_time

    layers = []
    for variable in gfs_extra_variables.hail:
        variable = "{}--{}".format('GFS',variable)
        try:
            current_array = data_handler.get_layer(variable, current_time, latest=False)
        except:
            if variable.endswith('avg') or variable.endswith('acc'):
                variable = variable+'_intermediate1h'
                current_array = data_handler.get_layer(variable, current_time, latest=False)
                 
        if len(current_array.shape) > 2:
            current_array = current_array[0,:,:]
        layers.append(current_array)
    
    grid_0 = np.dstack(layers)

    output_fine = compute_grid(grid_0)
    
    if fixed_time is None:
        data_handler.store_layer(output_fine, 'gfs-hail', arrow.utcnow(), time_alive=3600, latest=True)
    else:
        data_handler.store_layer(output_fine, 'gfs-hail', current_time, time_alive=-1, latest=False)

    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()