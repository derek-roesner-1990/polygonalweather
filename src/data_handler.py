import ast

import socket
import zstd
import arrow
import numpy as np
from redis import StrictRedis

if socket.gethostname() == 'ubuntu-cv1':
    #database = StrictRedis(host='localhost', port=6389)
    database = StrictRedis(host='localhost', port=26379)
else:
    database = StrictRedis(host='localhost', port=6379)

compressor = zstd.ZstdCompressor(level=3,write_content_size=True)
decompressor = zstd.ZstdDecompressor()

categoricals = ['CSNOW','CRAIN','CICEP','CFRZR']

to_append_1h = ['APCP_P8_L1_GLC0_acc', 'WIND_P8_L103_GLC0_max', 'APCP_P8_L1_GRLL0_acc', 'WIND_P8_L103_GRLL0_max', ]

def ssdb_keys(start,end,limit):
    return database.execute_command('keys',start,end,limit)

database.ssdb_keys = ssdb_keys

def process_bytes(key, data_name, verbose=True):
    string_shape = database.get('persistent:shape:{0}'.format(data_name))
    shape = ast.literal_eval(string_shape)
    #split_shape = string_shape.split('-')
    #shape = (int(split_shape[0]),int(split_shape[1]))
    #
    if verbose:
        print shape, data_name, key
    value = decompressor.decompress(database.get(key))
    try:
        array = np.frombuffer(value,np.float32).reshape(shape)
    except:
        print 'Should not have happened'
        array = np.frombuffer(value,np.float64).reshape(shape)
    array = np.array(array)

    return array

def interpolate_arrays_3h(data_name, desired_time):
    past = desired_time.floor('hour')
    pasts = [past,past.replace(hours=-1),past.replace(hours=-2),past.replace(hours=-3)]

    found = False
    for past in pasts:
        past_key = '{0}:{1}'.format(data_name, past)
        if past_key in database:
            found = True
            break

    if not found:
        print data_name, 'not found for', desired_time
        raise RuntimeError('Data array is missing: {} - {}'.format(data_name, desired_time))

    future = past.replace(hours=+3)

    delta = float(desired_time.timestamp - past.timestamp)/10800.0

    future_key = '{0}:{1}'.format(data_name, future)
    
    past_array = process_bytes(past_key, data_name)
    future_array = process_bytes(future_key, data_name)

    past_array += (future_array-past_array)*delta

    return past_array


def interpolate_arrays(data_name, desired_time, verbose=True):
    past = desired_time.floor('hour')
    future = past.replace(hours=+1) # #.timestamp+1 # timestamp always rounds down

    delta = float(desired_time.timestamp - past.timestamp)/3600.0

    past_key = '{0}:{1}'.format(data_name, past)

    if (past_key not in database) and data_name in to_append_1h:
        data_name = data_name+'1h' # grib2 code quirk, '1h' is appended for f02 onward for wind speed
        past_key = '{0}:{1}'.format(data_name, past)

    future_key = '{0}:{1}'.format(data_name, future)

    if (future_key not in database) and data_name in to_append_1h:
        data_name = data_name+'1h' # grib2 code quirk, '1h' is appended for f02 onward for wind speed
        future_key = '{0}:{1}'.format(data_name, future)

    #if data_name in to_append_1h:
    #    data_name = data_name+'1h'
    #future_key = '{0}:{1}'.format(data_name, future)

    past_array = process_bytes(past_key, data_name, verbose=verbose)
    future_array = process_bytes(future_key, data_name, verbose=verbose)

    past_array += (future_array-past_array)*delta

    return past_array

def get_coordinate_grid(source):
    key = 'persistent:{0}:x'.format(source)
    x_grid = process_bytes(key, source)

    key = 'persistent:{0}:y'.format(source)
    y_grid = process_bytes(key, source)

    return x_grid, y_grid

def accumulate_array_3h(data_name, desired_time):
    past_hours = int(data_name.split('_')[-1])
    valid_time = desired_time.floor('hour').replace(hours=+1)
    if data_name.startswith('accumulation_ECMWF'):
        variable = 'TP_GDS0_SFC_acc3h'
    else:
        raise RuntimeError('Accumulation failed because of a wrong argument')

    arrays = []
    for hour_difference in range(0,past_hours,3):
        array = interpolate_arrays_3h(variable, desired_time.replace(hours=-hour_difference))
        arrays.append(array)
    
    master_array = np.zeros(arrays[-1].shape,dtype=np.float32)
    for array in arrays:
        master_array += array

    return master_array

def accumulate_array(data_name, desired_time):
    past_hours = int(data_name.split('_')[-1])
    valid_time = desired_time.floor('hour').replace(hours=+1)
    if data_name.startswith('accumulation_MRMS'):
        variable = 'GaugeCorrQPE01H_P0_L102_GLL0'
    else:
        variable = 'APCP_P8_L1_GLC0_acc'

    arrays = []

    for hour_difference in range(1,past_hours+1):
        past_time = valid_time.replace(hours=-hour_difference)
        past_key = '{0}:{1}'.format(variable, past_time)
        if not database.exists(past_key):
            past_key = '{0}:{1}'.format(variable+'1h',past_time)
        past_array = process_bytes(past_key,variable)
        arrays.append(past_array)

    master_array = np.zeros(arrays[-1].shape,dtype=np.float32)
    for array in arrays:
        master_array += array

    return master_array

def get_closest_key(keys, desired_time):
    time_differences = np.abs(map(lambda x: (desired_time-arrow.get(':'.join(x.split(':')[1:]))).total_seconds(),keys))
    return keys[np.argmin(time_differences)]

def get_closest_layer(data_name, desired_time, verbose=True):
    past = desired_time.floor('hour').replace(hours=-1)
    future = past.replace(hours=+2) # #.timestamp+1 # timestamp always rounds down

    past_key = '{0}:{1}'.format(data_name, past)
    future_key = '{0}:{1}'.format(data_name, future)

    possible_keys = database.ssdb_keys(past_key,future_key,60) # limit of 60 should allow show all keys
    key = get_closest_key(possible_keys,desired_time)

    array = process_bytes(key,data_name, verbose=verbose)

    return array

def get_layer(data_name, desired_time, latest=True, interpolated=True, verbose=True, interpolation_timestep=1):

    if latest or desired_time == 'latest':
        key = database.get('{0}:{1}'.format(data_name,desired_time))
        array = process_bytes(key, data_name, verbose=verbose)

    elif desired_time == 'None':
        key = 'persistent:{0}'.format(data_name)
        array = process_bytes(key, data_name, verbose=verbose)

    elif data_name.startswith('accumulation'):
        if interpolation_timestep == 3:
            array = accumulate_array_3h(data_name, desired_time)
        else:
            array = accumulate_array(data_name, desired_time)

    elif interpolated:

        if interpolation_timestep == 3:
            array = interpolate_arrays_3h(data_name, desired_time)
        else:
            array = interpolate_arrays(data_name, desired_time,verbose=verbose)

        cflag = data_name[0:5]
        if cflag in categoricals: # Makes sure we give categorical variables (0 and 1 for HRRR)
            array = np.round(array)

    else:
        array = get_closest_layer(data_name, desired_time,verbose=verbose)

    return array

def get_any_data(data_name):
    return database.get('persistent:{0}'.format(data_name))

def store_layer(array, data_name, valid_time, time_alive=9000, latest=True):
    key = '{0}:{1}'.format(data_name,valid_time)

    compressed = compressor.compress(array.tobytes())

    database.set(key, compressed)
    if time_alive > 0:
        database.expire(key,time_alive)

    if latest:
        database.set('{0}:latest'.format(data_name), key)

    shape_check(array, data_name)

def store_persistent_layer(array, data_name):
    key = 'persistent:{0}'.format(data_name)

    compressed = compressor.compress(array.tobytes())
    database.set(key, compressed)

    #shape_check(array, data_name)

def store_any_data(data, data_name):
    """ Expects a string """
    key = 'persistent:{0}'.format(data_name)

    database.set(key,data)

def shape_check(array, data_name):
    shape_key = 'persistent:shape:{0}'.format(data_name)

    if not database.exists(shape_key):
        shape_value = str(array.shape)#'{0}-{1}'.format(array.shape[0],array.shape[1])
        database.set(shape_key,shape_value)

def existence_check(key):
    return database.exists(key)
