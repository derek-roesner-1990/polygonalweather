import time
import socket
import sys
sys.setcheckinterval(10000000)

import socketIO_client
import rtree
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()

import postgres_functions
import alarms_and_triggers

frontend_socket_trigger = True

def get_intersected_roads(road_index, prob_zones, roads):
    prepared_zones = [prepared.PreparedGeometry(prob_zone[0]) for prob_zone in prob_zones]

    print 'Intersecting roads'
    intersected = []
    for i,prepared_zone in enumerate(prepared_zones):
        prob_zone = prob_zones[i]
        for road in road_index.intersection(prob_zone[0].bounds):
            if prepared_zone.contains(roads[road]):
                intersected.append((roads[road], prob_zone[1], prob_zone[2]))
            elif prepared_zone.intersects(roads[road]):
                intersected.append((prob_zone[0].intersection(roads[road]),prob_zone[1], prob_zone[2]))
    return intersected

def main():
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    print "Creating index"
    try:
        with open('../persistent/larger_roads.wkb','r') as opened:
            roads = wkb.load(opened, hex=True)
    except:
        with open('/home/souellet/polygonalweather/persistent/larger_roads.wkb','r') as opened:
            roads = wkb.load(opened, hex=True)

    road_index = rtree.index.Index()
    for i,road in enumerate(roads):
        road_index.insert(i,road.bounds)

    if frontend_socket_trigger:
        if socket.gethostname() == 'ubuntu-cv1':
            current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('localhost', 8043))
    server_socket.listen(25)

    while True:
        print 'Waiting for connection'
        (client_socket,address) = server_socket.accept()

        forecast_time = int(client_socket.recv(1024))
        run_time = int(time.time())

        start_time = time.time()
        postgres_functions.push_start_time(['assessed_roads_f0'+str(forecast_time)],start_time)
        
        try:
            prob_zones = postgres_functions.retrieve_prob_zones_forecasted(forecast_time)
            #forecast_time = prob_zones[0][-1]
            print "Intersecting", time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
            intersected = get_intersected_roads(road_index, prob_zones, roads)
            
            print "Pushing to db", time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
            postgres_functions.push_assessed_roads_forecasted_temp_table(intersected, run_time, forecast_time)
            postgres_functions.push_end_time(['assessed_roads_f0'+str(forecast_time)],start_time,time.time())
            print time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        except Exception as e:
            print e

        reload(alarms_and_triggers)
        alarms_and_triggers.geoserver_trigger('assessed_roads_f{}'.format(forecast_time))

        if frontend_socket_trigger:
            current_socketIO.emit('assessed_roads_trigger', {'forecast': forecast_time, 'time':int(time.time())})
    
    current_socketIO.disconnect()

if __name__ == '__main__':
    main()
