import time

import live_feed_functions
import forecast_parameters
import postgres_functions

def main():
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    start_time = time.time()
    postgres_functions.push_start_time(['hrrr_download'],start_time)
    live_feed_functions.hrrproducts()
    live_feed_functions.forecasted_hrrrproducts(forecast_time=forecast_parameters.forecast_times[0])
    #for forecast_time in forecast_parameters.forecast_times:
    #    live_feed_functions.forecasted_hrrrproducts(forecast_time=forecast_time)

    live_feed_functions.remove_old_files()
    postgres_functions.push_end_time(['hrrr_download'],start_time,time.time())
    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()