"""

Transforms gridded datasets into thresholded geolocated polygons.
Many variables reside in parameters.py

"""
import socket
import sys
import time
import calendar
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import colors
from scipy import ndimage
from scipy import misc
import Nio
import fiona
import shapely
from shapely import wkb
from shapely import ops
import geojson
import json
import ast
import os
import socketIO_client
import toolz
import arrow

import postgres_functions
import data_handler

import mongodb_functions
from parameters import *

if using_geotab:
    import geotab_functions

import live_feed_functions
from live_feed_functions import hrrproducts, mrmsproducts, remove_old_files
from contour_functions import polygonize

import global_radar_functions
import alarms_and_triggers


# a distinct, cleaner function to handle the global radar
def process_global_radar(geolayers, mongo_client = None, observations = None, cameras = None, entities= None):
    ## processing of the global radar ##

    thresholds = threshold_map['seamless'] # use the thresholds from the seamless radar product (dumb name)
    dbz_colours = ['#6f6a32','#00eced','#009ef9','#0300cc','#02fd02','#01c501','#008e00','#fdf802','#e5bc00','#ff2700','#fd0000','#d40000','#bc0000','#f800fd','#9854c6','#fdfdfd']
    current_key = "global_radar"

    # set the master grid, onto which the image will be mapped to dbz
    master_lons = np.linspace(-180, 180, num=6000, endpoint=True)
    master_lats = np.flipud(np.linspace(-90, 90, num=3000, endpoint=True))

    full_grid = global_radar_functions.generate_radar_grid()

    print 'Creating polygons for global_radar'

    # levels are a list of shapely objects corresponding to the contours for each threshold
    # thresholds are the ranges for each set of contours
    levels, thresholds = global_radar_functions.polygonize_global_radar(master_lons, master_lats, full_grid, thresholds=thresholds)

    records = []

    for i, level in enumerate(levels):
        print(str(i) + " is being proessed.")

        try:
            exteriors = []
            print("length is " + str(len(level)))
            points_total = [x for x in level if x.geom_type == "Point"]
            only_polygons = [x.buffer(0.0001) for x in level if x.geom_type != 'Point'] # add a small buffer to prevent self-intersections
            print("there are " + str(len(points_total)) + " points")
            for geom in only_polygons:
                exteriors.append(shapely.geometry.Polygon(geom.exterior))
            multipolygon = ops.unary_union(exteriors)
            multipolygon = multipolygon.simplify(0.001, preserve_topology=True)
            #multipolygon = ops.unary_union(level) # combine all the contours for a specific threshold range into one object
            if multipolygon.area == 0:
                continue
        except Exception as e:
            print("for now")
            print(e)
            print("error")
            continue

        print("S1")
        
        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if 'coordinates' not in geolayer_dict.keys():
            continue

        print("S2")

        if i < (len(thresholds)-1):
            geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
        else:
            geolayer_dict['thresholdValues'] = [thresholds[i],999999.0]

        geolayer_dict['colour'] = dbz_colours[i]
        geolayer_dict['variableName'] = "global_radar"

        print("S3")
        if thresholds[i] < 5:
            riskLevel = 1
        elif thresholds[i] >= 5 and thresholds[i] < 30:
            riskLevel = 2
        elif thresholds[i] >= 30:
            riskLevel = 3

        new_geolayer_dict = dict()

        new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
        new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel}
        new_geolayer_dict['properties']['shape'] = multipolygon

        records.append(new_geolayer_dict)

        print("S4")
        #if mongo_client is not None:
        #    mongodb_functions.process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)
        #    mongodb_functions.process_cameras(mongo_client, cameras, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)

        # Adds to entities
        #if using_geotab:
        #    geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, current_key)

        print("S5")
    print("Processing of global radar done.")
    geolayers.extend(records)




def process_file(variable_index, geolayers, mongo_client = None, observations = None, cameras = None, entities= None):
    """ Opens and selects desired variables, returns records of polygons with relevant info """
    arrow_time = arrow.utcnow()

    if variable_indices.index(variable_index) < (len(variable_indices)-len(mrms_files)):
        source = 'HRRR'
        data = data_handler.get_layer(variable_index, arrow_time, latest=False)
    else:
        source = 'MRMS'
        data = data_handler.get_layer(variable_index, 'latest')

    x,y = data_handler.get_coordinate_grid(source)

    variable_name = variable_name_index_dict[variable_index]

    if data.shape[0] > 2000:
        gaussian_sigma = 2.0
    else:
        gaussian_sigma = 1.0

    if variable_name == 'posh':
        gaussian_sigma = 0.5
    
    data = ndimage.gaussian_filter(data,sigma=gaussian_sigma) #TODO Have it automatically computed based on grid resolution
    #data = ndimage.median_filter(data,(5,5))
    print 'Creating polygons for', variable_name

    current_key = None
    thresholds = None
    for key in threshold_map.keys():
        if key in variable_name.lower().replace(' ',''):
            current_key = key

    if 'Radar Precipitation' in variable_name:
        current_key = 'qpe'
    if 'Lightning' in variable_name:
        current_key = 'lightning'
    if 'Severe Hail' in variable_name:
        current_key = 'posh'
    if 'Downward' in variable_name:
        current_key = 'radiation'
    if 'Seamless' in variable_name:
        current_key = 'seamless'

    thresholds = threshold_map[current_key]
    print current_key

    # Takes care of whether a high value represents a high risk or the inverse
    inverted = False
    if current_key in inverse_variables:
        inverted = True

    # levels are a list of shapely objects corresponding to the contours for each threshold
    # thresholds are the ranges for each set of contours
    levels, thresholds = polygonize(x,y,data,which_variable=current_key,thresholds=thresholds, inverted=inverted)

    #opened.close()

    #multipolygons = []
    #outputs = []
    records = []

    for i, level in enumerate(levels):
        if current_key == 'seamless': # this seems redundant
            exteriors = []
            for geom in level:
                exteriors.append(shapely.geometry.Polygon(geom.exterior))
            multipolygon = ops.unary_union(exteriors)
            multipolygon = multipolygon.simplify(0.001, preserve_topology=True)
        else:
            multipolygon = ops.unary_union(level) # combine all the contours for a specific threshold range into one object
        if multipolygon.area == 0:
            continue
        if current_key == 'qpe':
            """
            all_land = []
            with fiona.open('landpolygon/ne_10m_land.shp') as opened:
                for geom in opened:
                    all_land.append(shapely.geometry.shape(geom['geometry']))
            northamerica = all_land[0][3821]
            polygon = all_land[0]

            cut_poly= polygon.intersection(shapely.geometry.Polygon([(-50,15),(-140,15),(-140,60),(-50,60)]))

            with open('northamerica.txt', 'w') as opened:
                opened.write(cut_poly.wkb_hex)
            """
            with open('landpolygon/northamerica.txt','r') as opened:
                northamerica = wkb.loads(opened.read(),hex=True)

            multipolygon = multipolygon.intersection(northamerica) # store the contours that lie within NA

        if not multipolygon.area > 0:
            #print geojson.dumps(multipolygon)
            continue

        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if current_key in inverse_variables:
            if i == 0:
                geolayer_dict['thresholdValues'] = [-999999.0, thresholds[i]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i-1],thresholds[i]]
        else:
            if i < (len(thresholds)-1):
                geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i],999999.0]

        # A bit ugly
        geolayer_dict['colour'] = color_map[current_key][value_to_string_map[current_key][thresholds[i]]]
        geolayer_dict['variableName'] = current_key

        if current_key == 'seamless':
            if thresholds[i] < 5:
                riskLevel = 1
            elif thresholds[i] >= 5 and thresholds[i] < 30:
                riskLevel = 2
            elif thresholds[i] >= 30:
                riskLevel = 3
        else:
            riskLevel = risk_map[value_to_string_map[current_key][thresholds[i]]]
        '''
        if current_key in inverse_variables:
            riskLevel *= -1
            riskLevel += 4
        '''
        new_geolayer_dict = dict()

        new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
        new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel}
        new_geolayer_dict['properties']['shape'] = multipolygon

        records.append(new_geolayer_dict)

        if mongo_client is not None:
            mongodb_functions.process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)
            mongodb_functions.process_cameras(mongo_client, cameras, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)

        # Adds to entities
        if using_geotab:
            geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, current_key)

    geolayers.extend(records)

def main(intermediate=False):
    """ Downloads files and push geojson polygons """
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    start_time = time.time()
    postgres_functions.push_start_time(['visibility','windspeed','qpe','lightning','posh','seamless'],start_time)

    mongo_client = None
    observations = None
    geolayers = []
    entities = []
    
    cameras = None
    if using_mongo:
        mongo_client, observations = mongodb_functions.prepare_mongo()
        cameras = mongodb_functions.get_cameras(mongo_client)

    #filepaths = [hrrproducts()]
    #filepaths = filepaths*number_of_hrrr_products
    #for mrms_file in mrms_files:
    #    filepaths.append(mrmsproducts(mrms_file))

    #for filepath,variable_index in zip(filepaths,variable_indices):

    if intermediate:
        for variable_index in variable_indices[:-1]:
            process_file(variable_index, geolayers, mongo_client, observations, cameras, entities=entities)
        return geolayers

    for variable_index in variable_indices:
        process_file(variable_index, geolayers, mongo_client, observations, cameras, entities=entities)

    # call the function to process the global radar
    try:
        process_global_radar(geolayers, mongo_client, observations, cameras, entities=entities)
    except Exception as e:
        print "Global radar failed."
        print e 

    if mongo_client is not None:
        mongodb_functions.push_to_mongo(mongo_client, [toolz.update_in(geolayer, ['properties','shape'], str) for geolayer in geolayers])
        mongodb_functions.push_devices_to_mongo(mongo_client, observations)
        mongodb_functions.push_cameras_to_mongo(mongo_client, cameras)

    if using_geotab: 
        cleaned_up = geotab_functions.deal_with_holes(entities)
        geotab_functions.write_multicalls(cleaned_up,'geotab_live_contours')
    #else: # Need to make sure we don't forget shenanigans like that down the line
    if using_postgres:
        postgres_functions.push_contours(geolayers)

    alarms_and_triggers.geoserver_trigger('hydroplaning')
    alarms_and_triggers.geoserver_trigger('windspeed')
    alarms_and_triggers.geoserver_trigger('visibility')
    alarms_and_triggers.geoserver_trigger('seamless')
    alarms_and_triggers.geoserver_trigger('posh')
    alarms_and_triggers.geoserver_trigger('lightning')

    end_time = time.time()
    print 'Updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    postgres_functions.push_end_time(['visibility','windspeed','qpe','lightning','posh','seamless', 'global_radar'],start_time,end_time)
    print 'Done updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    #remove_old_files()

    try:
        if socket.gethostname() == 'ubuntu-cv1' and False:
            current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
            #bla
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')

        current_socketIO.emit('contours_trigger', time.time())
        current_socketIO.emit('hrrr_trigger', time.time())
        current_socketIO.disconnect()
    
    except Exception as e:
        print e
    
    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()
