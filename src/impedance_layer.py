import time

import arrow
import Nio
import shapely
from shapely import ops
from shapely import speedups
from shapely import prepared
from shapely import geometry
speedups.enable()

import contour_functions
import postgres_functions
import live_feed_functions

speed_mapping = {'W':0.93, 'S':0.81, 'I':0.65}

precip_rate_variable = 'PRATE_P0_L1_GLC0'
rates = [0.0,0.02,0.04,0.06,0.08]

def get_current_data():
	zones = postgres_functions.retrieve_prob_zones()
	#hrrr_file = live_feed_functions.hrrproducts()

	return zones, time.time()# hrrr_file

def get_forecasted_data(forecast_time=1):
	zones = postgres_functions.retrieve_prob_zones_forecasted(forecast_time)
	#hrrr_file = live_feed_functions.forecasted_hrrrproducts(forecast_time = '0'+str(forecast_time+1))	

	return zones, arrow.utcnow().floor('hour').timestamp+(3600*forecast_time)#, hrrr_file

def produce_impedance_polygons(zones, valid_time, forecast_time):
	impedance_polygons = []
	for zone in zones:
		impedance_polygons.append({'shape':zone[0],'road_condition':zone[1],'speed_factor':speed_mapping[zone[1]], 'valid_time':valid_time, 'forecast_time':forecast_time})
	
	return impedance_polygons 

	if False:
		opened = Nio.open_file(hrrr_file)
		precip_rate = opened.variables[precip_rate_variable].get_value()
		x = opened.variables['gridlon_0'].get_value()
		y = opened.variables['gridlat_0'].get_value()
		opened.close()

		levels, level_values = contour_functions.polygonize(x,y,precip_rate,thresholds=rates)
		a = time.time()
		for level in levels:
			multipolygon = geometry.GeometryCollection(level)
			for zone in impedance_polygons:	
				print multipolygon.intersects(zone['shape'])
			print time.time() - a 

	

	#return impedance_polygons

def main():
	all_polygons = []

	zones, valid_time = get_current_data()
	all_polygons.extend(produce_impedance_polygons(zones, valid_time, 0))
	
	if True:
		for i in range(1,7):
			zones, valid_time = get_forecasted_data(forecast_time=i)
			all_polygons.extend(produce_impedance_polygons(zones, valid_time, i))

	postgres_functions.push_impedance_polygons(all_polygons)

if __name__ == '__main__':
	main()