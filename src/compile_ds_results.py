import json
import time

import pandas
import numpy as np 
from rasterio import Affine

import data_handler

from matplotlib import pyplot
# [250, 303, 787, 850]
def review():
    variables = ['ds_array']
    #variables = ['TMP_P0_L103_GLC0']
    for variable in variables:
        keys = data_handler.database.ssdb_keys(variable,variable[:-1]+'z',1000000)
        arrays = []
        print len(keys)
        for key in keys[1000:2000:5]:
            array = data_handler.process_bytes(key,variable)[250:303,787:850]
            
            pyplot.imshow(array,cmap='inferno')
            pyplot.show()
            
            arrays.append(array)
        sums = np.sum(np.dstack(arrays),axis=2)/len(arrays)
        print sums.min(), sums.max()
        pyplot.imshow(sums, cmap='inferno')
        pyplot.show()

def main():
    ds_array_keys = data_handler.database.ssdb_keys('ds_array','ds_arraz',1000000)

    array = data_handler.process_bytes(ds_array_keys[0],'ds_array')

    print array.shape
    print len(ds_array_keys)
    print ds_array_keys[0], ds_array_keys[-2]
    return
    count_arrays = [np.zeros(shape=array.shape) for _ in range(4)]

    total_count = 0

    for key in ds_array_keys[:-2]:
        total_count += 1
        print total_count
        array = data_handler.process_bytes(key,'ds_array')
        categories = array.astype(np.int32)
        
        count_arrays[0][np.where(categories == 0)] += 1
        count_arrays[1][np.where(categories == 10)] += 1
        count_arrays[2][np.where(categories == 20)] += 1
        count_arrays[3][np.where(categories == 30)] += 1

    print 'Last one:', key

    for i in range(4):
        count_arrays[i] /= total_count
        np.save('/home/souellet/Documents/count_array_{}'.format(i), count_arrays[i])

def find_indices_for_roads():
    positions_to_expand = set()

    arrays = []
    for i in range(4):
        arrays.append(np.load('/home/souellet/Documents/count_array_{}.npy'.format(i)))

    points = pandas.read_csv('/home/souellet/Downloads/Allstate_LinksToPoints_wgs84_northernIL.csv')
    xs = points['POINT_X']
    ys = points['POINT_Y']
    ids = points['link_id']

    mrms_grid_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
    world_to_grid_transform = ~mrms_grid_transform    

    links = {}
    for link_id, x, y in zip(ids,xs,ys):
        col, row = world_to_grid_transform*(x,y) #(longitude,latitude)
        links[link_id] = {}

        position = (int(round(row)),int(round(col)))
        conditions = ['dry','wet','snowy','icy']
        #flagged = False
        for condition,array in zip(conditions,arrays):
            if condition == 'dry':
                value = array[position]
                if value == 1.0:
                    positions_to_expand.add(position)
                    #flagged = True
            
            if position in positions_to_expand:
                values = array[position[0]-1:position[0]+2,position[1]-1:position[1]+2]
                values = [one_value for one_value in values.flatten() if (one_value < 0.9999 and one_value > 0.0000001)]
    
                value = np.mean(values)
            else:
                value = array[position]

            links[link_id][condition] = value 
    
    with open('/home/souellet/Downloads/allstate_IL_v2.json','w') as opened:
        opened.write(json.dumps(links))

def transform_to_csv():
    with open('/home/souellet/Downloads/allstate_IL_v2.json','r') as opened:
        links = json.loads(opened.read())

    keys = links.keys()
    to_write = ''
    for key in keys:
        to_write += '{},{},{},{},{}\n'.format(key,links[key]['dry'],links[key]['wet'],links[key]['snowy'],links[key]['icy'])

    with open('/home/souellet/Downloads/allstate_IL_v2.csv','w') as opened:
        opened.write('link_id,dry,wet,snowy,icy\n')
        opened.write(to_write)


if __name__ == '__main__':
    find_indices_for_roads()
    transform_to_csv()
    #main()