process_name = 'gfs_contours'

color_map = dict()
color_map['qpe'] = dict()
color_map['visibility'] = dict()
color_map['windspeed'] = dict()
color_map['posh'] = dict()
color_map['lightning'] = dict()
color_map['lightning_density'] = dict()
color_map['seamless'] = dict()

color_map['qpe']['low'] = '#E09485'
color_map['visibility']['low'] ='#ADE0EB'
color_map['windspeed']['low'] = '#85E085'

color_map['qpe']['medium'] = '#FF5533'
color_map['visibility']['medium'] = '#3DD6F5'
color_map['windspeed']['medium'] = '#089208' #3DF53D'

color_map['qpe']['high'] = '#CC2200'
color_map['visibility']['high'] ='#004C99'
color_map['windspeed']['high'] = '#044904' #009900'

color_map['posh']['low'] = '#FFFF99'
color_map['posh']['medium'] = '#FFFF00'
color_map['posh']['high'] = '#595904'

color_map['lightning']['low'] = '#BF7FBF'
color_map['lightning']['medium'] = '#990099'
color_map['lightning']['high'] = '#400040'

color_map['lightning_density']['low'] = '#BF7FBF'
color_map['lightning_density']['medium'] = '#990099'
color_map['lightning_density']['high'] = '#400040'

#seamless_colors = ['#de78b2','#711f88','#3c1148','#c8bf59','#6f6a32','#00eced','#009ef9','#0300cc','#02fd02','#01c501','#008e00','#fdf802','#e5bc00','#ff2700','#fd0000','#d40000','#bc0000','#f800fd','#9854c6','#fdfdfd']
seamless_colors = ['#6f6a32','#00eced','#009ef9','#0300cc','#02fd02','#01c501','#008e00','#fdf802','#e5bc00','#ff2700','#fd0000','#d40000','#bc0000','#f800fd','#9854c6','#fdfdfd']

threshold_map = dict()
threshold_map['qpe'] = [2,4,6]
threshold_map['visibility'] = [500,1000,3000]
threshold_map['windspeed'] = [10,15,20]
threshold_map['posh'] = [1,33,66]
threshold_map['lightning'] = [1,33,66]
threshold_map['lightning_density'] = [0.001,2,4] # Dimensionless threat number
#threshold_map['seamless'] = [-25,-20,-15,-10,-5,0,5,10,15,20,25,30,35,40,45,50,55,60,65,70]
threshold_map['seamless'] = [-5,0,5,10,15,20,25,30,35,40,45,50,55,60,65,70]

for i,thresholdvalue in enumerate(threshold_map['seamless']):
    color_map['seamless'][str(thresholdvalue)] = seamless_colors[i]

keys = ['qpe', 'visibility', 'windspeed', 'posh', 'lightning', 'seamless','lightning_density']

inverse_variables = ['visibility']

value_to_string_map = dict()
for key in keys:
    value_to_string_map[key] = dict()
    if key in inverse_variables:
        value_to_string_map[key][threshold_map[key][-1]] = 'low'
        value_to_string_map[key][threshold_map[key][0]] = 'high'
        value_to_string_map[key][threshold_map[key][1]] = 'medium'
    elif key == 'seamless':
        for thresholdvalue in threshold_map[key]:
            value_to_string_map[key][thresholdvalue] = str(thresholdvalue)
    else:
        value_to_string_map[key][threshold_map[key][-1]] = 'high'
        value_to_string_map[key][threshold_map[key][0]] = 'low'
        value_to_string_map[key][threshold_map[key][1]] = 'medium'


risk_map = dict()
risk_map['high'] = 1
risk_map['medium'] = 0.6
risk_map['low'] = 0.3

risk_map_level = dict()
risk_map_level['high'] = 3
risk_map_level['medium'] = 2
risk_map_level['low'] = 1

speed_mapping = dict()
speed_mapping['high'] = 0.9
speed_mapping['medium'] = 0.95
speed_mapping['low'] = 0.98


forecasted_times = []
#forecasted_times = range(1,2)

variable_indices = ['VIS_P0_L1_GLL0', 'GUST_P0_L1_GLL0', 'APCP_P8_L1_GLL0_acc']

source = 'GFS'
variable_names = ['visibility', 'windspeed', 'qpe', 'posh', 'lightning']

variable_name_index_dict = dict(zip(variable_indices+['gfs-hail','gfs-lightning'],variable_names))

using_postgres = True
using_geotab = False