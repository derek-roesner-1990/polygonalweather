﻿//This script is intended to be ran at https://my.geotab.com/sdk/api/apiRunner.html and can be used to automate the removal of ZoneTypes within a specific database.

var zone_types = [ 
{'comment': '', 'name': 'Alert', 'id': 'b500'},
{'comment': '', 'name': 'Hydroplaning Risk Low', 'id': 'b600'},
{'comment': '', 'name': 'Hydroplaning Risk Medium', 'id': 'b700'},
{'comment': '', 'name': 'Hydroplaning Risk High', 'id': 'b800'},
{'comment': '', 'name': 'Low Visibility Risk High', 'id': 'b900'},
{'comment': '', 'name': 'Low Visibility Risk Medium', 'id': 'bA00'},
{'comment': '', 'name': 'Low Visibility Risk Low', 'id': 'bB00'},
{'comment': '', 'name': 'Wind Risk High', 'id': 'bC00'},
{'comment': '', 'name': 'Wind Risk Medium', 'id': 'bD00'},
{'comment': '', 'name': 'Wind Risk Low', 'id': 'bE00'},
{'comment': '', 'name': 'Hail Risk High', 'id': 'bF00'},
{'comment': '', 'name': 'Hail Risk Medium', 'id': 'b1000'},
{'comment': '', 'name': 'Hail Risk Low', 'id': 'b1100'},
{'comment': '', 'name': 'Lightning Risk High', 'id': 'b1200'},
{'comment': '', 'name': 'Lightning Risk Medium', 'id': 'b1300'},
{'comment': '', 'name': 'Lightning Risk Low', 'id': 'b1400'},
{'comment': '', 'name': 'Icing Risk High', 'id': 'b1500'},
{'comment': '', 'name': 'Icing Risk Medium', 'id': 'b1600'},
{'comment': '', 'name': 'Icing Risk Low', 'id': 'b1700'},
{'comment': '', 'name': 'No Alert', 'id': 'b1800'},
{'comment': '', 'name': 'Air Quality Alert', 'id': 'b1900'},
{'comment': '', 'name': 'Air Stagnation Advisory', 'id': 'b1A00'},
{'comment': '', 'name': 'Blizzard Warning', 'id': 'b1B00'},
{'comment': '', 'name': 'Blizzard Watch', 'id': 'b1C00'},
{'comment': '', 'name': 'Blowing Dust Advisory', 'id': 'b1D00'},
{'comment': '', 'name': 'Dense Fog Advisory', 'id': 'b1E00'},
{'comment': '', 'name': 'Dense Smoke Advisory', 'id': 'b1F00'},
{'comment': '', 'name': 'Dust Storm Warning', 'id': 'b2000'},
{'comment': '', 'name': 'Extreme Fire Danger', 'id': 'b2100'},
{'comment': '', 'name': 'Extreme Wind Warning', 'id': 'b2200'},
{'comment': '', 'name': 'Fire Warning', 'id': 'b2300'},
{'comment': '', 'name': 'Fire Weather Watch', 'id': 'b2400'},
{'comment': '', 'name': 'Flash Flood Watch', 'id': 'b2500'},
{'comment': '', 'name': 'Flash Flood Warning', 'id': 'b2600'},
{'comment': '', 'name': 'Flood Advisory', 'id': 'b2700'},
{'comment': '', 'name': 'Flood Warning', 'id': 'b2800'},
{'comment': '', 'name': 'Flood Watch', 'id': 'b2900'},
{'comment': '', 'name': 'Freezing Fog Advisory', 'id': 'b2A00'},
{'comment': '', 'name': 'Freezing Rain Advisory', 'id': 'b2B00'},
{'comment': '', 'name': 'Freezing Spray Advisory', 'id': 'b2C00'},
{'comment': '', 'name': 'Hard Freeze Warning', 'id': 'b2D00'},
{'comment': '', 'name': 'Hard Freeze Watch', 'id': 'b2E00'},
{'comment': '', 'name': 'Heavy Freezing Spray Warning', 'id': 'b2F00'},
{'comment': '', 'name': 'Heavy Freezing Spray Watch', 'id': 'b3000'},
{'comment': '', 'name': 'High Wind Warning', 'id': 'b3100'},
{'comment': '', 'name': 'High Wind Watch', 'id': 'b3200'},
{'comment': '', 'name': 'Hurricane Force Wind Warning', 'id': 'b3300'},
{'comment': '', 'name': 'Hurricane Force Wind Watch', 'id': 'b3400'},
{'comment': '', 'name': 'Ice Storm Warning', 'id': 'b3500'},
{'comment': '', 'name': 'Lake Effect Snow Warning', 'id': 'b3600'},
{'comment': '', 'name': 'Severe Thunderstorm Warning', 'id': 'b3700'},
{'comment': '', 'name': 'Severe Thunderstorm Watch', 'id': 'b3800'},
{'comment': '', 'name': 'Tornado Warning', 'id': 'b3900'},
{'comment': '', 'name': 'Tornado Watch', 'id': 'b3A00'},
{'comment': '', 'name': 'Tropical Storm Warning', 'id': 'b3B00'},
{'comment': '', 'name': 'Tropical Storm Watch', 'id': 'b3C00'},
{'comment': '', 'name': 'Wind Advisory', 'id': 'b3D00'},
{'comment': '', 'name': 'Winter Storm Warning', 'id': 'b3E00'},
{'comment': '', 'name': 'Wet Surface Risk', 'id': 'b2001'},
{'comment': '', 'name': 'Snowy Surface Risk', 'id': 'b2002'},
{'comment': '', 'name': 'Icy Surface Risk', 'id': 'b2003'}
];


function GeoTabAPIRemoveZoneType(entity){
    return new Promise(function(resolve, reject) {
        api.call("Remove",
        {
            typeName : "ZoneType",
            entity: entity
        }, function(result){
            resolve("Removed '" + entity.name + "'.");
        }, function(error){
            reject("Error while removing '" + entity.name + "'.");
        });
    });
}


function removeZoneTypes(zone_types){
    
    console.log("Attempting to remove ZoneTypes.");
    
    for(var i=0; i<zone_types.length; ++i){
        
        GeoTabAPIRemoveZoneType(zone_types[i]).then(
            function(response) {
                console.log(response);
            }, function(error) {
                console.error(error);
            })
    }
}

removeZoneTypes(zone_types);
