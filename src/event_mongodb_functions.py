import pymongo
import shapely
from event_parameters import *

def prepare_mongo():
    mongo_client = pymongo.MongoClient(mongouri)
    input_collection = mongo_client[db_name][input_name]

    devices = input_collection.distinct('metadata.deviceNumber')

    observations = []
    for device in devices:
        observations.extend(list(input_collection.find({'metadata.deviceNumber' : device}, sort=[('time', -1)], limit=1)))

    return mongo_client, observations

def process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds, riskLevel):
    # Looks at whether the observation falls within any of the polygons
    for observation in observations:
        if multipolygon.contains(shapely.geometry.Point(observation['position']['coordinates'])):
            observation[current_key+'Alert'] = value_to_string_map[current_key][thresholds]
            observation[current_key+'RiskLevel'] = riskLevel
            observation[current_key+'AlertValues'] = geolayer_dict['thresholdValues']

def get_cameras(mongo_client):
    collection = mongo_client[db_name]['trafficcameras']
    return list(collection.find())

def process_cameras(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds, riskLevel):
    multipolygon_prep = shapely.prepared.prep(multipolygon)
    for observation in observations:
        if multipolygon_prep.contains(shapely.geometry.Point(observation['location']['coordinates'])):
            observation['displayed'] = True
            observation[current_key+'Alert'] = value_to_string_map[current_key][thresholds]
            observation[current_key+'RiskLevel'] = riskLevel
            observation[current_key+'AlertValues'] = geolayer_dict['thresholdValues']

def push_to_mongo(mongo_client, records):
    if len(records) > 0:
        output_collection = mongo_client[db_name][output_name]
        output_collection.drop()
        output_collection.insert_many(records)

def push_devices_to_mongo(mongo_client, observations):
    if len(observations) > 0:
        output_collection_devices = mongo_client[db_name][output_name_devices]
        output_collection_devices.drop()
        output_collection_devices.insert_many(observations)

def push_cameras_to_mongo(mongo_client, observations):
    if len(observations) > 0:
        output_collection_devices = mongo_client[db_name]['livecameras']
        output_collection_devices.drop()
        for observation in observations:
            to_add = True
            keys = observation.keys()
            if 'displayed' in keys:
                try:
                    output_collection_devices.insert_one(observation)
                except:
                    pass
        

def push_polygons_to_mongo(polygons, mongouri, collection):
    if len(polygons) > 0:
        mongo_client = pymongo.MongoClient(mongouri)
        output_collection = mongo_client[db_name][collection]

        output_collection.drop()
        output_collection.insert_many(polygons)