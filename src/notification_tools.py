import ast
import time
import smtplib
from email.mime.text import MIMEText

import arrow

with open('smtp_credentials.txt','r') as opened:
    smtp_credentials = ast.literal_eval(opened.read())

def send_email(tablename='unidentified',message_info='No details'):

    msg = MIMEText('{} - {}'.format(message_info, arrow.utcnow()))

    msg['Subject'] = tablename+' - issue found'
    msg['From'] = 'monitoringirregularities@fluentsolutions.com'
    msg['To'] = 'souellet@weathertelematics.com'

    s = smtplib.SMTP('smtp.com',2525)
    s.starttls()
    s.login(smtp_credentials['username'], smtp_credentials['password'])
    s.sendmail(msg['From'], [msg['To']], msg.as_string())
    s.quit()
    print 'Sent', tablename