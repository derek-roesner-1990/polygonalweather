import ast
import time

#from matplotlib import pyplot
import numpy as np
#from scipy import ndimage
import arrow
import rasterio
from rasterio import Affine
from rasterio import features
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()

import postgres_functions
import data_handler
import alarms_and_triggers

import data_downloader # Messing up the usual formula here but oh well

import europe_ds

ds_ranges = [[10, 10.5], [10.5, 11], [20, 20.5], [20.5, 21]]
risk_squashes = [[1.0, 3.0], [4.0, 10.0], [12.0, 14.0], [15.0, 20.0]]

buffer_factor = 0.05
buffer_divisor = 1.5
simplification = 0.02

def get_risk_score(mini, maxi, a, b, x):
    return ((((b - a)*(x - mini)) / (maxi - mini)) + a)

def calculate_risk(score):
    risk = None
    for arange, squash in zip(ds_ranges, risk_squashes):
        if score < 10:
            risk = 0
            break
        elif score > 30:
            risk = 30
            break
        if score > arange[0] and score <= arange[1]:
            risk = get_risk_score(arange[0], arange[1], squash[0], squash[1], score)
    return risk

def polygonize(array):
    bins = range(1,21)+[30]
    #array = ndimage.interpolation.zoom(array,5)
    #array = ndimage.gaussian_filter(array,sigma=0.1)
    #pyplot.imshow(array,cmap='inferno')
    #pyplot.show()
    digitized = np.digitize(array,bins).astype(np.int32)

    mask = digitized > 0
    #master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
    master_transform = Affine(0.09999999999999999, 0.0, -180.05, 0.0, -0.1, 90.05)

    shapes = rasterio.features.shapes(digitized,mask=mask,transform=master_transform)
    polygons = [[] for _ in bins]+[[],[]]

    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon)

    if buffer_factor > 0:
        buffered = [[] for _ in bins]+[[],[]]
        for i,polygon in enumerate(polygons):
            try:
                buffered[i] = [poly.buffer(buffer_factor) for poly in polygon]
            except:
                continue

        unioned = [[] for _ in bins]+[[],[]]
        if simplification > 0:
            for i,one_buffered in enumerate(buffered):
                try:
                    unioned[i] = ops.unary_union(one_buffered).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
                except:
                    continue
        else:
            for one_buffered in buffered:
                try:
                    unioned[i] = ops.unary_union(one_buffered).buffer(-buffer_factor/buffer_divisor)
                except:
                    continue

        final_poly = [[] for _ in bins]+[[],[]]
        for i,one_unioned in enumerate(unioned):
            current = one_unioned
            for above_poly in unioned[i+1:]:
                try:
                    current = current.difference(above_poly)
                except:
                    continue
            final_poly[i] = current

        polygons = final_poly

    else:
        pass

    return zip(polygons,bins)

def polygonize_no_smoothing(array):
    bins = range(1,21)+[30]
    #array = ndimage.interpolation.zoom(array,5)
    #array = ndimage.gaussian_filter(array,sigma=0.1)
    #pyplot.imshow(array,cmap='inferno')
    #pyplot.show()
    digitized = np.digitize(array,bins).astype(np.int32)

    mask = digitized > 0
    #master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
    master_transform = Affine(0.09999999999999999, 0.0, -180.05, 0.0, -0.1, 90.05)

    shapes = rasterio.features.shapes(digitized,mask=mask,transform=master_transform)
    polygons = [[] for _ in bins]+[[],[]]

    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon)

    polygons = [ops.unary_union(polygon) for polygon in polygons]
    return zip(polygons,bins)

def main(desired_time = None, timestep = 0):
    europe_ds.main(desired_time)
    ds_array = data_handler.get_layer('europe_ds_array','latest')
    #ds_array = np.load('/home/souellet/ecmwf_proto.npy')
    vectorized_calculate_risk = np.vectorize(calculate_risk)

    risk_array = vectorized_calculate_risk(ds_array)

    #x,y = data_handler.get_coordinate_grid('MRMS')
    #x = x[::down_factor, ::down_factor]
    #y = y[::down_factor, ::down_factor]

    #polygons = polygonize(x,y,risk_array)

    polygons = polygonize_no_smoothing(risk_array)

    current_time = desired_time

    zones = []
    for i,polygon in enumerate(polygons):
        zone = {'risk_index':polygon[1], 'shape':polygon[0], 'forecast_time':timestep, 'valid_time':current_time}
        print zone['shape'].area
        zones.append(zone)

    postgres_functions.push_ecmwf_live_risk(zones)

    #alarms_and_triggers.clean_cache('current_risk_polygons')

    alarms_and_triggers.geoserver_trigger('ecmwf_live_risk')
    alarms_and_triggers.clean_cache('ecmwf_live_risk')

    print 'Done!'

if __name__ == '__main__':

    data_downloader.main(mode='ecmwf')

    desired_time = arrow.utcnow()
    while desired_time.hour % 3 != 0:
        desired_time = desired_time.replace(hours=-1)

    desired_time = desired_time.floor('hour')

    ids = postgres_functions.clean_ecmwf_live_risk(ids = None)

    for timestep in range(0,120,3):
        try:
            main(desired_time = desired_time.replace(hours=+timestep), timestep=timestep)
        except:
            print "Could not process {}".format(timestep)

    postgres_functions.clean_ecmwf_live_risk(ids = ids)


