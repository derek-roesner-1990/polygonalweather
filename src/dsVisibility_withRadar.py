
# coding: utf-8

# In[1]:

from __future__ import print_function
from pyds import MassFunction
from itertools import product
import math


# In[2]:

'''
Visibility beliefs
Since the calculations use actual visibility being reported by HRRR as one of the inputs,
the final output is completely based on the masses for that variable. Other masses come into
play if that value is missing.
Wind masses are only used if precip is happening, as wind will not impact visibility otherwise.
'''


# In[2]:

'''
Function for handling the 
different precip flags
Flag	Description
-3	no coverage
0	no precipitation
1	warm stratiform rain
2	warm stratiform rain
3	snow
4	snow
5	reserved for future use
6	convective rain
7	rain mixed with hail
8	reserved for future use
9	flag no longer used
10	cold stratiform rain
91	tropical/stratiform rain mix
96	tropical/convective rain mix

MRMS
'''

def getPrecipFlagId(precipFlag):
    if (precipFlag in [0, 5, 9]):
        return 0
    elif (precipFlag in [1,2,7,10,91]):
        return 1
    elif (precipFlag in [6,96]):
        return 2
    elif (precipFlag in [3,4]):
        return 3
    elif (precipFlag == -3):
        return 4
    else:
        return -1
        
precipFlagMasses = [] 
precipFlagMasses.append(MassFunction({"GMB":1}))
precipFlagMasses.append(MassFunction({"G":0.05, "M":0.3, "B":0.3, "GM":0.05, "MB":0.3}))
precipFlagMasses.append(MassFunction({"M":0.2, "B":0.5, "MB":0.3}))
precipFlagMasses.append(MassFunction({"G":0.05, "M":0.3, "B":0.3, "GM":0.05, "MB":0.3}))
precipFlagMasses.append(MassFunction({"GMB":1}))


# In[3]:

'''
Function for handling the different ranges
of liquid (rain) precipitation rates mm/hr

MRMS
'''
def getPrecipRateLiquidId(precipRateLiquid):
    if (precipRateLiquid == 0):
        return 0
    elif (precipRateLiquid > 0.00001 and precipRateLiquid <= 2.5):
        return 1
    elif (precipRateLiquid > 2.5 and precipRateLiquid <= 500):
        return 2
    elif (precipRateLiquid == -3):
        return 3
    else:
        return -1
        
precipRateLiquidMasses = []
precipRateLiquidMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
precipRateLiquidMasses.append(MassFunction({"G":0.05, "M":0.5, "B":0.2, "GM":0.05, "MB":0.2,}))
precipRateLiquidMasses.append(MassFunction({"M":0.2, "B":0.5, "MB":0.3}))
precipRateLiquidMasses.append(MassFunction({"GMB":1}))


# In[11]:

'''
Handle doppler radar values (composite reflectivity)
dbZ values are:
0-20 : hardly anything/mist
20 - 30: light/moderate
30 - 75: moderate ot heavy
'''
def getRadarId(dbZVals):
    if (dbZVals > 0.00001 and dbZVals <= 20):
        return 0
    elif (dbZVals > 20 and dbZVals <= 30):
        return 1
    elif (dbZVals > 30):
        return 2
    else:
        return 3
        
radarMasses = []
radarMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
radarMasses.append(MassFunction({"G":0.05, "M":0.5, "B":0.2, "GM":0.05, "MB":0.2}))
radarMasses.append(MassFunction({"M":0.2, "B":0.5, "MB":0.3}))
radarMasses.append(MassFunction({"GMB":1}))


# In[5]:

'''
Function for handling the different ranges
of frozen (snow) precip rate mm/hr

MRMS
'''
def getPrecipRateFrozenId(precipRateFrozen):
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 2):
        return 1
    elif (precipRateFrozen > 2 and precipRateFrozen <= 500):
        return 2
    elif (precipRateFrozen == -3):
        return 3
    else:
        return -1
        
precipRateFrozenMasses = []
precipRateFrozenMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
precipRateFrozenMasses.append(MassFunction({"G":0.05, "M":0.5, "B":0.2, "GM":0.05, "MB":0.2}))
precipRateFrozenMasses.append(MassFunction({"M":0.1, "B":0.7, "MB":0.2}))
precipRateFrozenMasses.append(MassFunction({"GMB":1}))


# In[6]:

'''
Function for handling the different ranges
of wind speed
Use the Beaufort scale for wind
0
light breezes: (0,1,2,3)
moderate breezes: (4,5)
strong breezes" (6,7)
insanity: (8 onwards)
HRRR

THIS FUNCTION SHOULD ONLY BE CALLED IF ONE OF THE PRECIP VALUES IS > 1
'''
def getWindId(wind):
    if (wind== 0):
        return 0
    elif (wind > 0 and wind <= 5.5): # light winds
        return 1
    elif (wind > 5.5 and wind <= 10.7): # medium wind
        return 2
    elif (wind > 10.7 and wind <= 17.1): # strong wind
        return 3
    elif (wind > 17.1): # batshit crazy
        return 4
    else:
        return -1
        
windMasses = []
windMasses.append(MassFunction({"G":0.55, "M":0.1, "B":0.05, "GM":0.2, "MB":0.05, "GMB":0.05}))
windMasses.append(MassFunction({"G":0.3, "M":0.3, "B":0.1, "GM":0.2, "MB":0.1}))
windMasses.append(MassFunction({"G":0.1, "M":0.5, "B":0.3, "GM":0.05, "MB":0.15}))
windMasses.append(MassFunction({"M":0.2, "B":0.6, "MB":0.2}))
windMasses.append(MassFunction({"M":0.1, "B":0.7, "MB":0.2}))


# In[7]:

def getCategoricalSnow(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 2
    else:
        return -1
    
categoricalSnowMasses = []
categoricalSnowMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
categoricalSnowMasses.append(MassFunction({"M":0.3, "B":0.5, "MB":0.2}))
categoricalSnowMasses.append(MassFunction({"GMB":1.0}))

def getCategoricalFrz(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 2
    else:
        return -1
    
categoricalFrzMasses = []
categoricalFrzMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
categoricalFrzMasses.append(MassFunction({"M":0.3, "B":0.5, "MB":0.2}))
categoricalFrzMasses.append(MassFunction({"GMB":1.0}))

def getCategoricalIce(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 2
    else:
        return -1
    
categoricalIceMasses = []
categoricalIceMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
categoricalIceMasses.append(MassFunction({"M":0.4, "B":0.3, "MB":0.3}))
categoricalIceMasses.append(MassFunction({"GMB":1.0}))

def getCategoricalRain(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 2
    else:
        return -1
    
categoricalRainMasses = []
categoricalRainMasses.append(MassFunction({"G":0.6, "M":0.05, "B":0.05, "GM":0.1, "MB":0.1, "GMB":0.1}))
categoricalRainMasses.append(MassFunction({"M":0.4, "B":0.3, "MB":0.3}))
categoricalRainMasses.append(MassFunction({"GMB":1.0}))


# In[26]:

'''
Actual visibility values from 
HRRR

******* DISALBE THIS FUNCTION FOR NOW *******
'''
def getVisibilityId(viz):
    if viz <= 1000 and viz >= 0:
        return 0
    elif viz > 1000 and viz <= 10000:
        return 1
    elif viz > 10000:
        return 2
    else:
        return 3
    
visibilityMasses = []
visibilityMasses.append(MassFunction({"B":1}))
visibilityMasses.append(MassFunction({"G":0, "M":1, "B":0, "GM":0, "MB":0, "GMB":0}))
visibilityMasses.append(MassFunction({"G":1, "M":0, "B":0, "GM":0, "MB":0, "GMB":0}))
visibilityMasses.append(MassFunction({"G":0, "M":0, "B":0, "GM":0, "MB":0, "GMB":1}))


# # Function below is for nowcasting visibility

# In[8]:

'''
assume vector has following format:
(precipflag, preciprate, wind, catSnow, catRain, catFrz, catIcePellets, visibility, temperature) # THIS IS THE ORIGINAL INPUT
(precipflag, preciprate, wind, catSnow, catRain, catFrz, catIcePellets, REFLECTIVITY, temperature) # FEED THIS INPUT FORMAT NOW
'''
def getBeliefValues(weatherConditions): 
    useWind = 0
    
    if (weatherConditions[1] > 0): # should wind be used?
        useWind = 1
    
    precipType = 1 # 1=Liquid, 2=Frozen
        
    if (weatherConditions[0] in [3,4] or (weatherConditions[8] <= 275.16 and weatherConditions[8] > 0)): # see if precipType should be frozen
        precipType = 2

    vizState = 0

    if (useWind == 1):
        if (precipType == 1):
            vizState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]             & precipRateLiquidMasses[getPrecipRateLiquidId(weatherConditions[1])]             & windMasses[getWindId(weatherConditions[2])]             & radarMasses[getRadarId(weatherConditions[7])]             & categoricalSnowMasses[getCategoricalSnow(weatherConditions[3])]             & categoricalRainMasses[getCategoricalRain(weatherConditions[4])]             & categoricalFrzMasses[getCategoricalFrz(weatherConditions[5])]             & categoricalIceMasses[getCategoricalIce(weatherConditions[6])]
        else:
            vizState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]             & precipRateFrozenMasses[getPrecipRateFrozenId(weatherConditions[1])]             & windMasses[getWindId(weatherConditions[2])]             & radarMasses[getRadarId(weatherConditions[7])]             & categoricalSnowMasses[getCategoricalSnow(weatherConditions[3])]             & categoricalRainMasses[getCategoricalRain(weatherConditions[4])]             & categoricalFrzMasses[getCategoricalFrz(weatherConditions[5])]             & categoricalIceMasses[getCategoricalIce(weatherConditions[6])]
    else:
        if (precipType == 1):
            vizState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]             & precipRateLiquidMasses[getPrecipRateLiquidId(weatherConditions[1])]             & radarMasses[getRadarId(weatherConditions[7])]             & categoricalSnowMasses[getCategoricalSnow(weatherConditions[3])]             & categoricalRainMasses[getCategoricalRain(weatherConditions[4])]             & categoricalFrzMasses[getCategoricalFrz(weatherConditions[5])]             & categoricalIceMasses[getCategoricalIce(weatherConditions[6])]
        else:
            vizState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]             & precipRateFrozenMasses[getPrecipRateFrozenId(weatherConditions[1])]             & radarMasses[getRadarId(weatherConditions[7])]             & categoricalSnowMasses[getCategoricalSnow(weatherConditions[3])]             & categoricalRainMasses[getCategoricalRain(weatherConditions[4])]             & categoricalFrzMasses[getCategoricalFrz(weatherConditions[5])]             & categoricalIceMasses[getCategoricalIce(weatherConditions[6])]              
    
    if False:
        return vizState    
    else:
        string_state = str(vizState)
        #print(string_state)
        #if string_state.startswith("{set(['G',"):
        if string_state[9] == ',':
            return -3.0
        else:    
            result = vizState.max_bel()
            adjustment = 0
            string_result = string_state[7]

            if string_result == 'B':
                adjustment = 20
            elif string_result == 'M':
                adjustment = 10

            return vizState[result]+adjustment



# # Function below is for forecasting visibility

# In[9]:

'''
assume vector has following format:
(catSnow, catRain, catFrz, catIcePellets, visibility, wind)
(catSnow, catRain, catFrz, catIcePellets, REFLECTIVITY, wind)
'''
def getForecastBeliefValues(weatherConditions): 
    
    useWind = 0
    
    if (weatherConditions[0] != 0 or weatherConditions[1] != 0 or weatherConditions[2] != 0 or weatherConditions[3] != 0): # should wind be used?
        useWind = 1
        
    vizState = 0

    if (useWind == 1):
        vizState = windMasses[getWindId(weatherConditions[5])]         & radarMasses[getRadarId(weatherConditions[4])]         & categoricalSnowMasses[getCategoricalSnow(weatherConditions[0])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[1])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[2])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[3])]
    else:
        vizState = radarMasses[getRadarId(weatherConditions[4])]         & categoricalSnowMasses[getCategoricalSnow(weatherConditions[0])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[1])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[2])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[3])]
        
    return vizState    


# In[14]:

# (precipflag, preciprate, wind, catSnow, catRain, catFrz, catIcePellets, visibility, temperature)
# (catSnow, catRain, catFrz, catIcePellets, visibility, wind)

if False:
    sampleWeather1 = [2, .1, 0, 0, 1, 0, 0, 25, 280]
    sampleWeather2 = [0, 1, 0, 0, 28, 20]
    #sampleWeather2 = [2, .5, -3, 280, 350, 0]
    testMe = getBeliefValues(sampleWeather1)
    #testMe = getForecastBeliefValues(sampleWeather2)
    print(testMe)

