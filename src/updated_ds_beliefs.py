# ## Radiation

# In[12]:

'''
Function for handling the different ranges
of radiation
HRRR
'''
def getRadiationId(radiation):
    if (radiation >= 0 and radiation <= 250):
        return 0
    elif (radiation > 250 and radiation <= 500):
        return 1
    elif (radiation > 500 and radiation <= 750):
        return 2
    elif (radiation > 750 and radiation <= 1500):
        return 3
    elif radiation == -3:
        return 4
    else:
        return -1

""" 
radiationMasses = []
radiationMasses.append(MassFunction({"S":0.2, "W":0.2, "I":0.2, "D":0.05, "SW":0.1, "SI":0.1, "WI":0.1, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.175, "W":0.175, "I":0.175, "D":0.125, "SW":0.1, "SI":0.1, "WI":0.1, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.15, "W":0.15, "I":0.15, "D":0.275, "SW":0.075, "SI":0.075, "WI":0.075, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.1, "W":0.1, "I":0.1, "D":0.5, "SW":0.05, "SI":0.05, "WI":0.05,  "SWID":0.05}))
radiationMasses.append(MassFunction({       "SWID":1}))
"""
radiationMasses = []
radiationMasses.append(MassFunction({"S":0.125, "W":0.125, "I":0.125, "D":0.125, "SWID":0.5}))
radiationMasses.append(MassFunction({"S":0.1, "W":0.1, "I":0.1, "D":0.2, "SWID":0.5}))
radiationMasses.append(MassFunction({"S":0.075, "W":0.075, "I":0.075, "D":0.275, "SWID":0.5}))
radiationMasses.append(MassFunction({"S":0.05, "W":0.05, "I":0.05, "D":0.35, "SWID":0.5}))
radiationMasses.append(MassFunction({ "SWID":1}))

'''
Function for handling the 
different precip flags
Flag    Description
-3  no coverage
0   no precipitation
1   warm stratiform rain
2   warm stratiform rain
3   snow
4   snow
5   reserved for future use
6   convective rain
7   rain mixed with hail
8   reserved for future use
9   flag no longer used
10  cold stratiform rain
91  tropical/stratiform rain mix
96  tropical/convective rain mix

MRMS

update Feb 8th:
reduce beliefs for snow (3,4) to have equal chances for wet and snow. because we don't know the intensity
so rely on those fields for snow
original: precipFlagMasses.append(MassFunction({"S":0.5, "W":0.05, "I":0.3, "SI":0.1, "SW": "SWID":0.05}))
'''

def getPrecipFlagId(precipFlag):
    if (precipFlag in [0, 5, 9]):
        return 0
    elif (precipFlag in [1,2,7,10,91]):
        return 1
    elif (precipFlag in [6,96]):
        return 2
    elif (precipFlag in [3,4]):
        return 3
    elif (precipFlag == -3):
        return 4
    else:
        return -1
        
precipFlagMasses = [] 
precipFlagMasses.append(MassFunction({   "D":0.95,    "SWID":0.05}))
precipFlagMasses.append(MassFunction({"S":0.1, "W":0.65,   "SW":0.2, "SWID":0.05}))
precipFlagMasses.append(MassFunction({"W":0.95,   "SWID":0.05}))
precipFlagMasses.append(MassFunction({"S":0.3, "W":0.3, "I":0.3, "SI":0.025, "SW":0.025, "SWID":0.05}))
precipFlagMasses.append(MassFunction({       "SWID":1}))

# change 8th feb 2017
# precipRateLiquidMasses.append(MassFunction({"S":0.15, "W":0.7,   "SW":0.1, "SWID":0.05}))
def getPrecipRateLiquidId(precipRateLiquid):
    if (precipRateLiquid == 0):
        return 0
    elif (precipRateLiquid > 0.00001 and precipRateLiquid <= 0.5):
        return 1
    elif (precipRateLiquid > 0.5 and precipRateLiquid <= 2.5):
        return 2
    elif (precipRateLiquid > 2.5 and precipRateLiquid <= 500):
        return 2
    elif (precipRateLiquid == -3):
        return 4
    else:
        return -1
        
precipRateLiquidMasses = []
precipRateLiquidMasses.append(MassFunction({   "D":0.95,    "SWID":0.05}))
precipRateLiquidMasses.append(MassFunction({"S":0.05, "W":0.1, "D":0.7,   "SW":0.05, "SWID":0.05}))
precipRateLiquidMasses.append(MassFunction({"S":0.05, "W":0.7,  "D":0.1,  "SW":0.1, "SWID":0.05}))
precipRateLiquidMasses.append(MassFunction({"S":0.05, "W":0.85,   "SW":0.05, "SWID":0.05}))
precipRateLiquidMasses.append(MassFunction({       "SWID":1}))

'''
Function for handling the different ranges
of liquid precip accumulated over the past 1 hour mm

MRMS

change 8th feb 2017
increase beliefs for medium rain
precipAccum1LiquidMasses.append(MassFunction({"S":0.05, "W":0.6,  "D":0.2, "SW":0.05,   "SWID":0.1})) # medium

'''
def getPrecipAccum1LiquidId(precipAccum1Liquid):
    if (precipAccum1Liquid == 0): # no precip
        return 0
    elif (precipAccum1Liquid > 0.1 and precipAccum1Liquid <= 1): # light
        return 1
    elif (precipAccum1Liquid > 1 and precipAccum1Liquid <= 7.5): # medium - can set upper threshold to 5
        return 2
    elif (precipAccum1Liquid > 7.5 and precipAccum1Liquid <= 500): # heavy - can set lower threshold to 5
        return 3
    elif (precipAccum1Liquid == -3):
        return 4
    else:
        return -1

        
precipAccum1LiquidMasses = []
precipAccum1LiquidMasses.append(MassFunction({"D":0.7, "W":0.1,"SWID":0.1})) # no precip
precipAccum1LiquidMasses.append(MassFunction({"W":0.2,  "D":0.7,    "SWID":0.1})) # light
precipAccum1LiquidMasses.append(MassFunction({"W":0.7,  "D":0.2,   "SWID":0.1})) # medium
precipAccum1LiquidMasses.append(MassFunction({"W":0.9, "SWID":0.1})) # heavy
precipAccum1LiquidMasses.append(MassFunction({"SWID":1}))

'''
Function for handling the different ranges
of liquid precip accumulated over the past 3 hour mm

MRMS
'''
def getPrecipAccum3LiquidId(precipAccum1Liquid):
    if (precipAccum1Liquid <= 0.5): # no precip
        return 0
    elif (precipAccum1Liquid > 0.5 and precipAccum1Liquid <= 5): # light
        return 1
    elif (precipAccum1Liquid > 5 and precipAccum1Liquid <= 10): # medium - can set upper threshold to 7.5
        return 2
    elif (precipAccum1Liquid > 10 and precipAccum1Liquid <= 500): # heavy - can set lower threshold to 7.5
        return 3
    elif (precipAccum1Liquid == -3):
        return 4
    else:
        return -1

        
precipAccum3LiquidMasses = []
precipAccum3LiquidMasses.append(MassFunction({"D":0.75, "W":0.05,"SWID":0.2})) # no precip
precipAccum3LiquidMasses.append(MassFunction({"W":0.3,  "D":0.6,    "SWID":0.1})) # light
precipAccum3LiquidMasses.append(MassFunction({"W":0.75,  "D":0.15,    "SWID":0.1})) # medium
precipAccum3LiquidMasses.append(MassFunction({"W":0.85, "D": 0.05,  "SWID":0.1})) # heavy
precipAccum3LiquidMasses.append(MassFunction({"SWID":1}))

'''
Function for handling the different ranges
of liquid precip accumulated over the past 6 hours mm

MRMS
'''
def getPrecipAccum6LiquidId(precipAccumLiquid): # 6 hours
    if (precipAccumLiquid <= 1): # no precip
        return 0
    elif (precipAccumLiquid > 1 and precipAccumLiquid <= 7.5): # light
        return 1
    elif (precipAccumLiquid > 7.5 and precipAccumLiquid <= 15): # medium - can set upper threshold to 12.5
        return 2
    elif (precipAccumLiquid > 15 and precipAccumLiquid <= 500): # heavy - can set lower threshold to 12.5
        return 3
    elif (precipAccumLiquid == -3): # MISSING VALUE
        return 4
    else:
        return -1
        
precipAccum6LiquidMasses = []
precipAccum6LiquidMasses.append(MassFunction({   "D":0.7,    "SWID":0.3})) # no precip
precipAccum6LiquidMasses.append(MassFunction({"W":0.3,  "D":0.4, "SWID":0.3})) # light
precipAccum6LiquidMasses.append(MassFunction({"W":0.6, "D":0.1,   "SWID":0.3})) # medium
precipAccum6LiquidMasses.append(MassFunction({"W":0.7, "D":0.1,   "SWID":0.2})) # heavy
precipAccum6LiquidMasses.append(MassFunction({       "SWID":1}))

'''
Function for handling the different ranges
of frozen (snow) precip rate mm/hr

MRMS

#-- changes Feb 8th 2017 --#
# change the light snow rate to reflect more wet than snow:
original:
precipRateFrozenMasses.append(MassFunction({"S":0.2, "I":0.2, "W":0.2, "D":0.35, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.6, "I":0.2, "W":0.15, "SWID":0.05}))

MRMS

## -- the logic below is for giving higher priority to wet over snow
def getPrecipRateFrozenId(precipRateFrozen):
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 1.5):
        print("noon")
        return 1
    elif (precipRateFrozen > 1.5 and precipRateFrozen <= 2.5):
        return 2
    elif (precipRateFrozen > 2.5 and precipRateFrozen <= 500):
        return 3
    elif (precipRateFrozen == -3):
        return 4
    else:
        return -1
        
precipRateFrozenMasses = []
precipRateFrozenMasses.append(MassFunction({   "D":0.95,    "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.1, "I":0.1, "W":0.4, "D":0.35, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.3,"I":0.1,"W":0.3, "SW":0.1, "D":0.1, "SWID":0.1}))
OR: precipRateFrozenMasses.append(MassFunction({"S":0.35,"I":0.1,"W":0.25, "SW":0.1, "D":0.1, "SWID":0.1}))
precipRateFrozenMasses.append(MassFunction({"S":0.8, "I": 0.15, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({       "SWID":1}))

## -- the logic below gives a bit more weight to snow over wet
def getPrecipRateFrozenId(precipRateFrozen):
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 1):
        return 1
    elif (precipRateFrozen > 1 and precipRateFrozen <= 2.5):
        return 2
    elif (precipRateFrozen > 2.5 and precipRateFrozen <= 500):
        return 3
    elif (precipRateFrozen == -3):
        return 4
    else:
        return -1
        
precipRateFrozenMasses = []
precipRateFrozenMasses.append(MassFunction({   "D":0.95,    "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.1, "I":0.1, "W":0.4, "D":0.35, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.7, "I":0.1, "W":0.15, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.8, "I": 0.15, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({       "SWID":1}))
'''
def getPrecipRateFrozenId(precipRateFrozen):
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 1.5):
        return 1
    elif (precipRateFrozen > 1.5 and precipRateFrozen <= 2.5):
        return 2
    elif (precipRateFrozen > 2.5 and precipRateFrozen <= 500):
        return 3
    elif (precipRateFrozen == -3):
        return 4
    else:
        return -1
        
precipRateFrozenMasses = []
precipRateFrozenMasses.append(MassFunction({   "D":0.95,    "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.1, "I":0.1, "W":0.4, "D":0.35, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.75, "I":0.1, "W":0.1, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({"S":0.8, "I": 0.15, "SWID":0.05}))
precipRateFrozenMasses.append(MassFunction({       "SWID":1}))

'''
Function for handling the different ranges
of frozen precipn accumulated over the past 1 hour mm

MRMS

#-- changes Feb 8th 2017 --#
# change the light snow to reflect more wet than snow:
original:
precipAccum1FrozenMasses.append(MassFunction({"S":0.5,"I":0.2, "W":0.05,  "D":0.15, "SWID":0.1})) # light
precipAccum1FrozenMasses.append(MassFunction({"S":0.7, "I":0.2, "SWID":0.1})) # heavy

# change lower threshold to 0.1 from 0.00006
2.5 -> 5, new layer
'''
def getPrecipAccum1FrozenId(precipAccum1Frozen):
    if (precipAccum1Frozen <= 0.1): # no/minimal snow
        return 0
    elif (precipAccum1Frozen > 0.1 and precipAccum1Frozen <= 2.5): # light
        return 1
    elif (precipAccum1Frozen > 2.5 and precipAccum1Frozen <= 5): # light/light-medium snow
        return 2
    elif (precipAccum1Frozen > 5 and precipAccum1Frozen <= 500): # medium-heavy/heavy
        return 3
    elif (precipAccum1Frozen == -3):
        return 4
    else:
        return -1
        
precipAccum1FrozenMasses = []
precipAccum1FrozenMasses.append(MassFunction({"S":0.05, "D":0.75, "I":0.05, "W":0.05, "SWID":0.1})) # no snow
precipAccum1FrozenMasses.append(MassFunction({"S":0.3,"I":0.1,"W":0.3, "SW":0.1, "D":0.1, "SWID":0.1})) # light
precipAccum1FrozenMasses.append(MassFunction({"S":0.7, "W":0.1, "I":0.1, "SWID":0.1})) # heavy
precipAccum1FrozenMasses.append(MassFunction({"S":0.99, "SWID":0.01})) # heavy
precipAccum1FrozenMasses.append(MassFunction({"SWID":1}))

'''
Function for handling the different ranges
of frozen precipn accumulated over the past 3 hour mm

#-- changes Feb 8th 2017 --#
# change the light snow to reflect more wet than snow:
original:
precipAccum3FrozenMasses.append(MassFunction({"S":0.05,"D":0.65, "I":0.05, "W":0.05, "SWID":0.2})) # no snow
precipAccum3FrozenMasses.append(MassFunction({"S":0.2, "I":0.05, "D":0.5, "W":0.05, "SWID":0.2})) # light
precipAccum3FrozenMasses.append(MassFunction({"S":0.5, "I":0.1,  "D":0.15, "W":0.05, "SWID":0.2})) # medium
precipAccum3FrozenMasses.append(MassFunction({"S":0.7, "I":0.1, "SWID":0.2})) # heavy

# change lower threshold to 1 from 0.00006

MRMS
'''
def getPrecipAccum3FrozenId(precipAccum1Frozen):
    if (precipAccum1Frozen <= 1): # no snow
        return 0
    elif (precipAccum1Frozen > 1 and precipAccum1Frozen <= 2.5): # light snow
        return 1
    elif (precipAccum1Frozen > 2.5 and precipAccum1Frozen <= 7.5): # moderate snow
        return 2
    elif (precipAccum1Frozen > 7.5 and precipAccum1Frozen <= 500): # heavy
        return 3
    elif (precipAccum1Frozen == -3):
        return 4
    else:
        return -1
        
precipAccum3FrozenMasses = []
precipAccum3FrozenMasses.append(MassFunction({"S":0.05,"D":0.65, "I":0.05, "W":0.05, "SWID":0.2})) # no snow
precipAccum3FrozenMasses.append(MassFunction({"S":0.05, "I":0.05, "D":0.2, "W":0.5, "SWID":0.2})) # light
precipAccum3FrozenMasses.append(MassFunction({"S":0.35, "I":0.1,  "D":0.15, "W":0.2, "SWID":0.2})) # moderate
precipAccum3FrozenMasses.append(MassFunction({"S":0.95, "I":0.05, "SWID":0.05})) # heavy
precipAccum3FrozenMasses.append(MassFunction({"SWID":1}))

'''
Function for handling the different ranges
of frozen precip aummulated over the past 6 hours mm

#-- changes Feb 8th 2017 --#
# change the light snow to reflect more wet than snow:
original:
precipAccum6FrozenMasses.append(MassFunction({"S":0.2, "D":0.4, "W":0.05,"I":0.05,"SWID":0.3})) # light
precipAccum6FrozenMasses.append(MassFunction({"S":0.5, "D":0.1, "W":0.05,"I":0.05,"SWID":0.3})) # medium
precipAccum6FrozenMasses.append(MassFunction({"S":0.55, "W":0.05,"I":0.1,"SWID":0.3})) # heavy

# change lower threshold to 1 from 0.00006
20->15
MRMS
'''
def getPrecipAccum6FrozenId(precipAccumFrozen): # 6 hours
    if (precipAccumFrozen <= 1):
        return 0
    elif (precipAccumFrozen > 1 and precipAccumFrozen <= 5): # light
        return 1
    elif (precipAccumFrozen > 5 and precipAccumFrozen <= 10): # medium
        return 2
    elif (precipAccumFrozen > 10 and precipAccumFrozen <= 15): # heavy
        return 3
    elif (precipAccumFrozen > 15 and precipAccumFrozen <= 500): # very heavy
        return 4
    elif (precipAccumFrozen == -3):
        return 5
    else:
        return -1
        
precipAccum6FrozenMasses = []
precipAccum6FrozenMasses.append(MassFunction({"D":0.7,"SWID":0.3})) # no
precipAccum6FrozenMasses.append(MassFunction({"S":0.05, "D":0.1, "W":0.5,"I":0.05,"SWID":0.3})) # light
precipAccum6FrozenMasses.append(MassFunction({"S":0.35, "D":0.1, "W":0.2,"I":0.05,"SWID":0.3})) # medium
precipAccum6FrozenMasses.append(MassFunction({"S":0.75, "W":0.05,"I":0.1,"SWID":0.1})) # heavy
precipAccum6FrozenMasses.append(MassFunction({"S":0.9, "SWID":0.1})) # very heavy
precipAccum6FrozenMasses.append(MassFunction({"SWID":1}))

'''
Function for handling the different ranges
of air temperature Kelvins

*****Changes 30th september: 
*****Add for below 2C, ice beliefs

# Update notes Feb 8, 2017:
- reduce SWID to 0.1, as 0.5 can assign a high belief to snow when road temps are high

HRRR
'''
def getAirTempId(airTemp):
    if (math.isnan(airTemp)):
        return 3
    elif (airTemp > 275.16):
        return 0
    elif (airTemp >= 271.16 and airTemp <= 275.16):
        return 1
    elif (airTemp < 271.16 and airTemp >= 0):
        return 2
    elif (airTemp == -3):
        return 3
    else:
        return -1
        

airTempMasses = []
airTempMasses.append(MassFunction({"W":0.45, "D":0.45, "SWID":0.1})) #
airTempMasses.append(MassFunction({"S":0.15, "W":0.15,  "I": 0.15 , "D":0.15, "SW":0.15, "SI": .15, "SWID":0.1})) #
airTempMasses.append(MassFunction({"S":0.225, "I": 0.225 , "D":0.225, "SI": .225, "SWID":0.1})) #
airTempMasses.append(MassFunction({       "SWID":1}))

'''airTempMasses = []
airTempMasses.append(MassFunction({ "W":0.25,  "D":0.25,     "SWID":0.5}))
#airTempMasses.append(MassFunction({"S":0.0625, "W":0.0625,  "D":0.25, "SW":0.125,   "SWID":0.5}))
airTempMasses.append(MassFunction({"S":0.07, "W":0.07,  "I": 0.07 , "D":0.07, "SW":0.11, "SI": .11, "SWID":0.5})) # -- addition sept 30
#airTempMasses.append(MassFunction({"S":0.25,   "D":0.25,    "SWID":0.5}))
airTempMasses.append(MassFunction({"S":0.125, "I":0.125,  "D":0.125, "SI":0.125,   "SWID":0.5})) # -- addition sept 30
airTempMasses.append(MassFunction({       "SWID":1}))'''

'''
Function for handling the different ranges
of ROAD temperature Kelvins

Our own model

   
Used only if the air temperature is 0. consider freezing conditions only

# Update notes Feb 8, 2017:
- reduce SWID to 0.1, as 0.5 can assign a high belief to snow when road temps are high
'''
def getRoadTemperatureId(roadTemp):
    if (math.isnan(roadTemp)):
        return 3
    elif (roadTemp > 275.16):
        return 0
    elif (roadTemp >= 271.16 and roadTemp <= 275.16):
        return 1
    elif (roadTemp < 271.16 and roadTemp >= 0):
        return 2
    elif (roadTemp == -3):
        return 3
    else:
        return -1
        

roadTempMasses = []
roadTempMasses.append(MassFunction({"W":0.45, "D":0.45, "SWID":0.1})) #
roadTempMasses.append(MassFunction({"S":0.15, "W":0.15,  "I": 0.15 , "D":0.15, "SW":0.15, "SI": .15, "SWID":0.1})) #
roadTempMasses.append(MassFunction({"S":0.225, "I": 0.225 , "D":0.225, "SI": .225, "SWID":0.1})) #
roadTempMasses.append(MassFunction({       "SWID":1}))

'''
Function for handling the different ranges
of humidity
  
Used only if the air temperature is 0. consider freezing conditions only
'''
def getHumidityId(humidity):
    if (math.isnan(humidity)):
        return 2
    elif (humidity > 0 and humidity < 90):
        return 0
    elif (humidity >= 90 and humidity < 95 ):
        return 1
    elif (humidity >= 95):
        return 2
    elif (humidity == -3):
        return 2
    else:
        return -1
        
humidityMasses = []
humidityMasses.append(MassFunction({"S":0.05, "W":0.05,  "I": 0.05, "D":0.25, "SW":0.05, "SI": .05, "SWID":0.5})) 
humidityMasses.append(MassFunction({"S":0.1, "W":0.05,  "I": 0.3,  "D":0.15, "SW":0.05, "SI": .05, "SWID":0.3})) 
humidityMasses.append(MassFunction({"S":0.1, "I": 0.55,  "D":0.1,             "SI": .15, "SWID":0.1})) 
humidityMasses.append(MassFunction({       "SWID":1}))

# ## Categorical Sensors

# In[19]:

'''
change 8th Feb 2017
reflect to be the same as mrms
categoricalSnowMasses.append(MassFunction({"S":0.7,    "SW":0.1, "SI":0.15, "SWID":0.05}))
'''
def getCategoricalSnow(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalSnowMasses = []
categoricalSnowMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalSnowMasses.append(MassFunction({"S":0.3, "W":0.3, "I":0.15, "SI":0.1, "SW":0.1, "SWID":0.05}))

def getCategoricalFrz(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalFrzMasses = []
categoricalFrzMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalFrzMasses.append(MassFunction({  "I":0.99,   "SWID":0.01}))

def getCategoricalIce(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalIceMasses = []
categoricalIceMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalIceMasses.append(MassFunction({"W":0.15, "I":0.5,   "WI":0.3, "SWID":0.05}))

def getCategoricalRain(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalRainMasses = []
categoricalRainMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
#categoricalRainMasses.append(MassFunction({ "W":0.7,   "SW":0.15,  "WI":0.15}))
categoricalRainMasses.append(MassFunction({ "W":0.95,   "SW":0, "SWID":0.05}))


'''
assume vector has following format:
(precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr, catSnow, catRain, catFrz, catIcePellets, precipaccum3hr, roadTemperature, humidity)
'''
def getBeliefValues(weatherConditions, whole_belief=False): 
    precipType = 1 # 1=Liquid, 2=Frozen
    
    # flag condition check ensures that frozen conditions are checked ONLY if both HRRR and MRMS flags agree
    # on the precip type being frozen
    flagConditionCheck = (weatherConditions[0] in [3,4] and (weatherConditions[6] == 1 or weatherConditions[8] == 1 or weatherConditions[9] == 1)) 
    if (flagConditionCheck or (weatherConditions[3] <= 273.16 and weatherConditions[3] > 0)): # see if precipType should be frozen
        precipType = 2

    roadState = 0 # this stores the road state for the current conditions

    roadState = 0
    finalRoadState = 0
    finalRoadStateBelief = 0
    
    if (precipType == 1): # first check for current conditions - THIS IS FOR WET
        roadState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])] \
                    & precipAccum1LiquidMasses[getPrecipAccum1LiquidId(weatherConditions[5])] \
                    & precipRateLiquidMasses[getPrecipRateLiquidId(weatherConditions[1])] \
                    & airTempMasses[getAirTempId(weatherConditions[3])] \
                    & radiationMasses[getRadiationId(weatherConditions[4])] \
                    & categoricalFrzMasses[getCategoricalFrz(weatherConditions[8])] \
                    & categoricalIceMasses[getCategoricalIce(weatherConditions[9])] \
                    & roadTempMasses[getRoadTemperatureId(weatherConditions[11])]
                    #& categoricalSnowMasses[getCategoricalSnow(weatherConditions[6])] \
                    #& categoricalRainMasses[getCategoricalRain(weatherConditions[7])] \
                    
    else: # THIS IS FOR FROZEN
        roadState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])] \
                    & precipAccum1FrozenMasses[getPrecipAccum1FrozenId(weatherConditions[5])] \
                    & precipRateFrozenMasses[getPrecipRateFrozenId(weatherConditions[1])] \
                    & airTempMasses[getAirTempId(weatherConditions[3])] \
                    & radiationMasses[getRadiationId(weatherConditions[4])] \
                    & categoricalFrzMasses[getCategoricalFrz(weatherConditions[8])] \
                    & categoricalIceMasses[getCategoricalIce(weatherConditions[9])] \
                    & roadTempMasses[getRoadTemperatureId(weatherConditions[11])]
                    #& categoricalSnowMasses[getCategoricalSnow(weatherConditions[6])] \
                    #& categoricalRainMasses[getCategoricalRain(weatherConditions[7])] \
                    

        checkForIce = list(roadState.max_bel())[0] # get the road state with the maximum belief
        print('--> ' + str(roadState))
        
        if False and checkForIce not in ['S', 'W', 'I']: # current conditions predicted a dry road, check if it should be ICY 
            roadStateIce = roadTempMasses[getRoadTemperatureId(weatherConditions[11])] & humidityMasses[getHumidityId(weatherConditions[12])]
            #print('-->ice ' + str(roadStateIce))
            highestBeliefIce = roadStateIce.max_bel()
            maxBeliefStateIce = list(highestBeliefIce)[0] # get the road state with the maximum belief
            iceRoadStateBelief = roadStateIce[highestBeliefIce]
            
            if maxBeliefStateIce == 'I':
                roadState = roadStateIce # switch it to being icey
                finalRoadState = maxBeliefStateIce
                finalRoadStateBelief = iceRoadStateBelief
                roadState = roadStateIce
    
    # START THE CHECKS FOR THEP PAST
    highestBeliefCurrent = roadState.max_bel()
    currentRoadState = list(highestBeliefCurrent)[0] # get the road state with the maximum belief
    currentRoadStateBelief = roadState[highestBeliefCurrent]
    
    
    if currentRoadState not in ['S', 'W', 'I'] and (weatherConditions[2] > -3 and weatherConditions[5] > -3 and weatherConditions[10] > -3): # current conditions predicted a dry/uncertain road, so check for the past - this loop will not be entered if its an icy road
        roadState2 = 0
        if (precipType == 1): # check for the past conditions
            roadState2 = precipAccum6LiquidMasses[getPrecipAccum6LiquidId(weatherConditions[2])] \
                         & precipAccum1LiquidMasses[getPrecipAccum1LiquidId(weatherConditions[5])] \
                         & airTempMasses[getAirTempId(weatherConditions[3])] \
                         & radiationMasses[getRadiationId(weatherConditions[4])] \
                         & precipAccum3LiquidMasses[getPrecipAccum3LiquidId(weatherConditions[10])] \
                        & roadTempMasses[getRoadTemperatureId(weatherConditions[11])]
        else:
            roadState2 = precipAccum6FrozenMasses[getPrecipAccum6FrozenId(weatherConditions[2])] \
                         & precipAccum1FrozenMasses[getPrecipAccum1FrozenId(weatherConditions[5])] \
                         & airTempMasses[getAirTempId(weatherConditions[3])] \
                         & radiationMasses[getRadiationId(weatherConditions[4])] \
                         & precipAccum3FrozenMasses[getPrecipAccum3FrozenId(weatherConditions[10])] \
                        & roadTempMasses[getRoadTemperatureId(weatherConditions[11])]
                
        highestBeliefPast = roadState2.max_bel()
        pastRoadState = list(highestBeliefPast)[0] # get the road state with the maximum belief
        pastRoadStateBelief = roadState2[highestBeliefPast]
        
        #print('round 2 ' + str(roadState2))
        
        finalRoadState = pastRoadState
        finalRoadStateBelief = pastRoadStateBelief
        roadState = roadState2
    else:
        finalRoadState = currentRoadState
        finalRoadStateBelief = currentRoadStateBelief  
    
    adjustment = 0
    string_result = finalRoadState

    if string_result == 'I':
        adjustment = 30
    elif string_result == 'S':
        adjustment = 20
    elif string_result == 'W':
        adjustment = 10


    if whole_belief:
        return (finalRoadStateBelief+adjustment,roadState)
    else:
        return finalRoadStateBelief+adjustment
    #return finalRoadStateBelief+adjustment        
    #return (roadState)
