import time
import ast

import arrow
from flask import request
from flask import Flask
from raven import Client
import mygeotab
import psycopg2
from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

application = Flask('geotab_endpoint')

with open('credentials.txt','r') as opened:
    pg_credentials = ast.literal_eval(opened.read())

with open('sentry_credentials.txt', 'r') as opened:
    sentry_credentials = opened.read()
sentry = Client(sentry_credentials)

with open('twilio_credentials','r') as opened:
    twilio_credentials = ast.literal_eval(opened.read())

credentials_list = []
with open('geotab_credentials.txt', 'r') as opened:
    for line in opened.readlines()[1:8]:
        credentials_list.append(ast.literal_eval(line))

for credentials in credentials_list:
    if credentials['database'] != 'VerticalLimit':
        continue
    api = mygeotab.API(**credentials)
    counter = 0    
    while counter < 10:
        try:
            print api.authenticate()
            break
        except Exception as e:
            sentry.captureException()
            print e
            time.sleep(3)
            counter += 1

def define_table():
    cursor = psycopg2.connect(**pg_credentials).cursor()

    queries = []

    queries.append("""
    CREATE TABLE geotab_sms (
        id serial PRIMARY KEY,
        user_name text,
        phone_number text
    );
    """)

    for query in queries[-1:]:
        cursor.execute(query)
        cursor.connection.commit()
    cursor.connection.close()

def get_phone_number(alert):
    try:
        cursor = psycopg2.connect(**pg_credentials).cursor()

        query = 'select phone_number from geotab_sms where user_name = %s'

        user_name = alert['device'].split('(')[-1][:-1]
        if len(user_name) < 1:
            user_name = alert['device'].split('(')[0] # No driver paired with the device

        cursor.execute(query, (user_name,))
        phone_number = cursor.fetchone()[0]
        cursor.connection.commit()
        cursor.connection.close()
        print phone_number
    except:
        phone_number = 'None'

    if len(str(phone_number)) < 10:
        #phone_number = '16138086954'
        phone_number = 'None'

    return phone_number

def process_alert(alert):
    phone_number = get_phone_number(alert)
    if phone_number == 'None':
        print 'No one to alert'
        return

    send_message(alert, phone_number)

#def get_user():
#    devices = api.get('DeviceStatusInfo')

@application.route('/geotab_notification',methods=['GET','POST'])
def receive_alert():
    values = request.values
    process_alert(values)
    print values
    return 'Ok'

def send_message(alert, number):    
    account_sid = twilio_credentials['account_sid'] # Your Account SID from www.twilio.com/console
    auth_token  = twilio_credentials['auth_token']  # Your Auth Token from www.twilio.com/console

    client = TwilioRestClient(account_sid, auth_token)
    
    actual_body = 'Device (driver): {}, this is an alert for {}'.format(alert['device'],alert['rule'])

    try:
        message = client.messages.create(body=actual_body,
            to="+"+number,    # Replace with your phone number
            from_=twilio_credentials['number']) # Replace with your Twilio number
    except TwilioRestException as e:
        print(e)

if __name__ == '__main__':
    application.run(host='0.0.0.0', port=5015)

