import socket
import ast
import traceback
import ftplib
import glob
import sys
import gzip
import sys
from urllib import urlretrieve
import arrow
import time
import os
import multiprocessing

import numpy as np
import requests
from bs4 import BeautifulSoup

import rasterio # IMPORT BEFORE NIO
from rasterio import warp
from rasterio import Affine
import Nio # IMPORT AFTER RASTERIO

from parameters import mrms_url, hrrr_url, storage_path, hrdps_url

import parameters
import stacked_parameters
import forecast_parameters
import road_temp_parameters
import europe_ds_parameters
import rap_ds_parameters
import rap_contours_parameters
import rap_road_temp_parameters

import gfs_parameters
import gfs_ds_parameters
import gfs_extra_variables

# Add them to a list to know which weather products to get
parameter_modules = [parameters, stacked_parameters, forecast_parameters, road_temp_parameters, rap_ds_parameters, rap_contours_parameters, rap_road_temp_parameters, europe_ds_parameters]

import data_handler

time_format = '%m/%d/%Y (%H:%M)'

rap_url = "http://www.ftp.ncep.noaa.gov/data/nccf/com/rap/prod/"
gfs_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/'

with open('stormgeo_credentials.txt','r') as opened:
    stormgeo_credentials = ast.literal_eval(opened.read())

def prepare_global_land_cover(gfs_filepath,dest_affine, dest_crs):
    #gfs_filepath = get_gfs_products_once([],return_path=True)
    opened_rasterio = rasterio.open(gfs_filepath)
    crs = opened_rasterio.crs
    affine = opened_rasterio.affine 

    opened_rasterio.close()
    
    opened = Nio.open_file(gfs_filepath,format='grib2')
    land = opened.variables['LAND_P0_L1_GLL0'].get_value()
    opened.close()

    master_grid = np.empty((1801,3599))
    master_grid.fill(-3)

    with rasterio.drivers(): #projecting to wt2
        warp.reproject(land,
            master_grid,
            src_transform=affine,
            src_crs=crs,
            dst_transform=dest_affine,
            dst_crs=dest_crs,
            resampling=rasterio.warp.RESAMPLING.nearest)


    second_half = master_grid[:,:1799]
    first_half = master_grid[:,1799:]
    recut_land = np.hstack([first_half,second_half])

    data_handler.store_persistent_layer(recut_land, 'GFS:land')
    data_handler.store_any_data(str(recut_land.shape), 'shape:GFS:land')

def prepare_database(hrrr_filepath=None, mrms_filepath=None):
    
    remove_old_files(time_threshold=-1)

    hrrr_filepath = get_hrrr_products([],return_path=True)
    mrms_filepath = get_mrms_products('PrecipFlag/',return_path=True)
    rap_filepath = get_rap_products([],return_path=True)
    gfs_filepath = get_gfs_products_once([],return_path=True)

    wt2_filepath = get_stormgeo_wt2([],return_path=True)
    wt_filepath = get_stormgeo_wt([],return_path=True)

    # Coordinate system information for HRRR

    opened_rasterio = rasterio.open(hrrr_filepath)

    crs = opened_rasterio.crs
    stored_crs = str(crs)
    data_handler.store_any_data(stored_crs,'HRRR:crs')

    transform = opened_rasterio.affine
    stored_transform = str((transform.a,transform.b,transform.c,transform.d,transform.e,transform.f))
    data_handler.store_any_data(stored_transform,'HRRR:affine')

    opened_rasterio.close()

    for filepath,source in zip([wt_filepath, wt2_filepath],['ecwt_','ecwt2']):
        opened_rasterio = rasterio.open(filepath)

        crs = opened_rasterio.crs
        stored_crs = str(crs)
        data_handler.store_any_data(stored_crs,'{}:crs'.format(source))

        transform = opened_rasterio.affine
        stored_transform = str((transform.a,transform.b,transform.c,transform.d,transform.e,transform.f))
        data_handler.store_any_data(stored_transform,'{}:affine'.format(source))

        opened_rasterio.close()

        opened = Nio.open_file(filepath,format='grib')
        x,y = np.meshgrid(opened.variables['g0_lon_1'].get_value(),opened.variables['g0_lat_0'].get_value())
        x = (x-360).astype(np.float32) # unsigned longitudes to signed
        y = y.astype(np.float32)
        
        #x = opened.variables['g0_lon_1'].get_value()
        #y = opened.variables['g0_lat_0'].get_value()
        data_handler.store_persistent_layer(x,'{}:x'.format(source))
        data_handler.store_persistent_layer(y,'{}:y'.format(source))
        data_handler.store_any_data(str(x.shape),'shape:{}'.format(source))

    prepare_global_land_cover(gfs_filepath, transform, crs) # projecting to wt2, dependent on the order of the above

    # Coordinates grid for lon/lat

    opened = Nio.open_file(hrrr_filepath)
    x = opened.variables['gridlon_0'].get_value()
    y = opened.variables['gridlat_0'].get_value()
    data_handler.store_persistent_layer(x,'HRRR:x')
    data_handler.store_persistent_layer(y,'HRRR:y')
    data_handler.store_any_data(str(x.shape),'shape:HRRR')

    # Land surface mask

    land = opened.variables['LAND_P0_L1_GLC0'].get_value()
    data_handler.store_persistent_layer(land, 'HRRR:land')
    data_handler.store_any_data(str(land.shape), 'shape:HRRR:land')

    opened.close()

    # RAP

    opened = Nio.open_file(rap_filepath)
    x = opened.variables['gridlon_0'].get_value()
    y = opened.variables['gridlat_0'].get_value()
    data_handler.store_persistent_layer(x,'RAP:x')
    data_handler.store_persistent_layer(y,'RAP:y')
    data_handler.store_any_data(str(x.shape),'shape:RAP')    
    land = opened.variables['LAND_P0_L1_GRLL0'].get_value()
    data_handler.store_persistent_layer(land, 'RAP:land')
    data_handler.store_any_data(str(land.shape), 'shape:RAP:land')

    opened.close()

    # GFS

    opened = Nio.open_file(gfs_filepath,format='grib2')
    raw_x = opened.variables['lon_0'].get_value()
    raw_y = opened.variables['lat_0'].get_value()

    raw_x = np.hstack([raw_x[:720],raw_x[720:]-360])

    x,y = np.meshgrid(raw_x,raw_y)
    #x = (x-360).astype(np.float32) # unsigned longitudes to signed
    x = x.astype(np.float32)
    y = y.astype(np.float32)

    data_handler.store_persistent_layer(x,'GFS:x')
    data_handler.store_persistent_layer(y,'GFS:y')
    data_handler.store_any_data(str(x.shape),'shape:GFS')    
    land = opened.variables['LAND_P0_L1_GLL0'].get_value()
    data_handler.store_persistent_layer(land, 'GFS:land:uncut')
    data_handler.store_any_data(str(land.shape), 'shape:GFS:land:uncut')

    opened.close()


    # MRMS

    opened = Nio.open_file(mrms_filepath)
    x,y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
    x = (x-360).astype(np.float32) # unsigned longitudes to signed
    y = y.astype(np.float32)
    data_handler.store_persistent_layer(x,'MRMS:x')
    data_handler.store_persistent_layer(y,'MRMS:y')
    data_handler.store_any_data(str(x.shape),'shape:MRMS')
    opened.close()

    remove_old_files(time_threshold=-1)

def get_products_of_interest():
    files_to_download = []
    grib_products = []
    for module in parameter_modules:
        try:
            files_to_download.extend(module.mrms_files)
        except AttributeError:
            pass
        try:
            grib_products.extend(module.variable_indices)
        except AttributeError:
            pass
        try:
            grib_products.extend(module.forecasted_variable_indices)
        except AttributeError:
            pass
    files_to_download = list(set(files_to_download))
    grib_products = set(grib_products)
    if None in grib_products:
        grib_products.remove(None)
    grib_products = list(grib_products)
    grib_products = [product for product in grib_products if product[0].isupper()]

    # Add products here if needed
    grib_products.append('2T_GDS0_SFC')

    grib_products.append('WIND_P8_L103_GLC0_max1h')
    #grib_products.append('APCP_P8_L1_GLC0_acc')
    grib_products.append('APCP_P8_L1_GLC0_acc1h')
    grib_products.append('APCP_P8_L1_GRLL0_acc1h')
    grib_products.append('DPT_P0_L103_GRLL0')

    return files_to_download, grib_products

def extract_variables_gfs(filepath, source='GFS', no_expiration=False, format=None):
    if format is None:
        opened = Nio.open_file(filepath,format='grib2')
    else:
        opened = Nio.open_file(filepath, format=format)

    time_alive = 43200

    variables = list(set(gfs_parameters.variable_indices+gfs_ds_parameters.variable_indices+gfs_extra_variables.variable_indices))

    latest = False
    valid_time = None

    for variable in variables:
        if variable in opened.variables:
            time_string = opened.variables[variable].initial_time
            initial_time = arrow.Arrow.strptime(time_string,fmt=time_format)
            forecast_time = int(opened.variables[variable].forecast_time[0])
            valid_time = initial_time.replace(hours=+forecast_time)
            break
        
    if valid_time is None:
        os.remove(filepath+'.completed') #Corrupted/incomplete file
        return 

    present_variables = opened.variables.keys()
    more_processing = False

    for variable in variables:
        try:
            original_variable = variable
            if variable.endswith('avg') or variable.endswith('acc'):
                variable = [item for item in present_variables if item.startswith(variable)][0]

            array = opened.variables[variable].get_value().data

            if len(array.shape)>2:
                array = array[0,:,:]

            data_handler.store_layer(array, 'GFS--'+variable, valid_time, time_alive, latest)
            
            
            if original_variable.endswith('avg') or original_variable.endswith('acc'):
                data_handler.store_layer(array, 'GFS--'+original_variable+'_intermediate1h', valid_time, time_alive, latest)
            #    create_intermediate_products('GFS--'+original_variable+'_intermediate1h', valid_time, time_alive, latest)
            
            
        except:
            traceback.print_exc()
            print 'Warning - product and file incompatible:',(variable,filepath)

    opened.close()

def create_intermediate_products(variable, valid_time, time_alive, latest): #misunderstanding? parking it for now
    keys = data_handler.ssdb_keys(variable,variable+'z',1000) #returns up to 1000 keys, won't have that many

    if len(keys) < 2:
        return

    last_key = '{0}:{1}'.format(variable,valid_time)

    current_index = keys.index(last_key)    

    if (current_index % 6) == 0:
        return

    last_data = data_handler.process_bytes(last_key,variable,verbose=False)

    to_retrieve = []
    while (current_index % 6) != 0:
        to_retrieve.append(data_handler.process_bytes(keys[current_index],variable,verbose=False))
        current_index += -1

    total = np.sum(to_retrieve,axis=0)

    if '_acc_' in variable:
        final_data = last_data-total
    elif '_avg_' in variable:
        final_data = last_data*(len(to_retrieve)+1) - total
    else:
        return

    data_handler.store_layer(final_data, variable, valid_time, time_alive, latest)


def extract_variables(filepath, variables, source='HRRR',no_expiration=False, format=None):
    
    if format is None:
        opened = Nio.open_file(filepath)
    else:
        opened = Nio.open_file(filepath, format=format)

    if False:
        coord_grid_exist = []
        for variable in variables:
            if data_handler.existence_check('persistent:x:{0}'.format(variable)):
                coord_grid_exist.append(0)
        if sum(coord_grid_exist) > 0:
            coord_grid_exist = True

    time_alive = 9000
    latest = False

    if source == 'MRMS':
        time_alive = 4000
        latest = True
        variables[0] = [key for key in opened.variables.keys() if 'lon_0' not in key and 'lat_0' not in key][0]
        #if 'QPE06H' in variables[0] or 'QPE03H' in variables[0]
        #if coord_grid_exist:
        #    x,y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
    
    elif source == 'HRRR':
        time_alive = 9000
        latest = False
        #if coord_grid_exist:
        #    x = opened.variables['gridlon_0'].get_value()
        #    y = opened.variables['gridlat_0'].get_value()

    elif source.startswith('ecwt'):
        time_alive = 14400
        latest = False

    print len(opened.variables)
    """
    else:
        time_alive = 900000
        latest = False

    print len(opened.variables)

    if source == 'HRRR':
        if sorted(variables)[2] not in opened.variables: #Should make a better test now that we have more files
            print 'Invalid file'
            opened.close()
            return
    """
    valid_time = None

    for variable in variables:
        if variable in opened.variables:
            time_string = opened.variables[variable].initial_time
            initial_time = arrow.Arrow.strptime(time_string,fmt=time_format)
            forecast_time = int(opened.variables[variable].forecast_time[0])
            valid_time = initial_time.replace(hours=+forecast_time)
            break
    
    if valid_time is None:
        os.remove(filepath+'.completed') #Corrupted/incomplete file
        return    
    
    print filepath, valid_time, time_string, forecast_time

    apcp_acc_expire_time = 25200
    if no_expiration:
        time_alive = -1 # won't trigger an expire operation
        apcp_acc_expire_time = -1

    for variable in variables:
        try:
            array = opened.variables[variable].get_value().data

            if variable.startswith('APCP_P8_L1'): # A little hacky for now
                data_handler.store_layer(array, variable, valid_time, apcp_acc_expire_time, latest)
            else:
                data_handler.store_layer(array, variable, valid_time, time_alive, latest)
            
            #if coord_grid_exist:
            #    data_handler.store_persistent_layer(x,'x:{0}'.format(variable))
            #    data_handler.store_persistent_layer(y,'y:{0}'.format(variable))
    
        except:
	    traceback.print_exc()
            print 'Warning - product and file incompatible:',(variable,filepath)

    opened.close()

def remove_old_files(storage_path=storage_path, time_threshold = 120):
    """ Removes files older than some amount of time """
    all_files = os.listdir(storage_path)
    current_time = time.time()
    for one_file in all_files:
        if (current_time - os.path.getctime(os.path.join(storage_path,one_file)))/60 > time_threshold:
            os.remove(os.path.join(storage_path, one_file))

def seed_past_hrrr():
    for i in range(1,8): 
        get_past_hrrr_products(['APCP_P8_L1_GLC0_acc'], storage_path, -i)

"""
Functions for specific sources

MRMS, HRRR, HRDPS (Canada)

"""

def main(mode='standard'):
    remove_old_files() #TODO remove commenting if commented

    a = arrow.utcnow().timestamp

    mrms_variables, hrrr_variables = get_products_of_interest()
    
    processes = []

    if mode == 'standard':
        
        for var in mrms_variables:
            #get_mrms_products(var)
            processes.append(multiprocessing.Process(target=get_mrms_products,args=(var,)))
        
    elif mode == 'hrrr':
        processes.append(multiprocessing.Process(target=get_hrrr_products,args=(hrrr_variables,)))

        if socket.gethostname() == 'ubuntu-cv1':
            for forecast_time in forecast_parameters.forecast_times[0:3]:#+['08','09']: # Acts as buffer
                processes.append(multiprocessing.Process(target=get_forecasted_hrrr_products,args=(hrrr_variables,storage_path,forecast_time)))
        else:
            for forecast_time in forecast_parameters.forecast_times:#+['08','09']: # Acts as buffer
                processes.append(multiprocessing.Process(target=get_forecasted_hrrr_products,args=(hrrr_variables,storage_path,forecast_time)))
        
        for forecast_time in ['01','02','03','04']: # this was sebs original. 01 is potentially a forecast
        #for forecast_time in ['00','01','02','03']: # 00 will be current.
            processes.append(multiprocessing.Process(target=get_rap_products,args=(hrrr_variables,storage_path,forecast_time)))
        
    elif mode == 'ecmwf':
        processes.append(multiprocessing.Process(target=get_stormgeo_wt, args=(hrrr_variables,)))
        processes.append(multiprocessing.Process(target=get_stormgeo_wt2, args=(hrrr_variables,)))

    elif mode == 'gfs':
        processes.append(multiprocessing.Process(target=get_gfs_products, args=(hrrr_variables,)))        

    for process in processes:
        process.start()

    #time.sleep(60)

    for process in processes:
        process.join()

    print 'Took {0} seconds'.format(arrow.utcnow().timestamp-a)

def historical_input():
    data_path = '/home/souellet/livedata/'

    a = arrow.utcnow().timestamp

    mrms_variables, hrrr_variables = get_products_of_interest()

    #directories = sorted(glob.glob(data_path+'2017*hrrr'))
    directories = sorted(glob.glob(data_path+'2017-2-1*hrrr'))+sorted(glob.glob(data_path+'2017-2-2*hrrr'))+sorted(glob.glob(data_path+'2017-3-*hrrr'))

    for dir_index, directory in enumerate(directories):
        print directory, dir_index
        filepaths = sorted(glob.glob(directory+'/*'))
        for filepath in filepaths:
            try:
                extract_variables(filepath,hrrr_variables, no_expiration=True)
            except Exception as e:
		traceback.print_exc()
                print e

def mrms_historical_input():
    data_path = '/home/souellet/livedata/'

    a = arrow.utcnow().timestamp

    mrms_variables, hrrr_variables = get_products_of_interest()

    #directories = glob.glob(data_path+'*hrrr')
    #directories = glob.glob(data_path+'*Precip*')+glob.glob(data_path+'*Seamless*')+glob.glob(data_path+'*Radar*')+glob.glob(data_path+'*Gauge*')
    directories = glob.glob(data_path+'2017-2-1*Precip*')+glob.glob(data_path+'2017-2-1*Seamless*')+glob.glob(data_path+'2017-2-1*Radar*')+glob.glob(data_path+'2017-2-1*Gauge*')
    #directories = glob.glob(data_path+'2017-3-*Precip*')+glob.glob(data_path+'2017-3-*Seamless*')+glob.glob(data_path+'2017-3-*Radar*')+glob.glob(data_path+'2017-3-*Gauge*')
    #directories = glob.glob(data_path+'*Gauge*')
    #directories = glob.glob(data_path+'2017*Gauge*')

    directories = sorted(directories)

    for directory in directories:
        print directory
        filepaths = sorted(glob.glob(directory+'/*'))
        for filepath in filepaths:
            try:
                with gzip.open(filepath,'rb') as compressed:
                    with open(filepath[:-3],'wb') as uncompressed:
                        uncompressed.write(compressed.read())
                
                extract_variables(filepath[:-3],['placeholder'], no_expiration=True, source='MRMS')
                #os.remove(filepath[:-3])
            except:
                print 'File is weird'

            try:
                os.remove(filepath[:-3])
            except:
                print 'Did not decompress'
            #extract_variables(filepath,hrrr_variables, no_expiration=True)
            #extract_variables(filepath[:-3],mrms_variables, no_expiration=True, source='MRMS')

def acquire_latest_file_ftp(server_path, file_source, file_pattern, credentials=None):
    try:
        ftp = ftplib.FTP(server_path)
        if not (credentials is None):
            ftp.login(credentials['username'], credentials['password'])
        ftp.cwd(file_source)
        filenames = []
        ftp.dir('-t', filenames.append)
        for filename in filenames:
            filename = filename.split(' ')[-1]
            if filename.startswith(file_pattern):
                break
                
        filepath = os.path.join(storage_path, filename)
        with open(filepath, 'wb') as downloaded:
            ftp.retrbinary('RETR {}'.format(filename), downloaded.write)
        ftp.close()

        return filepath
    except Exception as e:
        traceback.print_exc()
        print e

def acquire_most_ftp_files(server_path, file_source, file_pattern, credentials=None):
    try:
        ftp = ftplib.FTP(server_path)
        if not (credentials is None):
            ftp.login(credentials['username'], credentials['password'])
        ftp.cwd(file_source)
        filenames = []
        ftp.dir('-t', filenames.append)
        found_filenames = []
        for filename in filenames:
            filename = filename.split(' ')[-1]
            if filename.startswith(file_pattern):
                found_filenames.append(filename)
        
        # Filter filenames
        analysis_runtime = [int(filename.split('_')[1]) for filename in found_filenames]
        time_to_keep = max(analysis_runtime)
        found_filenames = [filename for filename in found_filenames if '_{}_'.format(time_to_keep) in filename]
        
        filepaths = []
        for filename in found_filenames:        
            filepath = os.path.join(storage_path, filename)
            with open(filepath, 'wb') as downloaded:
                ftp.retrbinary('RETR {}'.format(filename), downloaded.write)
            filepaths.append(filepath)
            
        ftp.close()

        return filepaths

    except Exception as e:
        traceback.print_exc()
        print e

def acquire_latest_file_ftp_old(server_path, file_source, file_pattern, credentials=None):
    try:
        ftp = ftplib.FTP(server_path)
        if not (credentials is None):
            ftp.login(credentials['username'], credentials['password'])
        ftp.cwd(file_source)
        filenames = []
        ftp.dir('-t', filenames.append)
        take_next = []
        for filename in filenames:
            filename = filename.split(' ')[-1]
            if filename.startswith(file_pattern):
                if len(take_next) > 0:
                    break
                else:
                    take_next.append(True)
                    continue
                
        filepath = os.path.join(storage_path, filename)
        with open(filepath, 'wb') as downloaded:
            ftp.retrbinary('RETR {}'.format(filename), downloaded.write)
        ftp.close()

        return filepath
    except Exception as e:
        traceback.print_exc()
        print e


def get_stormgeo_wt(products, storage_path=storage_path, return_path=False):
    #filepath = acquire_latest_file_ftp('ftp.storm.no', 'model/ec', 'wt_', credentials=stormgeo_credentials)
    if return_path:
        return acquire_latest_file_ftp('ftp.storm.no', 'model/ec', 'wt_', credentials=stormgeo_credentials)
    else:
        filepaths = acquire_most_ftp_files('ftp.storm.no', 'model/ec', 'wt_', credentials=stormgeo_credentials)
        for filepath in filepaths:
            extract_variables(filepath, products, source='ecwt_', format='grib')
            os.remove(filepath)

def get_stormgeo_wt2(products, storage_path=storage_path, return_path=False):
    if return_path:
        return acquire_latest_file_ftp('ftp.storm.no', 'model/ec', 'wt2', credentials=stormgeo_credentials)
    else:
        filepaths = acquire_most_ftp_files('ftp.storm.no', 'model/ec', 'wt2', credentials=stormgeo_credentials)
        for filepath in filepaths:
            extract_variables(filepath, products, source='ecwt2', format='grib')
            os.remove(filepath)

def get_mrms_products(product, storage_path=storage_path, return_path=False):
    """ Downloads live radar-related products """
    try:
        url = mrms_url+product
        req = requests.get(url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').endswith('.gz')])
        link = links[-1]
        fileurl = url+link
        print fileurl
        filepath = os.path.join(storage_path,link)
        if not os.path.exists(filepath[:-3]):
            urlretrieve(fileurl, filepath)
            with gzip.open(filepath,'rb') as compressed:
                with open(filepath[:-3],'wb') as uncompressed:
                    uncompressed.write(compressed.read())
            
            if return_path:
                return filepath[:-3]
            else:
                extract_variables(filepath[:-3], [product], source='MRMS')

    except Exception as e:
	traceback.print_exc()
        print str(e)

def get_gfs_products(products, storage_path=storage_path, return_path=False):
    try:
        req = requests.get(gfs_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('gfs.')])
        link = links[-1]

        req = requests.get(gfs_url+link)
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        
        relevant_dirlinks = []

        timestamps = ['f00{}'.format(forecast_time) for forecast_time in range(1,10)]+['f0{}'.format(forecast_time) for forecast_time in range(10,17)]

        for timestamp in timestamps:
            found_a = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('pgrb2.0p25.{}'.format(timestamp))]) # file is split in two
            relevant_dirlinks.append(found_a[-1])

            #Useless for now
            #found_b = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('pgrb2b.0p25.{}'.format(timestamp))])
            #relevant_dirlinks.append(found_b[-1])
        
        for dirlink in relevant_dirlinks:
            fileurl = gfs_url+link+dirlink
            
            filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
            
            if return_path:
                return filepath
            elif True:#not already_there:
                extract_variables_gfs(filepath)
    except Exception as e:
        traceback.print_exc()

def get_gfs_products_once(products, storage_path=storage_path, return_path=False):
    try:
        req = requests.get(gfs_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('gfs.')])
        link = links[-1]

        req = requests.get(gfs_url+link)
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('pgrb2.0p25.f001')])
        dirlink = dirlinks[-1]

        fileurl = gfs_url+link+dirlink
        
        filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
        
        if return_path:
            return filepath
        elif True:#not already_there:
            extract_variables(filepath, products)

    except Exception as e:
	traceback.print_exc()
        print str(e)


def get_hrrr_products(products, storage_path=storage_path, return_path=False):
    """ Downloads live HRRR files """
    try:
        req = requests.get(hrrr_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('hrrr.')])
        link = links[-1]

        req = requests.get(hrrr_url+link + "conus/")
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfsfcf01.grib2')])
        dirlink = dirlinks[-1]

        fileurl = hrrr_url+link + "conus/" + dirlink
        
        filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
        
        if return_path:
            return filepath
        elif True:#not already_there:
            extract_variables(filepath, products)

    except Exception as e:
	traceback.print_exc()
        print str(e)

def get_rap_products(products, storage_path=storage_path, forecast_time='01', return_path=False):
    """ Downloads live RAP files """
    try:
        req = requests.get(rap_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('rap.')])
        link = links[-1]

        req = requests.get(rap_url+link)
        soup = BeautifulSoup(req.text, 'lxml') 
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfnatf{}.grib2'.format(forecast_time))])
        dirlink = dirlinks[-1]

        fileurl = rap_url+link+dirlink
        
        filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
        
        if return_path:
            return filepath
        elif True:#not already_there:
            extract_variables(filepath, products, source='RAP')

    except Exception as e:
	traceback.print_exc()
        print str(e)


def get_past_hrrr_products(products, storage_path=storage_path, past_time=-2, return_path=False):
    """ Downloads live HRRR files """
    try:
        req = requests.get(hrrr_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('hrrr.')])
        link = links[-1]

        req = requests.get(hrrr_url+link + "conus/")
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfsfcf01.grib2')])
        dirlink = dirlinks[past_time]

        fileurl = hrrr_url+link+ "conus/" + dirlink
        
        filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
        
        if return_path:
            return filepath
        else:
            extract_variables(filepath, products)

    except Exception as e:
	traceback.print_exc()
        print str(e)

def get_forecasted_hrrr_products(products, storage_path=storage_path,forecast_time='01',return_path=False):
    try:
        req = requests.get(hrrr_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('hrrr.')])
        link = links[-1]

        req = requests.get(hrrr_url+link + "conus/")
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfsfcf'+forecast_time+'.grib2')])
        dirlink = dirlinks[-1]

        fileurl = hrrr_url+link+ "conus/" + dirlink

        #return acquire_file(fileurl, dirlink, storage_path)
        filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
        
        if return_path:
            return filepath
        elif True:#not already_there:
            extract_variables(filepath, products)
        
    except Exception as e:
	traceback.print_exc()
        print str(e)


"""
Helper functions
"""

def download_file(fileurl, filepath):
    pass

def check_downloading(filepath):
    while not os.path.exists(filepath+'.completed'):
        if not os.path.exists(filepath):
            return True
        elif check_age(filepath) > 1:
            return True
        time.sleep(2)
    return False

def is_file_corrupted(filepath):
    try:
        opened = Nio.open_file(filepath)
        opened.close()
        return False
    except Nio.NIOError as e:
	traceback.print_exc()
        return True

def acquire_file(fileurl, dirlink, storage_path):
    print fileurl
    filepath = os.path.join(storage_path,dirlink)
    already_there = True
    if check_downloading(filepath):
    #if not os.path.exists(filepath):
        while is_file_corrupted(filepath):
            urlretrieve(fileurl, filepath)
            already_there = False
        
        with open(filepath+'.completed','w') as opened:
            opened.write('Download completed')
    
    return filepath, already_there

def check_age(filepath):
    current_time = time.time()
    age = (current_time - os.path.getctime(filepath))/60.0
    return age

if __name__ == '__main__': # Gotta make arguments a little more robust
    mode = 'standard' 
    if sys.argv[-1] == '--ecmwf':
        main(mode='ecmwf')
    elif sys.argv[-1] == '--gfs':
        main(mode='gfs')    
    elif sys.argv[-1] == '--hrrr':
        main(mode='hrrr')    
    elif sys.argv[-1] == '--prepare':
        prepare_database()
    elif sys.argv[-1] == '--seedhrrr':
        seed_past_hrrr()
    else:
        #if not data_handler.existence_check('persistent:shape:RAP'):
        if not data_handler.existence_check('persistent:shape:GFS'):
            prepare_database()
        
        main(mode=mode)
