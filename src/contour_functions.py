import matplotlib
matplotlib.use('Agg')
from cartopy.mpl import patch
import numpy as np
from matplotlib import pyplot
from parameters import *

from matplotlib import _cntr as cntr
import matplotlib.path as mpath


def polygonize(x,y,raster_data, which_variable=None, thresholds=None, inverted=False):
    """ Creates contour using given or computed levels """

    # If the minimum or the maximum of the variable is of interest
    extension = 'max'
    if inverted:
        extension = 'min'
    
    if False:
        if extension == 'min':
            minimum = raster_data.min()-1
            thresholds = list(thresholds)
            thresholds.insert(0,minimum)
        else:
            maximum = raster_data.max()+1
            thresholds = list(thresholds)
            thresholds.append(maximum)

        level_values = thresholds
        contour_obj = cntr.Cntr(x,y,raster_data)
        
        i = 0
        levels = []
        while i < len(thresholds)-1:
            levels.append([])
            nlist = contour_obj.trace(thresholds[i],thresholds[i+1])

            nseg = len(nlist)//2
            segs = nlist[:nseg]
            kinds = nlist[nseg:]

            paths = [mpath.Path(seg,codes=kind) for seg,kind in zip(segs,kinds)]
            for path in paths:
                try:
                    new_path = patch.path_to_geos(path)
                    levels[-1].extend(new_path)
                except Exception as e:
                    print(e)
                    continue
            i += 1

    else:
        contours = pyplot.contourf(x,y,raster_data,levels=thresholds,extend=extension)
        collections = contours.collections
        level_values = contours.levels
    
    #pyplot.savefig('/Users/soullet/Desktop/polygons.svg')

        levels = []
        for contour in collections:
            levels.append([])
            for path in contour.get_paths():
                try:
                    new_path = patch.path_to_geos(path)
                    levels[-1].extend(new_path)
                except Exception as e:
                    print(e)
                    continue

    return levels, level_values

class ContourSet():
    def __init__(self):
        pass