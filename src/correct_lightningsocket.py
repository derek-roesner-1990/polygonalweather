import calendar
import re
import time
import socketIO_client
import socket
import psycopg2
import ast
import logging

timeout_seconds = 8

try:
    logging.basicConfig(filename='../../logs/lightningsocket.log')
except:
    logging.basicConfig(filename='/home/souellet/logs/lightningsocket.log')

with open('credentials.txt') as opened:
    partner_id = opened.readline().strip()
    credentials = ast.literal_eval(opened.readline())

def create_tables():
    cursor = connection = psycopg2.connect(**credentials).cursor()

    queries = []
    queries.append("""
    CREATE TABLE lightning_pulses (
        id serial PRIMARY KEY,
        geom geometry,
        polarity int,
        peak_current float,
        timestamp int,
        height float,
        cgmultiply int,
        icmultiply int
    );
    """)
    for query in queries:
        cursor.execute(query)
        cursor.connection.commit()

def push_to_postgres(cursor, record):

    query = """
    insert into lightning_pulses (geom, polarity, peak_current, timestamp, height, cgmultiply, icmultiply)
    values (ST_SetSRID(ST_MakePoint(%s,%s),%s), %s, %s, %s, %s, %s, %s)
    returning id;
    """

    cursor.execute(query,(record['longitude'], record['latitude'], 4326, record['polarity'], record['peakCurrent'], record['unix_time'], record['icHeight'], record['cgMultiplicity'], record['icMultiplicity']))
    record_id = cursor.fetchone()
    cursor.connection.commit()
    return record_id[0]

def transform_and_emit(all_received, current_socketIO, cursor):
    to_emit_batch = []
    for received in all_received:
        if received[0:2] == '{{':
            received = received[1:]
        elif received[-2:] == '}}':
            received = received[:-1]

        received = ast.literal_eval(received)
        if received['peakCurrent'] > 0:
            received['polarity'] = 1
        else:
            received['polarity'] = -1
        split_time = received['time'].split('.')
        received['unix_time'] = calendar.timegm(time.strptime(split_time[0],'%Y-%m-%dT%H:%M:%S'))+float('0.'+split_time[1][0:-1])
        record_id = push_to_postgres(cursor, received)
        to_emit = {"type": "Feature","geometry": {"type": "Point","coordinates": [received['longitude'], received['latitude']]},"properties": {"polarity": received['polarity'], "timestamp": received['unix_time'], 'id': record_id}}
        if received['cgMultiplicity'] > 0:
        to_emit_batch.append(to_emit)
    if len(to_emit_batch) > 0:
    current_socketIO.emit('lightning-flash-src', to_emit_batch)

def main():
    current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')
    cursor = psycopg2.connect(**credentials).cursor()
    host = 'lx.datamart.earthnetworks.com'
    port = 80
    listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listener.settimeout(timeout_seconds)
    listener.connect((host, port))
    authentication = '{"p":"'+partner_id+'","v":3,"f":1, "t":1}'
    listener.sendall(authentication)
    failure = False
    start = time.time()
    while True:
    current = time.time()
    if current - start > 3600:
        cursor.execute('delete from lightning_pulses where timestamp < %s',(int(start),))
        cursor.connection.commit()
        start = current
    try:
            all_received = listener.recv(262144)
    except Exception as e:
        logging.exception(e)
        failure = True
    if len(all_received) < 1 or failure:
        try:
            listener.close()
        listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            listener.settimeout(timeout_seconds)
        listener.connect((host,port))
            failure = False
        listener.sendall(authentication)
        except Exception as e:
        logging.exception(e)
        continue
    else:
        try:
            all_received = re.findall('{.*?}', all_received)
                transform_and_emit(all_received, current_socketIO, cursor)
            time.sleep(1.0)
        except Exception as e:
        logging.exception(e)
        continue

if __name__ == '__main__':
    #create_tables()
    #query()
    main()