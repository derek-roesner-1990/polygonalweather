import gzip
import subprocess
import sys
from urllib import urlretrieve
import time
import calendar
import numpy as np
import requests
from bs4 import BeautifulSoup
import os
import Nio

from parameters import mrms_url, hrrr_url, storage_path, hrdps_url

def hrdpsproducts(product, time_difference = 0, storage_path=storage_path):
    runs = ['18','12','06','00']
    forecast_hours = ['000','001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020','021','022','023','024']

    current_time = time.gmtime()
    
    today_string = time.strftime('%Y%m%d',current_time)
    hour = current_time.tm_hour

    for run in runs:
        forecast_time = hour - int(run) 
        if forecast_time < 1:
            continue
        forecast_hour = forecast_hours[forecast_time+time_difference]
        
        filename = "CMC_hrdps_continental_{}_ps2.5km_{}-00.grib2".format(product,today_string+run+'_P'+forecast_hour)
        fileurl = hrdps_url+run+'/'+forecast_hour+'/'+filename
        filepath = os.path.join(storage_path, filename)
        try:
            if not os.path.exists(filepath):
                if requests.get(fileurl).ok: 
                    urlretrieve(fileurl, filepath)
                else:
                    continue
            return filepath
        except Exception as e:
            print str(e)
    return 'Not found'

def mrmsproducts(product, storage_path=storage_path):
    """ Downloads live radar-related products """
    try:
        url = mrms_url+product
        req = requests.get(url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').endswith('.gz')])
        link = links[-1]
        fileurl = url+link
        print fileurl
        filepath = os.path.join(storage_path,link)
        if not os.path.exists(filepath[:-3]):
            urlretrieve(fileurl, filepath)
            with gzip.open(filepath,'rb') as compressed:
                with open(filepath[:-3],'wb') as uncompressed:
                    uncompressed.write(compressed.read())
        return filepath[:-3]
    except Exception as e:
        print str(e)

def check_downloading(filepath):
    while not os.path.exists(filepath+'.completed'):
        if not os.path.exists(filepath):
            return True
        elif check_age(filepath) > 1:
            return True
        time.sleep(2)
    return False

def is_file_corrupted(filepath):
    try:
        opened = Nio.open_file(filepath)
        opened.close()
        return False
    except Exception as e:
        return True

def acquire_file(fileurl, dirlink, storage_path):
    print fileurl
    filepath = os.path.join(storage_path,dirlink)
    if check_downloading(filepath):
    #if not os.path.exists(filepath):
        while is_file_corrupted(filepath):
            urlretrieve(fileurl, filepath)
        
        with open(filepath+'.completed','w') as opened:
            opened.write('Download completed')
    
    return filepath

def check_age(filepath):
    current_time = time.time()
    age = (current_time - os.path.getctime(filepath))/60.0
    return age

def forecasted_hrrrproducts(storage_path=storage_path,forecast_time='01'):
    try:
        req = requests.get(hrrr_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('hrrr.')])
        link = links[-1]

        req = requests.get(hrrr_url+link+"conus/")
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfsfcf'+forecast_time+'.grib2')])
        dirlink = dirlinks[-1]

        fileurl = hrrr_url+link+"conus/"+dirlink

        return acquire_file(fileurl, dirlink, storage_path)
        
    except Exception as e:
        print str(e)

def hrrproducts(storage_path=storage_path):
    """ Downloads live HRRR files """
    try:
        req = requests.get(hrrr_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('hrrr.')])
        link = links[-1]

        req = requests.get(hrrr_url+link+"conus/")
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('wrfsfcf01.grib2')])
        dirlink = dirlinks[-1]

        fileurl = hrrr_url+link+"conus/"+dirlink
        
        return acquire_file(fileurl, dirlink, storage_path)

    except Exception as e:
        print str(e)

def remove_old_files(storage_path=storage_path):
    """ Removes files older than 90 minutes """
    all_files = os.listdir(storage_path)
    current_time = time.time()
    for one_file in all_files:
        if (current_time - os.path.getctime(os.path.join(storage_path,one_file)))/60 > 180:
            os.remove(os.path.join(storage_path, one_file))
