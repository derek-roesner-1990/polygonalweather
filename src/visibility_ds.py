import shutil
import gzip
import sys
sys.setcheckinterval(10000000)
import socket
import matplotlib
matplotlib.use('Agg')
import ast
from matplotlib import pyplot
import subprocess
import rasterio
from rasterio import warp
from rasterio import Affine
from scipy import ndimage
import numpy as np
import Nio
from cartopy.mpl import patch
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()
import os
from scipy import ndimage

#from dsRoadStatesPython_newBeliefs import getBeliefValues
#from dsVisibility_withRadar import getBeliefValues
from dsVisibility_more_sensitive import getBeliefValues
import postgres_functions
from live_feed_functions import *
from visibility_parameters import *

#master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
master_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
master_grid_width = 1400
master_grid_height = 700
master_crs = {'init': 'EPSG:4326'}

down_factor = 5
buffer_factor = 0.05
simplification = 0
#buffer_factor = 0.1
#simplification = 0.01

def compute_ds_array(array, indices):
    cache = dict()
    shape1,shape2 = array.shape[0:2]
    ds_array = np.zeros((shape1,shape2))
    #for i in xrange(shape1):
    #    for j in xrange(shape2):
    #for i,j in zip(indices[0],indices[1]):
    indices = np.dstack(indices).squeeze()
    for i,j in indices:
        array_slice = tuple(array[i,j])
        if cache.has_key(array_slice):
            result = cache[array_slice]
        else:
            result = getBeliefValues(array_slice)
            cache[array_slice] = result
        ds_array[i,j] = result
    return ds_array

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(cache=dict())
def cached_belief(array_slice):
    array_slice = tuple(array_slice)
    if cached_belief.cache.has_key(array_slice):
        return cached_belief.cache[array_slice]
    else:
        result = getBeliefValues(array_slice)
        cached_belief.cache[array_slice] = result
        return result

def main():
    print time.gmtime()

    filepaths = [hrrproducts(storage_path)]
    filepaths = filepaths*number_of_hrrr_products
    for mrms_file in mrms_files:
        filepaths.append(mrmsproducts(mrms_file,storage_path))

    grid_list = []
    truex, truey = [None,None]

    opened_rasterio = None

    for variable_index,filepath in zip(variable_indices,filepaths):

        opened = Nio.open_file(filepath)

        if "MRMS" in filepath:
            x,y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
            x = x-360 # fixing unsigned longitudes
            variable = [key for key in opened.variables.keys() if 'lon_0' not in key and 'lat_0' not in key][0] # One variable per file
            gaussian_sigma = base_gaussian_sigma*2
        else:
            if opened_rasterio is None:
                opened_rasterio = rasterio.open(filepath)

                crs = opened_rasterio.crs
                transform = opened_rasterio.affine

            #x = opened.variables['gridlon_0'].get_value()
            #y = opened.variables['gridlat_0'].get_value()
            gaussian_sigma = base_gaussian_sigma
            try:
                #index = variable_index
                #variable = opened.variables.keys()[index]
                variable = variable_index
            except Exception as e:
                print e

        data = opened.variables[variable].get_value()
        if len(data.shape) > 2:
            data = data[0,:,:]
        variable_name = opened.variables[variable].long_name

        print 'Stacking', variable_name

        if "MRMS" in filepath:
            grid_list.append(data[::down_factor,::down_factor])
            if truex is None:
                truex = x
                truey = y
        else:
            flipped_data = np.array(np.flipud(data))
            master_grid = np.empty((master_grid_height,master_grid_width))
            master_grid.fill(-3)

            with rasterio.drivers():
                warp.reproject(flipped_data,
                    master_grid,
                    src_transform=transform,
                    src_crs=crs,
                    dst_transform=master_transform,
                    dst_crs=master_crs,
                    resampling=rasterio.warp.RESAMPLING.nearest)
            grid_list.append(master_grid)

    reordered = [None]*9
    reordered[0] = grid_list[7]
    reordered[1] = grid_list[8]
    reordered[2] = grid_list[0]
    reordered[3] = grid_list[1]
    reordered[4] = grid_list[2]
    reordered[5] = grid_list[3]
    reordered[6] = grid_list[4]
    reordered[7] = grid_list[5]
    reordered[8] = grid_list[6]
    master_array = np.dstack(reordered)

    opened = Nio.open_file(filepaths[0])
    data = opened.variables['LAND_P0_L1_GLC0'].get_value()
    flipped_data = np.array(np.flipud(data))
    master_grid = np.empty((master_grid_height,master_grid_width))
    master_grid.fill(-3)

    with rasterio.drivers():
        warp.reproject(flipped_data,
            master_grid,
            src_transform=transform,
            src_crs=crs,
            dst_transform=master_transform,
            dst_crs=master_crs,
            resampling=rasterio.warp.RESAMPLING.nearest)
    land = master_grid.astype(np.bool)
    #master_array[~land] = -3
    #indices = np.where(land[::down_factor,::down_factor])
    indices = np.where(land)
    
    master_array = master_array.data
    #np.save('master_array',master_array)
    #np.save('indices', indices)
    #return
    #master_array = np.ma.masked_equal(master_array,-3)
    #print master_array[0,0]

    #downsampled = master_array[::down_factor,::down_factor]
    
    #print getBeliefValues([downsampled[90,180]])
    print 'Preprocessing completed'
    print time.gmtime()
    #result = np.apply_along_axis(getBeliefValues,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    result = compute_ds_array(master_array, indices)
    print 'Results calculated'
    print time.gmtime()

    polygons = polygonize(truex[::down_factor,::down_factor], truey[::down_factor,::down_factor],result)
    postgres_functions.push_visibility_zones_clean(polygons)
    
    print 'Polygons created'
    print time.gmtime()

    if True:
        to_save = np.dstack((master_array, result))
        filename = time.gmtime()
        filename = time.strftime('Visibility-%Y-%m-%dZ%H-%M-%S', filename)
        np.save('../persistent/'+filename, to_save)
        with open('../persistent/'+filename+'.npy', 'rb') as f_in, gzip.open('../persistent/'+filename+'.npy.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove('../persistent/'+filename+'.npy')

    #client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #client_socket.connect(('localhost',8042))
    #client_socket.close()

    remove_old_files()
    
    #postgres_functions.intersect_roads()
    #np.save('/Users/soullet/Downloads/dsresults',result)
    #print 'Roads intersected'
    #print time.gmtime()

def polygonize(x,y,array):

    #array = ndimage.gaussian_filter(array,sigma=0.3)

    contours = pyplot.contourf(x,y,array,levels=[20.10,21.10],cmap='inferno')

    levels = []
    for contour in contours.collections:
        levels.append([])
        for path in contour.get_paths():
            levels[-1].extend(patch.path_to_geos(path))
    if buffer_factor > 0:
        medium = [poly.buffer(buffer_factor) for poly in levels[0]]
        #bad = [poly.buffer(buffer_factor) for poly in levels[2]]

        if simplification > 0:
            better_medium = ops.unary_union(medium).buffer(-buffer_factor).simplify(simplification, preserve_topology=True)
            #better_bad = ops.unary_union(bad).buffer(-buffer_factor).simplify(simplification, preserve_topology=True)
        else:
            #better_medium = ops.unary_union(medium).buffer(-buffer_factor)
            better_medium = ops.unary_union(medium)
            #better_bad = ops.unary_union(bad).buffer(-buffer_factor)

        #better_medium = better_medium.difference(better_bad)

    else:
        better_medium = geometry.MultiPolygon(levels[0])
        #better_bad = geometry.MultiPolygon(levels[2])
    
    # 0.8 still placeholder until we want to have multiple levels

    return [[better_medium,'B',0.8]]
    #return [[better_medium,'M',0.8], [better_bad,'B',0.8]]

if __name__ == '__main__':
    main()
    #test()
    #validation_run()
