import random
import requests
import time
import ast
import datetime
import multiprocessing
import threading
import geotab_functions
import arrow
import pytz
import mygeotab
from raven import Client
from data_handler import database

contours_key = 'geotab_live_contours'
ds_key = 'geotab_stacked_ds'

keys = [contours_key,ds_key]

#all_names = [geotab_functions.contour_names, geotab_functions.ds_names, geotab_functions.nws_names]
all_names = [geotab_functions.nws_names, geotab_functions.contour_names, geotab_functions.ds_names]

with open('sentry_credentials.txt', 'r') as opened:
    sentry_credentials = opened.read()

sentry = Client(sentry_credentials)

credentials_list = []
with open('geotab_credentials.txt', 'r') as opened:
    for line in opened.readlines()[0:]:
    #for line in opened.readlines()[0:1]:
        credentials_list.append(ast.literal_eval(line))

apis = []
for credentials in credentials_list:
    api = mygeotab.API(**credentials)
    counter = 0    
    while counter < 10:
        #if api.credentials.database == 'allina_health': # Until access is resolved
        #if api.credentials.database == 'yrc_freight': # Until access is resolved
        #    break
        try:
            print api.authenticate()
            apis.append(api)
            break
        except Exception as e:
            sentry.captureException()
            print e
            time.sleep(3)
            counter += 1

    #apis.append(api)

def try_multicalls(api, multicalls):
    # Attempt all calls to the GeoTab API even if an error occurs in one or more of the calls.
    #  1. If an error occurs within multi_call() an exception will be thrown.
    #  2. The index of the call that caused the error is noted.
    #  3. All remaining calls will be reassigned to the list 'multicalls'.
    #  4. The remaining calls will be attempted.
    #  5. The process repeats until an error does not occur or all calls have been attempted.
    error_amt = 0
    try:
        while True:
            try:
                api.multi_call(multicalls)
                error_at = None
            except mygeotab.MyGeotabException as e:
                error_at = e.data['requestIndex']
                print "An error occured while making a call to the GeoTab API.\nError Details:\n{}\nCall Details:\n{}\n".format(e,multicalls[error_at])

            if error_at == None:
                break
            else:
                error_amt += 1
                list_length = len(multicalls)
                if error_at == list_length-1:
                    break
                else:
                    # List splice syntax: list[start:end], where 1 is automatically subtracted from end.
                    multicalls = multicalls[error_at + 1:list_length]
    except Exception as e:
        sentry.captureException()
        print e
    return error_amt

def add_zones_types():

    for api in apis[-1:]:
        multicalls = []

        for zone_type in geotab_functions.zone_types_id:
            multicalls.append(('Add', {'typeName':'ZoneType', 'entity':zone_type}))

        call_amt = len(multicalls)
        print "Attempting to add {} ZoneTypes to {}.".format(call_amt, api.credentials.database)

        # Prexisting version where if an error occured in one call all other calls after that point were not attempted.
        #api.multi_call(multicalls)

        # New version where all calls to the GeoTab API are attempted even if an error occurs in one or more of the calls.
        error_amt = try_multicalls(api,multicalls)
        print "Added {} out of {} ZoneTypes.".format(call_amt - error_amt, call_amt)

def add_ds_zones_types():

    for api in apis[-1:]:
        multicalls = []

        for zone_type in geotab_functions.ds_zone_types_id:
            multicalls.append(('Add', {'typeName':'ZoneType', 'entity':zone_type}))

        call_amt = len(multicalls)
        print "Attempting to add {} ZoneTypes to {}.".format(call_amt, api.credentials.database)

        # Prexisting version where if an error occured in one call all other calls after that point were not attempted.
        #api.multi_call(multicalls)

        # New version where all calls to the GeoTab API are attempted even if an error occurs in one or more of the calls.
        error_amt = try_multicalls(api,multicalls)
        print "Added {} out of {} ZoneTypes.".format(call_amt - error_amt, call_amt)

def add_nws_zone_types():
    for api in apis[-1:]:
        multicalls = []

        for zone_type in geotab_functions.nws_zone_types_id:
            multicalls.append(('Add', {'typeName':'ZoneType', 'entity':zone_type}))

        call_amt = len(multicalls)
        print "Attempting to add {} ZoneTypes to {}.".format(call_amt, api.credentials.database)

        # Prexisting version where if an error occured in one call all other calls after that point were not attempted.
        #api.multi_call(multicalls)

        # New version where all calls to the GeoTab API are attempted even if an error occurs in one or more of the calls.
        error_amt = try_multicalls(api,multicalls)
        print "Added {} out of {} ZoneTypes.".format(call_amt - error_amt, call_amt)

def get_nws_add_zone_multicalls():
    try:
        multicalls = []

        # Attempting to retrieve a FeatureCollection from the 'nws/alerts' endpoint of the Weather Telematics API.
        # Each alert is a Feature within the FeatureCollection.
        REQUEST_ATTEMPT_MAX = 5
        request_attempt_amt = 1
        while True:
            try:
                response = requests.get("https://wx2-api.weathertelematics.com/nws/alerts")
                response.raise_for_status()
                break
            except Exception as e:
                if request_attempt_amt >= REQUEST_ATTEMPT_MAX:
                    raise e
                else:
                    request_attempt_amt += 1
                    time.sleep(3)

        # Converting the retrieved content into a FeatureCollection.
        # See these resources for more information :
        # http://wiki.geojson.org/GeoJSON_draft_version_6#FeatureCollection
        # http://wiki.geojson.org/GeoJSON_draft_version_6
        feature_collection = ast.literal_eval(response.content)

        # Converting each alert to a Shapely Polygon where adjacent alerts of the same type are merged.
        polygons_by_zone_type = geotab_functions.nws_get_merged_polygons(feature_collection)

        polygons_by_zone_type = geotab_functions.nws_adjust_polygons_by_severity(polygons_by_zone_type)

        # Converting each Polygon to a Zone entity for use with the GeoTab API.
        #zones = geotab_functions.nws_transform_into_entities(polygons_by_zone_type)

        # Creating the API calls that will be used to created each Zone.
        for zone in zones:
            multicalls.append(('Add',{'typeName':'Zone','entity':zone}))

    except Exception as e:
        sentry.captureException()
        print e
        return None
    return multicalls

def inefficient_get_delete_multicalls(api,names,late_threshold):
    early_threshold = datetime.datetime(2016, 10, 30, 15, 1, 1, tzinfo=pytz.UTC)
    old_entities = api.get('Zone')

    multicalls = []
    for entity in old_entities:
        current_groups = entity['groups']
        group_flag = False
        for current_group in current_groups:
            if current_group['id'] == 'GroupCompanyId':
                group_flag = True
                break

        if group_flag:
            for name in names:
                if entity['name'].startswith(name) and entity['activeFrom'] > early_threshold and entity['activeFrom'] < late_threshold:
                    multicalls.append(('Remove',{'typeName':'Zone','entity':entity}))
                    break

    return multicalls

def get_delete_multicalls(api,names,late_threshold):
    early_threshold = datetime.datetime(2016, 10, 30, 15, 1, 1, tzinfo=pytz.UTC)

    old_entities = []
    for name in names:
        old_entities.extend(api.get('Zone',search={'name':name+'%'}))

    multicalls = []
    for entity in old_entities:
        current_groups = entity['groups']
        group_flag = False
        for current_group in current_groups:
            if current_group['id'] == 'GroupCompanyId':
                group_flag = True
                break

        if group_flag:
            if entity['activeFrom'] > early_threshold and entity['activeFrom'] < late_threshold:
                multicalls.append(('Remove',{'typeName':'Zone','entity':entity}))

    return multicalls

def delete_old_zones(api,multicalls):
    call_amt = len(multicalls)
    a = time.time()
    try:
        print "Attempting to delete {} entities from {}.".format(call_amt, api.credentials.database)

        # Prexisting version where if an error occured in one call all other calls after that point were not attempted.
        #api.multi_call(multicalls)

        # New version where all calls to the GeoTab API are attempted even if an error occurs in one or more of the calls.
        error_amt = try_multicalls(api,multicalls)

        print "Deleted {} out of {} entities.".format(call_amt - error_amt, call_amt)
        print 'Deletion for', api.credentials.database, time.time()-a
    except Exception as e:
        sentry.captureException()
        print e

def add_to_geotab(api, multicalls):
    call_amt = len(multicalls)
    a = time.time()
    print "Attempting to add {} entities to {}.".format(call_amt, api.credentials.database)
    try:
        # Version where each call is made separately.
        #PARAMS = 1
        #for call in multicalls:
        #    try:
        #        # Allowing the zones to be displayed on the myGeoTab Map.
        #        # For debugging purposes.
        #        call[PARAMS]['entity']['displayed'] = True

        #        api.add(call[PARAMS]['typeName'], call[PARAMS]['entity'])
        #    except Exception as e:
        #        error_amt += 1
        #        print "An error occured while making a call to the GeoTab API.\nError Details:\n{}\nCall Details:\n{}\n".format(e,call)

        # Prexisting version where if an error occured in one call all other calls after that point were not attempted.
        #api.multi_call(multicalls)

        # New version where all calls to the GeoTab API are attempted even if an error occurs in one or more of the calls.
        error_amt = try_multicalls(api,multicalls)

        print "Added {} out of {} entities.".format(call_amt - error_amt, call_amt)
        print "Addition for", api.credentials.database, time.time()-a, arrow.utcnow()
    except Exception as e:
        sentry.captureException()
        print e
    
def add_then_delete(api, multicalls, names, late_threshold):
    try:
        delete_multicalls = get_delete_multicalls(api, names, late_threshold)

        random.shuffle(multicalls) # Not prioritizing a type of zone
        add_to_geotab(api, multicalls)

        delete_old_zones(api, delete_multicalls)
        print 'Done updating', api.credentials.database, arrow.utcnow()
    except:
        sentry.captureException()

class Account():
    def __init__(self,api):
        self.api = api
        self.zone_types = []
        self.start_time = arrow.utcnow()
        self.processes = []

    def has_ongoing_processes(self):
        for process in self.processes:
            if process.is_alive():
                return True
        return False

    def is_ready(self):
        if (arrow.utcnow().timestamp - self.start_time.timestamp) > 1200:
            if self.has_ongoing_processes():
                if arrow.utcnow().timestamp - self.start_time.timestamp > 2400:
                    print 'This database is taking a long time to update: {}'.format(self.api.credentials.database), arrow.utcnow()
                    sentry.captureMessage('This database is taking a long time to update: {}'.format(self.api.credentials.database))
                return False
            else:
                return True
        else:
            return False

    def authenticate_again(self):
        db_to_find = self.api.credentials.database
        db_credentials = [credentials for credentials in credentials_list if credentials['database'] == db_to_find][0]
        self.api = mygeotab.API(**db_credentials)
        self.api.authenticate()

    def update_geotab(self):
        try:
            self.api.get('Zone',resultsLimit=1) # Tests for expired session or similar issue
        except:
            self.authenticate_again()

        print 'Updating', self.api.credentials.database, arrow.utcnow()
        self.processes = [] # Clears completed processes
        self.start_time = arrow.utcnow()

        all_multicalls = []

        # Retrieving the multicalls for the zone types related to active alerts issued by the US National Weather Service.
        all_multicalls.append(get_nws_add_zone_multicalls())

        # Retrieving the multicalls for the zone types that are unique to https://wx2.weathertelematics.com/#/.
        #for key in keys:
        #    multicalls = ast.literal_eval(database.get(key))
        #    all_multicalls.append(multicalls)

        #late_threshold = arrow.utcnow().replace(minutes=-1).datetime

        #for multicalls,names in zip(all_multicalls,all_names):
        #    self.processes.append(multiprocessing.Process(target=add_then_delete,args=(self.api,multicalls,names,late_threshold)))

        #for process in self.processes:
        #    process.start()

def main():
    accounts = []
    for api in apis:
        accounts.append(Account(api))

    print apis

    for account in accounts:
        account.update_geotab()

    print accounts

    while True:
        for account in accounts:
            if account.is_ready():
                print "Updating " + account
                account.update_geotab()
        
        time.sleep(60)


if __name__ == '__main__':
    main()
