import glob
import time
import sys
sys.setcheckinterval(10000000)
import socket
import matplotlib
matplotlib.use('Agg')
import ast
from matplotlib import pyplot
import subprocess
import rasterio
from rasterio import warp
from rasterio import Affine
from rasterio import features
from scipy import ndimage
import numpy as np
import socketIO_client
from cartopy.mpl import patch
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()
import os
import arrow

import data_handler
#from ds_computation_ice_forecast import getForecastBeliefValues
from dsRoadStatesTemporalSplit_withRoad_V4 import getForecastBeliefValues
import postgres_functions
import live_feed_functions
from forecast_parameters import *
import alarms_and_triggers

#master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
master_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
master_grid_width = 1400
master_grid_height = 700
master_crs = {'init': 'EPSG:4326'}

down_factor = 5
#buffer_factor = 0.1
buffer_factor = 0.05
buffer_divisor = 1.5
simplification = 0.01

bin_optimization = True

def compute_ds_array(array, frozen_array, frozen_mask, indices):
    cache = dict()
    shape1,shape2 = array.shape[0:2]
    ds_array = np.zeros((shape1,shape2))
    #for i in xrange(shape1):
    #    for j in xrange(shape2):
    #for i,j in zip(indices[0],indices[1]):
    indices = np.dstack(indices).squeeze()
    for i,j in indices:

        if frozen_mask[i,j]:
            array_slice = tuple(frozen_array[i,j])            
        else:
            array_slice = tuple(array[i,j])

        if cache.has_key(array_slice):
            result = cache[array_slice]
        else:
            result = getForecastBeliefValues(array_slice)
            cache[array_slice] = result
        ds_array[i,j] = result
        #if array_slice[-3] > array_slice[-2]:
        #    print array_slice
    return ds_array

def clean_past_arrays(threshold= 35):
    filepaths = glob.glob('../persistent/forecast_*')
    current_time = time.time()
    for filepath in filepaths:
        if (current_time - os.path.getctime(filepath)) / 60.0 > threshold:
            os.remove(filepath)


def main(forecast_index, forecast_time):
    print time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    
    start_time  = time.time()
    postgres_functions.push_start_time(['road_conditions_zones_f'+forecast_time],start_time)
    
    grid_list = []
    truex, truey = data_handler.get_coordinate_grid('MRMS')

    crs = data_handler.get_any_data('HRRR:crs')
    transform = data_handler.get_any_data('HRRR:affine')
    crs = ast.literal_eval(crs)
    transform = Affine(*ast.literal_eval(transform)) # takes six values to produce a matrix

    desired_time = arrow.utcnow().replace(hours=+forecast_index+1)

    # Checks if HRRR with index below the total length minus the number of expected MRMS layers 
    for var_index,variable in enumerate(variable_indices):
        
        if var_index < len(variable_indices)-len(mrms_files):
            data = data_handler.get_layer(variable, desired_time, latest=False)
            
            if len(data.shape) > 2:
                data = data[0,:,:]
            
            flipped_data = np.array(np.flipud(data))
            master_grid = np.empty((master_grid_height,master_grid_width))
            master_grid.fill(-3)

            with rasterio.drivers():
                warp.reproject(flipped_data,
                    master_grid,
                    src_transform=transform,
                    src_crs=crs,
                    dst_transform=master_transform,
                    dst_crs=master_crs,
                    resampling=rasterio.warp.RESAMPLING.nearest)
            
            grid_list.append(master_grid)

        else:
            data = data_handler.get_layer(variable, 'latest', latest=True)

            grid_list.append(data[::down_factor,::down_factor])
    utcnow = arrow.utcnow()

    reordered = [None]*10
    reordered[0] = grid_list[0]
    reordered[1] = grid_list[1]
    reordered[2] = grid_list[2]
    reordered[3] = grid_list[3]
    reordered[4] = grid_list[4]
    reordered[5] = grid_list[5]
    reordered[6] = grid_list[6]

    reordered[7] = data_handler.get_layer('accumulation_3',desired_time,latest=False)
    reordered[8] = data_handler.get_layer('accumulation_6',desired_time,latest=False)
    reordered[9] = data_handler.get_layer('road_temperatures','latest')

    #reordered[10] = grid_list[7] #reflectivity

    #print (reordered[7] > reordered[8]).sum()

    for array_to_warp,array_index in zip(reordered[7:10],range(7,10)):
        flipped_data = np.array(np.flipud(array_to_warp))
        master_grid = np.empty((master_grid_height,master_grid_width))
        master_grid.fill(-3)

        with rasterio.drivers():
            warp.reproject(flipped_data,
                master_grid,
                src_transform=transform,
                src_crs=crs,
                dst_transform=master_transform,
                dst_crs=master_crs,
                resampling=rasterio.warp.RESAMPLING.nearest)
        reordered[array_index] = master_grid

    #print (reordered[7] > reordered[8]).sum()
    
    """
    forecast_float = float(forecast_index+2) # starts at 0, but first forecast time is 2h later
    forecast_float_grid = np.empty((3500,7000))
    forecast_float_grid.fill(forecast_float)
    grid_list.append(forecast_float_grid)
    """

    #np.save('/home/souellet/Downloads/input_forecast_ds',np.dstack(reordered))

    clean_past_arrays()
    np.save('../persistent/forecast_input_{0}'.format(desired_time),np.dstack(reordered))

    raw_array = np.dstack(reordered)

    frozen_reordered = [np_grid.copy() for np_grid in reordered]
    
    frozen_mask = np.logical_and(raw_array[:,:,5] <= 273.16,raw_array[:,:,5] > 0)
    frozen_precip_array = frozen_mask.astype(np.float64)+1

    #print 'Starts binning', time.gmtime()
    if bin_optimization:
        bins = []
        
        bins.append([-1,0,0.00006,2.501,7.501,500])
        #bins.append([-1,0,0.00006,1,2.5,5000])
        bins.append([-1,0,271.16,275.16,5000])
        bins.append([-1,0,250,500,750,5000])

        #bins.append([-1,0,0.00006,5,10,5000]) #precip3h
        bins.append([-1,0,0.00006,2.5,10,5000]) #precip3h
        #bins.append([-1,0,0.00001,7.5,15,5000]) #precip6h
        bins.append([-1,0,0.00001,5,15,5000]) #precip6h
        bins.append([-1,0,271.16,275.16,5000]) #roadtemp
        
        corresponding_values = []
        corresponding_values.append([-3,-3,0,1,3,8])
        #corresponding_values.append([-3,-3,0,0.5,3,8])
        corresponding_values.append([-3,-3,2,275,276])
        corresponding_values.append([-3,-3,2,251,501,751])

        #corresponding_values.append([-3,-3,0,1,6,11]) #precip3h
        corresponding_values.append([-3,-3,0,1,3,11]) #precip3h
        #corresponding_values.append([-3,-3,0,1,8,16]) #precip6h
        corresponding_values.append([-3,-3,0,1,6,16]) #precip6h
        corresponding_values.append([-3,-3,2,275,276]) #roadtemp

        for i in range(4,10):
            layer_bin = bins[i-4]
            new_layer = np.digitize(reordered[i],layer_bin).astype(np.float64)
            for j,value in enumerate(layer_bin):
                new_layer[new_layer == j] = corresponding_values[i-4][j]
            reordered[i] = new_layer


    #grid_list = reordered

    #print 'Finished binning', time.gmtime()
    
    master_array = np.dstack(reordered+[frozen_precip_array])

    #Copy for frozen
    if bin_optimization:
        bins = []
        
        #bins.append([-1,0,0.00006,2,500]) #replaced with the one below
        bins.append([-1,0,0.00006,1,5,5000])
        bins.append([-1,0,271.16,275.16,5000])
        bins.append([-1,0,250,500,750,5000])
        bins.append([-1,0,0.00006,2.5,7.5,5000]) #accum3h
        bins.append([-1,0,0.00001,5,10,20,5000]) #Precip6h
        bins.append([-1,0,271.16,275.16,5000]) #roadtemp, same
        
        corresponding_values = []
        #corresponding_values.append([-3,-3,0,1,3]) #replaced with the one below
        corresponding_values.append([-3,-3,0,0.5,3,6])
        
        corresponding_values.append([-3,-3,2,275,276])
        corresponding_values.append([-3,-3,2,251,501,751])
        corresponding_values.append([-3,-3,0,1,3,11]) #precip3h
        corresponding_values.append([-3,-3,0,1,6,12,22]) #precip6h
        corresponding_values.append([-3,-3,2,275,276]) #roadtemp

        for i in range(4,10):
            layer_bin = bins[i-4]
            new_layer = np.digitize(frozen_reordered[i],layer_bin).astype(np.float64)
            for j,value in enumerate(layer_bin):
                new_layer[new_layer == j] = corresponding_values[i-4][j]
            frozen_reordered[i] = new_layer    

    frozen_master_array = np.dstack(frozen_reordered+[frozen_precip_array])   

    data = data_handler.get_layer('HRRR:land', 'None', latest=False)

    flipped_data = np.array(np.flipud(data))
    master_grid = np.empty((master_grid_height,master_grid_width))
    master_grid.fill(-3)

    with rasterio.drivers():
        warp.reproject(flipped_data,
            master_grid,
            src_transform=transform,
            src_crs=crs,
            dst_transform=master_transform,
            dst_crs=master_crs,
            resampling=rasterio.warp.RESAMPLING.nearest)

    land = master_grid.astype(np.bool)
    #master_array[~land] = -3
    #indices = np.where(land[::down_factor,::down_factor])
    indices = np.where(land)

    #master_array = master_array.data # Only useful if source files translate to masked arrays

    #np.save('master_array',master_array)
    #np.save('indices', indices)
    #return
    #master_array = np.ma.masked_equal(master_array,-3)
    #print master_array[0,0]

    #master_array = master_array[::down_factor,::down_factor]
    #downsampled = master_array

    #print getBeliefValues([downsampled[90,180]])
    print 'Preprocessing completed', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    #result = np.apply_along_axis(getBeliefValues,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    result = compute_ds_array(master_array, frozen_master_array, frozen_mask, indices)

    data_handler.store_layer(result.astype(np.float32), 'ds_array_f{}'.format(forecast_time), arrow.utcnow(), time_alive=3600, latest=True)
    #np.save('/home/souellet/Downloads/forecast_ds',result)
    #return #TODO remove

    print 'Results calculated', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    
    polygons = polygonize(truex[::down_factor,::down_factor], truey[::down_factor,::down_factor],result)

    #forecast_timestamp = int(time.time()+60*60*(forecast_index+1))
    run_time = int(time.time())
    forecast_timestamp = int(forecast_index+1)
    


    postgres_functions.push_prob_zones_forecasted(polygons, forecast_timestamp, run_time)

    postgres_functions.push_prob_zones(polygons, extras={'timestamp':desired_time})
    
    postgres_functions.push_end_time(['road_conditions_zones_f'+forecast_time],start_time,time.time())

    print 'Polygons created', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    alarms_and_triggers.clean_cache('probability_zones_f'+str(forecast_timestamp))
    alarms_and_triggers.geoserver_trigger('probability_zones_f'+str(forecast_timestamp))

    alarms_and_triggers.clean_cache('all_prob_zones')
    alarms_and_triggers.geoserver_trigger('all_prob_zones')

    np.save('../persistent/forecast_output_{0}'.format(desired_time),result)

    if socket.gethostname() == 'ubuntu-cv1':
        pass
        #current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
    else:
        current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')
    
    current_socketIO.emit('assessed_roads_trigger', {'forecast': forecast_timestamp, 'time':int(time.time())})
    current_socketIO.disconnect()

    #client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #client_socket.connect(('localhost',8043))
    #client_socket.send(str(forecast_timestamp))
    #client_socket.close()

    live_feed_functions.remove_old_files()

    #postgres_functions.intersect_roads()
    #np.save('/Users/soullet/Downloads/dsresults',result)
    #print 'Roads intersected'
    #print time.gmtime()


def polygonize(x,y,array,icy_computed=None):
    #contours = pyplot.contourf(x,y,array,levels=[10.50,11.10,20.50,21.10,30.50,31.10],cmap='inferno')
    #contours = pyplot.contourf(x,y,array,levels=[10.20,11.10,20.20,21.10,30.20,31.10],cmap='inferno',antialiased=False)

    bins = [10,20,30]
    digitized = np.digitize(array,bins).astype(np.int32)
    mask = digitized > 0
    shapes = rasterio.features.shapes(digitized,mask=mask,transform=master_transform)
        
    polygons = [[],[],[]]
    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon) # wet, snowy, icy in order


    if buffer_factor > 0:
        wet = [poly.buffer(buffer_factor) for poly in polygons[0]]
        snowy = [poly.buffer(buffer_factor) for poly in polygons[1]]
        icy = [poly.buffer(buffer_factor) for poly in polygons[2]]

        if simplification > 0:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
        else:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor)
        
        if icy_computed is not None:
            better_icy = icy_computed

        better_wet = better_wet.difference(better_snowy)
        better_snowy = better_snowy.difference(better_icy)
        better_wet = better_wet.difference(better_icy)

    else:
        better_wet = geometry.MultiPolygon(polygons[0])
        better_snowy = geometry.MultiPolygon(polygons[1])
        better_icy = geometry.MultiPolygon(polygons[2])
        if icy_computed is not None:
            better_icy = icy_computed


    return [[better_wet,'W',0.8], [better_snowy,'S',0.8], [better_icy,'I',0.8]]


if __name__ == '__main__':

    for forecast_index,forecast_time in enumerate(forecast_times):
        try:
            main(forecast_index,forecast_time)
        except Exception as e:
            print e

    #postgres_functions.clean_forecasted_zones(int(time.time()))


    #test()
    #validation_run()
