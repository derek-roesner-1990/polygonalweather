import geotab_handler

if __name__ == '__main__':
    geotab_handler.add_zones_types()
    geotab_handler.add_nws_zone_types()
    geotab_handler.add_ds_zones_types()