import sys
import os
import time
import calendar
import subprocess
#import logging

def main(slow_kill = False):
    
    start_time = calendar.timegm(time.gmtime())
    how_long = int(sys.argv[1])
    subprocess_arguments = sys.argv[2:]
    if 'file_acquisition' in subprocess_arguments[1]:
        slow_kill = True
    #log_filename='../../log/scheduler_{}.log'.format(subprocess_arguments[1])
    #logging.basicConfig(filename=log_filename)
    processes = []
 
    while True:
        if not slow_kill:
            for process in processes:
                if process.poll() is None:
                    #logging.warning(str(time.time())+' '+str(subprocess_arguments)+' process took too long')
                    print str(time.time())+' '+str(subprocess_arguments)+' process took too long'
                    process.kill()
            processes[:] = []

        else:
            counter = 0
            too_long = False
            while not too_long and len(processes) > 0:
                process = processes[-1]
                status = process.poll()

                if counter > 180:
                    process.kill()
                    too_long = True
                    processes[:] = []

                elif status is None:
                    time.sleep(how_long)
                    counter += how_long
                
                else:
                    processes[:] = []

        current_time = calendar.timegm(time.gmtime())
        print subprocess_arguments, how_long
        processes.append(subprocess.Popen(subprocess_arguments))
        end_time = calendar.timegm(time.gmtime())
        pause_length = how_long-(end_time-current_time)
        if pause_length > 0:
            time.sleep(pause_length)

if __name__ == '__main__':
    main()
