import time
import calendar
import datetime
import sys

import pymysql
import numpy as np
import yaml
import postgres_functions

def identify_errors(record):
    """ Looks for values that sensors report when errors occur """
    if record['observation']['atmosphere']['temperature'] < -0.005+273.15 and record['observation']['atmosphere']['temperature'] > -0.015+273.15:
        record['observation']['atmosphere']['temperature'] = np.nan

    if record['observation']['road']['temperatures'][0] < 0.005+273.15 and record['observation']['road']['temperatures'][0] > -0.021+273.15:
        record['observation']['road']['temperatures'][0] = np.nan

    if record['observation']['atmosphere']['relativeHumidity'] > 100.0:
        record['observation']['atmosphere']['relativeHumidity'] = np.nan

    if record['observation']['atmosphere']['stationPressure'] > 1100*100:
        record['observation']['atmosphere']['stationPressure'] = np.nan

    if record['observation']['atmosphere']['stationPressure'] < 600*100:
        record['observation']['atmosphere']['stationPressure'] = np.nan

    if record['position']['elevation'] < -0.09 and record['position']['elevation'] > -0.21:
        record['position']['elevation'] = np.nan

    if record['observation']['radiation']['lightLevel'] > 156000:
        record['observation']['radiation']['lightLevel'] = np.nan

    if record['observation']['radiation']['lightLevel'] < 0.01:
        record['observation']['radiation']['lightLevel'] = np.nan

    if record['observation']['atmosphere']['ozone'] > 253:
        record['observation']['atmosphere']['ozone'] = np.nan

    if record['observation']['precipitation']['intensity'] > 250:
        record['observation']['precipitation']['intensity'] = np.nan

    return record

def extract_weather_telematics_data(length = 100000):
    """ Gets a start and end date from the command line and runs a SQL query to the WTX database """ 
    start_date = "'2016-05-06 00:00:00'" # use this as they started recording in 2010: '2010-01-01'
    #end_date = "'2016-06-10 13:00:00'" # Use something far away: '2025-12-01'
    
    wtx_db_config = {}
    with open('../../wtx_sql_db_config.yml') as config_file:
        wtx_db_config = yaml.load(config_file)

    connection = pymysql.connect(**wtx_db_config)
    variables = ['id', 'unit_id', 'latitude', 'longitude', 'record_time', 'road_surface_temp', 'air_temp', 'rain_intensity', 'relative_humidity', 'light_level', 'pressure', 'ozone', 'gps_elevation', 'gps_speed']

    print time.gmtime()
    with connection.cursor() as cursor:
        
        result = [0]
        flags = 0
        while True:
            start_object = datetime.datetime.strptime(start_date,"'%Y-%m-%d %H:%M:%S'")
            start_object = start_object + datetime.timedelta(0,1)
            start_date = start_object.strftime("'%Y-%m-%d %H:%M:%S'")
            end_object = start_object + datetime.timedelta(0,3599)
            end_date =  end_object.strftime("'%Y-%m-%d %H:%M:%S'")
            
            #sql = "SELECT tr.id, tr.unit_id, CAST(tr.latitude/100000 AS DECIMAL(10,5)) as latitude, CAST(tr.longitude/100000 AS DECIMAL(10,5)) as longitude, tr.record_time,  RST.value AS road_surface_temp  FROM   tracking_records tr INNER JOIN  tracking_record_field_data RST ON RST.tracking_record_id = tr.id AND RST.field_id = 208  WHERE tr.record_time BETWEEN "+ start_date +" AND "+ end_date +" AND tr.latitude <> 0 AND tr.longitude <> 0 LIMIT "+str(length)

            sql = "SELECT tr.id, tr.unit_id, CAST(tr.latitude/100000 AS DECIMAL(10,5)) as latitude, CAST(tr.longitude/100000 AS DECIMAL(10,5)) as longitude, tr.record_time, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 208) road_surface_temp,  (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 209) air_temp, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 210) rain_intensity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 211) relative_humidity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10100) light_level, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10101) pressure, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10102) ozone, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10104) gps_elevation, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10106) gps_speed FROM tracking_records tr WHERE tr.record_time BETWEEN "+ start_date +" AND "+ end_date +" AND tr.latitude <> 0 AND tr.longitude <> 0 LIMIT "+str(length)

            # Just road temps
            #sql = "SELECT tr.id, tr.unit_id, CAST(tr.latitude/100000 AS DECIMAL(10,5)) as latitude, CAST(tr.longitude/100000 AS DECIMAL(10,5)) as longitude, tr.record_time, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 208) road_surface_temp FROM tracking_records tr WHERE tr.record_time BETWEEN "+ start_date +" AND "+ end_date +" AND tr.latitude <> 0 AND tr.longitude <> 0 LIMIT "+str(length)


            #sql = "SELECT tr.id, tr.unit_id, CAST(tr.latitude/100000 AS DECIMAL(10,5)) as latitude, CAST(tr.longitude/100000 AS DECIMAL(10,5)) as longitude, tr.record_time, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 208) road_surface_temp,  (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 209) air_temp, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 210) rain_intensity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 211) relative_humidity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10100) light_level, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10101) pressure, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10102) ozone, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10104) gps_elevation, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10106) gps_speed FROM tracking_records tr WHERE tr.record_time BETWEEN '2013-12-01' AND '2014-01-01' AND tr.latitude BETWEEN 4060000 AND 4350000 AND tr.longitude BETWEEN -9651670 AND -8908333 LIMIT "+str(length)+" OFFSET "

            #sql = "SELECT tr.id, tr.unit_id, CAST(tr.latitude/100000 AS DECIMAL(10,5)) as latitude, CAST(tr.longitude/100000 AS DECIMAL(10,5)) as longitude, tr.record_time, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 208) road_surface_temp,  (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 209) air_temp, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 210) rain_intensity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 211) relative_humidity, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10100) light_level, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10101) pressure, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10102) ozone, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10104) gps_elevation, (SELECT value FROM tracking_record_field_data fd WHERE fd.tracking_record_id = tr.id AND field_id = 10106) gps_speed FROM tracking_records tr WHERE tr.record_time BETWEEN %s AND %s AND tr.latitude <> 0 AND tr.longitude <> 0 LIMIT %s OFFSET %s"

            #cursor.execute(sql,(start_date,end_date,length,offset))
            
            cursor.execute(sql)

            result = cursor.fetchall()

            if len(result) < 1:
                print 'Query completed, start_date', start_date
                if (datetime.datetime.utcnow()-start_object).total_seconds < 3600:
                    time.sleep(30)
                    continue
                else:
                    start_date = end_date
                    continue

            last_reported_time = result[-1][4]
            print last_reported_time
            start_date = last_reported_time.strftime("'%Y-%m-%d %H:%M:%S'")

            resultarray = np.array(result)
            print resultarray.shape
            postgres_functions.push_wtx_data(resultarray)

            #offset += len(result)
            #print offset
            print time.gmtime()
            time.sleep(10)



            
    connection.close()

    #print offset

if __name__ == '__main__':
    extract_weather_telematics_data()
