import ast
import time

import arrow
import psycopg2

from notification_tools import send_email

with open('credentials.txt','r') as opened:
    credentials = ast.literal_eval(opened.read())
    
def main():
    contour_to_compare = [(1,1)]*2
    risk_index_comparison = [(1,1)]*2

    while True:
        cursor = psycopg2.connect(**credentials).cursor()

        cursor.execute("select minimum_value from road_temperature_contours order by minimum_value;")
        road_temps = cursor.fetchall()
        previous = road_temps[0][0]
        for road_temp in road_temps:
            if (road_temp[0] - previous) > 5:
                print road_temp[0], previous
                send_email('road_temperature_contours')
                break
            previous = road_temp[0]

        cursor.execute("select ST_AsText(geom) from polygon_contours order by id;")
        contour_to_compare.append(cursor.fetchall())
        contour_to_compare.pop(0)
        if len(contour_to_compare[0]) == len(contour_to_compare[1]):
            contour_count = 0
            for contour,new_contour in zip(contour_to_compare[-2],contour_to_compare[-1]):
                if contour == new_contour:
                    contour_count += 1
            if contour_count == len(contour_to_compare[0]):
                send_email('polygon_contours')

        cursor.execute("select ST_AsText(geom) from risk_polygons order by id;")
        risk_index_comparison.append(cursor.fetchall())
        risk_index_comparison.pop(0)
        if len(risk_index_comparison[0]) == len(risk_index_comparison[1]):
            contour_count = 0
            for contour,new_contour in zip(risk_index_comparison[-2],risk_index_comparison[-1]):
                if contour == new_contour:
                    contour_count += 1
            if contour_count == len(risk_index_comparison[0]):
                send_email('risk_index_count')

        cursor.connection.close()
        print 'Sleeping'
        time.sleep(1200)

if __name__ == '__main__':
    main()
