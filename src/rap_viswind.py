"""

Transforms gridded datasets into thresholded geolocated polygons.
Many variables reside in parameters.py

"""
import socket
import sys
import time
import calendar
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import colors
from scipy import ndimage
from scipy import misc
import fiona
import shapely
from shapely import wkb
from shapely import ops
import geojson
import json
import ast
import os
import socketIO_client
import toolz
import arrow

import data_handler
import postgres_functions

from rap_parameters import *

if using_geotab:
    import geotab_functions

import live_feed_functions
from live_feed_functions import hrrproducts, mrmsproducts, remove_old_files
from contour_functions import polygonize

def get_data(variable_index, forecasted):
    if forecasted == 'current':
        arrow_time = arrow.utcnow()
    else:
        arrow_time = arrow.utcnow().replace(hours=+forecasted)

    if not data_handler.existence_check(variable_index+':latest'):
        source = 'HRRR'
        data = data_handler.get_layer(variable_index, arrow_time, latest=False)
    else:
        source = 'MRMS'
        data = data_handler.get_layer(variable_index, 'latest')

    if len(data.shape) > 2: # Removing higher levels, the first one is generally the closest to the surface
        data = data[0,:,:]

    return data, source, arrow_time

def process_file(variable_index, geolayers, mongo_client = None, observations = None, cameras = None, entities= None, forecasted = 'current'):
    """ Opens and selects desired variables, returns records of polygons with relevant info """
    data,source,arrow_time = get_data(variable_index, forecasted)

    x,y = data_handler.get_coordinate_grid(source)

    variable_name = variable_name_index_dict[variable_index]
    #data[data == -3] = 0

    #data = ndimage.interpolation.zoom(data, 0.10, order=0)
    #data = ndimage.morphology.grey_closing(data, size=(5,5))
    
    gaussian_sigma = 0.3

    data = ndimage.gaussian_filter(data,sigma=gaussian_sigma) #TODO Have it automatically computed based on grid resolution
    #data = ndimage.median_filter(data,(5,5))
    print 'Creating polygons for', variable_name

    current_key = variable_name
    thresholds = threshold_map[current_key]

    # Takes care of whether a high value represents a high risk or the inverse
    inverted = False
    if current_key in inverse_variables:
        inverted = True

    levels, thresholds = polygonize(x,y,data,which_variable=current_key,thresholds=thresholds, inverted=inverted)

    #multipolygons = []
    #outputs = []
    records = []

    for i, level in enumerate(levels):
        if current_key == 'seamless':
            exteriors = []
            for geom in level:
                exteriors.append(shapely.geometry.Polygon(geom.exterior))
            multipolygon = ops.unary_union(exteriors)
            multipolygon = multipolygon.simplify(0.001, preserve_topology=True)
        else:
            multipolygon = ops.unary_union(level)
        if multipolygon.area == 0:
            continue
        if current_key == 'qpe':
            
            with open('landpolygon/northamerica.txt','r') as opened:
                northamerica = wkb.loads(opened.read(),hex=True)

            multipolygon = multipolygon.intersection(northamerica)
        
        if not multipolygon.area > 0:
            print geojson.dumps(multipolygon)
            continue

        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if current_key in inverse_variables:
            if i == 0:
                geolayer_dict['thresholdValues'] = [-9999999.0, thresholds[i]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i-1],thresholds[i]]
        else:
            if i < (len(thresholds)-1):
                geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i],9999999.0]

        geolayer_dict['colour'] = color_map[current_key][value_to_string_map[current_key][thresholds[i]]]
        if forecasted != 'current':
            geolayer_dict['variableName'] = current_key+'_f0{0}'.format(forecasted)
        else:
            geolayer_dict['variableName'] = current_key

        if current_key == 'seamless':
            riskLevel = threshold_map['seamless'].index(thresholds[i])
        else:
            riskLevel = risk_map[value_to_string_map[current_key][thresholds[i]]]
        '''
        if current_key in inverse_variables:
            riskLevel *= -1
            riskLevel += 4
        '''
        new_geolayer_dict = dict()

        new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
        new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel}
        new_geolayer_dict['properties']['shape'] = multipolygon
        new_geolayer_dict['properties']['risk_name'] = value_to_string_map[current_key][thresholds[i]]
        new_geolayer_dict['properties']['valid_time'] = arrow_time
        if forecasted != 'current':
            new_geolayer_dict['properties']['forecast_time'] = forecasted

        records.append(new_geolayer_dict)

        if mongo_client is not None:
            mongodb_functions.process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)
            mongodb_functions.process_cameras(mongo_client, cameras, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel)

        # Adds to entities
        if using_geotab:
            geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, current_key)

    geolayers.extend(records)

def main():
    """ Downloads files and push geojson polygons """
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    #start_time = time.time()
    start_time = arrow.utcnow().datetime
    #postgres_functions.push_start_time(process_name,start_time)

    mongo_client = None
    observations = None
    geolayers = []
    forecasted_geolayers = []
    entities = []
    
    cameras = None
    if using_mongo:
        mongo_client, observations = mongodb_functions.prepare_mongo()
        cameras = mongodb_functions.get_cameras(mongo_client)

    for variable_index in variable_indices:
        process_file(variable_index, geolayers, mongo_client, observations, cameras, entities=entities)

    for forecasted_time in forecasted_times:
        print 'Forecast: ', forecasted_time
        for variable_index in forecasted_variable_indices:
            process_file(variable_index, forecasted_geolayers, mongo_client, observations, cameras, entities=entities, forecasted=forecasted_time)        

    if mongo_client is not None:
        mongodb_functions.push_to_mongo(mongo_client, [toolz.update_in(geolayer, ['properties','shape'], str) for geolayer in geolayers])
        mongodb_functions.push_devices_to_mongo(mongo_client, observations)
        mongodb_functions.push_cameras_to_mongo(mongo_client, cameras)

    if using_postgres:
        postgres_functions.push_rap_contours(geolayers,process_name, start_time)
        postgres_functions.push_rap_contours(forecasted_geolayers,process_name, start_time)
        #postgres_functions.push_forecasted_contours(forecasted_geolayers,process_name,start_time)

    #end_time = time.time()
    end_time = arrow.utcnow().datetime
    #print 'Updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    #postgres_functions.push_end_time(process_name,start_time,end_time)
    #print 'Done updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    remove_old_files()

    try:
        if socket.gethostname() == 'ubuntu-cv1':
            #current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
            pass
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')

        current_socketIO.emit('contours_trigger', time.time())
        current_socketIO.emit('hrrr_trigger', time.time())
        current_socketIO.disconnect()
    
    except Exception as e:
        print e
    
    if using_geotab:
        geotab_functions.delete_old_zones()
        print 'Deleted', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        cleaned_up = geotab_functions.deal_with_holes(entities)
        geotab_functions.push_to_geotab(cleaned_up)

    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()