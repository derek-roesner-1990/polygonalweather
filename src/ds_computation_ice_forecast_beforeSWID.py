
# coding: utf-8

# In[48]:

from __future__ import print_function
from pyds import MassFunction
from itertools import product
import math


# In[2]:

'''
Global variable names. These hold the variables hold the initial beliefs.
Setting up the initial beliefs will be through the initMasses function.
Work on this after wards....TBD! Add a learning component to update beliefs
based on validation from "teachers"
radiationMasses
precipFlagMasses
'''


# In[27]:

'''
Function for handling the different ranges
of radiation
HRRR
'''
def getRadiationId(radiation):
    if (radiation >= 0 and radiation <= 250):
        return 0
    elif (radiation > 250 and radiation <= 500):
        return 1
    elif (radiation > 500 and radiation <= 750):
        return 2
    elif (radiation > 750 and radiation <= 1500):
        return 3
    elif radiation == -3:
        return 4
    else:
        return -1
    
radiationMasses = []
radiationMasses.append(MassFunction({"S":0.2, "W":0.2, "I":0.2, "D":0.05, "SW":0.1, "SI":0.1, "WI":0.1, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.175, "W":0.175, "I":0.175, "D":0.125, "SW":0.1, "SI":0.1, "WI":0.1, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.15, "W":0.15, "I":0.15, "D":0.275, "SW":0.075, "SI":0.075, "WI":0.075, "SWID":0.05}))
radiationMasses.append(MassFunction({"S":0.1, "W":0.1, "I":0.1, "D":0.5, "SW":0.05, "SI":0.05, "WI":0.05,  "SWID":0.05}))
radiationMasses.append(MassFunction({       "SWID":1}))


# In[28]:

'''
Function for handling the 
different precip flags
Flag	Description
-3	no coverage
0	no precipitation
1	warm stratiform rain
2	warm stratiform rain
3	snow
4	snow
5	reserved for future use
6	convective rain
7	rain mixed with hail
8	reserved for future use
9	flag no longer used
10	cold stratiform rain
91	tropical/stratiform rain mix
96	tropical/convective rain mix

MRMS
'''

def getPrecipFlagId(precipFlag):
    if (precipFlag in [0, 5, 9]):
        return 0
    elif (precipFlag in [1,2,7,10,91]):
        return 1
    elif (precipFlag in [6,96]):
        return 2
    elif (precipFlag in [3,4]):
        return 3
    elif (precipFlag == -3):
        return 4
    else:
        return -1
        
precipFlagMasses = [] 
precipFlagMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
precipFlagMasses.append(MassFunction({"S":0.1, "W":0.65,   "SW":0.25}))
precipFlagMasses.append(MassFunction({"S":0.05, "W":0.9,   "SW":0.05}))
precipFlagMasses.append(MassFunction({"S":0.55, "W":0.05, "I":0.1,  "SW":0.1, "SI":0.1, "WI":0.1}))
precipFlagMasses.append(MassFunction({       "SWID":1}))


# In[29]:

'''
Function for handling the different ranges
of liquid (rain) precipitation rates mm/hr

MRMS
'''
def getPrecipRateLiquidId(precipRateLiquid):
    if (precipRateLiquid == 0):
        return 0
    elif (precipRateLiquid > 0.00001 and precipRateLiquid <= 2.5):
        return 1
    elif (precipRateLiquid > 2.5 and precipRateLiquid <= 500):
        return 2
    elif (precipRateLiquid == -3):
        return 3
    else:
        return -1
        
precipRateLiquidMasses = []
precipRateLiquidMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipRateLiquidMasses.append(MassFunction({"S":0.2, "W":0.7,   "SW":0.1}))
precipRateLiquidMasses.append(MassFunction({"S":0.1, "W":0.8,   "SW":0.1}))
precipRateLiquidMasses.append(MassFunction({       "SWID":1}))


# In[30]:

'''
Function for handling the different ranges
of liquid precip accumulated over the past 6 hours mm

MRMS
'''
def getPrecipAccumLiquidId(precipAccumLiquid): # 6 hours
    if (precipAccumLiquid == 0):
        return 0
    elif (precipAccumLiquid > 0.00001 and precipAccumLiquid <= 5):
        return 1
    elif (precipAccumLiquid > 5 and precipAccumLiquid <= 10):
        return 2
    elif (precipAccumLiquid > 10 and precipAccumLiquid <= 500):
        return 3
    elif (precipAccumLiquid == -3): # MISSING VALUE
        return 4
    else:
        return -1
        
precipAccumLiquidMasses = []
precipAccumLiquidMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccumLiquidMasses.append(MassFunction({"S":0.2, "W":0.4,  "D":0.1, "SW":0.25,    "SWID":0.05}))
precipAccumLiquidMasses.append(MassFunction({"S":0.1, "W":0.75,   "SW":0.15}))
precipAccumLiquidMasses.append(MassFunction({"S":0.05, "W":0.9,   "SW":0.05}))
precipAccumLiquidMasses.append(MassFunction({       "SWID":1}))


# In[31]:

'''
Function for handling the different ranges
of liquid precip accumulated over the past 1 hour mm

MRMS
'''
def getPrecipAccum1LiquidId(precipAccum1Liquid):
    if (precipAccum1Liquid == 0):
        return 0
    elif (precipAccum1Liquid > 0.00006 and precipAccum1Liquid <= 2):
        return 1
    elif (precipAccum1Liquid > 2 and precipAccum1Liquid <= 500):
        return 2
    elif (precipAccum1Liquid == -3):
        return 3
    else:
        return -1

        
precipAccum1LiquidMasses = []
precipAccum1LiquidMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccum1LiquidMasses.append(MassFunction({"S":0.15, "W":0.5,  "D":0.1, "SW":0.2,   "SWID":0.05}))
precipAccum1LiquidMasses.append(MassFunction({"S":0.05, "W":0.8,   "SW":0.15}))
precipAccum1LiquidMasses.append(MassFunction({       "SWID":1}))


# In[32]:

'''
Function for handling the different ranges
of frozen (snow) precip rate mm/hr

MRMS
'''
def getPrecipRateFrozenId(precipRateFrozen):
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 2):
        return 1
    elif (precipRateFrozen > 2 and precipRateFrozen <= 500):
        return 2
    elif (precipRateFrozen == -3):
        return 3
    else:
        return -1
        
precipRateFrozenMasses = []
precipRateFrozenMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipRateFrozenMasses.append(MassFunction({"S":0.6, "W":0.3,   "SW":0.1}))
precipRateFrozenMasses.append(MassFunction({"S":0.9, "W":0.05,   "SW":0.05}))
precipRateFrozenMasses.append(MassFunction({       "SWID":1}))


# In[33]:

'''
Function for handling the different ranges
of frozen precip aummulated over the past 6 hours mm

MRMS
'''
def getPrecipAccumFrozenId(precipAccumFrozen): # 6 hours
    if (precipAccumFrozen == 0):
        return 0
    elif (precipAccumFrozen > 0.00001 and precipAccumFrozen <= 2.5):
        return 1
    elif (precipAccumFrozen > 2.5 and precipAccumFrozen <= 7.5):
        return 2
    elif (precipAccumFrozen > 7.5 and precipAccumFrozen <= 500):
        return 3
    elif (precipAccumFrozen == -3):
        return 4
    else:
        return -1
        
precipAccumFrozenMasses = []
precipAccumFrozenMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccumFrozenMasses.append(MassFunction({"S":0.3, "W":0.25,  "D":0.1, "SW":0.25,   "SWID":0.1}))
precipAccumFrozenMasses.append(MassFunction({"S":0.65, "W":0.1,   "SW":0.25}))
precipAccumFrozenMasses.append(MassFunction({"S":0.9, "W":0.05,   "SW":0.05}))
precipAccumFrozenMasses.append(MassFunction({       "SWID":1}))


# In[34]:

'''
Function for handling the different ranges
of frozen precipn accumulated over the past 1 hour mm

MRMS
'''
def getPrecipAccum1FrozenId(precipAccum1Frozen):
    if (precipAccum1Frozen == 0):
        return 0
    elif (precipAccum1Frozen > 0.00006 and precipAccum1Frozen <= 2):
        return 1
    elif (precipAccum1Frozen > 2 and precipAccum1Frozen <= 500):
        return 2
    elif (precipAccum1Frozen == -3):
        return 3
    else:
        return -1
        
precipAccum1FrozenMasses = []
precipAccum1FrozenMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccum1FrozenMasses.append(MassFunction({"S":0.6, "W":0.05,  "D":0.1, "SW":0.2,   "SWID":0.05}))
precipAccum1FrozenMasses.append(MassFunction({"S":0.8, "W":0.05,   "SW":0.15}))
precipAccum1FrozenMasses.append(MassFunction({       "SWID":1}))


# In[35]:

'''
Function for handling the different ranges
of air temperature Kelvins

HRRR
'''
def getAirTempId(airTemp):
    if (math.isnan(airTemp)):
        return 3
    elif (airTemp > 275.16):
        return 0
    elif (airTemp >= 271.16 and airTemp <= 275.16):
        return 1
    elif (airTemp < 271.16 and airTemp >= 0):
        return 2
    elif (airTemp == -3):
        return 3
    else:
        return -1
        
airTempMasses = []
airTempMasses.append(MassFunction({ "W":0.25,  "D":0.25,     "SWID":0.5}))
airTempMasses.append(MassFunction({"S":0.0625, "W":0.0625,  "D":0.25, "SW":0.125,   "SWID":0.5}))
airTempMasses.append(MassFunction({"S":0.25,   "D":0.25,    "SWID":0.5}))
airTempMasses.append(MassFunction({       "SWID":1}))


# In[36]:

'''
assume vector has following format:
(precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr, catSnow, catRain, catFrz, catIcePellets)
'''
def getBeliefValues(weatherConditions): 
    precipType = 1 # 1=Liquid, 2=Frozen
    
    if (weatherConditions[0] in [3,4] or (weatherConditions[3] <= 275.16 and weatherConditions[3] > 0)): # see if precipType should be frozen
        precipType = 2

    roadState = 0

    if (precipType == 1):
        roadState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]         & precipRateLiquidMasses[getPrecipRateLiquidId(weatherConditions[1])]         & precipAccumLiquidMasses[getPrecipAccumLiquidId(weatherConditions[2])]         & precipAccum1LiquidMasses[getPrecipAccum1LiquidId(weatherConditions[5])]         & airTempMasses[getAirTempId(weatherConditions[3])]         & radiationMasses[getRadiationId(weatherConditions[4])]         & categoricalSnowMasses[getCategoricalSnow(weatherConditions[6])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[7])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[8])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[9])]        
    else:
        roadState = precipFlagMasses[getPrecipFlagId(weatherConditions[0])]         & precipRateFrozenMasses[getPrecipRateFrozenId(weatherConditions[1])]         & precipAccumFrozenMasses[getPrecipAccumFrozenId(weatherConditions[2])]         & precipAccum1FrozenMasses[getPrecipAccum1FrozenId(weatherConditions[5])]         & airTempMasses[getAirTempId(weatherConditions[3])]         & radiationMasses[getRadiationId(weatherConditions[4])]         & categoricalSnowMasses[getCategoricalSnow(weatherConditions[6])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[7])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[8])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[9])]
                        
    if False:
        return roadState    
    else:
        string_state = str(roadState)
        #print(string_state)
        if string_state.startswith("{set(['S',"):
            return -3.0
        else:    
            result = roadState.max_bel()
            adjustment = 0
            string_result = string_state[7]

            if string_result == 'I':
                adjustment = 30
            elif string_result == 'S':
                adjustment = 20
            elif string_result == 'W':
                adjustment = 10

            return roadState[result]+adjustment

# # Forecast Functions 

# ## Precipitation type

# In[37]:

def getCategoricalSnow(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalSnowMasses = []
categoricalSnowMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalSnowMasses.append(MassFunction({"S":0.7,    "SW":0.15, "SI":0.15}))

def getCategoricalFrz(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalFrzMasses = []
categoricalFrzMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalFrzMasses.append(MassFunction({  "I":0.7,   "SI":0.15, "WI":0.15}))

def getCategoricalIce(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalIceMasses = []
categoricalIceMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
categoricalIceMasses.append(MassFunction({"S":0., "W":0.35, "I":0.35,   "SI":0.15, "WI":0.3}))

def getCategoricalRain(precipType):
    if precipType == 0:
        return 0
    elif precipType == 1:
        return 1
    elif precipType == -3:
        return 0
    else:
        return -1
    
categoricalRainMasses = []
categoricalRainMasses.append(MassFunction({   "D":0.5,    "SWID":0.5}))
#categoricalRainMasses.append(MassFunction({ "W":0.7,   "SW":0.15,  "WI":0.15}))
categoricalRainMasses.append(MassFunction({ "W":0.35,   "D":0.35, "SW":0.15,  "WI":0.15}))


# ## Precipitation accumulation

# In[49]:

def getTotalLiquidPrecipAccumTotal(totalAccum, time): # note kg/m^2 is the same as mm; time is in hours
    precipRateLiquid = totalAccum/time
    if (precipRateLiquid == 0):
        return 0
    elif (precipRateLiquid > 0.00001 and precipRateLiquid <= 2.5):
        return 1
    elif (precipRateLiquid > 2.5 and precipRateLiquid <= 500):
        return 2
    elif (precipRateLiquid == -3):
        return 3
    else:
        return -1
        
totalLiquidPrecipAccumMasses = []
totalLiquidPrecipAccumMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
totalLiquidPrecipAccumMasses.append(MassFunction({"S":0.2, "W":0.7,   "SW":0.1}))
totalLiquidPrecipAccumMasses.append(MassFunction({"S":0.1, "W":0.8,   "SW":0.1}))
totalLiquidPrecipAccumMasses.append(MassFunction({       "SWID":1}))


def getTotalFrozenPrecipAccumTotal(totalAccum, time):
    precipRateFrozen = totalAccum/time
    if (precipRateFrozen == 0):
        return 0
    elif (precipRateFrozen > 0.00001 and precipRateFrozen <= 2):
        return 1
    elif (precipRateFrozen > 2 and precipRateFrozen <= 500):
        return 2
    elif (precipRateFrozen == -3):
        return 3
    else:
        return -1
        
totalFrozenPrecipAccumMasses = []
totalFrozenPrecipAccumMasses.append(MassFunction({   "D":0.9,    "SWID":0.1}))
totalFrozenPrecipAccumMasses.append(MassFunction({"S":0.6, "W":0.3,   "SW":0.1}))
totalFrozenPrecipAccumMasses.append(MassFunction({"S":0.9, "W":0.05,   "SW":0.05}))
totalFrozenPrecipAccumMasses.append(MassFunction({       "SWID":1}))
    


# In[39]:

'''
HRRR one hour precip accumulation. 
Figured I'd write a separate function for it
Exactly the same beliefs as the MRMS ones
'''
def getPrecipAccum1LiquidHRRR(precipAccum1Liquid):
    if (precipAccum1Liquid == 0):
        return 0
    elif (precipAccum1Liquid > 0.00006 and precipAccum1Liquid <= 2):
        return 1
    elif (precipAccum1Liquid > 2 and precipAccum1Liquid <= 500):
        return 2
    elif (precipAccum1Liquid == -3):
        return 3
    else:
        return -1

        
precipAccum1LiquidMassesHRRR = []
precipAccum1LiquidMassesHRRR.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccum1LiquidMassesHRRR.append(MassFunction({"S":0.15, "W":0.5,  "D":0.1, "SW":0.2,   "SWID":0.05}))
precipAccum1LiquidMassesHRRR.append(MassFunction({"S":0.05, "W":0.8,   "SW":0.15}))
precipAccum1LiquidMassesHRRR.append(MassFunction({       "SWID":1}))


def getPrecipAccum1FrozenHRRR(precipAccum1Frozen):
    if (precipAccum1Frozen == 0):
        return 0
    elif (precipAccum1Frozen > 0.00006 and precipAccum1Frozen <= 2):
        return 1
    elif (precipAccum1Frozen > 2 and precipAccum1Frozen <= 500):
        return 2
    elif (precipAccum1Frozen == -3):
        return 3
    else:
        return -1
        
precipAccum1FrozenMassesHRRR = []
precipAccum1FrozenMassesHRRR.append(MassFunction({   "D":0.9,    "SWID":0.1}))
precipAccum1FrozenMassesHRRR.append(MassFunction({"S":0.6, "W":0.05,  "D":0.1, "SW":0.2,   "SWID":0.05}))
precipAccum1FrozenMassesHRRR.append(MassFunction({"S":0.8, "W":0.05,   "SW":0.15}))
precipAccum1FrozenMassesHRRR.append(MassFunction({       "SWID":1}))


# ## The belief function for predicting road state

# In[50]:

'''
assume vector has following format:
(catSnow, catRain, catFrz, catIcePellets, precip1HAccum, airtemp, radiation)
precipAccumHH -> APCP_P8_L1_GLC0_accHHh (precip accumulated till forecastTime)
'''
def getForecastBeliefValues(weatherConditions): 
    precipType = 1 # 1=Liquid, 2=Frozen
    
    if ((weatherConditions[5] <= 275.16 and weatherConditions[5] > 0)): # see if precipType should be frozen
        precipType = 2

    roadState = 0

    if (precipType == 1):
        roadState = categoricalSnowMasses[getCategoricalSnow(weatherConditions[0])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[1])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[2])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[3])]         & precipAccum1LiquidMassesHRRR[getPrecipAccum1LiquidHRRR(weatherConditions[4])]         & airTempMasses[getAirTempId(weatherConditions[5])] &    radiationMasses[getRadiationId(weatherConditions[6])]    
    else:
        roadState = categoricalSnowMasses[getCategoricalSnow(weatherConditions[0])]         & categoricalRainMasses[getCategoricalRain(weatherConditions[1])]         & categoricalFrzMasses[getCategoricalFrz(weatherConditions[2])]         & categoricalIceMasses[getCategoricalIce(weatherConditions[3])]         & precipAccum1FrozenMassesHRRR[getPrecipAccum1FrozenHRRR(weatherConditions[4])]         & airTempMasses[getAirTempId(weatherConditions[5])] &   radiationMasses[getRadiationId(weatherConditions[6])]    

    if False:
        return roadState    
    else:
        string_state = str(roadState)
        #print(weatherConditions)
        #print(string_state)
        #if string_state.startswith("{set(['S',"):
        if string_state[9] == ',':
            return -3.0
        else:    
            result = roadState.max_bel()
            adjustment = 0
            string_result = string_state[7]

            if string_result == 'I':
                adjustment = 30
            elif string_result == 'S':
                adjustment = 20
            elif string_result == 'W':
                adjustment = 10

            return roadState[result]+adjustment

# In[ ]:



getBeliefValues([3, 0, -3, -3, -3, 0, 0, 0, 1,1])
# In[55]:
"""
#sampleWeather = [3, 0, -3, -3, -3, 0, 0, 0, 1, 0]
sampleWeather = [0, 0, 0, 0, 0, 250, 2, 5]
#sampleWeather2 = [2, .5, -3, 280, 350, 0]
testMe = getForecastBeliefValues(sampleWeather)
print(testMe)
"""

# In[ ]:



