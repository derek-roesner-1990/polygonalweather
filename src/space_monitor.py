import ast
import time
import subprocess
import socket

import arrow
from raven import Client

from notification_tools import send_email

with open('sentry_credentials.txt', 'r') as opened:
    sentry_credentials = opened.read()
sentry = Client(sentry_credentials)

def main():
    while True:
        output = subprocess.check_output(['df']).split('\n')
        for line in output:
            words = line.split(' ')
            words = [word for word in words if word != '']
            #print words
            if words[0] == '/dev/sda1':
                #print words
                amount = int(words[-3])/1024.0
                break

        if amount < 4000:
            send_email('Insufficient space warning', 'Below 4 GB on {}'.format(socket.gethostname()))
            sentry.captureMessage('Insufficient space warning - below 4 GB on {} - {}'.format(socket.gethostname(),arrow.utcnow()))
        
        time.sleep(1200)

if __name__ == '__main__':
    main()