#mrms_files = ['PrecipRate/', 'PrecipFlag/', 'GaugeCorr_QPE_01H/', 'GaugeCorr_QPE_06H/']
#mrms_files = ['PrecipRate/', 'PrecipFlag/', 'RadarOnly_QPE_01H/', 'RadarOnly_QPE_06H/','RadarOnly_QPE_03H/']
mrms_files = []

# HRRR indices first, then same order as mrms_files
#variable_indices = ['RH_P0_L4_GLC0','DSWRF_P0_L1_GLC0', 'TMP_P0_L103_GLC0','CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','preciprate', 'precipflag', 'qpe1hour', 'qpe6hour','qpe3hour']
#variable_indices = ['RH_P0_L4_GLC0','DSWRF_P0_L1_GLC0', 'TMP_P0_L103_GLC0','CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','preciprate', 'precipflag', 'qpe1hour', 'qpe6hour','qpe3hour']
#variable_indices = ['RH_P0_L4_GLC0','DSWRF_P0_L1_GLC0', 'TMP_P0_L103_GLC0','CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','PrecipRate_P0_L102_GLL0', 'PrecipFlag_P0_L102_GLL0', 'RadarOnlyQPE01H_P0_L102_GLL0', 'RadarOnlyQPE06H_P0_L102_GLL0','RadarOnlyQPE03H_P0_L102_GLL0']
variable_indices = ['DSWRF_P0_L1_GRLL0', 'TMP_P0_L103_GRLL0','CSNOW_P0_L1_GRLL0','CRAIN_P0_L1_GRLL0','CFRZR_P0_L1_GRLL0','CICEP_P0_L1_GRLL0', 'APCP_P8_L1_GRLL0_acc']#, 'PRATE_P0_L1_GRLL0']

# 100*(np.exp((17.625*TD)/(243.04+TD))/np.exp((17.625*T)/(243.04+T))) 

###############################################################

number_of_hrrr_products = len(variable_indices)-len(mrms_files)

mrms_url = 'http://mrms.ncep.noaa.gov/data/2D/'
#grib_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/'

#hrrr_url = 'nonoperational/com/hrrr/prod/hrrr.'
#hrrr_tag = 'wrfnatfFF.grib2'

#hrrr_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/nonoperational/com/hrrr/prod/'

storage_path = '../liveproductdata/'

# DB parameters

gaussian_smoothing = True
median_smoothing = False

base_gaussian_sigma = 4
base_median_size = 9

# GeoJSON related

geojson_for_display = False

clean_up = True

using_geotab = True