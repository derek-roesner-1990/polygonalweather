import time

import numpy as np
from matplotlib import pyplot 
import Nio
import xgboost
import shapely
from shapely import wkb
from shapely import ops

import live_feed_functions
import contour_functions

bst = xgboost.Booster()
bst.load_model('../models/xgboost_roadtemp_hrdps_25var_noelevation')

forecast_canadian_variables = ['PRATE_SFC_0',
'None',
'SPFH_TGL_2',
'VGRD_ISBL_0200',
'None',
'VVEL_ISBL_1000',
'None',
'UGRD_TGL_10',
'None',
'UGRD_ISBL_0200',
'None',
'DSWRF_SFC_0',
'PRES_SFC_0',
'None',
'None',
'None',
'ULWRF_NTAT_0',
'HGT_ISBL_1015',
'None',
'APCP_SFC_0',
'CAPE_ETAL_10000',
'WEAPE_SFC_0',
'None',
'None',
'None',
'DLWRF_SFC_0',
'None',
'LHTFL_SFC_0',
'TMP_TGL_2',
'None',
'None',
'None',
'None',
'None',
'None',
'SHTFL_SFC_0',
'None',
'SPFH_ISBL_1015',
'TMP_ISBL_1015',
'None',
'None',
'WEASN_SFC_0',
'None',
'None',
'None',
'None',
'None',
'None',
'None',
'None',
'None',
'HGT_SFC_0',
'PRMSL_MSL_0',
'CWAT_EATM_0',
'VGRD_TGL_10',
'None',
]

def get_weather_grid(time_difference = 0):
    
    filepaths = []
    for variable in forecast_canadian_variables:
        if variable != 'None':
            filepaths.append(live_feed_functions.hrdpsproducts(variable,time_difference=time_difference))
            print filepaths[-1]
    
    bunch_arrays = []
    for filepath in filepaths:
        opened = Nio.open_file(filepath)
        variable = [variable for variable in opened.variables.keys() if not variable.startswith('grid')][0]
            
        current_array = opened.variables[variable].get_value()
        if len(current_array.shape) > 2:
            current_array = current_array[0,:,:]
        bunch_arrays.append(current_array)

    one_stack = np.dstack(bunch_arrays)
    one_stack = one_stack[200:,100:]
    #one_stack = np.flipud(one_stack)

    if time_difference == 0:
        latitudes = opened.variables['gridlat_0'].get_value()[200:,100:]
        longitudes = opened.variables['gridlon_0'].get_value()[200:,100:]
        return one_stack, (longitudes,latitudes)
    else:
        return one_stack

def compute_temp_grid(one_stack):

    rows = []
    for i,row in enumerate(one_stack):
        dtest = xgboost.DMatrix(row,missing=-999999)
        rows.append(bst.predict(dtest))
    output = np.vstack(rows)

    return output

def main(interpolated = False):

    grid, coordinates = get_weather_grid(time_difference = 0)
    
    if interpolated:
        hour_1 = get_weather_grid(time_difference = 1)
        delta = time.gmtime().tm_min/60.0
        grid += (hour_1 - grid)*delta

    output = compute_temp_grid(grid)

    levels, thresholds = contour_functions.polygonize(coordinates[0], coordinates[1],output,thresholds=[271.15,278,284,292,296.15,310])
    for i, level in enumerate(levels):
        #multipolygon = ops.unary_union(level)
        #if multipolygon.area == 0:
        #    continue
        print thresholds[i]
        
        for polygon in level:
            x,y = polygon.exterior.xy
            pyplot.plot(x,y)
        
        pyplot.show()


    #contours = pyplot.contourf(coordinates[0], coordinates[1],output,levels=[271.15,278,284,292,296.15,310],extend='both',cmap='inferno')
    #pyplot.clabel(contours, inline=1, fontsize=20, fmt='%d')

    #pyplot.imshow(output,cmap='inferno')
    pyplot.show()


if __name__ == '__main__':
    main(interpolated = False)


