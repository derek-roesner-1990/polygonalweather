import ast
import time

#from matplotlib import pyplot
import numpy as np
#from scipy import ndimage
import arrow
import rasterio
from rasterio import Affine
from rasterio import features
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()

import postgres_functions
import data_handler
import alarms_and_triggers

from stacked_ds import buffer_factor, buffer_divisor, simplification, master_transform, down_factor

ds_ranges = [[10, 10.5], [10.5, 11], [20, 20.5], [20.5, 21]]
risk_squashes = [[1.0, 3.0], [4.0, 10.0], [12.0, 14.0], [15.0, 20.0]]

#speed_mapping = {0:100.0, 10:0.93, 20:0.81, 30:0.65}
speed_mapping = [ 1.0, 0.993,  0.986,  0.979,  0.972,  0.965,  0.958,  0.951,  0.944,
        0.937,  0.93 ,  0.918,  0.906,  0.894,  0.882,  0.87 ,  0.858,
        0.846,  0.834,  0.822,  0.81 ,  0.794,  0.778,  0.762,  0.746,
        0.73 ,  0.714,  0.698,  0.682,  0.666,  0.65 ]


def get_risk_score(mini, maxi, a, b, x):
    return ((((b - a)*(x - mini)) / (maxi - mini)) + a)

def calculate_risk(score):
    risk = None
    for arange, squash in zip(ds_ranges, risk_squashes):
        if score < 10:
            risk = 0
            break
        elif score > 30:
            risk = 30
            break
        if score > arange[0] and score <= arange[1]:
            risk = get_risk_score(arange[0], arange[1], squash[0], squash[1], score)
        
    return risk

def polygonize(array):
    bins = range(1,21)+[30]
    #array = ndimage.interpolation.zoom(array,5)
    #array = ndimage.gaussian_filter(array,sigma=0.1)
    #pyplot.imshow(array,cmap='inferno')
    #pyplot.show()
    digitized = np.digitize(array,bins).astype(np.int32)
    
    mask = digitized > 0
    #master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
    shapes = rasterio.features.shapes(digitized,mask=mask,transform=master_transform)
    polygons = [[] for _ in bins]+[[],[]]
    
    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon)
    
    polygons = [ops.unary_union(polygon) for polygon in polygons]
    return zip(polygons,bins)

def main(intermediate=False):
    
    ds_array = data_handler.get_layer('ds_array','latest')
    
    vectorized_calculate_risk = np.vectorize(calculate_risk)
    
    risk_array = vectorized_calculate_risk(ds_array)
    
    #x,y = data_handler.get_coordinate_grid('MRMS')
    #x = x[::down_factor, ::down_factor]
    #y = y[::down_factor, ::down_factor]

    #polygons = polygonize(x,y,risk_array)
    polygons = polygonize(risk_array)

    current_time = arrow.utcnow().timestamp

    zones = []
    for i,polygon in enumerate(polygons):
        zone = {'risk_index':polygon[1], 'shape':polygon[0], 'forecast_time':0, 'valid_time':current_time}
        zone['speed_factor'] = speed_mapping[int(polygon[1])]
        print zone['shape'].area
        zones.append(zone)


    for forecast_time in range(1,7):
        forecast_string = str(forecast_time+1)
        if len(forecast_string) < 2:
            forecast_string = '0{}'.format(forecast_string)
        ds_array = data_handler.get_layer('ds_array_f{}'.format(forecast_string),'latest')
        
        risk_array = vectorized_calculate_risk(ds_array)
        
        #x,y = data_handler.get_coordinate_grid('MRMS')
        #x = x[::down_factor, ::down_factor]
        #y = y[::down_factor, ::down_factor]

        #polygons = polygonize(x,y,risk_array)
        polygons = polygonize(risk_array)

        current_time = arrow.utcnow().replace(hours=+forecast_time).timestamp

        for i,polygon in enumerate(polygons):
            zone = {'risk_index':polygon[1], 'shape':polygon[0], 'forecast_time':forecast_time, 'valid_time':current_time}
            zone['speed_factor'] = speed_mapping[int(polygon[1])]
            print zone['shape'].area
            zones.append(zone)

    if intermediate:
        return zones

    postgres_functions.push_risk_polygons(zones)

    alarms_and_triggers.clean_cache('current_risk_polygons')
    alarms_and_triggers.geoserver_trigger('current_risk_polygons')

    print 'Done!'

if __name__ == '__main__':
    main()