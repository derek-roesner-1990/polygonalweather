"""

Transforms gridded datasets into thresholded geolocated polygons. 
Many variables reside in parameters.py

"""
import socket
import sys
import time
import calendar
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import colors
from scipy import ndimage
from scipy import misc
import Nio
import fiona
import shapely
from shapely import wkb
from shapely import ops
import geojson
import json
import ast
import os
import socketIO_client
import toolz

import postgres_functions
import geotab_functions
import event_mongodb_functions as mongodb_functions
from event_parameters import *
import live_feed_functions
from live_feed_functions import hrrproducts, mrmsproducts, remove_old_files
from contour_functions import polygonize
            
def process_file(filepath, variable_index, geolayers, mongo_client = None, observations = None, cameras = None):
    """ Opens and selects desired variables, returns records of polygons with relevant info """
    opened = Nio.open_file(filepath)

    if "MRMS" in filepath:
        x,y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
        x = x-360 # fixing unsigned longitudes
        variable = [key for key in opened.variables.keys() if 'lon_0' not in key and 'lat_0' not in key][0] # One variable per file
        gaussian_sigma = 6.0
    else:
        x = opened.variables['gridlon_0'].get_value()
        y = opened.variables['gridlat_0'].get_value() 
        gaussian_sigma = 4.0
        try:
            index = variable_index
            variable = opened.variables.keys()[index]
        except Exception as e:
            print e    

    data = opened.variables[variable].get_value()
    variable_name = opened.variables[variable].long_name
    #data[data == -3] = 0
    
    #data = ndimage.interpolation.zoom(data, 0.10, order=0)
    #data = ndimage.morphology.grey_closing(data, size=(5,5))

    data = ndimage.gaussian_filter(data,sigma=gaussian_sigma) #TODO Have it automatically computed based on grid resolution
    #data = ndimage.median_filter(data,(5,5))
    print 'Creating polygons for', variable_name

    current_key = None
    thresholds = None
    for key in threshold_map.keys():
        if key in variable_name.lower().replace(' ',''):
            current_key = key
    
    if 'Radar Precipitation' in variable_name:
        current_key = 'qpe'
    if 'Lightning' in variable_name:
        current_key = 'lightning'
    if 'Severe Hail' in variable_name:
        current_key = 'posh'
    if 'Downward' in variable_name:
        current_key = 'radiation'
    if 'Seamless' in variable_name:
        current_key = 'seamless'

    thresholds = threshold_map[current_key]
    print current_key

    # Takes care of whether a high value represents a high risk or the inverse
    inverted = False
    if current_key in inverse_variables:
        inverted = True

    levels, thresholds = polygonize(x,y,data,which_variable=current_key,thresholds=thresholds, inverted=inverted)

    opened.close()

    #multipolygons = []
    #outputs = []
    records = []

    entities = []

    for i, level in enumerate(levels):
        if current_key == 'seamless':
            exteriors = []
            for geom in level:
                exteriors.append(shapely.geometry.Polygon(geom.exterior))
            multipolygon = ops.unary_union(exteriors)
            multipolygon = multipolygon.simplify(0.001, preserve_topology=True)
        else:
            if current_key == 'windspeed':
                exteriors = []
                for geom in level:
                    exteriors.append(shapely.geometry.Polygon(geom.exterior))
                multipolygon = ops.unary_union(exteriors)

            else:
                multipolygon = ops.unary_union(level)
        if multipolygon.area == 0:
            continue
        if current_key == 'qpe':
            """
            all_land = []
            with fiona.open('landpolygon/ne_10m_land.shp') as opened:
                for geom in opened:
                    all_land.append(shapely.geometry.shape(geom['geometry']))
            northamerica = all_land[0][3821]
            polygon = all_land[0]

            cut_poly= polygon.intersection(shapely.geometry.Polygon([(-50,15),(-140,15),(-140,60),(-50,60)]))

            with open('northamerica.txt', 'w') as opened:
                opened.write(cut_poly.wkb_hex)
            """
            with open('landpolygon/northamerica.txt','r') as opened:
                northamerica = wkb.loads(opened.read(),hex=True)
        
            multipolygon = multipolygon.intersection(northamerica)
        
        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if current_key in inverse_variables:
            if i == 0:
                geolayer_dict['thresholdValues'] = [float(data.min()), thresholds[i]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i-1],thresholds[i]]
        else:
            if i < (len(thresholds)-1):
                geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i],float(data.max())]

        # A bit ugly
        geolayer_dict['colour'] = color_map[current_key][value_to_string_map[current_key][thresholds[i]]]
        geolayer_dict['variableName'] = current_key

        if current_key == 'seamless':
            if thresholds[i] < 5:
                riskLevel = 1
            elif thresholds[i] >= 5 and thresholds[i] < 30:
                riskLevel = 2
            elif thresholds[i] >= 30:
                riskLevel = 3
        else:
            riskLevel = risk_map[value_to_string_map[current_key][thresholds[i]]]
        '''
        if current_key in inverse_variables:
            riskLevel *= -1
            riskLevel += 4
        '''
        new_geolayer_dict = dict()

        new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
        new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel}
        new_geolayer_dict['properties']['shape'] = multipolygon

        records.append(new_geolayer_dict)

        if mongo_client is not None:
            pass
            #mongodb_functions.process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel) 
            #mongodb_functions.process_cameras(mongo_client, cameras, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel) 

        # Adds to entities
        if using_geotab:
            geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, variable_name)

    geolayers.extend(records)

    if using_geotab:
        cleaned_up = geotab_functions.deal_with_holes(entities)
        geotab_functions.push_to_geotab(cleaned_up)

def main():
    """ Downloads files and push geojson polygons """
    print time.gmtime()
    if using_geotab:
        geotab_functions.delete_old_zones()
        print 'Deleted'

    mongo_client = None
    observations = None
    geolayers = []

    cameras = None
    if using_mongo:
        mongo_client, observations = mongodb_functions.prepare_mongo()
        #cameras = mongodb_functions.get_cameras(mongo_client)    

    filepaths = [hrrproducts()]
    filepaths = filepaths*number_of_hrrr_products
    for mrms_file in mrms_files:
        filepaths.append(mrmsproducts(mrms_file))
    
    for filepath,variable_index in zip(filepaths,variable_indices):
        process_file(filepath, variable_index, geolayers, mongo_client, observations, cameras)

    if mongo_client is not None:
        mongodb_functions.push_to_mongo(mongo_client, [toolz.update_in(geolayer, ['properties','shape'], str) for geolayer in geolayers])
        #mongodb_functions.push_devices_to_mongo(mongo_client, observations)
        #mongodb_functions.push_cameras_to_mongo(mongo_client, cameras)

    if using_postgres:
        postgres_functions.push_event_contours(geolayers)

    remove_old_files()

    try:
        if socket.gethostname() == 'ubuntu-cv1':
            current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')
        current_socketIO.emit('contours_trigger', time.time())
        for item in live_feed_functions.updated:
            if 'hrrr' in item:
                current_socketIO.emit('hrrr_trigger', time.time())
        current_socketIO.disconnect()
    except Exception as e:
        print e

    print time.gmtime()
if __name__ == '__main__':
    main()
