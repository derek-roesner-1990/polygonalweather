import os
import glob
import shutil
import sys
sys.setcheckinterval(10000000)
import socket
import matplotlib
matplotlib.use('Agg')
import ast
from matplotlib import pyplot
import subprocess
import rasterio
from rasterio import warp
from rasterio import Affine
from rasterio import features
from scipy import ndimage
import numpy as np
import Nio
from cartopy.mpl import patch
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()
import arrow
import socketIO_client

import alarms_and_triggers
import data_handler
import geotab_functions

from dsRoadStatesTemporalSplit_withRoad_V4 import getForecastBeliefValues_ecmwf
#from dsRoadStatesTemporalSplit_withRoad_V4 import getForecastBeliefValues


import postgres_functions
from live_feed_functions import *
from europe_ds_parameters import *

#master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
#master_transform = Affine.translation(-130,55)*Affine.scale(0.02,-0.02)
#master_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
#master_grid_width = 3500
#master_grid_height = 1750
#master_grid_width = 1400
#master_grid_height = 700

#master_crs = {'init': 'EPSG:4326'}

#down_factor = 5
#buffer_factor = 0.1
buffer_factor = 0.05
buffer_divisor = 1.5
simplification = 0.01

bin_optimization = True

def compute_ds_array(array, frozen_array, frozen_mask, indices):
    cache = dict()
    shape1,shape2 = array.shape[0:2]
    ds_array = np.zeros((shape1,shape2))
    #for i in xrange(shape1):
    #    for j in xrange(shape2):
    #for i,j in zip(indices[0],indices[1]):
    indices = np.dstack(indices).squeeze()
    for i,j in indices:
        if frozen_mask[i,j]:
            array_slice = tuple(frozen_array[i,j])
        else:
            array_slice = tuple(array[i,j])

        if cache.has_key(array_slice):
            result = cache[array_slice]
        else:
            result = getForecastBeliefValues_ecmwf(array_slice)
            cache[array_slice] = result
        ds_array[i,j] = result
    return ds_array

def clean_past_arrays(threshold=240):
    filepaths = glob.glob('../persistent/europe_*')
    current_time = time.time()
    for filepath in filepaths:
        if (current_time - os.path.getctime(filepath)) / 60.0 > threshold:
            os.remove(filepath)

def main(desired_time = None):
    print time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    start_time  = time.time()
    postgres_functions.push_start_time(['europe_ds'],start_time)
    
    grid_list = []
    truex, truey = data_handler.get_coordinate_grid('ecwt_')
    truex = truex[:,:3599] # Make it fit with everything
    truey = truey[:,:3599]

    if desired_time is None:
        desired_time = arrow.utcnow()
    
    #desired_time = arrow.get('2017-02-14T15:02:00+00:00') #TODO remove
    #desired_time = arrow.get('2017-02-28T21:12:00+00:00') #TODO remove

    # Checks if HRRR with index below the total length minus the number of expected MRMS layers 
    for var_index,variable in enumerate(variable_indices):
        
        if variable.startswith('SSRD') or variable.startswith('SF_'):
            data = data_handler.get_layer(variable, desired_time, latest=False, interpolation_timestep=3)
            
            second_half = data[:,:1799]
            first_half = data[:,1799:]
            recut_data = np.hstack([first_half,second_half])
            
            grid_list.append(recut_data)

        else:
            data = data_handler.get_layer(variable, desired_time, latest=False, interpolation_timestep=3)
            data = data[:,:3599] # Make it fit with everything
            grid_list.append(data)

    
    #(airtemp, radiation, precipaccum3Hr, precipaccum6Hr)
    reordered = [None]*5
    reordered[0] = grid_list[0]
    reordered[1] = grid_list[1]
    reordered[2] = grid_list[2]*1000 # meters to millimeters
    reordered[3] = data_handler.get_layer('accumulation_ECMWF_6',desired_time,latest=False, interpolation_timestep=3)[:,:3599]*1000 # meters to millimeters
    
    snowfall = grid_list[3]
    snowfall[snowfall < 0.001] = 0
    snowfall[snowfall > 0] = 1
    
    reordered[4] = snowfall

    clean_past_arrays()
    #np.save('../persistent/europe_input_{0}'.format(desired_time),np.dstack(reordered))
    frozen_reordered = [np_grid.copy() for np_grid in reordered]

    #np.save('/home/souellet/Downloads/feb14_test_input',np.dstack(reordered))
    #return
    raw_array = np.dstack(reordered)

    frozen_mask = np.logical_and(raw_array[:,:,0] <= 273.16,raw_array[:,:,0] > 0)
    frozen_precip_array = frozen_mask.astype(np.float64)+1    

    if bin_optimization:
        bins = []
        
        bins.append([-1,0,271.16,275.16,5000]) #airtemp
        bins.append([-1,0,250,500,750,5000]) #radiation
        bins.append([-1,0,0.00006,2.5,10,5000]) #precip3h
        bins.append([-1,0,0.00001,5,15,5000]) #precip6h
        
        
        corresponding_values = []        
        corresponding_values.append([-3,-3,2,275,276]) #airtemp
        corresponding_values.append([-3,-3,2,251,501,751]) #radiation
        corresponding_values.append([-3,-3,0,1,3,11]) #precip3h
        corresponding_values.append([-3,-3,0,1,6,16]) #precip6h
        
        for i in range(0,4):
            layer_bin = bins[i]
            new_layer = np.digitize(reordered[i],layer_bin).astype(np.float64)
            for j,value in enumerate(layer_bin):
                new_layer[new_layer == j] = corresponding_values[i][j]
            reordered[i] = new_layer


    #print 'Finished binning', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    
    master_array = np.dstack(reordered+[frozen_precip_array])

    # Copy for frozen precipitation
    if bin_optimization:
        bins = []
        
        bins.append([-1,0,271.16,275.16,5000]) #airtemp
        bins.append([-1,0,250,500,750,5000]) #radiation
        bins.append([-1,0,0.00006,2.5,7.5,5000]) #accum3h
        bins.append([-1,0,0.00001,5,10,20,5000]) #Precip6h
        
        corresponding_values = []
        
        corresponding_values.append([-3,-3,2,275,276])
        corresponding_values.append([-3,-3,2,251,501,751])
        corresponding_values.append([-3,-3,0,1,3,11]) #precip3h
        corresponding_values.append([-3,-3,0,1,6,12,22]) #precip6h
        
        for i in range(0,4):
            layer_bin = bins[i]
            new_layer = np.digitize(frozen_reordered[i],layer_bin).astype(np.float64)
            for j,value in enumerate(layer_bin):
                new_layer[new_layer == j] = corresponding_values[i][j]
            frozen_reordered[i] = new_layer    
    
    frozen_master_array = np.dstack(frozen_reordered+[frozen_precip_array])    
    
    land = data_handler.get_layer('GFS:land', 'None', latest=False)
    land = land.astype(np.bool)
    indices = np.where(land)

    #np.save('/home/souellet/Downloads/master_array',master_array)
    #np.save('master_array',master_array)
    #np.save('indices', indices)
    #return
    #master_array = np.ma.masked_equal(master_array,-3)
    #print master_array[0,0]

    #downsampled = master_array[::down_factor,::down_factor]
    
    #print getBeliefValues([downsampled[90,180]])
    print 'Preprocessing completed', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    #result = np.apply_along_axis(getBeliefValues,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    result = compute_ds_array(master_array, frozen_master_array, frozen_mask, indices)
    
    #np.save('/home/souellet/Downloads/feb14test_output_{}'.format(arrow.utcnow()),result)
    #pyplot.imshow(result)
    #pyplot.savefig('/home/souellet/what.pdf')
    print 'Results calculated', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    
    data_handler.store_layer(result.astype(np.float32), 'europe_ds_array', arrow.utcnow(), time_alive=3600, latest=True)
    #np.save('../persistent/binned_input_{0}'.format(desired_time),master_array)
    #np.save('../persistent/binned_input_frozen_{0}'.format(desired_time),frozen_master_array)
    #np.save('../persistent/europe_output_{0}'.format(desired_time),result)
    #np.save('../persistent/non_opt_stacked_output_{0}'.format(desired_time),result)
    #return #TODO remove this before deploying

    #polygons = polygonize(truex, truey,result,icy_computed=None)
    
    """
    counter = 0
    for polygon in polygons:
        geom = polygon[0]
        if hasattr(geom, 'geoms'):
            for geom_one in geom:
                counter += 1
    print counter
    """
    #postgres_functions.push_prob_zones_clean(polygons)
    #postgres_functions.push_prob_zones(polygons)
    
    #postgres_functions.push_end_time(['europe_ds'],start_time,time.time())

    #print 'Polygons created', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    #client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #client_socket.connect(('localhost',8042))
    #client_socket.close()

    remove_old_files()
    
def polygonize(x,y,array,icy_computed=None):
    #contours = pyplot.contourf(x,y,array,levels=[10.50,11.10,20.50,21.10,30.50,31.10],cmap='inferno')
    #contours = pyplot.contourf(x,y,array,levels=[10.20,11.10,20.20,21.10,30.20,31.10],cmap='inferno',antialiased=False)

    bins = [10,20,30]
    digitized = np.digitize(array,bins).astype(np.int32)
    mask = digitized > 0
    shapes = rasterio.features.shapes(digitized,mask=mask,transform=master_transform)
        
    polygons = [[],[],[]]
    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon) # wet, snowy, icy in order


    if buffer_factor > 0:
        wet = [poly.buffer(buffer_factor) for poly in polygons[0]]
        snowy = [poly.buffer(buffer_factor) for poly in polygons[1]]
        icy = [poly.buffer(buffer_factor) for poly in polygons[2]]

        if simplification > 0:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
        else:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor)
        
        if icy_computed is not None:
            better_icy = icy_computed

        better_wet = better_wet.difference(better_snowy)
        better_snowy = better_snowy.difference(better_icy)
        better_wet = better_wet.difference(better_icy)

    else:
        better_wet = geometry.MultiPolygon(polygons[0])
        better_snowy = geometry.MultiPolygon(polygons[1])
        better_icy = geometry.MultiPolygon(polygons[2])
        if icy_computed is not None:
            better_icy = icy_computed


    return [[better_wet,'W',0.8], [better_snowy,'S',0.8], [better_icy,'I',0.8]]



if __name__ == '__main__':
    main()
    #test()
    #validation_run()
