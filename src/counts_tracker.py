import time
import json
import ast

import arrow
import requests
import psycopg2

got_a_offset = None
#got_a_offset = 36536280

with open('credentials.txt','r') as opened:
    credentials = ast.literal_eval(opened.read())

def create_table():
    query = """
    CREATE TABLE wtx_fleet_tracking (
        id serial PRIMARY KEY,
        units integer[],
        observations int,
        first_offset int,
        last_offset int,
        start_time timestamp,
        end_time timestamp,
        queried_time timestamp
    );
    """

    cursor = psycopg2.connect(**credentials).cursor()
    cursor.execute(query)
    cursor.connection.commit()
    cursor.connection.close()

def store_count(to_store):
    cursor = psycopg2.connect(**credentials).cursor()

    query = """ insert into wtx_fleet_tracking(units, observations, first_offset, last_offset, start_time, end_time, queried_time)
    values (%s,%s,%s,%s,%s,%s,%s); """

    cursor.execute(query, to_store)
    cursor.connection.commit()

    cursor.connection.close()

def get_offset():
    if got_a_offset is None:
        cursor = psycopg2.connect(**credentials).cursor()
        query = "select max(last_offset) from wtx_fleet_tracking;"
        cursor.execute(query)
        return cursor.fetchall()[0][0]
    else:
        return got_a_offset

def main():
    while True:
        time.sleep(60)

        offset = get_offset()
        resp = requests.get('http://130.211.154.46:5005/id_offset/{}'.format(offset))
        observations = json.loads(resp.content)
        new_offset = observations['last_id']

        number_of_observations = len(observations['observations'])
        units_set = set()
        for observation in observations['observations']:
            units_set.add(observation['unit'])
        units = list(units_set)

        start_time = min(observations['observations'],key=lambda x: x['observation_datetime'])['observation_datetime']
        end_time = max(observations['observations'],key=lambda x: x['observation_datetime'])['observation_datetime']

        last_offset = observations['last_id']

        to_store = [units, number_of_observations, offset, last_offset, start_time, end_time, arrow.utcnow().datetime]
        store_count(to_store)

        got_a_offset = last_offset
        #time.sleep(120)

if __name__ == '__main__':
    main()
