import matplotlib
matplotlib.use('Agg')
import sys
import os
import ast
from scipy import ndimage
import time
from rasterio import Affine
import Nio
import numpy as np
from matplotlib import pyplot
import datetime
import shapely
import requests
from shapely import geometry
from shapely import ops
from cartopy.mpl import patch

def get_levels(contours):
    wet = []
    snowy = []
    levels = []
    for contour in contours.collections:
        levels.append([])
        for path in contour.get_paths():
            levels[-1].extend(patch.path_to_geos(path))
    buffered = []
    for polygon in levels[0]:
        buffered.append(polygon.buffer(0.2))
    wet = ops.unary_union(buffered)

    buffered = []
    for polygon in levels[2]:
        buffered.append(polygon.buffer(0.2))
    snowy = ops.unary_union(buffered)

    return wet, snowy

filenames = os.listdir('/Users/soullet/Downloads/ds_outputs/')

mping_content = []
with open('/Users/soullet/road_weather_assessment_system/Scripts/mpingjanforward.txt', 'r') as opened:
    mping_content = opened.read()

success = False
while not success:
    try:
        mping_content = eval(mping_content)
        success = True
    except:
        continue

print "Alright"
searched = 0
ds_dict = dict()
for filename in filenames:
    #print filename, searched
    ds_dict[filename] = []
    ds_time = datetime.datetime.strptime(filename[0:-7], '%Y-%m-%d %H-%M')
    to_search = mping_content[searched:]
    first_found = False
    for i,item in enumerate(to_search):
        mping_time = datetime.datetime.strptime(item['obtime'][0:19], '%Y-%m-%dT%H:%M:%S')
        difference = ds_time-mping_time
        difference_seconds = difference.total_seconds()
        if difference_seconds < 1800:
            if not first_found:
                searched += i
                first_found = True
            if difference_seconds > -1800:
                ds_dict[filename].append(item)
            else:
                break


stateDescriptions = []
stateDescriptions.append(["None", "Drizzle", "NULL"]) # for DRY
stateDescriptions.append(["Drizzle", "Freezing Drizzle", "Rain", "Freezing Rain", "Ice Pellets/Sleet", "Mixed Rain and Snow",
                          "Mixed Rain and Ice Pellets", 'Snow and/or Graupel', 'Mixed Ice Pellets and Snow']) # for WET
stateDescriptions.append(["Snow and/or Graupel", "Freezing Drizzle", "Freezing Rain", "Ice Pellets/Sleet", "Snow",
                          "Mixed Ice Pellets and Snow"]) # for SNOW

opened = Nio.open_file('/Users/soullet/Downloads/MRMS_SeamlessHSR_00.00_20160520-125800.grib2')
data = opened.variables[opened.variables.keys()[0]].get_value()
x, y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
x = x-360

x = x[::5,::5]
y = y[::5,::5]

keys = ds_dict.keys()

actualDryTotal = 0
actualWetTotal = 0
actualSnowTotal = 0
predDryTotal = 0
predWetTotal = 0
predSnowTotal = 0

mispredicted_wet = 0
mispredicted_snowy = 0
mispredicted_dry = 0
total = 0.0

starting = int(sys.argv[1])
print starting
desc = set()
for key_number,key in enumerate(sorted(keys)[starting:starting+144]):
    print key, key_number

    """
    print predWetTotal, actualWetTotal
    print predDryTotal, actualDryTotal
    print predSnowTotal, actualSnowTotal

    if actualDryTotal > 0 and actualWetTotal > 0 and actualSnowTotal > 0:
        print float(predWetTotal)/actualWetTotal
        print float(predDryTotal)/actualDryTotal
        print float(predSnowTotal)/actualSnowTotal
    """
    raster = np.load('/Users/soullet/Downloads/ds_outputs/'+key)

    if True:
        contours = pyplot.contourf(x,y,raster,levels=[10.10,11.1,20.10,21.11])
        slowwet, slowsnowy = get_levels(contours)
        wet = shapely.prepared.PreparedGeometry(slowwet)
        snowy = shapely.prepared.PreparedGeometry(slowsnowy)

    print key
    mpings = ds_dict[key]
    for mping in mpings:
        #coords = reverse*mping['geom']['coordinates']
        #pixel_location = (int(round(coords[1])),int(round(coords[0])))
        description = mping['description']
        #print raster[pixel_location], state

        if False:
            try:
                roadState = raster[pixel_location]
            except:
                print pixel_location
                continue

            if (description in stateDescriptions[0]): # this is actually a dry road
                actualDryTotal += 1
                if (roadState > 0 and roadState < 1.1):
                    predDryTotal += 1
            elif (description in stateDescriptions[1]): # this is actually a wet road
                actualWetTotal += 1
                if (roadState > 10 and roadState < 11.1):
                    predWetTotal += 1
            elif (description in stateDescriptions[2]): # this is actually a snowy road
                actualSnowTotal += 1
                if (roadState > 20 and roadState < 21.1):
                    predSnowTotal += 1
            else:
                desc.add(description)
                continue # what the hell happened?
        elif False:
            point = geometry.shape(mping['geom'])

            if (description in stateDescriptions[0]): # this is actually a dry road
                actualDryTotal += 1
                if wet.contains(point) or snowy.contains(point):
                    predDryTotal += 0
                else:
                    predDryTotal += 1
            elif (description in stateDescriptions[1]): # this is actually a wet road
                actualWetTotal += 1
                if wet.contains(point):
                    predWetTotal += 1
            elif (description in stateDescriptions[2]): # this is actually a snowy road
                actualSnowTotal += 1
                if snowy.contains(point):
                    predSnowTotal += 1
            else:
                desc.add(description)
                continue # what the hell happened?

        else:
            total += 1
            point = geometry.shape(mping['geom'])
            if wet.contains(point):
                if description in stateDescriptions[1]:
                    pass
                elif description in stateDescriptions[2]:
                    mispredicted_wet += 1
                elif description in stateDescriptions[0]:
                    mispredicted_wet += 1

            elif snowy.contains(point):
                if description in stateDescriptions[2]:
                    pass
                elif description in stateDescriptions[0]:
                    mispredicted_snowy += 1
                elif description in stateDescriptions[1]:
                    mispredicted_snowy += 1
            else:
                if description in stateDescriptions[0]:
                    pass
                elif description in stateDescriptions[1]:
                    mispredicted_dry += 1
                elif description in stateDescriptions[2]:
                    mispredicted_dry += 1
    print 'Counts'
    print mispredicted_dry
    print mispredicted_wet
    print mispredicted_snowy
    print 'Total', total
    print 'Percentages'

    if total > 0:
        print mispredicted_dry/total
        print mispredicted_wet/total
        print mispredicted_snowy/total

with open("counts_weekly","a") as opened:
    opened.write("Start_of_week"+keys[starting])
    opened.write("\n")
    opened.write(str(mispredicted_dry))
    opened.write("\n")
    opened.write(str(mispredicted_wet))
    opened.write("\n")
    opened.write(str(mispredicted_snowy))
    opened.write("\n")
    opened.write(str(total))
    opened.write("\n")
    opened.write("End_of_week\n")
