import numpy as np
from scipy import ndimage
from scipy import misc
import matplotlib.pyplot as plt
import matplotlib
import json
import ast
from cartopy.mpl import patch
from matplotlib import pyplot
import geojson
from shapely import wkb
from shapely import ops

import base64
import datetime
import hmac
import time
import urllib
import urllib2

try:
    from hashlib import sha1 as sha
except ImportError:
    # hashlib was added in Python 2.5
    import sha

    
host = "http://api.velocityweather.com/v1"
access_key = "1adeILRwLsKP"
access_key_secret = "kp2wKamtpLpXRCECf09lMOqOEwcSZJeW0ZlHzgoLl3"

def get_indices(lat,lon,x,y):
    
    x_diff = np.abs(x-lat)
    y_diff = np.abs(y-lon)

    lat_ind = np.where(x_diff == np.min(x_diff))[0][0]
    lon_ind = np.where(y_diff == np.min(y_diff))[0][0]

    return (lat_ind, lon_ind)


# x and y are the number of rows and columns in the data
def polygonize_global_radar(x, y, data, thresholds = None, inverted = False):
    """ Creates contour using given or computed levels """

    # If the minimum or the maximum of the variable is of interest
    extension = 'max'
    if inverted:
        extension = 'min'
    
    contours = pyplot.contourf(x, y, data, levels=thresholds, extend=extension)
    collections = contours.collections # for each threshold range, a set of contours
    level_values = contours.levels # these are the ranges corresponding to each set of contours

    levels = [] # an array of size N, where N is the number of contour sets
    for contour in collections: # for each set of contours for a corresponding threshold level
        levels.append([])
        for path in contour.get_paths(): # get all the paths within that contour set, and iterate through them
            try:
                new_path = patch.path_to_geos(path) # convert path into a list of shapely geometric objects
                levels[-1].extend(new_path)
            except Exception as e:
                print(e)
                continue

    return levels, level_values # return the array of shapely geometric objects, and the corresponding thresholds for the contours


def get_dbz_grid(data, colour_values):
    
    num_rows = data.shape[0]
    num_cols = data.shape[1]
    dbz_grid =np.empty((num_rows,num_cols))
    
    try:
        for row in range(num_rows):
            for col in range(num_cols):
                values = data[row,col]
                pixel_colour = rgb2hex(values[0], values[1], values[2], values[3])
                if pixel_colour == "#00000000":
                    dbz_grid[row,col] = -10
                    continue
                dbz_value_str = [colour_value["value"] for colour_value in colour_values if colour_value["color"] == pixel_colour]
                dbz_value = ast.literal_eval(dbz_value_str[0].split(" dBZ")[0])
                dbz_grid[row,col] = dbz_value
    except:
        print((row, col, values))
        print(pixel_colour)
            
    return dbz_grid


def get_radar_polygons(minLon, minLat, maxLon, maxLat):
    
    min_coords = get_indices(master_lats, master_lons, minLat, minLon)
    max_coords = get_indices(master_lats, master_lons, maxLat, maxLon)
    
    data_subset = data[max_coords[0]:(min_coords[0]+1), min_coords[1]:(max_coords[1]+1)]
    
    lat_subset = master_lats[max_coords[0]:(min_coords[0]+1)]
    lon_subset = master_lons[min_coords[1]:(max_coords[1]+1)]
    
    objects, thresholds = polygonize(lat_subset, lon_subset, data_subset, thresholds=dbz_thresholds)


def rgb2hex(r,g,b,a):
    hex = "#{:02x}{:02x}{:02x}{:02x}".format(r,g,b,a)
    return hex

def sign(string_to_sign, secret):
    """ Returns the signature for string_to_sign
    """

    return base64.urlsafe_b64encode(hmac.new(secret.encode('utf-8'), string_to_sign.encode('utf-8'), sha).digest())


def sign_request(url, key, secret):
    """ Returns signed url
    """

    ts = str(int(time.time()))
    sig = sign(key + ":" + ts, secret)
    q = '?' if url.find("?") == -1 else '&'
    url += "%ssig=%s&ts=%s" % (q, sig, ts)
    return url

def request_tile(product, product_config, z, x, y):
    url = "%s/%s/meta/tiles/product-instances/%s/%s" % (host, access_key, product, product_config)
    meta_url = sign_request(url, access_key, access_key_secret)

    response = urllib.urlopen(meta_url).read()
    data = json.loads(response)
    if not data:
        return

    meta_date = data[0]['time']
    url = "%s/%s/tms/1.0.0/%s+%s+%s/%d/%d/%d.png" % (host, access_key, product, product_config, meta_date, z, x, y)
    return sign_request(url, access_key, access_key_secret)


def request_wms(product, product_config, image_size_in_pixels, image_bounds):
    """
    Requests a WMS image and saves it to disk in the current directory.
    @param product: The product code, such as 'C39-0x0302-0'
    @param product_config: The product configuration, such as 'Standard-Mercator' or 'Standard-Geodetic'.
    @param image_size_in_pixels: The image width and height in pixels, such as [1024, 1024].
    @param image_bounds: The bounds of the image. This value has several caveats, depending
        on the projection being requested.
        A. If requesting a Mercator (EPSG:3857) image:
            1. The coordinates must be in meters.
            2. The WMS 1.3.0 spec requires the coordinates be in this order [xmin, ymin, xmax, ymax]
            3. As an example, to request the whole world, you would use [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244].
               Because this projection stretches to infinity as you approach the poles, the ymin and ymax values
               are clipped to the equivalent of -85.05112877980659 and 85.05112877980659 latitude, not -90 and 90 latitude,
               resulting in a perfect square of projected meters.
        B. If requesting a Geodetic (EPSG:4326) image:
            1. The coordinates must be in decimal degrees.
            2. The WMS 1.3.0 spec requires the coordinates be in this order [lat_min, lon_min, lat_max, lon_max].
            3. As an example, to request the whole world, you would use [-90, -180, 90, 180].

    Theoretically it is possible to request any arbitrary combination of image_size_in_pixels and image_bounds,
    but this is not advisable and is actually discouraged. It is expected that the proportion you use for
    image_width_in_pixels/image_height_in_pixels is equal to image_width_bounds/image_height_bounds. If this is
    not the case, you have most likely done some incorrect calculations. It will result in a distorted (stretched
    or squished) image that is incorrect for the requested projection. One fairly obvious sign that your
    proportions don't match up correctly is that the image you receive from your WMS request will have no
    smoothing (interpolation), resulting in jaggy or pixelated data.
    """

    # We're using the TMS-style product instances API here for simplicity. If you
    # are using a standards-compliant WMS client, do note that we also provide a
    # WMS-style API to retrieve product instances which may be more suitable to your
    # needs. See our documentation for details.

    # For this example, we use the optional parameter "page_size" to limit the
    # list of product instances to the most recent instance.
    meta_url = '{}/{}/meta/tiles/product-instances/{}/{}?page_size=1'.format(host, access_key, product, product_config)
    meta_url = sign_request(meta_url, access_key, access_key_secret)

    request = urllib2.Request(meta_url)
    try:
        response = urllib2.urlopen(request)
    except urllib2.HTTPError as e:
        print 'HTTP status code:', e.code
        print 'content:'
        print e.read()
        return
    assert response.code == 200

    # Decode the product instance response and get the most recent product instance time,
    # to be used in the WMS image request.
    content = json.loads(response.read())
    product_instance_time = content[0]['time']

    # WMS uses EPSG codes, while our product configuration code uses 'Geodetic' or
    # 'Mercator'. We map between the two here to prepare for the WMS CRS query parameter.
    epsg_code = 'EPSG:4326' if product_config.endswith('-Geodetic') else 'EPSG:3857'

    # Convert the image bounds to a comma-separated string.
    image_bounds = ','.join(str(x) for x in image_bounds)

    wms_url = '{}/{}/wms/{}/{}?VERSION=1.3.0&SERVICE=WMS&REQUEST=GetMap&CRS={}&LAYERS={}&BBOX={}&WIDTH={}&HEIGHT={}'.format(
        host,
        access_key,
        product,
        product_config,
        epsg_code,
        product_instance_time,
        image_bounds,
        image_size_in_pixels[0],
        image_size_in_pixels[1]
    )
    wms_url = sign_request(wms_url, access_key, access_key_secret)
    print wms_url

    request = urllib2.Request(wms_url)
    try:
        response = urllib2.urlopen(request)
    except urllib2.HTTPError as e:
        print 'HTTP status code:', e.code
        print 'content:'
        print e.read()
        return
    assert response.code == 200

    content = response.read()
    filename = './wms_img_{}_{}.png'.format(product, product_config)
    print 'Read {} bytes, saving as {}'.format(len(content), filename)
    with open(filename, 'wb') as f:
        f.write(content)

    return filename


# this function generates the latest radar grid by getting images from Baron and converting the pixels to dbz values
def generate_radar_grid():

    b1 = [0, -180, 90, 0] # 
    b2 = [-90, -180, 0, 0]
    b3 = [0, 0, 90, 180]
    b4 = [-90, 0, 0, 180]
    bounds = [b1, b2, b3, b4]


    # read the colour palette from baron. this maps the colour of each pixel to a dbz value
    with open('legend.json') as json_file:  
        colours = json.load(json_file)
    colour_values = colours["palettes"][0]["entries"]

    # make the grid that has dbz values
    all_grids = [] # this will store each image quadrant converted into dbz grids. all will be merged into one single grid for further processing

    # for each bound
    for bound in bounds:

        # get the image for that bound. the filename is returned, image is stored in the same folder as this file
        image_name = request_wms('C09-0x0374-0', 'Standard-Geodetic', [3000, 1500], bound)

        # read the image and store it into a numpy array
        data = misc.imread(image_name)

        # convert this coloured image into a dbz array
        dbz_grid = get_dbz_grid(data, colour_values)

        all_grids.append(dbz_grid)

    # merge all quadrants. order is [top left, bottom left, top right, bottom right]
    full_grid = np.vstack([ np.hstack([all_grids[0], all_grids[2]]), np.hstack([all_grids[1], all_grids[3]]) ])


	
    return full_grid

	