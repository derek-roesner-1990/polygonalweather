import socketIO_client

import alarms_and_triggers

def on_hrrr_contours(*args):
    alarms_and_triggers.geoserver_trigger('visibility')
    alarms_and_triggers.geoserver_trigger('windspeed')
    print 'cleaned hrrr contours'

def on_contours(*args):
    alarms_and_triggers.geoserver_trigger('posh')
    alarms_and_triggers.geoserver_trigger('hydroplaning')
    alarms_and_triggers.geoserver_trigger('seamless')
    alarms_and_triggers.geoserver_trigger('lightning')
    print 'cleaned contours'

def main():
    current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')
    current_socketIO.on('update-hrrr-contours', on_hrrr_contours)
    current_socketIO.on('update-contours', on_contours)

    current_socketIO.wait()

if __name__ == '__main__':
    main()