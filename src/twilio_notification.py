import os
import time
import socket
import ast
import pickle

from twilio import TwilioRestException
from twilio.rest import TwilioRestClient

import postgres_functions

with open('twilio_credentials','r') as opened:
    twilio_credentials = ast.literal_eval(opened.read())

if os.path.exists('pickled_cache.pkl'):
    with open('pickled_cache.pkl','r') as opened:
        cache = pickle.load(opened)
else:
    cache = dict()

#known_types_events = postgres_functions.known_alert_types()

def is_it_safe():
    current_time = time.time()

    previous_alerts = cache.items()
    for alert in previous_alerts:
        if alert[0][2] == 'windspeed':
            if current_time - alert[1] > 30:
                send_message(*alert[0],message_body='Wind speeds are now below')
                cache.pop(alert[0],None)
        else:
            if current_time - alert[1] > 900:
                send_message(*alert[0],message_body='Lightning all clear within')
                cache.pop(alert[0],None)

def check_superfluous(alert):
    current_time = time.time()
    if cache.has_key(alert):
        cache[alert] = current_time
        return True
    else:
        cache[alert] = current_time
        return False

def send_message(event_name, number, alert_type, risk_level, already_sent=None, message_body = None):
    print event_name, number, alert_type, message_body, risk_level, time.gmtime()
    
    if message_body is None:
        if risk_level == 1:
            if (event_name,alert_type.split('_')[0]) in already_sent.keys():
                if already_sent[(event_name,alert_type.split('_')[0])] == 2:
                    return
        else:
            already_sent[(event_name,alert_type.split('_')[0])] = risk_level

    account_sid = twilio_credentials['account_sid'] # Your Account SID from www.twilio.com/console
    auth_token  = twilio_credentials['auth_token']  # Your Auth Token from www.twilio.com/console

    client = TwilioRestClient(account_sid, auth_token)
    risk_string = ''
    if risk_level == 2:
        risk_string = 'high risk'
    elif risk_level == 1:
        risk_string = 'low risk'

    try:
        if message_body is None:
            if 'lightning' in alert_type:
                dist_string = 'Lightning strike detected within'
                if risk_string == 'high risk':
                    dist_string = dist_string+' 8 miles '
                else:
                    dist_string = dist_string+' 20 miles '
            else:
                dist_string = "Winds above"
                if risk_string == 'high risk':
                    dist_string = dist_string+' 30 mph '
                else:
                    dist_string = dist_string+' 50 mph '

            actual_body = dist_string+'- '+event_name[:-1]

            message = client.messages.create(body=actual_body,
                to="+"+number,    # Replace with your phone number
                from_=twilio_credentials['number']) # Replace with your Twilio number
        else:
            dist_string = ''
            if 'lightning' in alert_type:
                if risk_string == 'high risk':
                    dist_string = ' 8 miles '
                elif risk_string == 'low risk':
                    dist_string = ' 20 miles '
            else:
                if risk_string == 'high risk':
                    dist_string = dist_string+' 30 mph '
                else:
                    dist_string = dist_string+' 50 mph '

            actual_body = message_body+dist_string+'- '+event_name[:-1]
            
            message = client.messages.create(body=actual_body,
                to="+"+number,    # Replace with your phone number
                from_=twilio_credentials['number']) # Replace with your Twilio number
    except TwilioRestException as e:
        print(e)

def main(): 
    while True:
        postgres_functions.update_alerts()
        all_alerts = postgres_functions.retrieve_alerts()
        all_alerts = sorted(all_alerts,key=lambda x: x[3],reverse=True)
        
        already_sent = dict()
        
        for alert in all_alerts:
            superfluous = False
            #if alert[2].startswith('lightning'):
            superfluous = check_superfluous(alert)
            
            if not superfluous:
                send_message(*alert, already_sent=already_sent)
        
        is_it_safe()

        with open('pickled_cache.pkl','w') as opened:
            pickle.dump(cache,opened)

        time.sleep(10)

        
if __name__ == '__main__':
    main()