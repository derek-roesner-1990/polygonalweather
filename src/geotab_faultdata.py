import time
import ast

import arrow
import mygeotab

'''
credentials_list = []
with open('geotab_credentials.txt', 'r') as opened:
    for line in opened.readlines()[1:10]:
    #for line in opened.readlines()[0:1]:
        credentials_list.append(ast.literal_eval(line))
'''
credentials_list = [{'username':'optimum','password':'Geotab123','database':'OPTIMUM_PILOT_DATABASE'}]

apis = []
for credentials in credentials_list:
    api = mygeotab.API(**credentials)
    counter = 0    
    while counter < 10:
        #if api.credentials.database == 'allina_health': # Until access is resolved
        #if api.credentials.database == 'yrc_freight': # Until access is resolved
        #    break
        try:
            print api.authenticate()
            apis.append(api)
            break
        except Exception as e:
            sentry.captureException()
            print e
            time.sleep(3)
            counter += 1

def main():
    current_time = arrow.utcnow().replace(days=-1)
    day = str(current_time).split('T')[0]
    for api in apis:
        faults = api.multi_call(*[("Get", {'typeName': "FaultData", 'search':{'fromDate':day}})])
        print faults


if __name__ == '__main__':
    main()