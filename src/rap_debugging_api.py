import socket
import arrow
import numpy as np
import glob

from dsRoadStatesTemporalSplit_withRoad_V4 import getBeliefValues
from dsRoadStatesTemporalSplit_withRoad_V4 import getForecastBeliefValues

from rasterio import Affine

import data_handler

from flask import Flask
application = Flask(__name__)

storage_path = '/home/souellet/polygonalweather/persistent/'

destination_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
world_to_grid = ~destination_transform

rx,ry = data_handler.get_coordinate_grid('RAP')

def fetch_data(lat,lon,desired_time='latest'):
    warning = None
    try:
        data = data_handler.get_layer('rap_ds_array', desired_time)
        
        #affine = data.get_any_data('HRRR:affine') #to use for projective geometry
        #crs = data.get_any_data('HRRR:crs')

        lat = float(lat)
        lon = float(lon)

        x_diff = np.abs(rx-lon)
        y_diff = np.abs(ry-lat)

        search_space = x_diff+y_diff

        if search_space.min() > 0.1:
            return {'error':"Data point is geographically too far from desired coordinates"}

        row, col = np.where(search_space == search_space.min())
        
        row = row[0]
        col = col[0]

        value = round(float(data[row,col]),2)

        return {'warning': warning, 'time': str(desired_time), 'value':value, 'distance_in_degrees_approximate':float(search_space.min())}
    except:
        return {'error':"Either data is missing or the time parameter is out of range"}

@application.route('/debugging/<lon>/<lat>/<desired_time>')
def fetch_values(lon,lat, desired_time='latest'):
    
    forecast_flag = False

    filepaths = sorted(glob.glob(storage_path+'rap_stacked_output*.npy'))

    if desired_time == 'latest':
        filepath = filepaths[-1]
    else:
        try:
            desired_time = arrow.get(desired_time)
        except:
            desired_time = arrow.get(int(desired_time)/1000)

        if desired_time > arrow.utcnow():
            filepaths = sorted(glob.glob(storage_path+'rap_forecast_output*.npy'))
            forecast_flag = True

        for i,filepath in enumerate(filepaths):
            file_time = arrow.get(filepath.split('_')[-1].split('.npy')[0])
            if file_time > desired_time:
                break

        if filepath != filepaths[-1]:
            index = i-1
            if index < 0:
                return 'No array found'
            filepath = filepaths[index]


        file_time = arrow.get(filepath.split('_')[-1].split('.npy')[0])
        if forecast_flag:
            if np.abs(desired_time.timestamp - file_time.timestamp) > 55*60:
                return 'No array found'
        else:    
            if np.abs(desired_time.timestamp - file_time.timestamp) > 25*60:
                return 'No array found'

    print filepath
    output_array = np.load(filepath)
    input_array = np.load(filepath.replace('output','input'))
    #binned_array = np.load(filepath.replace('input','output'))
    #binned_frozen_array = np.load(filepath.replace('input','output'))
    file_time = arrow.get(filepath.split('_')[-1].split('.npy')[0])


    x,y = world_to_grid*(float(lon),float(lat))
    y = int(round(y))
    x = int(round(x))
    if x < 0 or x >= 953:
        return 'Out of bounds'
    if y < 0 or y >= 834:
        return 'Out of bounds'
    values = input_array[y,x] # row then column
    ds_result = output_array[y,x]

    frozen_precip = 1

    if forecast_flag:
        if values[3] <= 273.16 and values[3] > 0: # see if precipType should be frozen
        #if values[0] == 4 or values[0] == 3 or (values[3] <= 273.16 and values[3] > 0):
            frozen_precip = 2

        values = list(values)+[frozen_precip]

        whole_belief_result = getForecastBeliefValues(values,whole_belief=True)
        keys=('catSnow', 'catRain','catFrz', 'catIcePellets', 'precip1HAccum', 'airtemp', 'radiation','precipaccum3hr', 'precipaccum6hr', 'roadTemperature','frozen_precip_bool')
        values = str(dict(zip(keys,values)))
        return '{}:{}:{}:{}:{}'.format(values,ds_result,whole_belief_result,(x,y),file_time)


    else:
        flagConditionCheck = (values[0] in [3,4] and (values[6] == 1 or values[8] == 1 or values[9] == 1))
        if (flagConditionCheck or (values[3] <= 273.16 and values[3] > 0)): # see if precipType should be frozen
        #if values[0] == 4 or values[0] == 3 or (values[3] <= 273.16 and values[3] > 0):
            frozen_precip = 2

        values = list(values)+[frozen_precip]

        whole_belief_result = getBeliefValues(values,whole_belief=True)
        keys=('precipflag', 'preciprate', 'precipaccum6Hr', 'air_temp', 'radiation', 'precipaccum1Hr', 'catSnow', 'catRain', 'catFrz', 'catIcePellets', 'precipaccum3hr', 'roadTemperature', 'humidity','frozen_precip_bool')
        values = str(dict(zip(keys,values)))
        return '{}:{}:{}:{}:{}'.format(values,ds_result,whole_belief_result,(x,y),file_time)

if __name__ == "__main__":
    if socket.gethostname() == 'ubuntu-cv1':
        application.run(host='192.168.5.116', port=4252, threaded=False)
    else:
        application.run(host='0.0.0.0', port=4252, threaded=False)
