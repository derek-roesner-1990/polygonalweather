from geoserver.catalog import Catalog
from geoserver.support import JDBCVirtualTable, JDBCVirtualTableGeometry, JDBCVirtualTableParam

#address = 'http://localhost:8080/geoserver/rest/'

config = {  'service_url': 'https://tiles.weathertelematics.com/geoserver/rest/', 
            'username': 'admin',
            'password': 'flu3ntn3ss',
            'disable_ssl_certificate_validation': True
        }

def create_style(style_name, style_xml):
    cat = Catalog(**config)
    cat.create_style(style_name, style_xml, overwrite=True)
