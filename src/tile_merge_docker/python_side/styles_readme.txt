Post to custom_style/layer_name/style_name

where layer_name is the name of the endpoint/layer, and style_name is an arbitrary name that you'll use to reference to the style when making requests later.

Post with a data field containing a json document with two fields, "keys" and "colours", both describing arrays of values. 

For wind speed, for example:
{"keys": '[10,15,20]', "colours" : '["#b99092", "#791047","#d353b4"]'}
The "keys" array are wind speed values that are respectively associated with the hexadecimal colour representations in the "colours" array.

More examples:
Visibility:
{"keys": '[3000,1000,500]', "colours" : '["#b99092", "#791047","#d353b4"]'}

Risk index:
{"keys": '["min_wet", "max_wet","min_snowy","max_snowy","min_icy","max_icy]', "colours" : '["#b99092", "#791047","#d353b4","#c36a2d","#a24a13","#632019"]'}