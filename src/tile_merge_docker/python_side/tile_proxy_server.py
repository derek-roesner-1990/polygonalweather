import json
import socket
import ast
import cStringIO
from urllib2 import urlopen
import requests
from os.path import exists
import imghdr
import time

from lxml import etree
import arrow
import PIL
from PIL import Image
from PIL import ImageFilter
import numpy as np
import redis
from flask import send_file
from flask import Flask
from flask import request
application = Flask(__name__)
application.use_x_sendfile = True

import tileconversion
import geoserver_style_manager

if socket.gethostname() == 'ubuntu-cv1':
    #database = StrictRedis(host='localhost', port=6389)
    cache = redis.StrictRedis(host='localhost', port=6379)
else:
    #database = StrictRedis(host='localhost', port=6379)
    cache = redis.StrictRedis(host='redis',port=6379)

with open('stormgeo_credentials.txt','r') as opened:
    stormgeo_credentials = ast.literal_eval(opened.read())

mercator = tileconversion.GlobalMercator()

base_wms_url = "https://tiles.weathertelematics.com/geoserver/wms?"
base_wms_request = 'SERVICE=WMS&REQUEST=GetMap&VERSION=1.3.0&LAYERS=wtx%3A{}&STYLES={}&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&CRS=EPSG%3A3857&BBOX={}'

fill_names = ['probabilityzones, road_cond']
contour_names = ['hail','ice','lit', 'qpe', 'refl', 'vis', 'wind', 'road_temp']
interpolated_names = ['risk_index']

#sld_types = ['contours', 'interpolated', 'filled']

@application.route('/what')
def whatis():
    return 'Yup'

@application.route('/custom_styled_tiles/<style_name>/<layer_name>/<tile1>/<tile2>/<tile3>.png')
def fetch_styled_tile(style_name,layer_name,tile1,tile2,tile3):
    wms_bounds = mercator.TileBounds(int(tile2),int(tile3),int(tile1))
    bounding_box = ','.join([str(item) for item in wms_bounds])
    query_string = base_wms_request.format(layer_name,style_name,bounding_box)

    geoserver_path_color = base_wms_url+query_string

    if layer_name in ['road_temperatures', 'probabilityzones', 'risk_index']:
        geoserver_path_geometry = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:larger_road_segments@EPSG:900913@png/'

        tile = "{0}/{1}/{2}.png".format(tile1,tile2,tile3)
        filename = "{0}-{1}-{2}-{3}-{4}.png".format(style_name,layer_name,tile1,tile2,tile3)
        filename_source = "{0}-{1}-{2}.png_source".format(tile1,tile2,tile3)
        
        request_destination = geoserver_path_color
        request_source = geoserver_path_geometry + tile

        return merge_tiles('{}-{}'.format(style_name,layer_name), filename, filename_source, request_source, request_destination)

    else:
        response = requests.get(geoserver_path_color)
        response_content = cStringIO.StringIO(response.content)
        response_content.seek(0)
        return send_file(response_content, mimetype='image/png')


def write_contours_sld(style_name, keys, colours, threshold_value='minimum_value'):
    ''' SLD for stroke-only polygons '''
    with open('base_style_template.xml') as opened:
        style_xml = opened.read()

    style_xml = style_xml.replace('name_to_replace', style_name)
    style_xml = style_xml.replace('vendor_option_to_replace', '<VendorOption name="sortBy">risk_level</VendorOption>')

    symbolizer = etree.Element('Stroke')

    stroke_width = etree.Element('CssParameter', attrib={'name':'stroke-width'})
    stroke_width.text = '1'
    symbolizer.append(stroke_width)

    css_parameter = etree.Element('CssParameter', attrib={'name':'stroke'})
    symbolizer.append(css_parameter)

    recode = etree.Element('ogc---Function', attrib={'name':'Recode'})
    
    css_parameter.append(recode)

    property_name = etree.Element('ogc---PropertyName')
    property_name.text = threshold_value

    recode.append(property_name)

    for key, colour in zip(keys,colours):
        literal_key = etree.Element('ogc---Literal') 
        literal_key.text = str(key)
        literal_colour = etree.Element('ogc---Literal') 
        literal_colour.text = str(colour)
        
        recode.append(literal_key)
        recode.append(literal_colour)

    style_xml = style_xml.replace('symbolizer_to_replace', etree.tostring(symbolizer).replace('---',':')) #The colon character is not allowed without proper namespacing, so using string replacement instead with --- 

    geoserver_style_manager.create_style(style_name, style_xml)

def write_interpolated_sld(style_name, keys, colours):
    with open('base_style_template.xml') as opened:
        style_xml = opened.read()

    style_xml = style_xml.replace('name_to_replace', style_name)
    style_xml = style_xml.replace('vendor_option_to_replace', '') # No sort necessary here

    symbolizer = etree.Element('Fill')
    css_parameter = etree.Element('CssParameter', attrib={'name':'fill'})
    
    symbolizer.append(css_parameter)

    interpolate = etree.Element('ogc---Function', attrib={'name':'Interpolate'})
    
    css_parameter.append(interpolate)

    property_name = etree.Element('ogc---PropertyName')
    property_name.text = 'risk_index'

    interpolate.append(property_name)

    mapping = {'min_wet':'1','max_wet':'10', 'min_snowy':'10.01', 'max_snowy':'20', 'min_icy':'20.01', 'max_icy':'31'}

    for key, colour in zip(keys,colours):
        literal_key = etree.Element('ogc---Literal') 
        literal_key.text = str(mapping[key])
        literal_colour = etree.Element('ogc---Literal') 
        literal_colour.text = str(colour)
        
        interpolate.append(literal_key)
        interpolate.append(literal_colour)

    method = etree.Element('ogc---Literal')
    method.text = 'color'
    interpolate.append(method)

    style_xml = style_xml.replace('symbolizer_to_replace', etree.tostring(symbolizer).replace('---',':')) #The colon character is not allowed without proper namespacing, so using string replacement instead with --- 

    geoserver_style_manager.create_style(style_name, style_xml)

def write_fill_sld(style_name, keys, colours):
    ''' Creates an XML for filled polygons and pushes it to geoserver as a style '''
    with open('base_style_template.xml') as opened:
        style_xml = opened.read()

    style_xml = style_xml.replace('name_to_replace', style_name)
    style_xml = style_xml.replace('vendor_option_to_replace', '') # No sort necessary here

    symbolizer = etree.Element('Fill')
    css_parameter = etree.Element('CssParameter', attrib={'name':'fill'})
    
    symbolizer.append(css_parameter)

    recode = etree.Element('ogc---Function', attrib={'name':'Recode'})
    
    css_parameter.append(recode)

    string_trim = etree.Element('ogc---Function', attrib={'name':'strTrim'})
    
    recode.append(string_trim)

    property_name = etree.Element('ogc---PropertyName')
    property_name.text = 'zone_type'

    string_trim.append(property_name)
    
    for key, colour in zip(keys,colours):
        literal_key = etree.Element('ogc---Literal') 
        literal_key.text = str(key)
        literal_colour = etree.Element('ogc---Literal') 
        literal_colour.text = str(colour)
        
        recode.append(literal_key)
        recode.append(literal_colour)

    style_xml = style_xml.replace('symbolizer_to_replace', etree.tostring(symbolizer).replace('---',':')) #The colon character is not allowed without proper namespacing, so using string replacement instead with --- 

    geoserver_style_manager.create_style(style_name, style_xml)

def choose_sld_template(layer_name, style_name, keys, colours):
    ''' Transforms an user-defined color palettes into an SLD file pushed to geoserver '''

    if layer_name in fill_names:
        write_fill_sld(style_name, keys, colours)

    elif layer_name in contour_names:
        if layer_name == 'vis':
            threshold_value = 'maximum_value'
        else:
            threshold_value = 'minimum_value'
        write_contours_sld(style_name, keys, colours, threshold_value=threshold_value)
    
    elif layer_name in interpolated_names:
        write_interpolated_sld(style_name, keys, colours)

    pass

@application.route('/custom_style/<layer_name>/<style_name>',methods=['POST'])
def define_custom_style(layer_name, style_name):
    ''' JSON should have two fields: colours (hex), keys (in the weather relevant unit) '''
    try:
        keys = json.loads(request.form.get('keys'))
        colours = json.loads(request.form.get('colours'))
        print keys, colours
        choose_sld_template(layer_name, style_name, keys, colours)
        return 'Style successfully added'
    except Exception as e:
        print e
        return 'Style could not be added, here is an error message to forward: {}'.format(e)

@application.route('/geoserver/wms')
def passthrough_query_own_wms():
    response = requests.get(base_wms_url+request.query_string)
    response_content = cStringIO.StringIO(response.content)
    response_content.seek(0)
    return send_file(response_content, mimetype='image/png')
    #return response.content

@application.route('/geoserver/gwc/<path:path>')
def passthrough_query_own_tms(path):
    response = requests.get("https://tiles.weathertelematics.com/geoserver/gwc/"+path)
    response_content = cStringIO.StringIO(response.content)
    response_content.seek(0)
    return send_file(response_content, mimetype='image/png')
    #return response.content

@application.route('/passthrough/wms')
def passthrough_query():
    response = requests.get("http://geo.stormgeo.com/wms?"+request.query_string, auth=(stormgeo_credentials['username'],stormgeo_credentials['password']))
    return response.content

def adjust_time(desired_time):
    current_time = desired_time
    hour_time = current_time.floor('hour')
    lowest_bound = hour_time.replace(minutes=int(current_time.minute/10)*10)
    highest_bound = lowest_bound.replace(minutes=59)
    time_field = 'TIME={}Z/{}Z'.format(str(lowest_bound)[0:-6], str(highest_bound)[0:-6])
    return time_field

@application.route('/custom_wms_slider/<layer_name>')
def fetch_wms_tiles(layer_name):
    if layer_name == 'ecmwf_live_risk':
        road_source = 'planet_osm_motorways'
    else:
        road_source = 'larger_road_segments'
        
    wms_url = 'https://tiles.weathertelematics.com/geoserver/wms?'
    query_string = request.query_string
    query_string += '&TILED=true'

    query_string_splitted = query_string.split('&')
    time_string = [line for line in query_string_splitted if 'TIME' in line][0]
    #print time_string
    converted = arrow.get(time_string[5:-1].replace('%2d','-').replace('%3a',':').replace('%2e','.'))
    
    if layer_name != 'ecmwf_live_risk':

        not_floored = arrow.utcnow()
        floored = not_floored.floor('hour').replace(minutes=int(not_floored.minute/10)*10)
        
        if converted.timestamp - floored.timestamp >= 599:
            new_time_field = adjust_time(converted)
            rebuilt_query_string = '?SERVICE=WMS'
            for line in query_string_splitted:
                if 'TIME' in line:
                    rebuilt_query_string += '&'+new_time_field
                else:
                    rebuilt_query_string += '&'+line

            query_string = rebuilt_query_string
        #print '{}Z'.format(str(arrow.utcnow())[0:-6]), converted

    else:
        while converted.hour % 3 != 0:
            converted = converted.replace(hours=-1)

        hour_floored = converted.floor('hour')
        new_time_field = 'TIME={}Z'.format(str(hour_floored)[0:-6])

        rebuilt_query_string = '?SERVICE=WMS'
        for line in query_string_splitted:
            if 'TIME' in line:
                rebuilt_query_string += '&'+new_time_field
            else:
                rebuilt_query_string += '&'+line

        query_string = rebuilt_query_string

        
    relevant = [line for line in query_string_splitted if 'TIME' not in line]
    query_string_without_time ='&'.join(relevant)
    roads_query = query_string_without_time.replace(layer_name,road_source)
    #query_string_without_time = query_string_splitted[0]+'Z&CRS'+''.join(query_string_splitted[1].split('Z&')[1])
    
    request_destination = wms_url+query_string
    request_source = wms_url+roads_query

    filename = '{}-{}'.format(layer_name,query_string)
    filename_source = roads_query+'-source'
    #print query_string_without_time
    #return requests.get(request_destination).content
    return merge_tiles(layer_name, filename, filename_source, request_source, request_destination)

@application.route('/internal/cache_control/<layer_name>/clean_that_layer')
def clean_cache(layer_name):    
    entries = cache.smembers(layer_name+'-index')
    if len(entries) == 0:
        return 'Already empty'

    pipeline = cache.pipeline()
    pipeline.delete(layer_name+'-index')
    pipeline.delete(*entries)
    pipeline.execute()
    return 'Done'

def merge_tiles(layer_name, filename, filename_source, request_source, request_destination, image_format='png'):
    cached = cache.get(filename)
    if cached is not None:
        if cached == '':
            return ''
        to_send = cStringIO.StringIO(cached)
        return send_file(to_send, mimetype='image/{}'.format(image_format))

    destination_image = requests.get(request_destination)
    if not destination_image.ok:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''
    
    destination_image = cStringIO.StringIO(destination_image.content)
    
    destination = np.array(Image.open(destination_image))
    
    if destination[:,:,3].sum() == 0:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''

    source_image = cache.get(filename_source)
    if source_image is None:
        source_image = requests.get(request_source)
        source_image = cStringIO.StringIO(source_image.content)
        cache.set(filename_source,source_image.getvalue())
    else:
        source_image = cStringIO.StringIO(source_image)

    source = np.array(Image.open(source_image))
    
    destination[:,:,3] = source[:,:,3]

    indices = np.where(destination[:,:,2] == 255)

    if len(indices[0]) > 65535:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''
    
    destination[indices[0],indices[1],3] = 0

    result = Image.fromarray(destination,mode='RGBA')
    to_send = cStringIO.StringIO()
    result.save(to_send,'{}'.format(image_format).upper())
    to_send.seek(0)
    
    cache.set(filename,to_send.getvalue())
    cache.sadd(layer_name+'-index',filename)

    return send_file(to_send, mimetype='image/{}'.format(image_format))

@application.route('/custom_tiles/<layer_name>/<tile1>/<tile2>/<tile3>.png')
def fetch_tile(tile1,tile2,tile3,layer_name):
    high_lat = 71.0
    low_lat = 24.5
    high_lon = -48.5
    low_lon = -169.0
    
    bounds = mercator.TileLatLonBounds(int(tile2), int(tile3), int(tile1))
    world_tiles = False
    if min(bounds[0],bounds[2]) < low_lat:
        world_tiles = True
    if max(bounds[0],bounds[2]) > high_lat:
        world_tiles = True
    if max(bounds[1],bounds[3]) > high_lon:
        world_tiles = True
    if max(bounds[1],bounds[3]) < low_lon:
        world_tiles = True

    #geoserver_path_color = 'http://192.168.5.116:8080/geoserver/gwc/service/tms/1.0.0/wtx:{0}@EPSG:900913@png/'.format(layer_name)
    geoserver_path_color = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:{0}@EPSG:900913@png/'.format(layer_name)
    #geoserver_path_geometry = 'http://192.168.5.116:8080/geoserver/gwc/service/tms/1.0.0/wtx:larger_road_segments@EPSG:900913@png/'
    if world_tiles:
        geoserver_path_geometry = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:planet_osm_motorways@EPSG:900913@png/'
        filename_source = "{0}-{1}-{2}.png_source_world".format(tile1,tile2,tile3)
    else:
        geoserver_path_geometry = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:larger_road_segments@EPSG:900913@png/'
        filename_source = "{0}-{1}-{2}.png_source".format(tile1,tile2,tile3)

    tile = "{0}/{1}/{2}.png".format(tile1,tile2,tile3)
    filename = "{0}-{1}-{2}-{3}.png".format(layer_name,tile1,tile2,tile3)
    #filename_source = "{0}-{1}-{2}.png_source".format(tile1,tile2,tile3)
    
    request_destination = geoserver_path_color + tile
    request_source = geoserver_path_geometry + tile

    return merge_tiles(layer_name, filename, filename_source, request_source, request_destination)
    
@application.route('/custom_tiles_world/<layer_name>/<tile1>/<tile2>/<tile3>.png')
def fetch_tile_world(tile1,tile2,tile3,layer_name):
    
    #geoserver_path_color = 'http://192.168.5.116:8080/geoserver/gwc/service/tms/1.0.0/wtx:{0}@EPSG:900913@png/'.format(layer_name)
    geoserver_path_color = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:{0}@EPSG:900913@png/'.format(layer_name)
    #geoserver_path_geometry = 'http://192.168.5.116:8080/geoserver/gwc/service/tms/1.0.0/wtx:larger_road_segments@EPSG:900913@png/'
    geoserver_path_geometry = 'https://tiles.weathertelematics.com/geoserver/gwc/service/tms/1.0.0/wtx:planet_osm_motorways@EPSG:900913@png/'

    tile = "{0}/{1}/{2}.png".format(tile1,tile2,tile3)
    filename = "{0}-{1}-{2}-{3}.png".format(layer_name,tile1,tile2,tile3)
    filename_source = "{0}-{1}-{2}.png_source_world".format(tile1,tile2,tile3)
    
    request_destination = geoserver_path_color + tile
    request_source = geoserver_path_geometry + tile

    cached = cache.get(filename)
    if cached is not None:
        if cached == '':
            return ''
        to_send = cStringIO.StringIO(cached)
        return send_file(to_send, mimetype='image/png')

    destination_image = requests.get(request_destination)
    if not destination_image.ok:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''
    
    destination_image = cStringIO.StringIO(destination_image.content)
    
    destination = np.array(Image.open(destination_image))
    
    if destination[:,:,3].sum() == 0:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''

    source_image = cache.get(filename_source)
    if source_image is None:
        source_image = requests.get(request_source)
        source_image = cStringIO.StringIO(source_image.content)
        cache.set(filename_source,source_image.getvalue())
    else:
        source_image = cStringIO.StringIO(source_image)

    source = np.array(Image.open(source_image))
    
    destination[:,:,3] = source[:,:,3]

    indices = np.where(destination[:,:,2] == 255)

    if len(indices[0]) > 65535:
        cache.set(filename,'')
        cache.sadd(layer_name+'-index',filename)
        return ''
    
    destination[indices[0],indices[1],3] = 0

    result = Image.fromarray(destination,mode='RGBA')
    to_send = cStringIO.StringIO()
    result.save(to_send,'PNG')
    to_send.seek(0)
    
    cache.set(filename,to_send.getvalue())
    cache.sadd(layer_name+'-index',filename)

    return send_file(to_send, mimetype='image/png')


if __name__ == "__main__":
    application.run(host='192.168.2.43',port=4554,threaded=True)