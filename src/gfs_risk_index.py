import ast
import time

#from matplotlib import pyplot
import numpy as np
#from scipy import ndimage
import arrow
import rasterio
from rasterio import Affine
from rasterio import features
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()

import postgres_functions
import data_handler
import alarms_and_triggers

import gfs_ds
from gfs_ds import buffer_factor, buffer_divisor, simplification, down_factor

ds_ranges = [[10, 10.5], [10.5, 11], [20, 20.5], [20.5, 21]]
risk_squashes = [[1.0, 3.0], [4.0, 10.0], [12.0, 14.0], [15.0, 20.0]]

#speed_mapping = {0:1.0, 10:0.93, 20:0.81, 30:0.65} Created the piecewise linear below from it
speed_mapping = [ 1.0, 0.993,  0.986,  0.979,  0.972,  0.965,  0.958,  0.951,  0.944,
        0.937,  0.93 ,  0.918,  0.906,  0.894,  0.882,  0.87 ,  0.858,
        0.846,  0.834,  0.822,  0.81 ,  0.794,  0.778,  0.762,  0.746,
        0.73 ,  0.714,  0.698,  0.682,  0.666,  0.65 ]

import calculate_road_risk_index

def get_risk_score(mini, maxi, a, b, x):
    return ((((b - a)*(x - mini)) / (maxi - mini)) + a)

def calculate_risk(score):
    risk = None
    for arange, squash in zip(ds_ranges, risk_squashes):
        if score < 10:
            risk = 0
            break
        elif score > 30:
            risk = 30
            break
        if score > arange[0] and score <= arange[1]:
            risk = get_risk_score(arange[0], arange[1], squash[0], squash[1], score)
        
    return risk

def transform(lon_grid,lat_grid,shapes):
    all_shapes = []
    for shape in shapes:
        new_coordinates = []
        for coordinate in shape[0]['coordinates'][0]:
            new_lon = lon_grid[coordinate[::-1]]
            new_lat = lat_grid[coordinate[::-1]]
            new_coordinates.append((new_lon,new_lat))
        
        shape[0]['coordinates'][0] = new_coordinates
        all_shapes.append(shape)
    
    return all_shapes

def extend_grids(grids):
    grids_to_return = []
    for grid in grids:
        row_diff = grid[-1,:]-grid[-2,:]
        new_row = grid[-1,:]+row_diff
        
        grid = np.vstack([grid,new_row[None,:]])

        col_diff = grid[:,-1]-grid[:,-2]
        new_col = grid[:,-1]+col_diff

        grid = np.hstack([grid,new_col[:,None]])

        grids_to_return.append(grid)

    return grids_to_return

def polygonize(array):
    bins = range(1,21)+[30]
    #array = ndimage.interpolation.zoom(array,5)
    #array = ndimage.gaussian_filter(array,sigma=0.1)
    #pyplot.imshow(array,cmap='inferno')
    #pyplot.show()

    lon_grid,lat_grid = data_handler.get_coordinate_grid('GFS')
    
    #lon_grid = lon_grid[29:631,149:-49] # Fixing looping around of longitude values
    #lat_grid = lat_grid[29:631,149:-49]
    #array = array[30:630,150:-50]

    digitized = np.digitize(array,bins).astype(np.int32)
    
    mask = digitized > 0
    
    shapes = rasterio.features.shapes(digitized,mask=mask)
    lon_grid, lat_grid = extend_grids([lon_grid, lat_grid])

    shapes = transform(lon_grid, lat_grid, shapes)

    polygons = [[] for _ in bins]+[[],[]]
    
    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0]).buffer(0) # avoids self-intersection GEOS bug
        polygons[int(shape[1])-1].append(polygon)
    
    polygons = [ops.unary_union(polygon) for polygon in polygons]
    return zip(polygons,bins)

def create_new_zones(source_data,forecast_time=0, minutes=False):

    gfs_ds.main(forecast_time=forecast_time, minutes=minutes)

    ds_array = data_handler.get_layer(source_data,'latest')
    
    vectorized_calculate_risk = np.vectorize(calculate_risk)
    
    risk_array = vectorized_calculate_risk(ds_array)
    
    #x,y = data_handler.get_coordinate_grid('MRMS')
    #x = x[::down_factor, ::down_factor]
    #y = y[::down_factor, ::down_factor]

    #polygons = polygonize(x,y,risk_array)
    polygons = polygonize(risk_array)

    current_time = arrow.utcnow().replace(hours=+forecast_time)

    zones = []
    for i,polygon in enumerate(polygons):
        risk_index = polygon[1]
        
        ignoring_type = (risk_index%10)/10.0 # Bring it down between 0 and 1
        if ignoring_type == 0:
            ignoring_type = 1.0
            
        zone = {'risk_index':risk_index,'risk_index_normalized':ignoring_type, 'shape':polygon[0], 'forecast_time':forecast_time, 'valid_time':current_time, 'risk_and_type':risk_index}
        zone['speed_factor'] = speed_mapping[int(polygon[1])]
        
        #if risk_index < 20:
        #    zone['condition_type'] = 'wet'
        #elif risk_index < 30:
        #    zone['condition_type'] = 'snowy'
        #elif risk_index >= 30:
        #    zone['condition_type'] = 'icy'

        # the conditional block below will use the risk scores
        if risk_index in range(1, 12):
            zone['condition_type'] = 'wet'
        elif risk_index in range(12, 21):
            zone['condition_type'] = 'snowy'
        elif risk_index >= 30:
            zone['condition_type'] = 'icy'
        else:
            print "No match: " + str(risk_index)

        print zone['shape'].area
        zones.append(zone)

    #postgres_functions.push_rap_road_conditions(zones)

    #alarms_and_triggers.clean_cache('current_risk_polygons')
    #alarms_and_triggers.geoserver_trigger('current_risk_polygons')

    print 'Done!'
    return zones

def combine_geolayers(na_geolayers, geolayers):
    final_geolayers = []

    polygons = postgres_functions.get_road_temperature_polygons()
    shapes = [wkb.loads(polygon[-1],hex=True) for polygon in polygons]

    na_convex_hull = shapely.geometry.MultiPolygon(shapes).convex_hull

    for poly in geolayers:
        geom = poly['shape']
        new_geom = geom.difference(na_convex_hull)
        poly['shape'] = new_geom
        #poly['risk_index'] = poly['risk_and_type']
        poly['risk_index'] = poly['risk_index_normalized'] # SAVE THE NORMALIZED VALUES
        poly['zone_type'] = poly['condition_type'][0].upper()

        final_geolayers.append(poly)

    for na_geolayer in na_geolayers:
        na_risk_index = na_geolayer['risk_index']

        # logic fixed
        if na_risk_index in range(1, 12):
            na_geolayer['condition_type'] = 'wet'
        elif na_risk_index in range(12, 21):
            na_geolayer['condition_type'] = 'snowy'
        elif na_risk_index >= 30:
            na_geolayer['condition_type'] = 'icy'

        #if na_risk_index < 20:
        #    na_geolayer['condition_type'] = 'wet'
        #elif na_risk_index < 30:
        #    na_geolayer['condition_type'] = 'snowy'
        #elif na_risk_index >= 30:
        #    na_geolayer['condition_type'] = 'icy'

        na_geolayer['risk_and_type'] = na_risk_index

        # EDIT TO SAVE THE NORMALIZED VALUE AS THE ACTUAL RISK
        normalzied_val = (na_risk_index%10)/10.0 # Bring it down between 0 and 1
        if normalzied_val == 0:
            normalzied_val = 1.0
        na_geolayer['risk_index'] = normalzied_val


        na_geolayer['valid_time'] = arrow.get(na_geolayer['valid_time'])     
        na_geolayer['zone_type'] = na_geolayer['condition_type'][0].upper()

    final_geolayers.extend(na_geolayers)

    return final_geolayers

def main():
    na_zones = calculate_road_risk_index.main(intermediate=True)
    zones = []
    
    zones.extend(create_new_zones('gfs_ds_array'))
    for hours_ahead in range(1,7):
        zones.extend(create_new_zones('gfs_ds_array_f{}'.format(hours_ahead),forecast_time=hours_ahead))
    

    zones = combine_geolayers(na_zones, zones)
    #for minutes_ahead in range(15,46,15):
    #    zones.extend(create_new_zones('rap_ds_array_f{}m'.format(minutes_ahead),forecast_time=minutes_ahead,minutes=True))
    
    postgres_functions.push_gfs_road_conditions(zones)

    for hours_ahead in range(0,7):
        alarms_and_triggers.clean_cache('worldwide_risk_index_f0{}'.format(hours_ahead))
        alarms_and_triggers.geoserver_trigger('worldwide_risk_index_f0{}'.format(hours_ahead))


if __name__ == '__main__':
    main()