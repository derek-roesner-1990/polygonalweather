import socket
import ast
import traceback
import ftplib
import glob
import sys
import gzip
import sys
from urllib import urlretrieve
import arrow
import time
import os
import multiprocessing

import numpy as np
import requests
from bs4 import BeautifulSoup

gfs_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/'

def get_gfs_products():
    try:
        current_time = arrow.utcnow().floor('hour')

        req = requests.get(gfs_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('gfs.')])
        link = links[-2]

        print "link: " + str(link)

        req = requests.get(gfs_url+link)
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        
        relevant_dirlinks = []

        timestamps = ['f00{}'.format(forecast_time) for forecast_time in range(1,10)]+['f0{}'.format(forecast_time) for forecast_time in range(10,17)]

        print "timestamps: " + str(timestamps)

        for timestamp in timestamps:
            found_a = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('pgrb2.0p25.{}'.format(timestamp))]) # file is split in two
            relevant_dirlinks.append(found_a[-1])

            #Useless for now
            #found_b = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith('pgrb2b.0p25.{}'.format(timestamp))])
            #relevant_dirlinks.append(found_b[-1])
        
        for dirlink in relevant_dirlinks:
            fileurl = gfs_url+link+dirlink
            print "link[:-1]" + str(link[:-1])
            yearmonthdayhour = link[:-1].split('.')[1]
            print "yearmonthdayhour: " + str(yearmonthdayhour)
            year = int(yearmonthdayhour[0:4])
            month = int(yearmonthdayhour[4:6])
            day = int(yearmonthdayhour[6:8])
            hour = int(yearmonthdayhour[8:10])
            extra_hour = int(dirlink.split('.')[-1][1:])
            hour = hour+extra_hour
            if hour > 23:
                hour = hour % 23
                day += 1

            # hour = str(hour)
            # if len(hour) < 2:
            #     hour = '0'+hour

            file_time = "{}-{:02d}-{:02d}T{:02d}:00:00".format(year,month,day,hour)

            if arrow.get(file_time) == current_time: 
                print fileurl
                urlretrieve(fileurl, '/home/souellet/gfs_archive/{}_{}'.format(link[:-1],dirlink))
                return True
            
            #filepath, already_there = acquire_file(fileurl, dirlink, storage_path)
            
    except Exception as e:
        traceback.print_exc()
        return False

if __name__ == '__main__':
    while True:
        a = time.clock()
        if not get_gfs_products():
            print arrow.utcnow()
            #get_gfs_products()
        difference = time.clock()-a
        time.sleep(3600-difference)
