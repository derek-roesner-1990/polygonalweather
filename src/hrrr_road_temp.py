import numpy as np
from matplotlib import pyplot 
import Nio
import xgboost

import live_feed_functions

bst = xgboost.Booster()
bst.load_model('../models/xgboost_roadtemp_hrrr_21var_noelevation')

hrrr_corresponding = ['PRATE_P0_L1_GLC0',
 'None',
 'None',
 'None',
 'HPBL_P0_L1_GLC0',
 'None',
 'LTNG_P0_L1_GLC0',
 'UGRD_P0_L103_GLC0',
 'None',
 'None',
 'None',
 'DSWRF_P0_L1_GLC0',
 'None',
 'None',
 'None',
 'None',
 'ULWRF_P0_L8_GLC0',
 'None',
 'None',
 'None',
 'CAPE_P0_2L108_GLC0',
 'None',
 'GUST_P0_L1_GLC0',
 'None',
 'None',
 'None',
 'None',
 'None',
 'TMP_P0_L1_GLC0',
 'None',
 'None',
 'None',
 'None',
 'TMP_P0_L103_GLC0',
 'None',
 'None',
 'CIN_P0_2L108_GLC0',
 'SPFH_P0_L103_GLC0',
 'None',
 'VGTYP_P0_L1_GLC0',
 'None',
 'WEASD_P0_L1_GLC0',
 'None',
 'None',
 'None',
 'WEASD_P8_L1_GLC0_acc',
 'None',
 'None',
 'SNOD_P0_L1_GLC0',
 'None',
 'ICEC_P0_L1_GLC0',
 'HGT_P0_L1_GLC0',
 'None',
 'PWAT_P0_L200_GLC0',
 'VGRD_P0_L103_GLC0',
 'None']

def get_road_temp_grid():
	bunch_arrays = []
	filepath = live_feed_functions.hrrproducts()
	opened = Nio.open_file(filepath)
	for variable in hrrr_corresponding:
	    if variable == 'None':
	        continue
	    current_array = opened.variables[variable].get_value()

	    if len(current_array.shape) > 2:
	        current_array = current_array[0,:,:]

	    bunch_arrays.append(current_array)

	hrrr_one_file = np.dstack(bunch_arrays)

	hrrr_one_file = np.flipud(hrrr_one_file)

	rows = []
	for i,row in enumerate(hrrr_one_file):
	    dtest = xgboost.DMatrix(row,missing=-999999)
	    rows.append(bst.predict(dtest))
	output = np.vstack(rows)
	pyplot.imshow(output[50:400,150:400],cmap='inferno')
	pyplot.show()

if __name__ == '__main__':
	get_road_temp_grid()