
# coding: utf-8

# In[33]:

'''
This file produces a set of belief values for a road pertaining to visibility.
Possible values are C (Clear), P (Partial) and B (Bad)
Input is a weather string as defined by NDFD standards.
Output is a set of belief values for the road defined by the weather string.
Usage:
Call function getCollectiveBeliefValues(<weather_string>)
'''


# In[34]:

from __future__ import print_function
from pyds import MassFunction
from itertools import product
import math


# In[35]:

'''
Function for handling the different types of coverage
<NoCov>	No Coverage / probability	Iso	Isolated
Sct	Scattered	Num	Numerous
Wide	Widespread	Ocnl	Occasional
SChc	Slight Chance	Chc	Chance of
Lkly	Likely	Def	Definite
Patchy	Patchy	Areas	Areas of
Added 8/13/2004
Pds	Periods of	Frq	Frequent
Inter	Intermittent	Brf	Brief
'''
def getCoverageId(coverage):
    noProb = ['<NoCov>']
    highProb = ['Wide', 'Num', 'Lkly', 'Def', 'Frq']
    lowProb = ['SChc', 'Patchy', 'Brf']
    medProb = ['Ocnl', 'Chc', 'Areas', 'Pds', 'Inter']
    
    if coverage in noProb:
        return 0
    elif coverage in highProb:
        return 1
    elif coverage in lowProb:
        return 2
    elif coverage in medProb:
        return 3
    else:
        return -1

coverageMasses = []
coverageMasses.append(MassFunction({"C":0.75, "P":0.05, "B":0.00, "CP":0.05,"PB":0.0, "CPB":0.15}))
coverageMasses.append(MassFunction({"C":0.00, "P":0.05, "B":0.85, "CP":0.0,"PB":0.05, "CPB":0.05}))
coverageMasses.append(MassFunction({"C":0.5, "P":0.25, "B":0.05, "CP":0.1,"PB":0.05, "CPB":0.05}))
coverageMasses.append(MassFunction({"C":0.25, "P":0.5, "B":0.05, "CP":0.1,"PB":0.05, "CPB":0.05}))


# In[36]:

'''
Function for handling the different types of weather
<NoWx>	No Weather	K	Smoke
BD	Blowing Dust	BS	Blowing Snow
H	Haze	F	Fog
L	Drizzle	R	Rain
RW	Rain Showers	
FR	Frost	ZL	Freezing Drizzle
ZR	Freezing Rain	IP	Ice Pellets (sleet)
S	Snow	SW	Snow Showers
T	Thunderstorms		
Added 1/20/2004
BN	Blowing Sand	ZF	Freezing Fog
IC	Ice Crystals	IF	Ice Fog
VA	Volcanic Ash	ZY	Freezing Spray
WP	Water Spouts		
'''

def getWeatherId(weather):
    noWx = ['<NoWx>', 'FR'] # No impact on visibility
    badWx = ['F', 'BS', 'K', 'BD', 'BN', 'ZF', 'IF', 'VA', 'ZY', 'WP'] # Guaranteed bad visibility, irrespective of intensity
    medWx = ['R', 'ZR', 'IP', 'S', 'T', 'H'] # Moderate to heavy precip weather
    lightWx = ['L', 'RW', 'ZL', 'SW'] # light precip

    
    if weather in noWx:
        return 0
    elif weather in badWx:
        return 1
    elif weather in medWx:
        return 2
    elif weather in lightWx:
        return 3
    else:
        return -1
        
weatherMasses = []
weatherMasses.append(MassFunction({"C":0.75, "P":0.05, "B":0.00, "CP":0.05,"PB":0.0, "CPB":0.15}))
weatherMasses.append(MassFunction({"C":0.00, "P":0.05, "B":0.85, "CP":0.0,"PB":0.05, "CPB":0.05}))
weatherMasses.append(MassFunction({"C":0.00, "P":0.25, "B":0.55, "CP":0.00,"PB":0.1, "CPB":0.1}))
weatherMasses.append(MassFunction({"C":0.05, "P":0.45, "B":0.25, "CP":0.05,"PB":0.15, "CPB":0.05}))


# In[37]:

'''
Function for handling the different types of intensity
<NoInten>	No Intensity	--	Very Light
-	Light	m	Moderate
+	Heavy		
'''
def getIntensityId(intensity):
    noInt = ['<NoInten>'] # No impact on visibility
    medInt = ['--', '-'] # lightish intensity
    heavyInt = ['m', '+'] # heavy intensity

    if intensity in noInt:
        return 0
    elif intensity in medInt:
        return 1
    elif intensity in heavyInt:
        return 2
    else:
        return -1
        
intensityMasses = []
intensityMasses.append(MassFunction({"C":0.75, "P":0.05, "B":0.00, "CP":0.05,"PB":0.0, "CPB":0.15}))
intensityMasses.append(MassFunction({"C":0.2, "P":0.6, "B":0.00, "CP":0.15,"PB":0.0, "CPB":0.05}))
intensityMasses.append(MassFunction({"C":0.00, "P":0.05, "B":0.85, "CP":0.0,"PB":0.05, "CPB":0.05}))


# In[38]:

'''
Function for handling visibility readings
<NoVis>, 0SM, 1/4SM, 1/2SM, 3/4SM, 1SM, 11/2SM, 2SM, 21/2SM, 3SM, 4SM, 5SM, 6SM, P6SM
'''

def getVisibilityId(visibility):
    noVis = ['<NoVis>'] # missing
    poorVis = ['0SM', '1/4SM', '1/2SM', '3/4SM', '1SM', '11/2SM']
    medVis = ['21/2SM', '3SM', '4SM', '5SM', '6SM']
    heavyVis = ['P6SM']

    if visibility in noVis:
        return 0
    elif visibility in poorVis:
        return 1
    elif visibility in medVis:
        return 2
    elif visibility in heavyVis:
        return 3
    else:
        return -1
        
visibilityMasses = []
visibilityMasses.append(MassFunction({"C":0.0, "P":0.0, "B":0.00, "CP":0.0,"PB":0.0, "CPB":1}))
visibilityMasses.append(MassFunction({"C":0.0, "P":0.0, "B":.95, "CP":0.15,"PB":0.0, "CPB":0.05}))
visibilityMasses.append(MassFunction({"C":0.00, "P":0.95, "B":0.0, "CP":0.0,"PB":0.0, "CPB":0.05}))
visibilityMasses.append(MassFunction({"C":0.95, "P":0.0, "B":0.0, "CP":0.0,"PB":0.0, "CPB":0.05}))


# In[39]:

'''
assume vector has following format:
(precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr)
'''
def getBeliefValues(weatherConditions): 
    visibilityState = 0

    visibilityState = coverageMasses[getCoverageId(weatherConditions[0])]     & weatherMasses[getWeatherId(weatherConditions[1])]     & intensityMasses[getIntensityId(weatherConditions[2])]     & visibilityMasses[getVisibilityId(weatherConditions[3])]                         
    return visibilityState    


# In[40]:

'''
Function to parse a weather string.
Each string has at most 5 weather words, separated by ^
Each word has 5 pieces separated by :
For this implementation, ignore the last piece (Hazard)
Function returns a list of lists
'''
def parseWeatherString(weatherString):
    words = [] # this is a list of lists that holds all the weather words
    
    splitWords = weatherString.split("^") # get all the individual words
    
    for aWord in splitWords:
        splitPieces = aWord.split(":")
        words.append(splitPieces[0:4]) # ignore the last piece as it is typically absent/not used
        
    return words
    


# In[41]:

'''
Function to obtain collective beliefs of multiple words.
For this version, just combine them using DS again.
Input is a weather string.
First parse it using parseWeatherString
Then obtain beliefs for each parsed word
Combine them using DS theory to get collective belief
'''

def getCollectiveBeliefValues(weatherString):
    weatherWords = parseWeatherString(weatherString) # get a list of parsed weather words (list of lists)
    
    if (len(weatherWords) == 1): # only one word
        return (getBeliefValues(weatherWords[0]))
    else:
        wordBeliefs = [] # a list of lists to hold the beliefs for each word converted to mass functions
        
        for weatherWord in weatherWords:
            beliefValues = getBeliefValues(weatherWord) # return value is a dictionary
            wordBeliefs.append(MassFunction(beliefValues)) # convert the beliefs for that word to a mass function
        
        visibilityBelief = 0 # the value of the final combined beliefs
        
        if (len(wordBeliefs) == 2):
            visibilityBelief = wordBeliefs[0] & wordBeliefs[1]
        elif (len(wordBeliefs) == 3):
            visibilityBelief = wordBeliefs[0] & wordBeliefs[1] & wordBeliefs[2]
        elif (len(wordBeliefs) == 4):
            visibilityBelief = wordBeliefs[0] & wordBeliefs[1] & wordBeliefs[2] & wordBeliefs[3]
        elif (len(wordBeliefs) == 5):
            visibilityBelief = wordBeliefs[0] & wordBeliefs[1] & wordBeliefs[2] & wordBeliefs[3] & wordBeliefs[4]
        else:
            return (-1)
        
        return (visibilityBelief)


# In[42]:

## CODE BELOW IS ALL FOR TESTING ##

#Chc:T:<NoInten>:P6SM:^Chc:RW:-:P6SM:
#<NoCov>:<NoWx>:<NoInten>:<NoVis>:
#Chc:T:<NoInten>:4SM:^Chc:RW:m:4SM:

sampleWeather1 = ['Chc', 'T', '<NoInten>', 'P6SM']
sampleWeather2 = ['Chc', 'T', '+', '<NoVis>']
sampleWeather3 = ['<NoCov>', '<NoWx>', '<NoInten>', '<NoVis>']

testMe = getBeliefValues(sampleWeather1)
print(testMe)
testMe = getBeliefValues(sampleWeather2)
print(testMe)
testMe = getBeliefValues(sampleWeather3)
print(testMe)

sample1 = "Chc:T:<NoInten>:4SM:^Chc:RW:m:4SM:"
x = getCollectiveBeliefValues(sample1)
print(x)

