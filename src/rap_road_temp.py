import time
import arrow
import numpy as np
import xgboost
from scipy import ndimage

import postgres_functions
import contour_functions
import shapely
from shapely import ops
from shapely import geometry
from shapely import wkb
from shapely import speedups
speedups.enable()

import alarms_and_triggers
import data_handler

import rap_road_temp_parameters

#colors = ['#995229','#F8A758','#FFF2B0']
#colors = ['#A3C7C5', '#A38F5A', '#AD280E']
#fixed_thresholds = [200.15,273.15,296.15]

colors = ['#000003','#07051d','#170b3b','#2e0a5a','#450a69','#5b116e','#70196e','#86216a','#9b2864','#b1315a','#c43c4e','#d64a3f','#e55b30','#f0701e','#f8870d','#fba007','#fbb91e','#f6d542','#f1ed70','#fcfea4']
fixed_thresholds = range(250,330,4)
fixed_thresholds[0] = 1 # Contours will extend in both directions

bst = xgboost.Booster()
#bst.load_model('../models/xgboost_roadtemp_hrrr_21var_noelevation')
bst.load_model('../models/hrrr_7_hrrr_summer_winter')
#with open('../models/sklearn_gbt.p','rb') as opened: 
#    sklearn_classifier = pickle.load(opened)

#sklearn_variables = road_temp_parameters.variable_indices

def row_based_compute_temperatures(grid):
    rows = []
    for i,row in enumerate(grid):
        dtest = xgboost.DMatrix(row,missing=-999999)
        rows.append(bst.predict(dtest))
    output = np.vstack(rows)
    return output

def compute_temperatures(grid):
    shape = grid.shape
    reshaped = grid.reshape(shape[0]*shape[1],shape[2])
    dtest = xgboost.DMatrix(reshaped,missing=-999999)
    output = bst.predict(dtest).reshape(shape[0],shape[1])
    return output

def compute_temperatures_sklearn(grid):
    shape = grid.shape(grid)
    reshaped = grid.reshape(shape[0]*shape[1],shape[2])
    output = sklearn_classifier.predict(reshaped).reshape(shape[0],shape[1])
    return output

def main(fixed_time = None):
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    start_time  = time.time()
    postgres_functions.push_start_time(['road_temperature_rap'],start_time)

    if fixed_time is None:
        current_time = arrow.utcnow()
    else:
        current_time = fixed_time

    layers = []
    for variable in rap_road_temp_parameters.variable_indices:
        current_array = data_handler.get_layer(variable, current_time, latest=False)
        if len(current_array.shape) > 2:
            current_array = current_array[0,:,:]
        layers.append(current_array)
    
    grid_0 = np.dstack(layers)

    x_coordinates,y_coordinates = data_handler.get_coordinate_grid('RAP')

    output_fine = compute_temperatures(grid_0)
    
    if fixed_time is None:
        data_handler.store_layer(output_fine, 'road_temperatures_rap', arrow.utcnow(), time_alive=3600, latest=True)

    else:
        data_handler.store_layer(output_fine, 'road_temperatures_rap', current_time, time_alive=-1, latest=False)
        return
    #np.save('/home/souellet/Documents/road_temp_latest',output_fine)
    
    #Until we need to use the polygons
    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())    
    return    

    output = ndimage.gaussian_filter(output_fine,sigma=1.0)
    
    monitoring = []

    levels, thresholds = contour_functions.polygonize(x_coordinates, y_coordinates,output,thresholds=fixed_thresholds)
    records = []
    for i, level in enumerate(levels):
        multipolygon = ops.unary_union(level)
        
        with open('landpolygon/northamerica.txt','r') as opened:
            northamerica = wkb.loads(opened.read(),hex=True)

        multipolygon = multipolygon.intersection(northamerica)
        
        geolayer_dict = {}

        if i < (len(thresholds)-1):
            geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
        else:
            geolayer_dict['thresholdValues'] = [thresholds[i],999999.0]
        
        geolayer_dict['riskLevel'] = i
        geolayer_dict['risk_name'] = i
        geolayer_dict['colour'] = colors[i]
        geolayer_dict['variableName'] = 'road_temperatures'
        
        geolayer_dict['shape'] = multipolygon

        print thresholds[i], multipolygon.area
        monitoring.append(multipolygon.area)

        records.append(geolayer_dict)

    #print records[0]
    #print records[-1]
    
    monit_index = 1
    while monit_index < len(monitoring)-2:
        before = monitoring[monit_index-1]
        current = monitoring[monit_index]
        after = monitoring[monit_index+1]
        if current == 0 and (before != 0 and after != 0):
            print 'Error'
            break
        monit_index += 1

    postgres_functions.push_road_temperature_contours(records)
    postgres_functions.push_end_time(['road_temperature_rap'],start_time, time.time())

    #alarms_and_triggers.clean_cache('road_temperatures')
    #alarms_and_triggers.geoserver_trigger('road_temperatures')

    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()