import time
import shapely
from shapely import geometry
from shapely import prepared

import postgres_functions

def get_weather_for_route(path):
    coordinates = [(point[1],point[0]) for point in path]
    timestamps = [point[-1] for point in path]

    duration = (timestamps[-1]-timestamps[0])/3600.0
    duration = min(duration,6)
    hours = [[] for _ in range(int(duration+1))]

    start = timestamps[0]

    for i,timestamp in enumerate(timestamps):
        hour_index = int((timestamp-start)/3600.0)
        hour_index = min(hour_index,6)
        hours[hour_index].append(i)
    #print hours

    prob_zones = postgres_functions.retrieve_prob_zones()
    forecasted_zones = postgres_functions.retrieve_prob_zones_forecasted_all()

    multipoints = []
    for i,hour in enumerate(hours):
        multipoint = shapely.geometry.MultiPoint([coordinates[point] for point in hour])
        if i == 0:
            multipoints.append((multipoint,prob_zones))
        else:
            #multipoints.append((multipoint,prob_zones))
            multipoints.append((multipoint,[prob_zone for prob_zone in forecasted_zones if prob_zone[-1] == i]))
    
    time_delay = 0
    print time.time()
    for h,multipoint in enumerate(multipoints):
        hour = hours[h]
        intersected = []
        for prob_zone in multipoint[1]:
            if multipoint[0].intersects(prob_zone[0]):
                intersected.append(prob_zone)
        intersected_points = []
        for zone in intersected:
            intersected_points.append([])
            for i,point in enumerate(multipoint[0]):
                if zone[0].intersects(point):
                    intersected_points[-1].append((hour[i],point,zone[1:]))


        segments = []
        for point_set in intersected_points:
            if len(point_set) < 2:
                continue
            current_segment = []
            current_segment.append((point_set[0][0],point_set[0][2]))
            for point in point_set[1:]:
                if current_segment[-1][0]+1 != point[0]:
                    if len(current_segment) > 1:
                        segments.append(current_segment)
                    current_segment = []
                    current_segment.append((point[0],point[2]))
                elif current_segment[-1][1][0] != point[2][0]:
                    if len(current_segment) > 1:
                        segments.append(current_segment)
                    current_segment = []
                    current_segment.append((point[0],point[2]))
                else:
                    current_segment.append((point[0],point[2]))
            if len(current_segment) > 1:
                segments.append(current_segment)

        for segment in segments:
            start = min(segment)[0]
            stop = max(segment)[0]
            time_seconds = timestamps[stop]-timestamps[start]
            if segment[0][1][0] == 'W':
                time_factor = 0.1
            elif segment[0][1][0] == 'S':
                time_factor = 0.3
            elif segment[0][1][0] == 'I':
                time_factor = 0.4
            time_delay += time_seconds * time_factor

    print time_delay

    if time_delay > 0:
        print "Non-ideal conditions ahead"
    else:
        print 'Everything is fine'

if __name__ == '__main__':
    import routing_service
    print time.time()
    path = routing_service.get_route([(49.303636,-110.01709),(49.131408,-101.260986)])
    print time.time()
    get_weather_for_route(path)
    print time.time()

    pass
