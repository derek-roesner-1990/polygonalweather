import time
import ast

import psycopg2
import shapely
from shapely import wkb

with open('credentials.txt') as opened:
    credentials = ast.literal_eval(opened.readline())

def main():
    while True:
        to_write = []

        cursor = psycopg2.connect(**credentials).cursor()
        cursor.execute("select * from lightning_pulses")
        data = cursor.fetchall()

        for point in data:
            try:
                to_write.append(point+shapely.wkb.loads(point[1],hex=True).bounds[0:2])
            except:
                continue

        with open('../../lightninghistory.csv','a') as opened:
            for item in to_write:
                opened.write(str(item)+'\n')   

        time.sleep(3600)

if __name__ == '__main__':
    main()