mrms_files = ['PrecipFlag/', 'PrecipRate/']

#(precipflag, preciprate, wind, catSnow, catRain, catFrz, catIcePellets, REFLECTIVITY, temperature) # FEED THIS INPUT FORMAT NOW
# HRRR indices first, then same order as mrms_files
variable_indices = ['WIND_P8_L103_GLC0_max', 'CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','REFD_P0_L103_GLC0','TMP_P0_L103_GLC0','precipflag', 'preciprate']

# HRRR indices for forecasted files 
#variable_indices = [51, 5, 'preciprate', 'precipflag', 'qpe1hour', 'qpe6hour']

###############################################################

number_of_hrrr_products = len(variable_indices)-len(mrms_files)

mrms_url = 'http://mrms.ncep.noaa.gov/data/2D/'
#grib_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/'

#hrrr_url = 'nonoperational/com/hrrr/prod/hrrr.'
#hrrr_tag = 'wrfnatfFF.grib2'

hrrr_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/nonoperational/com/hrrr/prod/'

storage_path = '../liveproductdata/'

# DB parameters

gaussian_smoothing = True
median_smoothing = False

base_gaussian_sigma = 4
base_median_size = 9

# GeoJSON related

geojson_for_display = False

clean_up = True