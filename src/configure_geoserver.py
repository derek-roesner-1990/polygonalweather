from geoserver.catalog import Catalog
from geoserver.support import JDBCVirtualTable, JDBCVirtualTableGeometry, JDBCVirtualTableParam

with open('geoserver_credentials.txt') as opened:
    password=opened.read().strip()

#address = 'http://localhost:8080/geoserver/rest/'
address = 'https://tiles.weathertelematics.com/geoserver/rest/'

names = ['windspeed','visibility','posh','seamless','lightning']
names = ['qpe']

def create_forecasted_roads():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for forecast_time in range(1,7):
        name = 'assessed_roads_f'+str(forecast_time)
        #store = cat.get_store('wtx')
        store = cat.get_store('wtx_fuzion')
        geom = JDBCVirtualTableGeometry('geom','Geometry','4326')
        ft_name = name
        epsg_code = 'EPSG:4326'

        sql = "select * from assessed_roads_forecasted where forecast_time = "+str(forecast_time)

        keyColumn = None
        parameters = None

        jdbc_vt = JDBCVirtualTable(ft_name, sql, 'false', geom, keyColumn, parameters)
        ft = cat.publish_featuretype(ft_name, store, epsg_code, jdbc_virtual_table=jdbc_vt)

        current_layer = cat.get_resource(name,workspace='wtx')
        current_layer.native_bbox = ('-180','180','-90','90','EPSG:4326')
        cat.save(current_layer)

        current_layer = cat.get_layer(name)
        style = cat.get_style('bread_style_roads')
        current_layer.default_style = style
        cat.save(current_layer)

def create_conditions_worldwide():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for forecast_time in range(0,7):
        if forecast_time == 0:
            name = 'probabilityzones'
        else:
            name = 'probability_zones_f'+str(forecast_time)
        #store = cat.get_store('wtx')
        store = cat.get_store('wtx_fuzion')
        geom = JDBCVirtualTableGeometry('geom','Geometry','4326')
        ft_name = name
        epsg_code = 'EPSG:4326'

        sql = "select * from gfs_road_conditions where forecast_time = "+str(forecast_time)

        keyColumn = None
        parameters = None

        jdbc_vt = JDBCVirtualTable(ft_name, sql, 'false', geom, keyColumn, parameters)
        ft = cat.publish_featuretype(ft_name, store, epsg_code, jdbc_virtual_table=jdbc_vt)

        current_layer = cat.get_resource(name,workspace='wtx')
        current_layer.native_bbox = ('-180','180','-90','90','EPSG:4326')
        cat.save(current_layer)

        current_layer = cat.get_layer(name)
        style = cat.get_style('gfs_risk_index_style')
        current_layer.default_style = style
        cat.save(current_layer)


def create_layers():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for name in names:
        #store = cat.get_store('postgisB') # Change accordingly
        store = cat.get_store('wtx_fuzion')
        geom = JDBCVirtualTableGeometry('geom','MultiPolygon','4326')
        ft_name = name
        epsg_code = 'EPSG:4326'
        if name == 'visibility':
            sql = "select * from polygon_contours where variable_name = '"+name+"' order by minimum_value desc"
        else:
            sql = "select * from polygon_contours where variable_name = '"+name+"' order by minimum_value"
        keyColumn = None
        parameters = None

        jdbc_vt = JDBCVirtualTable(ft_name, sql, 'false', geom, keyColumn, parameters)
        ft = cat.publish_featuretype(ft_name, store, epsg_code, jdbc_virtual_table=jdbc_vt)

def bound():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for name in names:
        current_layer = cat.get_resource(name,workspace='wtx')
        current_layer.native_bbox = ('-180','180','-90','90','EPSG:4326')
        cat.save(current_layer)

def create_styles():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for name in ['reflectivity_style', 'contours_style']:
        with open(name+'.xml','r') as opened:
            cat.create_style(name, opened.read(),overwrite=True)

def style():
    cat = Catalog(address, 'admin', password, disable_ssl_certificate_validation=True)

    for name in names:
        current_layer = cat.get_layer(name)
        if name == 'seamless':
            style = cat.get_style('reflectivity_style')
            current_layer.default_style = style
        else:
            style = cat.get_style('contours_style')
            current_layer.default_style = style

        cat.save(current_layer)

def main():
    #create_layers()
    #bound()
    #create_styles()
    #style()
    create_forecasted_roads()


if __name__ == '__main__':
    main()
