import arrow

import stacked_ds
import predict_road_temp

def stacked():
    start_time = arrow.Arrow(2017,01,16,21,5)
    for hour in range(560):
        desired_time = start_time.replace(hours=+hour)
        try:
            stacked_ds.main(desired_time)
        except Exception as e:
            print e

def road_temp():
    start_time = arrow.Arrow(2017,01,01,1,5)
    for hour in range(960):
        desired_time = start_time.replace(hours=+hour)
        try:
            predict_road_temp.main(desired_time)
        except Exception as e:
            print e


if __name__ == "__main__":
    #road_temp()
    stacked()
