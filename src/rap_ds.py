import os
import glob
import shutil
import sys
sys.setcheckinterval(10000000)
import socket
import matplotlib
#matplotlib.use('Agg')
import ast
from matplotlib import pyplot
import subprocess
import rasterio
from rasterio import warp
from rasterio import Affine
from rasterio import features
from scipy import ndimage
import numpy as np
import Nio
from cartopy.mpl import patch
import shapely
from shapely import geometry
from shapely import prepared
from shapely import ops
from shapely import wkb
from shapely import speedups
speedups.enable()
import arrow
import socketIO_client

import alarms_and_triggers
import data_handler
import geotab_functions

from dsRoadStatesTemporalSplit_withRoad_V4 import getRapForecastBeliefValues
#from dsRoadStatesTemporalSplit_withRoad_V4 import getForecastBeliefValues


import postgres_functions
from live_feed_functions import *
from rap_ds_parameters import *

#master_transform = Affine.translation(-130,55)*Affine.scale(0.01,-0.01)
#master_transform = Affine.translation(-130,55)*Affine.scale(0.02,-0.02)
#master_transform = Affine.translation(-130,55)*Affine.scale(0.05,-0.05)
#master_grid_width = 3500
#master_grid_height = 1750
#master_grid_width = 1400
#master_grid_height = 700

#master_crs = {'init': 'EPSG:4326'}

down_factor = 1
#buffer_factor = 0.1
buffer_factor = 0.05
buffer_divisor = 1.5
simplification = 0.01

bin_optimization = True

def compute_ds_array(array, frozen_array, frozen_mask, indices):
    cache = dict()
    shape1,shape2 = array.shape[0:2]
    ds_array = np.zeros((shape1,shape2))
    #for i in xrange(shape1):
    #    for j in xrange(shape2):
    #for i,j in zip(indices[0],indices[1]):
    indices = np.dstack(indices).squeeze()
    for i,j in indices:
        if frozen_mask[i,j]:
            array_slice = tuple(frozen_array[i,j])
        else:
            array_slice = tuple(array[i,j])

        if cache.has_key(array_slice):
            result = cache[array_slice]
        else:
            result = getRapForecastBeliefValues(array_slice)
            cache[array_slice] = result
        ds_array[i,j] = result
    return ds_array

def clean_past_arrays(threshold= 240):
    filepaths = glob.glob('../persistent/rap_stacked_*')
    current_time = time.time()
    for filepath in filepaths:
        if (current_time - os.path.getctime(filepath)) / 60.0 > threshold:
            os.remove(filepath)

def main(fixed_time=None, forecast_time = 0, minutes=False):
    print time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    start_time  = time.time()
    #postgres_functions.push_start_time(['road_conditions_zones_rap'],start_time)

    grid_list = []

    if fixed_time is None:
        if minutes:
            desired_time = arrow.utcnow().replace(minutes=+forecast_time)
        else:
            desired_time = arrow.utcnow().replace(hours=+forecast_time)
    else:
        desired_time = fixed_time
    #desired_time = arrow.get('2017-02-14T15:02:00+00:00') #TODO remove

    # Checks if HRRR with index below the total length minus the number of expected MRMS layers
    for var_index,variable in enumerate(variable_indices):    
        if fixed_time is None:
            data = data_handler.get_layer(variable, desired_time, latest=False)
        else:
            if '03H' in variable:
                data = data_handler.get_layer('accumulation_MRMS_3', desired_time, latest=False, interpolated=False)
            else:
                data = data_handler.get_layer(variable, desired_time, latest=False, interpolated=False)

        grid_list.append(data[::down_factor,::down_factor])

    # (precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr)
    # variable_indices = [15, 48, 'preciprate', 'precipflag', 'qpe1hour', 'qpe6hour']
    #variable_indices = ['DSWRF_P0_L1_GLC0', 'TMP_P0_L103_GLC0','CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','preciprate', 'precipflag', 'qpe1hour', 'qpe6hour']
    #(precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr, catSnow, catRain, catFrz, catIcePellets)

    #(precipflag, preciprate, precipaccum6Hr, air_temp, radiation, precipaccum1Hr, catSnow, catRain, catFrz, catIcePellets, precipaccum3hr, roadTemperature, humidity)
    
    #(catSnow, catRain, catFrz, catIcePellets, precip1HAccum, airtemp, radiation, roadTemp)
    #['DSWRF_P0_L1_GRLL0', 'TMP_P0_L103_GRLL0','CSNOW_P0_L1_GRLL0','CRAIN_P0_L1_GRLL0','CFRZR_P0_L1_GRLL0','CICEP_P0_L1_GRLL0', 'APCP_P8_L1_GRLL0_acc']
    reordered = [None]*8
    reordered[0] = grid_list[2] #catsnow
    reordered[1] = grid_list[3] #catrain
    reordered[2] = grid_list[4] #frz
    reordered[3] = grid_list[5] #icep
    reordered[4] = grid_list[6] #acc
    reordered[5] = grid_list[1] #airtemp
    reordered[6] = grid_list[0] #rad
    reordered[7] = None

    #road_temp_array = np.load('/home/souellet/Documents/road_temp_latest.npy')

    if fixed_time is None:
        road_temp_array = data_handler.get_layer('road_temperatures_rap','latest')
    else:
        road_temp_array = data_handler.get_layer('road_temperatures_rap',desired_time, latest=False, interpolated=False)

    reordered[7] = road_temp_array

    #for thing in reordered:
    #    pyplot.imshow(thing,cmap='inferno')
    #    pyplot.show()



    #reordered = np.load('/home/souellet/Downloads/feb8test_input.npy')
    #reordered = [reordered[:,:,i] for i in range(reordered.shape[-1])]

    clean_past_arrays()
    if fixed_time is None:
        np.save('../persistent/rap_stacked_input_{0}'.format(desired_time),np.dstack(reordered))
    frozen_reordered = [np_grid.copy() for np_grid in reordered]

    #np.save('/home/souellet/Downloads/feb14_test_input',np.dstack(reordered))
    #return
    raw_array = np.dstack(reordered)

    if False:
        thirtyfour = np.logical_or(raw_array[:,:,0] == 3, raw_array[:,:,0] == 4)
        sixeightnine = np.logical_or(np.logical_or(raw_array[:,:,6] == 1, raw_array[:,:,8] == 1), raw_array[:,:,9] == 1)
        flag_condition = np.logical_and(thirtyfour, sixeightnine)

        frozen_mask = np.logical_or(flag_condition,np.logical_and(raw_array[:,:,3] <= 273.16,raw_array[:,:,3] > 0))
    
    frozen_mask = np.logical_and(raw_array[:,:,5] <= 273.16,raw_array[:,:,5] > 0)
    frozen_precip_array = frozen_mask.astype(np.float64)+1

    #(catSnow, catRain, catFrz, catIcePellets, precip1HAccum, airtemp, radiation, roadTemp)
    if bin_optimization:
        #print 'Starts binning', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        bins = []
        bins.append([-1,0,0.101,1.001,7.501,5000]) #precip1h
        bins.append([-1,0,271.16,275.16,5000]) #airtemp
        bins.append([-1,0,250,500,750,5000]) #radiation
        

        bins.append([-1,0,271.16,275.16,5000]) #roadtemp

        corresponding_values = []
        corresponding_values.append([-3,-3,0,0.5,3,8])
        corresponding_values.append([-3,-3,2,275,276])
        corresponding_values.append([-3,-3,2,251,501,751])

        corresponding_values.append([-3,-3,2,275,276])

        for i in range(4,8):
            layer_bin = bins[i-4]
            new_layer = np.digitize(reordered[i],layer_bin).astype(np.float64)
            new_layer_final = new_layer.copy()
            for j,value in enumerate(layer_bin):
                new_layer_final[new_layer == j] = corresponding_values[i-4][j]
            reordered[i] = new_layer_final

    #print 'Finished binning', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    master_array = np.dstack(reordered+[frozen_precip_array])

    # Copy for frozen precipitation
    if bin_optimization:
        #print 'Starts binning', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        bins = []
        bins.append([-1,0,0.1,2.501,5.001,5000]) #precip1h
        bins.append([-1,0,271.16,275.16,5000]) #airtemp, same
        bins.append([-1,0,250,500,750,5000]) #Radiation, same

        bins.append([-1,0,271.16,275.16,5000]) #roadtemp, same
        
        corresponding_values = []
        corresponding_values.append([-3,-3,0,1,3,6])
        corresponding_values.append([-3,-3,2,275,276])
        corresponding_values.append([-3,-3,2,251,501,751])

        corresponding_values.append([-3,-3,2,275,276])
        
        for i in range(4,8):
            layer_bin = bins[i-4]
            new_layer = np.digitize(frozen_reordered[i],layer_bin).astype(np.float64)
            new_layer_final = new_layer.copy()
            for j,value in enumerate(layer_bin):
                new_layer_final[new_layer == j] = corresponding_values[i-4][j]
            frozen_reordered[i] = new_layer_final

    frozen_master_array = np.dstack(frozen_reordered+[frozen_precip_array])

    land_data = data_handler.get_layer('RAP:land', 'None', latest=False)

    land = land_data.astype(np.bool)
    #master_array[~land] = -3
    #indices = np.where(land[::down_factor,::down_factor])
    indices = np.where(land)

    #np.save('/home/souellet/Downloads/master_array',master_array)
    #np.save('master_array',master_array)
    #np.save('indices', indices)
    #return
    #master_array = np.ma.masked_equal(master_array,-3)
    #print master_array[0,0]

    #downsampled = master_array[::down_factor,::down_factor]

    #print getBeliefValues([downsampled[90,180]])
    print 'Preprocessing completed', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    #result = np.apply_along_axis(getBeliefValues,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    #result = np.apply_along_axis(cached_belief,2,downsampled)
    result = compute_ds_array(master_array, frozen_master_array, frozen_mask, indices)

    #result[530:,:570] = 0 # hacking away the mexican artifact

    np.save('../persistent/rap_stacked_output_{}'.format(arrow.utcnow()),result)
    #pyplot.imshow(result,cmap='inferno')
    #pyplot.show()
    #pyplot.savefig('/home/souellet/what.pdf')
    print 'Results calculated', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    if fixed_time is None:
        if forecast_time == 0:
            data_handler.store_layer(result.astype(np.float32), 'rap_ds_array', arrow.utcnow(), time_alive=3600, latest=True)
        else:
            if minutes:
                data_handler.store_layer(result.astype(np.float32), 'rap_ds_array_f{}m'.format(forecast_time), arrow.utcnow(), time_alive=3600, latest=True)
            else:
                data_handler.store_layer(result.astype(np.float32), 'rap_ds_array_f{}'.format(forecast_time), arrow.utcnow(), time_alive=3600, latest=True)
    else:
        data_handler.store_layer(result.astype(np.float32), 'rap_ds_array', desired_time, time_alive=-1, latest=False)
        return
    #np.save('../persistent/binned_input_{0}'.format(desired_time),master_array)
    #np.save('../persistent/binned_input_frozen_{0}'.format(desired_time),frozen_master_array)
    
    #if fixed_time is None:
    #    np.save('../persistent/stacked_output_{0}'.format(desired_time),result)
    
    #np.save('../persistent/non_opt_stacked_output_{0}'.format(desired_time),result)
    #return #TODO remove this before deploying

    #polygons = polygonize(truex[::down_factor,::down_factor], truey[::down_factor,::down_factor],result,icy_computed=None)

    """
    counter = 0
    for polygon in polygons:
        geom = polygon[0]
        if hasattr(geom, 'geoms'):
            for geom_one in geom:
                counter += 1
    print counter
    """
    #postgres_functions.push_prob_zones_clean(polygons)
    #postgres_functions.push_prob_zones(polygons)

    #postgres_functions.push_prob_zones(polygons)

    #postgres_functions.push_end_time(['road_conditions_zones_rap'],start_time,time.time())

    print 'Polygons created', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    #client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #client_socket.connect(('localhost',8042))
    #client_socket.close()

    if False:
        to_save = np.dstack((master_array, result))
        filename = time.gmtime()
        filename = time.strftime('Road-%Y-%m-%dZ%H-%M-%S', filename)
        np.save('../persistent/'+filename, to_save)
        with open('../persistent/'+filename+'.npy', 'rb') as f_in, gzip.open('../persistent/'+filename+'.npy.gz', 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)
        os.remove('../persistent/'+filename+'.npy')

    remove_old_files()

    #alarms_and_triggers.clean_cache('probabilityzones')
    #alarms_and_triggers.clean_cache('current_impedance')
    #alarms_and_triggers.geoserver_trigger('probabilityzones')
    #alarms_and_triggers.geoserver_trigger('current_impedance')

    #alarms_and_triggers.clean_cache('all_prob_zones')
    #alarms_and_triggers.geoserver_trigger('all_prob_zones')

    if False:
        if socket.gethostname() == 'ubuntu-cv1' and False:
            current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')

        current_socketIO.emit('assessed_roads_trigger', {'forecast': 0, 'time':int(time.time())})

    if using_geotab and False:
        entities = []
        for polygon in polygons:
            geotab_functions.ds_transform_into_entities(polygon,entities)

        cleaned_up = geotab_functions.deal_with_holes(entities)

        geotab_functions.write_multicalls(cleaned_up,'geotab_stacked_ds')

def transform(lon_grid,lat_grid,shapes):
    all_shapes = []
    for shape in shapes:
        new_coordinates = []
        for coordinate in shape[0]['coordinates'][0]:
            new_lon = lon_grid[coordinate[::-1]]
            new_lat = lat_grid[coordinate[::-1]]
            new_coordinates.append((new_lon,new_lat))
        
        shape[0]['coordinates'][0] = new_coordinates
        all_shapes.append(shape)

    return all_shapes
    
def polygonize(x,y,array,icy_computed=None):
    #contours = pyplot.contourf(x,y,array,levels=[10.50,11.10,20.50,21.10,30.50,31.10],cmap='inferno')
    #contours = pyplot.contourf(x,y,array,levels=[10.20,11.10,20.20,21.10,30.20,31.10],cmap='inferno',antialiased=False)

    bins = [10,20,30]
    digitized = np.digitize(array,bins).astype(np.int32)
    mask = digitized > 0
    shapes = rasterio.features.shapes(digitized,mask=mask)

    shapes = transform(lon_grid, lat_grid, shapes)

    polygons = [[],[],[]]
    for shape in shapes:
        polygon = shapely.geometry.shape(shape[0])
        polygons[int(shape[1])-1].append(polygon) # wet, snowy, icy in order


    if buffer_factor > 0:
        wet = [poly.buffer(buffer_factor) for poly in polygons[0]]
        snowy = [poly.buffer(buffer_factor) for poly in polygons[1]]
        icy = [poly.buffer(buffer_factor) for poly in polygons[2]]

        if simplification > 0:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor).simplify(simplification, preserve_topology=True)
        else:
            better_wet = ops.unary_union(wet).buffer(-buffer_factor/buffer_divisor)
            better_snowy = ops.unary_union(snowy).buffer(-buffer_factor/buffer_divisor)
            better_icy = ops.unary_union(icy).buffer(-buffer_factor/buffer_divisor)

        if icy_computed is not None:
            better_icy = icy_computed

        better_wet = better_wet.difference(better_snowy)
        better_snowy = better_snowy.difference(better_icy)
        better_wet = better_wet.difference(better_icy)

    else:
        better_wet = geometry.MultiPolygon(polygons[0])
        better_snowy = geometry.MultiPolygon(polygons[1])
        better_icy = geometry.MultiPolygon(polygons[2])
        if icy_computed is not None:
            better_icy = icy_computed


    return [[better_wet,'W',0.8], [better_snowy,'S',0.8], [better_icy,'I',0.8]]

if __name__ == '__main__':
    main()

    if False:
        start_time = arrow.Arrow(2016,11,20,21,5)
        for hour in range(1224):
            desired_time = start_time.replace(hours=+hour)
            try:
                main(desired_time)
            except Exception as e:
                print e

    #test()
    #validation_run()


