import requests
import xml.etree.cElementTree as ET
import pymongo
mongouri = "mongodb://10.128.0.3,10.128.0.4,10.128.0.5/?replicaSet=rs0"
#mongouri = "mongodb://192.168.5.45/"
mongo_client = pymongo.MongoClient(mongouri)
mongo_collection = mongo_client['fuzion_db']['trafficcameras']
mongo_collection.drop()
mongo_collection.create_index([('location',pymongo.GEOSPHERE)])
#########################
#       Get Regions     #
#########################
url = "http://www.vizzion.com/TrafficCamsService/TrafficCams.asmx/GetRegions"
querystring = {"strPassword":"OQVAZTD9DV[SURL^SB]BNUMAV(ROK@BDTMKU"}
headers = {
    'cache-control': "no-cache",
    'postman-token': "2448f87a-a0e2-8874-5bd8-ac91ceed5ebf"
    }
response = requests.request("GET", url, headers=headers, params=querystring)
root = ET.fromstring(response.text.encode('utf-8'))
dataSetCameras = root[1][0]
country_usa = 1
stateIDs = []
for state in dataSetCameras.iterfind('{http://tempuri.org/DataSetCameras.xsd}States'):
  if int(state.find('{http://tempuri.org/DataSetCameras.xsd}CountryID').text) == country_usa:
    stateIDs.append(int(state.find('{http://tempuri.org/DataSetCameras.xsd}StateID').text))
regionsIDs = []
for region in dataSetCameras.iterfind('{http://tempuri.org/DataSetCameras.xsd}Regions'):
  if int(region.find('{http://tempuri.org/DataSetCameras.xsd}StateID').text) in stateIDs:
    regionsIDs.append(int(region.find('{http://tempuri.org/DataSetCameras.xsd}RegionID').text))
#########################
#       Get Cameras     #
#########################
url = "http://www.vizzion.com/TrafficCamsService/TrafficCams.asmx/GetRegionCameras2"
i = 0
for regionID in regionsIDs:
  #print('{0} ({1})'.format(regionID, i / len(regionIDs)))
  querystring = {"intRegionID":regionID, "intOptions":0, "strPassword":"OQVAZTD9DV[SURL^SB]BNUMAV(ROK@BDTMKU"}
  response = requests.request("GET", url, headers=headers, params=querystring)
  root = ET.fromstring(response.text.encode('utf-8'))
  dataSetCameras = root[1][0] if len(root[1]) > 0 else root[1]
  cameraList = []
  for cameraXML in dataSetCameras.iterfind('{http://tempuri.org/DataSetCameras.xsd}Cameras'):
    camera = {
      'cameraID': int(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}CameraID').text),
      'location': {
        'type': "Point",
        'coordinates': [float(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}Longitude').text), float(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}Latitude').text)]
      },
      'name': cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}Name').text,
      'regionID': int(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}RegionID').text),
      'updateFrequency': int(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}UpdateFrequency').text) if cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}UpdateFrequency') else None,
      'detectedOutOfService': bool(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}DetectedOutOfService').text) if cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}DetectedOutOfService') else None,
      'markerPost': int(cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}MarkerPost').text) if cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}MarkerPost') else None,
      'closestLane': cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}ClosestLane').text if cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}ClosestLane') else None,
      'streamWebAddress': cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}StreamWebAddress').text if cameraXML.find('{http://tempuri.org/DataSetCameras.xsd}StreamWebAddress') else None
    }
    cameraList.append(camera)
  if len(cameraList) > 0:
    mongo_collection.insert_many(cameraList)