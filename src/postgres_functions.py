import socket
import StringIO
import calendar
import time
import ast

import psycopg2
import numpy as np
import arrow
import shapely
from shapely import wkb
from shapely import speedups
from shapely import geometry
speedups.enable()

from raven import Client

with open('credentials.txt','r') as opened:
    credentials = ast.literal_eval(opened.read())

with open('sentry_credentials.txt', 'r') as opened:
    sentry_credentials = opened.read()
#sentry = Client(sentry_credentials)

if socket.gethostname() == 'ubuntu-cv1':
    #with open('122_credentials.txt','r') as opened:
    with open('credentials.txt','r') as opened:
        credentials = ast.literal_eval(opened.read())

def define_indexes():
    connection = psycopg2.connect(**credentials)
    cursor = connection.cursor()

    queries = []
    queries.append(""" create index gist_index_polygons on thresholdedpolygons using gist (geom); """)
    queries.append(""" create index gist_index_zones on probabilityzones using gist (geom); """)
    queries.append(""" create index road_id_index on roadconditions (road_id); """)
    queries.append(""" create index all_prob_time on all_prob_zones (timestamp); """)
    queries.append(""" create index assessed_roads_index on assessed_roads using gist (geom); """)
    queries.append(""" create index union_road_zones_index on union_road_zones using gist (geom); """)
    queries.append(""" create index type_union_road_zones_index on union_road_zones (zone_type); """)
    queries.append(""" create index contour_geom_index on polygon_contours using gist (geom); """)
    queries.append(""" create index forecasted_zones_geom_index on probability_zones_forecasted using gist (geom); """)
    queries.append(""" create index forecasted_roads_geom_index on assessed_roads_forecasted using gist (geom); """)
    queries.append(""" create index visibility_zones_geom_index on visibility_zones using gist (geom); """)
    queries.append(""" create index wtx_geom_index on wtx_vehicles using gist (geom); """)
    queries.append(""" create index wtx_number_index on wtx_vehicles (vehicle_number); """)
    queries.append(""" create index wtx_time_index on wtx_vehicles (time); """)
    queries.append(""" create index unique_wtx_geom_index on unique_wtx_vehicles using gist (geom); """)
    queries.append(""" create index update_times_end on update_times (end_time); """)
    queries.append(""" create index update_times_variable on update_times (variable_name); """)
    queries.append(""" create index risk_polygons_geom on risk_polygons using gist (geom); """)
    queries.append(""" create index ecmwf_risk_polygons_geom on ecmwf_risk_polygons using gist (geom); """)
    queries.append(""" create index ecmwf_live_risk_geom on ecmwf_live_risk using gist (geom); """)

    for query in queries[-1:]:
        cursor.execute(query)
        cursor.connection.commit()
    cursor.connection.close()

def define_tables():
    connection = psycopg2.connect(**credentials)
    cursor = connection.cursor()

    queries = []

    queries.append("""
    CREATE TABLE thresholdedpolygons (
        id serial PRIMARY KEY,
        variable_name varchar(20),
        minimum_value float,
        maximum_value float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE probabilityzones (
        id serial PRIMARY KEY,
        probability float,
        zone_type varchar(20),
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE roadconditions (
        id serial PRIMARY KEY,
        road_id integer,
        condition varchar(20),
        probability float
    );
    """)

    queries.append("""
    CREATE TABLE all_prob_zones (
        id serial PRIMARY KEY,
        probability float,
        zone_type text,
        timestamp timestamp,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE assessed_roads (
        id serial PRIMARY KEY,
        osm_id integer,
        osm_name text,
        zone_type text,
        probability float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE union_road_zones (
        id serial PRIMARY KEY,
        zone_type text,
        probability float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE polygon_contours (
        id serial PRIMARY KEY,
        variable_name text,
        minimum_value float,
        maximum_value float,
        colour text,
        risk_level integer,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE probability_zones_forecasted (
        id serial PRIMARY KEY,
        probability float,
        zone_type text,
        forecast_time integer,
        run_time integer,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE assessed_roads_forecasted (
        id serial PRIMARY KEY,
        probability float,
        zone_type text,
        forecast_time integer,
        run_time integer,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE visibility_zones (
        id serial PRIMARY KEY,
        probability float,
        zone_type text,
        risk_level integer,
        colour text,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE event_locations (
        id serial PRIMARY KEY,
        event_name text,
        coordinates text,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE alert_types (
        id serial PRIMARY KEY,
        variable_name text,
        range float,
        number_of_flashes int

    );
    """)

    queries.append("""
    CREATE TABLE alerts (
        id serial PRIMARY KEY,
        event_id int,
        variable_name text,
        risk_level int
    );
    """)

    queries.append("""
    CREATE TABLE event_polygon_contours (
        id serial PRIMARY KEY,
        variable_name text,
        minimum_value float,
        maximum_value float,
        colour text,
        risk_level integer,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE wtx_vehicles (
        id integer PRIMARY KEY,
        time float,
        vehicle_number integer,
        elevation float,
        temperature float,
        road_temperature float,
        relative_humidity float,
        station_pressure float,
        ozone float,
        precip_intensity float,
        light_level float,
        speed float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE unique_wtx_vehicles (
        id integer PRIMARY KEY,
        time float,
        vehicle_number integer,
        elevation float,
        temperature float,
        road_temperature float,
        relative_humidity float,
        station_pressure float,
        ozone float,
        precip_intensity float,
        light_level float,
        speed float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE road_temperature_contours (
        id serial PRIMARY KEY,
        variable_name text,
        minimum_value float,
        maximum_value float,
        colour text,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE update_times (
        id serial PRIMARY KEY,
        variable_name text,
        start_time float,
        end_time float
    );
    """)

    queries.append("""
    CREATE TABLE impedance_polygons (
        id bigserial PRIMARY KEY,
        road_condition text,
        speed_factor float,
        forecast_time float,
        valid_time float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE risk_polygons (
        id bigserial PRIMARY KEY,
        risk_index float,
        forecast_time float,
        valid_time float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE rap_road_conditions (
        id bigserial PRIMARY KEY,
        risk_index float,
        forecast_time text,
        valid_time timestamp without time zone,
        condition_type text,
        speed_factor float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE rap_contours (
        id bigserial PRIMARY KEY,
        risk_index float,
        forecast_time text,
        speed_factor float,
        valid_time timestamp without time zone,
        colour text,
        variable_name text,
        minimum_value text,
        maximum_value text,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE ecmwf_risk_polygons (
        id bigserial PRIMARY KEY,
        risk_index float,
        forecast_time float,
        valid_time float,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE ecmwf_live_risk (
        id bigserial PRIMARY KEY,
        risk_index float,
        forecast_time float,
        valid_time timestamp without time zone,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE gfs_contours (
        id bigserial PRIMARY KEY,
        risk_index float,
        risk_level integer,
        forecast_time text,
        speed_factor float,
        valid_time timestamp without time zone,
        colour text,
        variable_name text,
        minimum_value text,
        maximum_value text,
        geom geometry
    );
    """)

    queries.append("""
    CREATE TABLE gfs_road_conditions (
        id bigserial PRIMARY KEY,
        risk_and_type float,
        risk_index float,
        forecast_time text,
        valid_time timestamp without time zone,
        condition_type text,
        speed_factor float,
        geom geometry
    );
    """)

    for query in queries[-1:]:
        cursor.execute(query)
        connection.commit()
    connection.close()

def push_start_time(variable_names, current_time):
    cursor = psycopg2.connect(**credentials).cursor()
    
    push_string = """ insert into update_times(variable_name, start_time)
    values """

    template = "(%s,%s)"
    
    full_template = []
    templated_data = []
    
    for variable_name in variable_names:
        full_template.append(template)
        templated_data.extend([variable_name, current_time])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)

    cursor.connection.commit()
    cursor.connection.close()

def push_end_time(variable_names, start_time, end_time):
    cursor = psycopg2.connect(**credentials).cursor()    

    for variable_name in variable_names:
        push_string = """ update update_times set end_time = %s where start_time = %s and variable_name = %s """
        cursor.execute(push_string, (end_time, start_time, variable_name))

    cursor.connection.commit()
    cursor.connection.close()

def push_road_temperature_contours(geolayers):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate road_temperature_contours;')
    cursor.connection.commit()

    push_string = """ insert into road_temperature_contours(variable_name, minimum_value, maximum_value, colour, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    current_time = int(time.time())

    full_template = []
    templated_data = []

    for geolayer in geolayers:
        variable_name = geolayer['variableName']
        minimum_value = geolayer['thresholdValues'][0]
        maximum_value = geolayer['thresholdValues'][1]
        colour = geolayer['colour']
        geometry = geolayer['shape']

        if geometry.area > 0:
            if geometry.type == 'MultiPolygon' or geometry.type == 'GeometryCollection':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([variable_name, minimum_value, maximum_value, colour, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([variable_name, minimum_value, maximum_value, colour, geometry.wkb_hex, 4326])
            else:
                print geometry.type
                sentry.captureMessage('Missing a polygon strangely enough {}'.format(arrow.utcnow()))
                print 'What could be happening here'
        #full_template.append(template)
        #templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)

    cursor.connection.commit()
    cursor.connection.close()

def push_wtx_data(observations):
    cursor = psycopg2.connect(**credentials).cursor()

    push_string = """ insert into wtx_vehicles(id,time,vehicle_number,elevation,temperature,road_temperature,relative_humidity,station_pressure,ozone,precip_intensity,light_level, speed, geom) values """

    template = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,ST_SetSRID(ST_MakePoint(%s,%s),4326))"

    current_time = int(time.time())

    full_template = []
    templated_data = []

    values = observations
    id_stuff = values[:,0:5]
    id_stuff[:,2:4] = id_stuff[:,2:4].astype(np.float32)
    measurements = values[:,5:].astype(np.float32)

    measurements[:,0:2] += 273.15 # Celsius to Kelvin
    measurements[:,5] *= 100 # Millibar to Pascal

    values = np.hstack([id_stuff,measurements])

    for value in values:
        
        record_time = calendar.timegm(value[4].utctimetuple())

        record_id = value[0]
        vehicle_number = value[1]
        elevation = value[12]
        
        ozone = value[11]
        station_pressure = value[10]
        relative_humidity = value[8]
        temperature = value[6]

        precip_intensity = value[7]

        light_level = value[9]

        road_temperature = value[5]

        speed = value[13]

        if temperature < -0.005+273.15 and temperature > -0.015+273.15:
            temperature = None

        if road_temperature < 0.005+273.15 and road_temperature > -0.021+273.15:
            road_temperature = None

        full_template.append(template)
        templated_data.extend([record_id,record_time,vehicle_number,elevation,temperature,road_temperature,relative_humidity,station_pressure,ozone,precip_intensity,light_level, speed,value[3],value[2]])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.connection.close()

def fill_alert_types():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute("""
    insert into alert_types(variable_name, range,number_of_flashes)
    values ('lightning_low', 32186, 1), ('lightning_high', 12874, 1), ('windspeed', 1, 1)
    """)
    cursor.connection.commit()

    cursor.connection.close()

def example_events():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate event_locations;')
    cursor.connection.commit()

    p1 = shapely.geometry.Point(-90,45)
    p2 = shapely.geometry.Point(-86.15,39.766667)
    p3 = shapely.geometry.Point(-99.75,32.45)
    p4 = shapely.geometry.Point(-93.255,36.637778)


    cursor.execute("""
    insert into event_locations(event_name, coordinates, geom)
    values ('Mid', '16138086954',ST_SetSRID(%s::geometry,4326)), ('Indianapolis', '16138086954',ST_SetSRID(%s::geometry,4326)), ('Abilene,TX','16138086954',ST_SetSRID(%s::geometry,4326)), ('Branson, Missouri','16138086954',ST_SetSRID(%s::geometry,4326))
    """,(p1.wkb_hex,p2.wkb_hex,p3.wkb_hex,p4.wkb_hex))

    cursor.connection.commit()

    cursor.connection.close()

def add_event(event_name='None',coordinates_phone='None',lon_lat=(1,1)):
    cursor = psycopg2.connect(**credentials).cursor()

    #p1 = shapely.geometry.Point(-87.898012,43.028602)
    p1 = shapely.geometry.Point(lon_lat[0],lon_lat[1])

    cursor.execute("""
    insert into event_locations(event_name, coordinates, geom)
    values (%s, %s,ST_SetSRID(%s::geometry,4326));
    """,(event_name,coordinates_phone,p1.wkb_hex))

    cursor.connection.commit()
    cursor.connection.close()


def known_alert_types():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate alerts;')
    cursor.connection.commit()

    query = """
    select variable_name from alert_types;
    """

    cursor.execute(query)
    alert_types = cursor.fetchall()

    query = """
    select event_name, number from event_locations;
    """

    cursor.execute(query)
    event_names = cursor.fetchall()

    cursor.connection.close()

    return (alert_types,event_names)

def update_alerts():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate alerts;')
    cursor.connection.commit()

    query = """
    select * from alert_types;
    """
    current_time = int(time.time())

    cursor.execute(query)
    alert_types = cursor.fetchall()
    for alert_type in alert_types:
        variable_name = alert_type[1]
        alert_range = alert_type[2]

        if variable_name == 'lightning_low':
            flashes_threshold = alert_type[3]
            query = """
            with event_flashes as (
            select lightning_pulses.cgmultiply,event_locations.id from lightning_pulses
            join event_locations
            on ST_DWithin(event_locations.geom::geography, lightning_pulses.geom::geography, %s)
            where cgmultiply > 0 and %s - lightning_pulses.timestamp < 30)
            insert into alerts(event_id, variable_name, risk_level)
            select id, 'lightning_low', 1 from event_flashes
            group by id
            having count(cgmultiply) > %s
            ;
            """
            cursor.execute(query,(alert_range,current_time,flashes_threshold))
            cursor.connection.commit()
            #print cursor.fetchall()

        elif variable_name == 'lightning_high':
            flashes_threshold = alert_type[3]
            query = """
            with event_flashes as (
            select lightning_pulses.cgmultiply,event_locations.id from lightning_pulses
            join event_locations
            on ST_DWithin(event_locations.geom::geography, lightning_pulses.geom::geography, %s)
            where cgmultiply > 0 and %s - lightning_pulses.timestamp < 30)
            insert into alerts(event_id, variable_name, risk_level)
            select id, 'lightning_high', 2 from event_flashes
            group by id
            having count(cgmultiply) > %s
            ;
            """
            cursor.execute(query,(alert_range,current_time,flashes_threshold))
            cursor.connection.commit()
            #print cursor.fetchall()

        else:
            query = """
            insert into alerts(event_id, variable_name, risk_level)
            select event_locations.id, event_polygon_contours.variable_name, event_polygon_contours.risk_level
            from event_polygon_contours
            join event_locations
            on ST_Within(event_locations.geom, event_polygon_contours.geom)
            where event_polygon_contours.variable_name = %s;
            """
            cursor.execute(query,(variable_name,))
            cursor.connection.commit()

    cursor.connection.close()

def retrieve_alerts():

    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('select event_name, coordinates, variable_name, risk_level from alerts join event_locations on event_id = event_locations.id;')
    all_alerts = cursor.fetchall()

    cursor.connection.close()
    return all_alerts

def push_contours(geolayers):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate polygon_contours;')
    cursor.connection.commit()

    push_string = """ insert into polygon_contours(variable_name, minimum_value, maximum_value, colour, risk_level, geom)
    values """

    template = "(%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    current_time = int(time.time())

    full_template = []
    templated_data = []

    for geolayer in geolayers:
        geolayer = geolayer['properties']
        variable_name = geolayer['variableName']
        minimum_value = geolayer['thresholdValues'][0]
        maximum_value = geolayer['thresholdValues'][1]
        colour = geolayer['colour']
        risk_level = geolayer['riskLevel']
        geometry = geolayer['shape']

        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geometry.wkb_hex, 4326])


        #full_template.append(template)
        #templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.connection.close()

def push_rap_contours(geolayers):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate rap_contours;')
    cursor.connection.commit()

    push_string = """ insert into rap_contours(variable_name, minimum_value, maximum_value, colour, risk_index, forecast_time, valid_time, speed_factor, geom)
    values """

    template = "(%s,%s,%s,%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    current_time = int(time.time())

    full_template = []
    templated_data = []

    for geolayer in geolayers:
        geolayer = geolayer['properties']
        variable_name = geolayer['variableName']
        minimum_value = geolayer['thresholdValues'][0]
        maximum_value = geolayer['thresholdValues'][1]
        colour = geolayer['colour']
        risk_level = geolayer['riskLevel']
        geometry = geolayer['shape']
        forecast_time = geolayer['forecast_time']
        valid_time = geolayer['valid_time'].datetime
        speed_factor = geolayer['speed_factor']

        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, forecast_time, valid_time, speed_factor, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, forecast_time, valid_time, speed_factor, geometry.wkb_hex, 4326])


        #full_template.append(template)
        #templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.connection.close()

def push_gfs_contours(geolayers):
    cursor = psycopg2.connect(**credentials).cursor()
    print 'About to truncate'
    cursor.execute('delete from gfs_contours') #Makes no sense, but truncate is super slow
    cursor.connection.commit()
    print 'Truncated'
    
    push_string = """ insert into gfs_contours(variable_name, minimum_value, maximum_value, colour, risk_index, risk_level, forecast_time, valid_time, speed_factor, geom)
    values """
    
    template = "(%s,%s,%s,%s,%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    current_time = int(time.time())

    full_template = []
    templated_data = []
    
    for geolayer in geolayers:
        geolayer = geolayer['properties']
        variable_name = geolayer['variableName']
        minimum_value = geolayer['thresholdValues'][0]
        maximum_value = geolayer['thresholdValues'][1]
        colour = geolayer['colour']
        risk_level = geolayer['riskLevel']
        risk_level_int = geolayer['riskLevelInt']
        geometry = geolayer['shape']
        forecast_time = geolayer['forecast_time']
        valid_time = geolayer['valid_time'].datetime
        speed_factor = geolayer['speed_factor']

        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, risk_level_int, forecast_time, valid_time, speed_factor, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, risk_level_int, forecast_time, valid_time, speed_factor, geometry.wkb_hex, 4326])


        #full_template.append(template)
        #templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])
    
    values_string = ','.join(full_template)
    push_string = push_string+values_string
    
    print 'Executing query'

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    
    cursor.connection.close()

def push_event_contours(geolayers):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate event_polygon_contours;')
    cursor.connection.commit()

    push_string = """ insert into event_polygon_contours(variable_name, minimum_value, maximum_value, colour, risk_level, geom)
    values """

    template = "(%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    current_time = int(time.time())

    full_template = []
    templated_data = []

    for geolayer in geolayers:
        geolayer = geolayer['properties']
        variable_name = geolayer['variableName']
        minimum_value = geolayer['thresholdValues'][0]
        maximum_value = geolayer['thresholdValues'][1]
        colour = geolayer['colour']
        risk_level = geolayer['riskLevel']
        geom = geolayer['shape']

        full_template.append(template)
        templated_data.extend([variable_name, minimum_value, maximum_value, colour, risk_level, geom.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.connection.close()

def push_thresholded_polygons(cursor, geolayers):
    """ Inserts geometries into thresholdedpolygons table, given a cursor to a database where it's defined """

    # Deletes obsolete records
    cursor.execute('delete from thresholdedpolygons')
    cursor.connection.commit()

    push_string = """ insert into thresholdedpolygons(variable_name, minimum_value, maximum_value, geom)
    values """

    # Uses a string template and fill a list with strings representing the data to insert
    template = "(%s,%s,%s,ST_SetSRID(%s::geometry, %s))"
    values_string = ','.join([template]*len(geolayers))

    templated_data = []

    for geolayer in geolayers:
        templated_data.extend([geolayer['variableName'],geolayer['thresholdValues'][0], geolayer['thresholdValues'][1], geolayer['shape'].wkb_hex, 4326])

    push_string = push_string+values_string

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

def union_road_and_zones():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate union_road_zones;')
    cursor.connection.commit()

    query = """
    insert into union_road_zones(zone_type, geom)
    select zone_type, geom from probabilityzones
    union all
    select osm_name, geom_way from road_segments;
    """

    cursor.execute(query)
    cursor.connection.commit()
    cursor.connection.close()

def add_zones_to_union():
    cursor = psycopg2.connect(**credentials).cursor()

    query = """
    delete from union_road_zones where zone_type = 'S' or zone_type = 'W';
    """

    cursor.execute(query)
    cursor.connection.commit()

    query = """
    insert into union_road_zones(zone_type, geom)
    select zone_type, geom from probabilityzones;
    """

    cursor.execute(query)
    cursor.connection.commit()
    cursor.connection.close()

def push_prob_zones_clean(zones):
    """ Explodes all geometries into single polygons with a probability of a road condition then inserts it """
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate probabilityzones;')
    cursor.connection.commit()

    push_string = """ insert into probabilityzones(probability, zone_type, geom)
    values """

    template = "(%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        zone_type = zone[1]
        probability = zone[2]
        geometry = zone[0]
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([probability, zone_type, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([probability, zone_type, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_rap_road_conditions(zones):
    """ Explodes all geometries into single polygons with a probability of a road condition then inserts it """
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate rap_road_conditions;')
    cursor.connection.commit()

    push_string = """ insert into rap_road_conditions(risk_index, condition_type, forecast_time, valid_time, speed_factor, geom)
    values """

    template = "(%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        risk_index = zone['risk_index']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time'].datetime
        geometry = zone['shape']
        speed_factor = zone['speed_factor']
        condition_type = zone['condition_type']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([risk_index, condition_type, forecast_time, valid_time, speed_factor, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([risk_index, condition_type, forecast_time, valid_time, speed_factor, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_gfs_road_conditions(zones):
    """ Explodes all geometries into single polygons with a probability of a road condition then inserts it """
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('delete from gfs_road_conditions;') # what is up with truncate and gfs products
    cursor.connection.commit()

    push_string = """ insert into gfs_road_conditions(zone_type, risk_and_type, risk_index, condition_type, forecast_time, valid_time, speed_factor, geom)
    values """

    template = "(%s,%s,%s,%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        zone_type = zone['zone_type']
        risk_and_type = zone['risk_and_type']
        risk_index = zone['risk_index']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time'].datetime
        geometry = zone['shape']
        speed_factor = zone['speed_factor']
        condition_type = zone['condition_type']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([zone_type, risk_and_type, risk_index, condition_type, forecast_time, valid_time, speed_factor, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([zone_type, risk_and_type, risk_index, condition_type, forecast_time, valid_time, speed_factor, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_impedance_polygons(zones):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate impedance_polygons;')
    cursor.connection.commit()

    push_string = """ insert into impedance_polygons(road_condition, speed_factor, forecast_time, valid_time, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        road_condition = zone['road_condition']
        speed_factor = zone['speed_factor']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time']
        geometry = zone['shape']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([road_condition, speed_factor, forecast_time, valid_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([road_condition, speed_factor, forecast_time, valid_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()


def push_risk_polygons(zones):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate risk_polygons;')
    cursor.connection.commit()

    push_string = """ insert into risk_polygons(risk_index, forecast_time, valid_time, geom)
    values """

    template = "(%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        risk_index = zone['risk_index']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time']
        geometry = zone['shape']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([risk_index, forecast_time, valid_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([risk_index, forecast_time, valid_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_ecmwf_risk_polygons(zones):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate ecmwf_risk_polygons;')
    cursor.connection.commit()

    push_string = """ insert into ecmwf_risk_polygons(risk_index, forecast_time, valid_time, geom)
    values """

    template = "(%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        risk_index = zone['risk_index']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time']
        geometry = zone['shape']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([risk_index, forecast_time, valid_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([risk_index, forecast_time, valid_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def clean_ecmwf_live_risk(ids = None):
    cursor = psycopg2.connect(**credentials).cursor()

    if ids is None:
        cursor.execute('select id from ecmwf_live_risk;')
        all_ids = cursor.fetchall()
        cursor.connection.commit()
        return all_ids

    #cursor.execute('truncate ecmwf_live_risk;')
    cursor.execute('delete from ecmwf_live_risk where id in %s;',ids)
    cursor.connection.commit()

    cursor.connection.close()

def push_ecmwf_live_risk(zones):
    cursor = psycopg2.connect(**credentials).cursor()

    #cursor.execute('truncate ecmwf_live_risk;')
    #cursor.connection.commit()

    push_string = """ insert into ecmwf_live_risk(risk_index, forecast_time, valid_time, geom)
    values """

    template = "(%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        risk_index = zone['risk_index']
        forecast_time = zone['forecast_time']
        valid_time = zone['valid_time'].datetime
        geometry = zone['shape']
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([risk_index, forecast_time, valid_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([risk_index, forecast_time, valid_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()


def push_visibility_zones_clean(zones):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate visibility_zones;')
    cursor.connection.commit()

    push_string = """ insert into visibility_zones(probability, zone_type, risk_level, colour, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        zone_type = zone[1]
        probability = zone[2]
        colour = '#004C99'
        if zone_type == 'B':
            risk_level = 2
        else:
            risk_level = 1
        geometry = zone[0]
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([probability, zone_type, risk_level,colour, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([probability, zone_type, risk_level, colour, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_prob_zones_forecasted(zones, forecast_time, run_time):
    """ Explodes all geometries into single polygons with a probability of a road condition then inserts it """
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('delete from probability_zones_forecasted where forecast_time = %s;',(forecast_time,))
    cursor.connection.commit()

    push_string = """ insert into probability_zones_forecasted(probability, zone_type, forecast_time, run_time, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for zone in zones:
        zone_type = zone[1]
        probability = zone[2]
        geometry = zone[0]
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    if geom.area > 0:
                        full_template.append(template)
                        templated_data.extend([probability, zone_type, forecast_time, run_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([probability, zone_type, forecast_time, run_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def clean_forecasted_zones(run_time):
    cursor = psycopg2.connect(**credentials).cursor()
    cursor.execute('delete from probability_zones_forecasted where run_time < %s;',(run_time-60*60*2,))
    cursor.connection.commit()
    cursor.connection.close()

def retrieve_prob_zones_forecasted(forecast_time):
    cursor = psycopg2.connect(**credentials).cursor()
    
    cursor.execute("select geom, zone_type, probability, forecast_time from probability_zones_forecasted where forecast_time=%s",(forecast_time,))# where %s - run_time < 30', (run_time,))
    geoms = cursor.fetchall()

    prob_zones = [(wkb.loads(geom[0],hex=True),geom[1],geom[2],geom[3]) for geom in geoms]

    cursor.connection.close()

    return prob_zones

def retrieve_prob_zones_forecasted_all():
    cursor = psycopg2.connect(**credentials).cursor()
    
    cursor.execute("select geom, zone_type, probability, forecast_time from probability_zones_forecasted")
    geoms = cursor.fetchall()

    prob_zones = [(wkb.loads(geom[0],hex=True),geom[1],geom[2],geom[3]) for geom in geoms]

    cursor.connection.close()

    return prob_zones


def push_prob_zones(zones, extras=None):
    """ Explodes all geometries into single polygons with a probability of a road condition then inserts it """
    cursor = psycopg2.connect(**credentials).cursor()

    past_threshold = arrow.utcnow().replace(hours=-24).datetime

    cursor.execute('delete from all_prob_zones where timestamp < %s;',(past_threshold,))
    cursor.connection.commit()

    push_string = """ insert into all_prob_zones(probability, zone_type, timestamp, geom)
    values """

    template = "(%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    #current_time = int(time.time())
    current_time = arrow.utcnow() 
    
    if extras is not None:
        current_time = extras['timestamp']
        current_time_arrow = current_time

    hour_time = current_time.floor('hour')
    current_time = hour_time.replace(minutes=int(current_time.minute/10)*10).datetime # WMS knows to query for exact minute

    full_template = []
    templated_data = []
    for zone in zones:
        zone_type = zone[1]
        probability = zone[2]
        geometry = zone[0]
        if geometry.area > 0:
            if geometry.type == 'MultiPolygon':
                for geom in geometry:
                    full_template.append(template)
                    templated_data.extend([probability, zone_type, current_time, geom.wkb_hex, 4326])
            elif geometry.type == 'Polygon':
                full_template.append(template)
                templated_data.extend([probability, zone_type, current_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string

    cursor.execute('delete from all_prob_zones where timestamp = %s ',(current_time,)) # Replacing forecasted products
    cursor.connection.commit()

    if extras is not None:
        cursor.execute('delete from all_prob_zones where timestamp < %s and timestamp > %s and timestamp > %s;',(current_time,current_time_arrow.replace(minutes=-51).datetime,arrow.utcnow().datetime)) # Replacing forecasted products, part 2
    cursor.connection.commit()

    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.connection.close()

def retrieve_prob_zones():
    cursor = psycopg2.connect(**credentials).cursor()
    cursor.execute('select geom, zone_type, probability from probabilityzones')
    geoms = cursor.fetchall()
    prob_zones = [(wkb.loads(geom[0],hex=True),geom[1],geom[2]) for geom in geoms]

    cursor.connection.close()

    return prob_zones

def prepare_larger_road_segments():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('select geom_way from larger_road_segments')
    geoms = cursor.fetchall()

    shapely_geoms = [wkb.loads(geom[0],hex=True) for geom in geoms]

    cursor.connection.close()

    roads = geometry.GeometryCollection(shapely_geoms)

    with open('../persistent/larger_roads.wkb','w') as opened:
        wkb.dump(roads,opened, hex=True)


def prepare_persistent_data():
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('select geom_way from road_segments')
    geoms = cursor.fetchall()

    shapely_geoms = [wkb.loads(geom[0],hex=True) for geom in geoms]

    cursor.connection.close()

    roads = geometry.GeometryCollection(shapely_geoms)

    with open('../persistent/roads.wkb','w') as opened:
        wkb.dump(roads,opened, hex=True)

def push_assessed_roads_1(roads):
    return
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate assessed_roads;')
    cursor.connection.commit()

    push_string = """ insert into assessed_roads(probability, zone_type, geom)
    values """

    template = "(%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()
    cursor.connection.close()

def push_assessed_roads_4(roads):
    return
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('truncate assessed_roads;')
    cursor.connection.commit()

    cursor.execute(""" drop index assessed_roads_index """)
    cursor.connection.commit()

    push_string = """ insert into assessed_roads(probability, zone_type, geom)
    values """

    template = "(%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    cursor.execute(""" create index assessed_roads_index on assessed_roads using gist (geom); """)
    cursor.connection.commit()

    cursor.connection.close()

def push_assessed_roads_rename(roads):
    cursor = psycopg2.connect(**credentials).cursor()

    #cursor.execute('truncate assessed_roads;')
    #cursor.connection.commit()

    #cursor.execute(""" drop index assessed_roads_index """)
    #cursor.connection.commit()

    cursor.execute(""" create table temp_assessed_roads as select * from assessed_roads with no data; """)
    cursor.execute(""" create index temp_assessed_roads_index on temp_assessed_roads using gist (geom); """)
    #cursor.connection.commit()

    push_string = """ insert into temp_assessed_roads(probability, zone_type, geom)
    values """

    template = "(%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string+';', templated_data)
    #cursor.connection.commit()

    #print 'Temp table done', time.gmtime()

    cursor.execute("drop table assessed_roads;")
    cursor.execute("alter table temp_assessed_roads rename to assessed_roads;")
    cursor.execute("alter index temp_assessed_roads_index rename to assessed_roads_index;")
    cursor.connection.commit()

    #cursor.execute(""" create index assessed_roads_index on assessed_roads using gist (geom); """)
    #cursor.connection.commit()

    cursor.connection.close()

def push_assessed_roads_3(roads):
    return
    cursor = psycopg2.connect(**credentials).cursor()

    #cursor.execute('truncate assessed_roads;')
    #cursor.connection.commit()

    #cursor.execute(""" drop index assessed_roads_index """)
    #cursor.connection.commit()

    cursor.execute(""" create temp table temp_assessed_roads as select * from assessed_roads with no data """)
    cursor.connection.commit()

    push_string = """ insert into temp_assessed_roads(probability, zone_type, geom)
    values """

    template = "(%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string, templated_data)
    cursor.connection.commit()

    print 'Temp table done', time.gmtime()

    cursor.execute("truncate assessed_roads")
    cursor.execute("insert into assessed_roads(probability,zone_type,geom) select probability,zone_type,geom from temp_assessed_roads")
    cursor.connection.commit()

    #cursor.execute(""" create index assessed_roads_index on assessed_roads using gist (geom); """)
    #cursor.connection.commit()

    cursor.connection.close()


def push_assessed_roads_forecasted(roads, run_time, forecast_time):
    cursor = psycopg2.connect(**credentials).cursor()

    cursor.execute('delete from assessed_roads_forecasted where forecast_time = %s;',(forecast_time,))
    #cursor.connection.commit()

    push_string = """ insert into assessed_roads_forecasted(probability, zone_type, run_time, forecast_time, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, run_time, forecast_time, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, run_time, forecast_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string+';', templated_data)
    
    cursor.connection.commit()
    cursor.connection.close()


def push_assessed_roads_forecasted_temp_table(roads, run_time, forecast_time):
    cursor = psycopg2.connect(**credentials).cursor()

    #cursor.execute('delete from assessed_roads_forecasted where forecast_time = %s;',(forecast_time,))
    #cursor.connection.commit()

    cursor.execute(""" create temp table temp_assessed_roads_forecasted as select * from assessed_roads_forecasted with no data; """)
    #cursor.execute(""" create index temp_assessed_roads_index on temp_assessed_roads using gist (geom); """)
    #cursor.connection.commit()


    push_string = """ insert into temp_assessed_roads_forecasted(probability, zone_type, run_time, forecast_time, geom)
    values """

    template = "(%s,%s,%s,%s,ST_SetSRID(%s::geometry,%s))"

    full_template = []
    templated_data = []
    for road in roads:
        zone_type = road[1]
        probability = road[2]
        geometry = road[0]
        if geometry.type == 'MultiLineString':
            for geom in geometry:
                full_template.append(template)
                templated_data.extend([probability, zone_type, run_time, forecast_time, geom.wkb_hex, 4326])
        elif geometry.type == 'LineString':
            full_template.append(template)
            templated_data.extend([probability, zone_type, run_time, forecast_time, geometry.wkb_hex, 4326])

    values_string = ','.join(full_template)
    push_string = push_string+values_string
    cursor.execute(push_string+';', templated_data)
    

    cursor.execute('delete from assessed_roads_forecasted where forecast_time = %s;',(forecast_time,))
    cursor.execute("insert into assessed_roads_forecasted(probability,zone_type,run_time, forecast_time, geom) select probability,zone_type,run_time, forecast_time, geom from temp_assessed_roads_forecasted")

    cursor.connection.commit()
    cursor.connection.close()


def intersect_roads():
    """ Fills a table with roads and their states (avoiding Dry) """

    cursor = psycopg2.connect(**credentials).cursor()

    #cursor.execute('alter index assessed_roads_index ON Schema.assessed_roads disable;')
    #cursor.connection.commit()
    cursor.execute('drop index assessed_roads_index;')
    cursor.connection.commit()

    cursor.execute('truncate assessed_roads;')
    cursor.connection.commit()

    query = """
    create temp table temp_assessed_roads as (
    select
        road_segments.osm_id,
        road_segments.osm_name,
        probabilityzones.zone_type,
        probabilityzones.probability,
        case
            when ST_CoveredBy(road_segments.geom_way, probabilityzones.geom)
            then road_segments.geom_way
            else ST_Multi(ST_Intersection(road_segments.geom_way, probabilityzones.geom))
        end as geom
    from road_segments
    join probabilityzones
    on ST_Intersects(road_segments.geom_way, probabilityzones.geom)
    );

    truncate assessed_roads;

    insert into assessed_roads(osm_id, osm_name, zone_type, probability, geom)
    select * from temp_assessed_roads;
    """

    query = """
    with to_insert as (
        select osm_id, osm_name, probabilityzones.zone_type, probabilityzones.probability, geom_way
        from road_segments
        join probabilityzones
        on ST_Within(geom_way, probabilityzones.geom)
        )
    insert into assessed_roads(osm_id, osm_name, zone_type, probability, geom)
        select * from to_insert;
    """

    query2 = """
    insert into assessed_roads(osm_id, osm_name, zone_type, probability, geom)
    select
        road_segments.osm_id,
        road_segments.osm_name,
        probabilityzones.zone_type,
        probabilityzones.probability,
        case
            when ST_CoveredBy(road_segments.geom_way, probabilityzones.geom)
            then road_segments.geom_way
            else ST_Multi(ST_Intersection(road_segments.geom_way, probabilityzones.geom))
        end as geom
    from road_segments
    join probabilityzones
    on ST_Intersects(road_segments.geom_way, probabilityzones.geom);
    """

    cursor.execute(query)
    cursor.connection.commit()

    #cursor.execute('alter index assessed_roads_index ON Schema.assessed_roads rebuild;')
    #cursor.connection.commit()
    cursor.execute('create index assessed_roads_index on assessed_roads using gist(geom);')
    cursor.connection.commit()

    cursor.connection.close()

def get_road_temperature_polygons():
    cursor = psycopg2.connect(**credentials).cursor()
    
    cursor.execute('select * from road_temperature_contours')
    polygons = cursor.fetchall()
    
    cursor.connection.close()
    
    return polygons    

def get_polygons():
    cursor = psycopg2.connect(**credentials).cursor()

    query = """
    select variable_name, minimum_value, maximum_value, geom from thresholdedpolygons
    """

    cursor.execute(query)

    results = cursor.fetchall()

    dicts = []
    for result in results:
        dicts.append({'variableName':result[0], 'thresholdValues': [result[1],result[2]], 'geometry': result[3]})

    cursor.connection.close()

    return dicts

def get_cursor():
    connection = psycopg2.connect(**credentials)
    cursor = connection.cursor()
    return cursor

if __name__ == '__main__':
    define_tables()
    define_indexes()
    #get_polygons()
    #test_query()
