import subprocess
import time
import os
import shutil
import json
import requests
from bs4 import BeautifulSoup
from urllib import urlretrieve

unit_conversion = 1/0.44704
os.environ['JAVA_HOME'] = '/usr/lib/jvm/java-1.7.0-openjdk-amd64/'

conversion_command = "../../grib2json/target/grib2json-0.8.0-SNAPSHOT/bin/grib2json --names --data --fp wind --fs 103 --fv 10.0"
 #~/Downloads/gfs.t12z.pgrb2.0p25.f000 > gfs.json
gfs_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/com/gfs/prod/'
storage_path = '../../'

destination_path = '/var/www/fuzion-webpack/dist/assets/json/'
#destination_path = '/var/windtest/'

def gfsproducts(storage_path=storage_path, end='.pgrb2.1p00.f000'):
    """ Downloads live GFS files """

    try:
        req = requests.get(gfs_url)
        soup = BeautifulSoup(req.text, 'lxml')
        links = soup.find_all('a')
        links = sorted([link.get('href') for link in links if link.get('href').startswith('gfs.')])
        link = links[-1]

        req = requests.get(gfs_url+link)
        soup = BeautifulSoup(req.text, 'lxml')
        dirlinks = soup.find_all('a')
        dirlinks = sorted([dirlink.get('href') for dirlink in dirlinks if dirlink.get('href').endswith(end)])
        dirlink = dirlinks[-1]

        fileurl = gfs_url+link+dirlink
        print fileurl
        filepath = os.path.join(storage_path,dirlink)
        if not os.path.exists(filepath):
            urlretrieve(fileurl, filepath)
        return filepath

    except Exception as e:
        print str(e)

def main():

    while True:
        filepath = gfsproducts()
        forecast_filepath = gfsproducts(end='.pgrb2.1p00.f003')
        print 'Downloaded'

        to_execute = conversion_command.split(' ')+[filepath]#,'>',storage_path+'gfs.json']
        result = json.loads(subprocess.check_output(to_execute))
        forecast_to_execute = conversion_command.split(' ')+[forecast_filepath]#,'>',storage_path+'gfs-next.json']
        forecast_result = json.loads(subprocess.check_output(forecast_to_execute))

        print 'Converted'
        result[0]['data'] = [value*unit_conversion for value in result[0]['data']]
        forecast_result[0]['data'] = [value*unit_conversion for value in forecast_result[0]['data']]

        os.remove(filepath)
        os.remove(forecast_filepath)

        with open(destination_path+'gfs.json','w') as opened:
            json_string = json.dumps(result)
            json_string = json_string.replace(' ','')
            opened.write(json_string)
        print 'Written'
        time.sleep(10800)

        with open(destination_path+'gfs.json','w') as opened:
            json_string = json.dumps(forecast_result)
            json_string = json_string.replace(' ','')
            opened.write(json_string)
        print 'Written again'
        time.sleep(10800)

if __name__ == '__main__':
    main()
