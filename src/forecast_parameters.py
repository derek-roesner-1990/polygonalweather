#mrms_files = ['PrecipRate/', 'PrecipFlag/', 'GaugeCorr_QPE_01H/', 'GaugeCorr_QPE_06H/']
mrms_files = []
# HRRR indices first, then same order as mrms_files
#variable_indices = [15, 48, 'preciprate', 'precipflag', 'qpe1hour', 'qpe6hour']
#variable_indices = ['TMP_P0_L103_GLC0','APCP_P8_L1_GLC0_acc1h', 'CSNOW_P0_L1_GLC0', 'CICEP_P0_L1_GLC0', 'CFRZR_P0_L1_GLC0', 'CRAIN_P0_L1_GLC0']
variable_indices = ['CSNOW_P0_L1_GLC0','CRAIN_P0_L1_GLC0','CFRZR_P0_L1_GLC0','CICEP_P0_L1_GLC0','APCP_P8_L1_GLC0_acc','TMP_P0_L103_GLC0','DSWRF_P0_L1_GLC0']#,'REFD_P0_L103_GLC0']#,'APCP_P8_L1_GLC0_accHHh']

forecast_times = ['02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18']

###############################################################

number_of_hrrr_products = len(variable_indices)-len(mrms_files)

mrms_url = 'http://mrms.ncep.noaa.gov/data/2D/'
#grib_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/'

#hrrr_url = 'nonoperational/com/hrrr/prod/hrrr.'
#hrrr_tag = 'wrfnatfFF.grib2'

hrrr_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/nonoperational/com/hrrr/prod/'

storage_path = '../liveproductdata/'

# DB parameters

gaussian_smoothing = True
median_smoothing = False

base_gaussian_sigma = 4
base_median_size = 9

# GeoJSON related

geojson_for_display = False

clean_up = True