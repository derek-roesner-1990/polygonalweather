"""

Transforms gridded datasets into thresholded geolocated polygons. 
Many variables reside in parameters.py

"""

import sys
import time
import calendar
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
pyplot.rcParams['path.simplify_threshold'] = 0.00001
from matplotlib import colors
from scipy import ndimage
from scipy import misc
import Nio
import shapely
from shapely import ops
import geojson
import json
import ast
import os

import postgres_functions
import mongodb_functions
from dsparameters import *
from live_feed_functions import hrrproducts, mrmsproducts, remove_old_files
from contour_functions import polygonize

from shapely import speedups
speedups.enable()
            
def process_file(filepath, variable_index, geolayers, mongo_client = None, observations = None):
    """ Opens and selects desired variables, returns records of polygons with relevant info """
    opened = Nio.open_file(filepath)

    if "MRMS" in filepath:
        x,y = np.meshgrid(opened.variables['lon_0'].get_value(),opened.variables['lat_0'].get_value())
        x = x-360 # fixing unsigned longitudes
        variable = [key for key in opened.variables.keys() if 'lon_0' not in key and 'lat_0' not in key][0] # One variable per file
        gaussian_sigma = base_gaussian_sigma*2
    else:
        x = opened.variables['gridlon_0'].get_value()
        y = opened.variables['gridlat_0'].get_value() 
        gaussian_sigma = base_gaussian_sigma
        try:
            index = variable_index
            variable = opened.variables.keys()[index]
        except Exception as e:
            print e    

    data = opened.variables[variable].get_value()
    variable_name = opened.variables[variable].long_name
    #data[data == -3] = 0
    
    #data = ndimage.interpolation.zoom(data, 0.10, order=0)
    #data = ndimage.morphology.grey_closing(data, size=(5,5))

    print 'Creating polygons for', variable_name

    current_key = None
    thresholds = None
    for key in threshold_map.keys():
        if key in variable_name.lower().replace(' ',''):
            current_key = key
    
    if current_key == None and isinstance(variable_index, str):
        current_key = variable_index

    thresholds = threshold_map[current_key]
    print current_key

    # No smoothing necessary if no one is going to see them, probably
    if gaussian_smoothing:
        if current_key not in categorical_variables:
            data = ndimage.gaussian_filter(data,sigma=gaussian_sigma) #TODO Have it automatically computed based on grid resolution
        else:
            data = ndimage.median_filter(data,(base_median_size,base_median_size))
    #data = ndimage.median_filter(data,(5,5))
    

    # Takes care of whether a high value represents a high risk or the inverse
    inverted = False
    if current_key in inverse_variables:
        inverted = True

    levels, thresholds = polygonize(x,y,data,which_variable=current_key,thresholds=thresholds, inverted=inverted)

    opened.close()

    #multipolygons = []
    #outputs = []
    records = []

    entities = []

    for i, level in enumerate(levels):
        if clean_up:
            #simplified = [polygon.simplify(tolerance=0.01, preserve_topology=True) for polygon in level] 
            simplified = [polygon.buffer(0) for polygon in level] 
            level = simplified

        multipolygon = ops.unary_union(level)
        if multipolygon.area == 0:
            continue
        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if current_key in inverse_variables:
            if i == 0:
                geolayer_dict['thresholdValues'] = [float(data.min()), thresholds[i]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i-1],thresholds[i]]
        else:
            if i < (len(thresholds)-1):
                geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i],float(data.max())]

            geolayer_dict['variableName'] = current_key

        if geojson_for_display:
            geolayer_dict['colour'] = color_map[current_key][value_to_string_map[current_key][thresholds[i]]]
            riskLevel = risk_map[value_to_string_map[current_key][thresholds[i]]]

            new_geolayer_dict = dict()

            new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
            new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel}
        
        else:
            new_geolayer_dict = geolayer_dict
        
        if using_postgres:
            new_geolayer_dict['shape'] = multipolygon
        records.append(new_geolayer_dict)

        if mongo_client is not None:
            if using_truck_risk:
                mongodb_functions.process_observations(mongo_client, observations, multipolygon, current_key, geolayer_dict, thresholds[i], riskLevel) 

        # Adds to entities
        if using_geotab:
            geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, variable_name)

    geolayers.extend(records)

    if using_geotab:
        cleaned_up = geotab_functions.deal_with_holes(entities)
        geotab_functions.push_to_geotab(cleaned_up)

    print 'Geolayer done!'

def main():
    """ Downloads files and push geojson polygons """
    print time.gmtime()
    if using_geotab:
        geotab_functions.delete_old_zones()
        print 'Deleted'

    mongo_client = None
    observations = None
    geolayers = []

    if using_postgres:
        cursor = postgres_functions.get_cursor()

    if using_mongo:
        mongo_client, observations = mongodb_functions.prepare_mongo()    

    filepaths = [hrrproducts(storage_path)]
    filepaths = filepaths*number_of_hrrr_products
    for mrms_file in mrms_files:
        filepaths.append(mrmsproducts(mrms_file,storage_path))
    
    for filepath,variable_index in zip(filepaths,variable_indices):
        process_file(filepath, variable_index, geolayers, mongo_client, observations)

    if mongo_client is not None:
        mongodb_functions.push_to_mongo(mongo_client, geolayers)
        if using_truck_risk:
            mongodb_functions.push_devices_to_mongo(mongo_client, observations)

    if using_mongo_polygon:
        mongodb_functions.push_polygons_to_mongo(geolayers, mongouri, polygon_collection)

    if using_postgres:
        postgres_functions.push_thresholded_polygons(cursor,geolayers)
        cursor.connection.close()

    remove_old_files(storage_path)

    if mongo_client is not None:
        mongo_client.close()

    print time.gmtime()
if __name__ == '__main__':
    main()
