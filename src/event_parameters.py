""" Keeps tracks of which layers/files/variables we want and how we color/threshold them """

color_map = dict()
color_map['qpe'] = dict()
color_map['visibility'] = dict()
color_map['windspeed'] = dict()
color_map['posh'] = dict()
color_map['lightning'] = dict()
color_map['seamless'] = dict()

color_map['qpe']['low'] = '#E09485'
color_map['visibility']['low'] ='#ADE0EB'
color_map['windspeed']['low'] = '#85E085'

color_map['qpe']['medium'] = '#FF5533'
color_map['visibility']['medium'] = '#3DD6F5'
color_map['windspeed']['medium'] = '#089208' #3DF53D'

color_map['qpe']['high'] = '#CC2200'
color_map['visibility']['high'] ='#004C99'
color_map['windspeed']['high'] = '#044904' #009900'

color_map['posh']['low'] = '#FFFF99'
color_map['posh']['medium'] = '#FFFF00'
color_map['posh']['high'] = '#595904'

color_map['lightning']['low'] = '#BF7FBF'
color_map['lightning']['medium'] = '#990099'
color_map['lightning']['high'] = '#400040'

#seamless_colors = ['#de78b2','#711f88','#3c1148','#c8bf59','#6f6a32','#00eced','#009ef9','#0300cc','#02fd02','#01c501','#008e00','#fdf802','#e5bc00','#ff2700','#fd0000','#d40000','#bc0000','#f800fd','#9854c6','#fdfdfd']
seamless_colors = ['#6f6a32','#00eced','#009ef9','#0300cc','#02fd02','#01c501','#008e00','#fdf802','#e5bc00','#ff2700','#fd0000','#d40000','#bc0000','#f800fd','#9854c6','#fdfdfd']

threshold_map = dict()
threshold_map['qpe'] = [2,4,6]
threshold_map['visibility'] = [500,1000,3000]
threshold_map['windspeed'] = [13.41,22.35,40.00]
threshold_map['posh'] = [1,33,66]
threshold_map['lightning'] = [1,33,66]
#threshold_map['seamless'] = [-25,-20,-15,-10,-5,0,5,10,15,20,25,30,35,40,45,50,55,60,65,70]
threshold_map['seamless'] = [-5,0,5,10,15,20,25,30,35,40,45,50,55,60,65,70]

for i,thresholdvalue in enumerate(threshold_map['seamless']):
    color_map['seamless'][str(thresholdvalue)] = seamless_colors[i]

keys = ['qpe', 'visibility', 'windspeed', 'posh', 'lightning', 'seamless']

inverse_variables = ['visibility']

value_to_string_map = dict()
for key in keys:
    value_to_string_map[key] = dict()
    if key in inverse_variables:
        value_to_string_map[key][threshold_map[key][-1]] = 'low'
        value_to_string_map[key][threshold_map[key][0]] = 'high'
        value_to_string_map[key][threshold_map[key][1]] = 'medium'
    elif key == 'seamless':
        for thresholdvalue in threshold_map[key]:
            value_to_string_map[key][thresholdvalue] = str(thresholdvalue)
    else:
        value_to_string_map[key][threshold_map[key][-1]] = 'high'
        value_to_string_map[key][threshold_map[key][0]] = 'low'
        value_to_string_map[key][threshold_map[key][1]] = 'medium'


risk_map = dict()
risk_map['high'] = 3
risk_map['medium'] = 2
risk_map['low'] = 1

mrms_url = 'http://mrms.ncep.noaa.gov/data/2D/'
#grib_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/'

#hrrr_url = 'nonoperational/com/hrrr/prod/hrrr.'
#hrrr_tag = 'wrfnatfFF.grib2'

hrrr_url = 'http://www.ftp.ncep.noaa.gov/data/nccf/nonoperational/com/hrrr/prod/'

storage_path = '../liveproductdata/'

number_of_hrrr_products = 2
mrms_files = ['RadarOnly_QPE_01H/', 'POSH/', 'LightningProbabilityNext30min/', 'SeamlessHSR/']

variable_indices = [11, 50, None, None, None, None]

# DB parameters

mongouri = 'mongodb://10.128.0.3,10.128.0.4,10.128.0.5/fuzion_db?replicaSet=rs0'
#mongouri = 'mongodb://192.168.5.45:27017'
db_name = 'fuzion_db'
input_name = 'wtxobservations'
output_name = 'eventlivevalayers'
output_name_devices = 'livealerteddevices'
using_mongo = False
using_postgres = True

using_geotab = False
