import time
import ast
import datetime
import multiprocessing
import traceback
import logging #Gotta actually add calls to logging
import arrow
import pytz
import mygeotab
import toolz
import json
import operator
from matplotlib import colors
from shapely.geometry import Polygon
from shapely.ops import unary_union
from data_handler import database

zone_types_id = [{u'comment': u'', u'name': u'Alert', u'id': u'b500'}, {u'comment': u'', u'name': u'Hydroplaning Risk Low', u'id': u'b600'}, {u'comment': u'', u'name': u'Hydroplaning Risk Medium', u'id': u'b700'}, {u'comment': u'', u'name': u'Hydroplaning Risk High', u'id': u'b800'}, {u'comment': u'', u'name': u'Low Visibility Risk High', u'id': u'b900'}, {u'comment': u'', u'name': u'Low Visibility Risk Medium', u'id': u'bA00'}, {u'comment': u'', u'name': u'Low Visibility Risk Low', u'id': u'bB00'}, {u'comment': u'', u'name': u'Wind Risk High', u'id': u'bC00'}, {u'comment': u'', u'name': u'Wind Risk Medium', u'id': u'bD00'}, {u'comment': u'', u'name': u'Wind Risk Low', u'id': u'bE00'}, {u'comment': u'', u'name': u'Hail Risk High', u'id': u'bF00'}, {u'comment': u'', u'name': u'Hail Risk Medium', u'id': u'b1000'}, {u'comment': u'', u'name': u'Hail Risk Low', u'id': u'b1100'}, {u'comment': u'', u'name': u'Lightning Risk High', u'id': u'b1200'}, {u'comment': u'', u'name': u'Lightning Risk Medium', u'id': u'b1300'}, {u'comment': u'', u'name': u'Lightning Risk Low', u'id': u'b1400'}, {u'comment': u'', u'name': u'Icing Risk High', u'id': u'b1500'}, {u'comment': u'', u'name': u'Icing Risk Medium', u'id': u'b1600'}, {u'comment': u'', u'name': u'Icing Risk Low', u'id': u'b1700'}, {u'comment': u'', u'name': u'No Alert', u'id': u'b1800'}]

nws_zone_types_id = [{u'comment': u'', u'name': u'Air Quality Alert', u'id': u'b1900'},{u'comment': u'', u'name': u'Air Stagnation Advisory', u'id': u'b1A00'},{u'comment': u'', u'name': u'Blizzard Warning', u'id': u'b1B00'},{u'comment': u'', u'name': u'Blizzard Watch', u'id': u'b1C00'},{u'comment': u'', u'name': u'Blowing Dust Advisory', u'id': u'b1D00'},{u'comment': u'', u'name': u'Dense Fog Advisory', u'id': u'b1E00'},{u'comment': u'', u'name': u'Dense Smoke Advisory', u'id': u'b1F00'},{u'comment': u'', u'name': u'Dust Storm Warning', u'id': u'b2000'},{u'comment': u'', u'name': u'Extreme Fire Danger', u'id': u'b2100'},{u'comment': u'', u'name': u'Extreme Wind Warning', u'id': u'b2200'},{u'comment': u'', u'name': u'Fire Warning', u'id': u'b2300'},{u'comment': u'', u'name': u'Fire Weather Watch', u'id': u'b2400'},{u'comment': u'', u'name': u'Flash Flood Watch', u'id': u'b2500'},{u'comment': u'', u'name': u'Flash Flood Warning', u'id': u'b2600'},{u'comment': u'', u'name': u'Flood Advisory', u'id': u'b2700'},{u'comment': u'', u'name': u'Flood Warning', u'id': u'b2800'},{u'comment': u'', u'name': u'Flood Watch', u'id': u'b2900'},{u'comment': u'', u'name': u'Freezing Fog Advisory', u'id': u'b2A00'},{u'comment': u'', u'name': u'Freezing Rain Advisory', u'id': u'b2B00'},{u'comment': u'', u'name': u'Freezing Spray Advisory', u'id': u'b2C00'},{u'comment': u'', u'name': u'Hard Freeze Warning', u'id': u'b2D00'},{u'comment': u'', u'name': u'Hard Freeze Watch', u'id': u'b2E00'},{u'comment': u'', u'name': u'Heavy Freezing Spray Warning', u'id': u'b2F00'},{u'comment': u'', u'name': u'Heavy Freezing Spray Watch', u'id': u'b3000'},{u'comment': u'', u'name': u'High Wind Warning', u'id': u'b3100'},{u'comment': u'', u'name': u'High Wind Watch', u'id': u'b3200'},{u'comment': u'', u'name': u'Hurricane Force Wind Warning', u'id': u'b3300'},{u'comment': u'', u'name': u'Hurricane Force Wind Watch', u'id': u'b3400'},{u'comment': u'', u'name': u'Ice Storm Warning', u'id': u'b3500'},{u'comment': u'', u'name': u'Lake Effect Snow Warning', u'id': u'b3600'},{u'comment': u'', u'name': u'Severe Thunderstorm Warning', u'id': u'b3700'},{u'comment': u'', u'name': u'Severe Thunderstorm Watch', u'id': u'b3800'},{u'comment': u'', u'name': u'Tornado Warning', u'id': u'b3900'},{u'comment': u'', u'name': u'Tornado Watch', u'id': u'b3A00'},{u'comment': u'', u'name': u'Tropical Storm Warning', u'id': u'b3B00'},{u'comment': u'', u'name': u'Tropical Storm Watch', u'id': u'b3C00'},{u'comment': u'', u'name': u'Wind Advisory', u'id': u'b3D00'},{u'comment': u'', u'name': u'Winter Storm Warning', u'id': u'b3E00'}]

# Rankings are from low to high.
# The following zone types have been excluded from this list and their is nothing to rank each against:
# Air Quality Alert, Air Stagnation Advisory, Dense Fog Advisory, Dense Smoke Advisory, Lake Effect Snow Warning
nws_zone_types_by_severity = { 'dust': {'Blowing Dust Advisory': 1, 'Dust Storm Warning': 2}, 'fire': {'Fire Weather Watch': 1, 'Fire Warning': 2, 'Extreme Fire Danger': 3 }, 'flash_flood': {'Flash Flood Watch': 1, 'Flash Flood Warning': 2}, 'flood': {'Flood Advisory': 1, 'Flood Watch': 2, 'Flood Warning': 3}, 'thunderstorm': {'Severe Thunderstorm Watch': 1, 'Severe Thunderstorm Warning': 2}, 'wind': {'Wind Advisory': 1, 'High Wind Watch': 2, 'High Wind Warning': 3, 'Tropical Storm Watch': 4, 'Tropical Storm Warning': 5, 'Hurricane Force Wind Watch': 6, 'Hurricane Force Wind Warning': 7, 'Extreme Wind Warning': 8}, 'tornado': {'Tornado Watch': 1, 'Tornado Warning': 2}, 'freezing_spray': {'Freezing Spray Advisory': 1, 'Heavy Freezing Spray Watch': 2, 'Heavy Freezing Spray Warning': 3}, 'freezing_rain': {'Freezing Fog Advisory': 1, 'Freezing Rain Advisory': 2, 'Ice Storm Warning': 3}, 'hard_freeze': {'Hard Freeze Watch': 1, 'Hard Freeze Warning': 2}, 'blizzard': {'Blizzard Watch': 1, 'Blizzard Warning': 2}}

#nws_zone_types_by_severity = { 'dust': {'Blowing Dust Advisory': 1, 'Dust Storm Warning': 2}}

#zone_types_id = [ {u'comment': u'', u'name': u'Alert'}, {u'comment': u'', u'name': u'Hydroplaning Risk Low'}, {u'comment': u'', u'name': u'Hydroplaning Risk Medium'}, {u'comment': u'', u'name': u'Hydroplaning Risk High'}, {u'comment': u'', u'name': u'Low Visibility Risk High'}, {u'comment': u'', u'name': u'Low Visibility Risk Medium'}, {u'comment': u'', u'name': u'Low Visibility Risk Low'}, {u'comment': u'', u'name': u'Wind Risk High'}, {u'comment': u'', u'name': u'Wind Risk Medium'}, {u'comment': u'', u'name': u'Wind Risk Low'}, {u'comment': u'', u'name': u'Hail Risk High'}, {u'comment': u'', u'name': u'Hail Risk Medium'}, {u'comment': u'', u'name': u'Hail Risk Low'}, {u'comment': u'', u'name': u'Lightning Risk High'}, {u'comment': u'', u'name': u'Lightning Risk Medium'}, {u'comment': u'', u'name': u'Lightning Risk Low'}, {u'comment': u'', u'name': u'Icing Risk High'}, {u'comment': u'', u'name': u'Icing Risk Medium'}, {u'comment': u'', u'name': u'Icing Risk Low'}, {u'comment': u'', u'name': u'No Alert'} ]

#nws_zone_types_id = [ {u'comment': u'', u'name': u'Air Quality Alert'}, {u'comment': u'', u'name': u'Air Stagnation Advisory'}, {u'comment': u'', u'name': u'Blizzard Warning'}, {u'comment': u'', u'name': u'Blizzard Watch'}, {u'comment': u'', u'name': u'Blowing Dust Advisory'}, {u'comment': u'', u'name': u'Dense Fog Advisory'}, {u'comment': u'', u'name': u'Dense Smoke Advisory'}, {u'comment': u'', u'name': u'Dust Storm Warning'}, {u'comment': u'', u'name': u'Extreme Fire Danger'}, {u'comment': u'', u'name': u'Extreme Wind Warning'}, {u'comment': u'', u'name': u'Fire Warning'}, {u'comment': u'', u'name': u'Fire Weather Watch'}, {u'comment': u'', u'name': u'Flood Warning'}, {u'comment': u'', u'name': u'Flood Watch'}, {u'comment': u'', u'name': u'Freezing Fog Advisory'}, {u'comment': u'', u'name': u'Freezing Rain Advisory'}, {u'comment': u'', u'name': u'Freezing Spray Advisory'}, {u'comment': u'', u'name': u'Hard Freeze Warning'}, {u'comment': u'', u'name': u'Hard Freeze Watch'}, {u'comment': u'', u'name': u'Heavy Freezing Spray Warning'}, {u'comment': u'', u'name': u'Heavy Freezing Spray Watch'}, {u'comment': u'', u'name': u'High Wind Warning'}, {u'comment': u'', u'name': u'High Wind Watch'}, {u'comment': u'', u'name': u'Hurricane Force Wind Warning'}, {u'comment': u'', u'name': u'Hurricane Force Wind Watch'}, {u'comment': u'', u'name': u'Ice Storm Warning'}, {u'comment': u'', u'name': u'Lake Effect Snow Warning'}, {u'comment': u'', u'name': u'Severe Thunderstorm Warning'}, {u'comment': u'', u'name': u'Severe Thunderstorm Watch'}, {u'comment': u'', u'name': u'Tornado Warning'}, {u'comment': u'', u'name': u'Tornado Watch'}, {u'comment': u'', u'name': u'Tropical Storm Warning'}, {u'comment': u'', u'name': u'Tropical Storm Watch'}, {u'comment': u'', u'name': u'Wind Advisory'}, {u'comment': u'', u'name': u'Winter Storm Warning'} ]

#no_alert = {u'comment': u'', u'name': u'No Alert'}

#ds_zone_types_id = [{u'comment': u'', u'name': u'Wet Surface Risk'},{u'comment': u'', u'name': u'Snowy Surface Risk'},{u'comment': u'', u'name': u'Icy Surface Risk'}]

no_alert = {u'comment': u'', u'name': u'No Alert', u'id': u'b1800'}

ds_zone_types_id = [{u'comment': u'', u'name': u'Wet Surface Risk', u'id': u'b2001'},{u'comment': u'', u'name': u'Snowy Surface Risk', u'id': u'b2002'},{u'comment': u'', u'name': u'Icy Surface Risk', u'id': u'b2003'}]

all_zone_types_id = zone_types_id+nws_zone_types_id+ds_zone_types_id

#Commented out as this variable is not currently used throughout the solution.
#just_ids = [one_zone_types_id['id'] for one_zone_types_id in zone_types_id]

zone_types = {'Hydroplaning':'Hydroplaning Risk','visibility':'Low Visibility Risk','windspeed':'Wind Risk','posh':'Hail Risk', 'lightning':'Lightning Risk', 'icing':'Icing Risk', 'W':'Wet Surface Risk', 'S':'Snowy Surface Risk','I':'Icy Surface Risk'}
risk_map = {1: ' Low', 2:' Medium', 3:' High'}

contour_names = ['lightning_','Hydroplaning_','visibility_','windspeed_','posh_','icing_']
ds_names = ['Wet Surface Risk','Snowy Surface Risk','Icy Surface Risk']
nws_names = ['nwsalert_']

def write_multicalls(entities, key):
    multicalls = []
    for entity in entities:
        multicalls.append(('Add',{'typeName':'Zone','entity':entity}))
    
    to_save = str(multicalls)

    database.set(key, to_save)

def get_rgba_from_hex(hex_color_val, alpha):
	try:
		fill = {'r': None, 'g': None, 'b': None, 'a': None}
		rgb_color = colors.hex2color(hex_color_val)
		fill['r'] = int(rgb_color[0]*255)
		fill['g'] = int(rgb_color[1]*255)
		fill['b'] = int(rgb_color[2]*255)
		fill['a'] = alpha
	except Exception as e:
		raise Exception("An error occured while getting RBGa values from the provided hexadecimal value.\nDetails: {}".format(e))
	return fill

def get_zone_type_entity(alert_type, list_of_zone_types):
	try:
		zone_type = None

		for type in list_of_zone_types:
			if alert_type == type['name']:
				zone_type = type
				break

	except Exception as e:
		raise Exception("An error occured while getting the ZoneType related to the NWS alert type of {}.\nDetails: {}".format(alert_type, e))
	return zone_type

# Returning a list of Shapely Polygons based on the geometry associated with a specific Feature from a FeatureCollection.
# More info :
# https://shapely.readthedocs.io/en/latest/manual.html#Polygon
# http://wiki.geojson.org/GeoJSON_draft_version_6#FeatureCollection
def get_polygons_from_geometry(geometry):
	try:
		polygons = []

		if geometry['type'] == "MultiPolygon":
			# The value of geometry['coordinates'] will be in this form - [[[[x,y],[x,y],[x,y],[x,y]]], [[[x,y],[x,y],[x,y],[x,y]]]]
			for coordinates in geometry['coordinates']:
				polygons.append(Polygon(coordinates[0]))
		else:
			# The value of geometry['coordinates'] will be in this form - [[[x,y],[x,y],[x,y],[x,y]]]
			polygons.append(Polygon(geometry['coordinates'][0]))

	except Exception as e:
		raise Exception("An error occured while creating polygons from this Features geometry.\nDetails {}".format(e))
	return polygons

# Polygons is a list of Shapely Polygon instances.
def merge_adjacent_polygons(polygons):
	try:
		merged_polygons = []

		# Collecting a slightly inflated version of each Polygon.
		# More info on buffer() - https://shapely.readthedocs.io/en/latest/manual.html#object.buffer
		exteriors = [ Polygon(polygon.exterior).buffer(0.01, cap_style=2, join_style=2) for polygon in polygons ]

		# Merging any touching Polygons.
		# More info on unary_union() :
		# https://shapely.readthedocs.io/en/latest/manual.html#shapely.ops.unary_union
		# https://shapely.readthedocs.io/en/latest/manual.html#object.union
		merge_result = unary_union(exteriors)

		if merge_result.geom_type == "MultiPolygon":
			merged_polygons.extend(merge_result)
		else:
			merged_polygons.append(merge_result)

	except Exception as e:
		raise Exception("An error occured while attempting to merge adjacent polygons.\nDetails {}".format(e))
	return merged_polygons

def nws_adjust_polygons_by_severity(polygons_by_zone_type):
	try:
		# Printing out what is in nws_zone_types_by_severity
		#for group in nws_zone_types_by_severity.keys():
		#	print group
		#	for type in nws_zone_types_by_severity[group].keys():
		#		print type
		#		ranking = nws_zone_types_by_severity[group][type]
		#		print ranking

		#	select the list of severity rankings associated with a group - done
		#		compare the polygons associated with the keys of one group to the polygons associated with the keys in the other group - done
		#			if h-polygon has a portion of itself within l-polygon
		#				create a hole in l-polygon the size of h-polygon
		#		go through the rankings from high to low - done
		#	move to the next group - done

		group_to_compare = None
		zone_names = None

		for group in nws_zone_types_by_severity.keys():
			group_to_compare = nws_zone_types_by_severity[group]
			# Sorting the zones in the severity group from high to low and selecting only the name of each zone from the resulting tuple.
			zone_names = [ x[0] for x in sorted(group_to_compare.items(), key=operator.itemgetter(1))] 

			print group_to_compare
			#print zone_names

			low_index = 0
			# Comparing all zone types in the selected group with rankings higher than 1 against all other types in that group.
			while low_index < len(zone_names):
				high_index = len(zone_names)-1
				while high_index > low_index:
					print "index:{} name:{}".format(low_index,zone_names[low_index])
					print "index:{} name:{}".format(high_index,zone_names[high_index])

					# If there are polygons of both types being compared.
					if polygons_by_zone_type.has_key(zone_names[low_index]) == True and polygons_by_zone_type.has_key(zone_names[high_index]) == True:
						polygons_low = polygons_by_zone_type[zone_names[low_index]]['polygons']
						polygons_high = polygons_by_zone_type[zone_names[high_index]]['polygons']
						print polygons_low
						print polygons_high

						i = 0
						while i < len(polygons_low):
							#if h-polygon has a portion of itself within l-polygon
								#create a hole in l-polygon the size of h-polygon
							j = 0
							while j < len(polygons_high) :
								intersection = polygons_low[i].intersection(polygons_high[j])
								print intersection
								if intersection.geom_type == "Polygon":
									polygons_low[i] = Polygon(polygons_low[i].exterior, holes=intersection.exterior)
									print polygons_low[i].exterior
									print polygons_low[i].interior
								j += 1
							i += 1

						print ""
					high_index -= 1
				low_index += 1

	except Exception as e:
		raise e
	return polygons_by_zone_type

def nws_get_merged_polygons(feature_collection):

	try:
		polygons_by_zone_type = {}

		# Finding each alert as a Feature within the retrieved FeatureCollection.
		for collection in feature_collection['features']:
			for feature in collection['features']:
				zone_type = get_zone_type_entity(feature['properties']['event'], nws_zone_types_id)
				# If the alert is not related to a type we are concerned with, skip this alert.
				if zone_type == None:
					continue

				# If this is the first time a particular zone type has been found within the collection, create a key for this zone type.
				if polygons_by_zone_type.has_key(zone_type['name']) == False:
					polygons_by_zone_type[zone_type['name']] = { 'zone_type_entity': zone_type, 'fill': get_rgba_from_hex(feature['properties']['fill'], alpha=100), 'polygons': [] }

				# Assign all polygons within the current Feature to the key related to this zone type.
				polygons_by_zone_type[zone_type['name']]['polygons'].extend(get_polygons_from_geometry(feature['geometry']))

		# Now that the polygons for each zone type have been collected, merge adjacent polygons of the same zone type.
		for key in polygons_by_zone_type.keys():
			polygons_by_zone_type[key]['polygons'] = merge_adjacent_polygons(polygons_by_zone_type[key]['polygons'])

	except Exceptions as e:
		raise e
	return polygons_by_zone_type

def nws_transform_into_entities(polygons_by_zone_type):

	try:
		entities = []
		active_from = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime())
		# Setting the expiration date for each zone to 1 hour and 30 minutes after it's creation time.
		# The zone should always be removed before this expiration date is reached.
		active_to = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', time.gmtime(time.time() + (60*90)))

		for type in polygons_by_zone_type.keys():
			for polygon in polygons_by_zone_type[type]['polygons']:
				entity = None
				# See the GeoTab API for the definition of the Zone entity.
				# https://my.geotab.com/sdk/api/apiReference.html#T:Geotab.Checkmate.ObjectModel.Zone
				entity = {
					"name": nws_names[0]+str(type),
					"mustIdentifyStops":False,
					#"displayed":False,
					"displayed":True,
					"activeFrom": active_from,
					"activeTo": active_to,
					"fillColor": polygons_by_zone_type[type]['fill'],
					"zoneTypes":[polygons_by_zone_type[type]['zone_type_entity']],
					"points": [{'x':point[0], 'y':point[1]} for point in polygon.exterior.coords],
					"groups":[{'id':"GroupCompanyId"}]
				}
				entities.append(entity)
				#if len(entities) == 1:
				#	break
			#if len(entities) == 1:
			#	break
	except Exception as e:
			raise Exception("An error occured while creating Zone entities from the provided polygons.\nDetails: {}".format(e))

	return entities

def transform_into_entities(level, new_geolayer_dict, entities, variable_name):
    if variable_name == 'seamless':
        return

    fill = get_rgba_from_hex(new_geolayer_dict['properties']['colour'], alpha=100)
    #rgb_color = colors.hex2color(new_geolayer_dict['properties']['colour'])
    #red = int(rgb_color[0]*255)
    #green = int(rgb_color[1]*255)
    #blue = int(rgb_color[2]*255)

    current_seconds = time.time()
    current_time_tuple = time.gmtime()
    active_from = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', current_time_tuple)
    later_seconds = current_seconds + (60*90)
    later = time.gmtime(later_seconds)
    active_to = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', later)

    if 'qpe' in variable_name:
        variable_name = 'Hydroplaning'

    for id_counter,polygon in enumerate(level):
        exteriorpoints = polygon.exterior.coords.xy
        points = zip(list(exteriorpoints[0]),list(exteriorpoints[1]))
        points = [{'x':point[0], 'y':point[1]} for point in points]

        zone_type = zone_types[variable_name]+risk_map[new_geolayer_dict['properties']['riskLevel']]
        current_zone_type_id = get_zone_type_entity(zone_type, all_zone_types_id)
        #for zone_type_id in all_zone_types_id:
        #    if zone_type_id['name'] == zone_type:
        #        current_zone_type_id = zone_type_id
        #        break

        entity = {
            "name": variable_name+'_'+str(new_geolayer_dict['properties']['riskLevel']),
            "mustIdentifyStops":False,
            "displayed":False,
            "activeFrom":active_from,
            "activeTo":active_to,
            #"fillColor":{"r":red, "g":green, "b":blue, "a":100},
            "fillColor": fill,
            "zoneTypes":[current_zone_type_id],
            "points": points,
            "groups":[{'id':"GroupCompanyId"}],
            "dontkeep" : polygon.exterior
        }
        entities.append(entity)

        # Holes in polygons
        interiors = list(polygon.interiors)
        for interior in interiors:
            intpoints = interior.coords.xy
            points = zip(list(intpoints[0]),list(intpoints[1]))
            points = [{'x':point[0], 'y':point[1]} for point in points]
            if 'Local' in variable_name:
                variable_name = 'Hydroplaning'
            entity = {
                "name": variable_name+'_None_'+str(new_geolayer_dict['properties']['riskLevel']),
                "mustIdentifyStops":False,
                "displayed":False,
                "activeFrom":active_from,
                "activeTo":active_to,
                "fillColor":{"r":255, "g":255, "b":255, "a":100},
                "zoneTypes":[no_alert],
                "points": points,
                "groups":[{'id':"GroupCompanyId"}],
                "dontkeep" : interior
            }
            #"fillColor":{"r":red, "g":green, "b":blue, "a":100}
            entities.append(entity)


def ds_transform_into_entities(multipolygon,entities):
    state = multipolygon[1]
    multipolygon = multipolygon[0]

    state_colors = {'W':'#8BBA71','S':'#0F78A5','I':'#C554B3'}

    fill = get_rgba_from_hex(new_geolayer_dict['properties']['colour'], alpha=100)
    #rgb_color = colors.hex2color(state_colors[state])
    #red = int(rgb_color[0]*255)
    #green = int(rgb_color[1]*255)
    #blue = int(rgb_color[2]*255)

    current_seconds = time.time()
    current_time_tuple = time.gmtime()
    active_from = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', current_time_tuple)
    later_seconds = current_seconds + (60*90)
    later = time.gmtime(later_seconds)
    active_to = time.strftime('%Y-%m-%dT%H:%M:%S.000Z', later)

    if multipolygon.type == 'Polygon': # Make it into an iterable in case it's just a single polygon
        multipolygon = [multipolygon]

    for id_counter,polygon in enumerate(multipolygon):
        try:
            exteriorpoints = polygon.exterior.coords.xy
            points = zip(list(exteriorpoints[0]),list(exteriorpoints[1]))
            points = [{'x':point[0], 'y':point[1]} for point in points]

            zone_type = zone_types[state]
            current_zone_type_id = get_zone_type_entity(zone_type, all_zone_types_id)
            #for zone_type_id in all_zone_types_id:
            #    if zone_type_id['name'] == zone_type:
            #        current_zone_type_id = zone_type_id
            #        break

            entity = {
                "name": zone_type,
                "mustIdentifyStops":False,
                "displayed":False,
                "activeFrom":active_from,
                "activeTo":active_to,
                #"fillColor":{"r":red, "g":green, "b":blue, "a":100},
                "fillColor": fill,
                "zoneTypes":[current_zone_type_id],
                "points": points,
                "groups":[{'id':"GroupCompanyId"}],
                "dontkeep" : polygon.exterior
            }
            entities.append(entity)

            # Holes in polygons
            interiors = list(polygon.interiors)
            for interior in interiors:
                intpoints = interior.coords.xy
                points = zip(list(intpoints[0]),list(intpoints[1]))
                points = [{'x':point[0], 'y':point[1]} for point in points]

                entity = {
                    "name": zone_type,
                    "mustIdentifyStops":False,
                    "displayed":False,
                    "activeFrom":active_from,
                    "activeTo":active_to,
                    "fillColor":{"r":255, "g":255, "b":255, "a":100},
                    "zoneTypes":[no_alert],
                    "points": points,
                    "groups":[{'id':"GroupCompanyId"}],
                    "dontkeep" : interior
                }
                #"fillColor":{"r":red, "g":green, "b":blue, "a":100}
                entities.append(entity)
        except:
            traceback.print_exc()

def deal_with_holes(entities):
    """ Removes polygons that represent holes filled by higher risk polygons """

    to_remove = []
    holes = [entity for entity in entities if 'None' in entity['name']]
    for hole in holes:
        for entity in entities:
            if not 'None' in entity['name']:
               #if entity['dontkeep'].almost_equals(hole['dontkeep'],decimal=0):
                if entity['dontkeep'].centroid.equals_exact(hole['dontkeep'].centroid,tolerance=0.1):
                    to_remove.append(hole)
                    break
    for entity in to_remove:
        entities.remove(entity)
    cleaned_up = [toolz.dissoc(entity,'dontkeep') for entity in entities]

    return cleaned_up