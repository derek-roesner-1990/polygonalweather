"""

Transforms gridded datasets into thresholded geolocated polygons.
Many variables reside in parameters.py

"""
import socket
import sys
import time
import calendar
import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot
from matplotlib import colors
from scipy import ndimage
from scipy import misc
import fiona
import shapely
from shapely import wkb
from shapely import ops
import geojson
import json
import ast
import os
import socketIO_client
import toolz
import arrow

import alarms_and_triggers
import data_handler
import postgres_functions

from gfs_parameters import *

if using_geotab:
    import geotab_functions

import live_feed_functions
from live_feed_functions import hrrproducts, mrmsproducts, remove_old_files
from contour_functions import polygonize

import live_contours

def get_data(variable_index, forecasted):
    if forecasted == 'current':
        arrow_time = arrow.utcnow()
    else:
        arrow_time = arrow.utcnow().replace(hours=+forecasted)

    if variable_index.startswith('gfs-'): # lowercase is relevant here
        data = data_handler.get_layer(variable_index, 'latest')
    else:
        data = data_handler.get_layer('{}--{}'.format(source,variable_index), arrow_time, latest=False)

    if len(data.shape) > 2: # Removing higher levels, the first one is generally the closest to the surface
        data = data[0,:,:]

    return data, source, arrow_time

def cut_array(array):
    array = np.roll(array[20:600,:],675,axis=1)
    return array

def process_file(variable_index, geolayers, mongo_client = None, observations = None, cameras = None, entities= None, forecasted = 'current'):
    """ Opens and selects desired variables, returns records of polygons with relevant info """
    variable_name = variable_name_index_dict[variable_index]

    if variable_index.startswith('APCP'):
        variable_index = 'APCP_P8_L1_GLL0_acc_intermediate1h'
    data,source,arrow_time = get_data(variable_index, forecasted)

    x,y = data_handler.get_coordinate_grid(source)

    #x = x[30:630,250:-100] # Fixing looping around of longitude values
    #y = y[30:630,250:-100]
    #data = data[30:630,250:-100]

    x = cut_array(x)
    y = cut_array(y)
    data = cut_array(data)

    #data[data == -3] = 0

    #data = ndimage.interpolation.zoom(data, 0.10, order=0)
    #data = ndimage.morphology.grey_closing(data, size=(5,5))
    gaussian_sigma = 0.1
    data = ndimage.gaussian_filter(data,sigma=gaussian_sigma) #TODO Have it automatically computed based on grid resolution

    land_data = data_handler.get_layer('GFS:land:uncut', 'None', latest=False)
    land_data = cut_array(land_data)

    current_key = variable_name
    thresholds = threshold_map[current_key]

    # Takes care of whether a high value represents a high risk or the inverse
    inverted = False
    if current_key in inverse_variables:
        inverted = True

    land_data[:,1395] = 0 # Removes antimeridian line, we don't care about data along that line anyway
    if not inverted:
        data = data*land_data
    else:
        land_data[land_data == 0] = 100000 #high visibility
        land_data[land_data == 1] = 0
        data = data+land_data

    #data = ndimage.median_filter(data,(5,5))
    print 'Creating polygons for', variable_name

    data = ndimage.interpolation.zoom(data.astype(np.float32),4,order=2)
    data = ndimage.gaussian_filter(data,2)
    #if not inverted:
    #    data[:,1395*4] = 0
    #else:
    #    data[:,1395*4] = 100000
    x = ndimage.interpolation.zoom(x,4,order=2)
    y = ndimage.interpolation.zoom(y,4,order=2)

    levels, thresholds = polygonize(x,y,data,which_variable=current_key,thresholds=thresholds, inverted=inverted)

    #multipolygons = []
    #outputs = []
    records = []

    for i, level in enumerate(levels):
        print i, len(level)
        if current_key == 'seamless':
            exteriors = []
            for geom in level:
                exteriors.append(shapely.geometry.Polygon(geom.exterior))
            multipolygon = ops.unary_union(exteriors)
            multipolygon = multipolygon.simplify(0.001, preserve_topology=True)
        else:
            new_levels = []
            for poly in level:
                if max(poly.bounds[2],poly.bounds[0]) > 179.9:
                    continue
                if np.fabs(poly.bounds[2]-poly.bounds[0]) < 178:
                    if np.fabs(poly.bounds[2]-poly.bounds[0]) / np.fabs(poly.bounds[3]-poly.bounds[1]) < 50.0:
                        new_levels.append(poly.buffer(0))

            level = new_levels
            multipolygon = ops.unary_union(level)

        if multipolygon.area == 0:
            continue
        if current_key == 'qpe':
            pass
            #with open('landpolygon/northamerica.txt','r') as opened:
            #    northamerica = wkb.loads(opened.read(),hex=True)

            #multipolygon = multipolygon.intersection(northamerica)

        if not multipolygon.area > 0:
            print geojson.dumps(multipolygon)
            continue

        geolayer = geojson.dumps(multipolygon)
        geolayer_dict = ast.literal_eval(geolayer)

        if current_key in inverse_variables:
            if i == 0:
                geolayer_dict['thresholdValues'] = [-9999999.0, thresholds[i]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i-1],thresholds[i]]
        else:
            if i < (len(thresholds)-1):
                geolayer_dict['thresholdValues'] = [thresholds[i], thresholds[i+1]]
            else:
                geolayer_dict['thresholdValues'] = [thresholds[i],9999999.0]

        geolayer_dict['colour'] = color_map[current_key][value_to_string_map[current_key][thresholds[i]]]
        if forecasted != 'current':
            geolayer_dict['variableName'] = current_key#+'_f0{0}'.format(forecasted)
        else:
            geolayer_dict['variableName'] = current_key

        if current_key == 'seamless':
            riskLevel = threshold_map['seamless'].index(thresholds[i])
        else:
            riskLevel = risk_map[value_to_string_map[current_key][thresholds[i]]]
            riskLevelInt = risk_map_level[value_to_string_map[current_key][thresholds[i]]]

        speed_factor = speed_mapping[value_to_string_map[current_key][thresholds[i]]]

        '''
        if current_key in inverse_variables:
            riskLevel *= -1
            riskLevel += 4
        '''
        new_geolayer_dict = dict()

        new_geolayer_dict['geometry'] = {'type': geolayer_dict['type'], 'coordinates' : geolayer_dict['coordinates']}
        new_geolayer_dict['properties'] = {'thresholdValues' : geolayer_dict['thresholdValues'], 'variableName' : geolayer_dict['variableName'], 'colour' : geolayer_dict['colour'], 'riskLevel' : riskLevel, 'riskLevelInt': riskLevelInt}
        new_geolayer_dict['properties']['shape'] = multipolygon
        new_geolayer_dict['properties']['risk_name'] = value_to_string_map[current_key][thresholds[i]]
        new_geolayer_dict['properties']['valid_time'] = arrow_time
        new_geolayer_dict['properties']['speed_factor'] = speed_factor
        if forecasted != 'current':
            new_geolayer_dict['properties']['forecast_time'] = forecasted
        else:
            new_geolayer_dict['properties']['forecast_time'] = 0

        records.append(new_geolayer_dict)

        # Adds to entities
        if using_geotab:
            geotab_functions.transform_into_entities(level, new_geolayer_dict, entities, current_key)

    geolayers.extend(records)

def combine_geolayers(na_geolayers, geolayers):
    final_geolayers = []

    na_polygons = dict()

    for grid in ['HRRR', 'MRMS']:
        x,y = data_handler.get_coordinate_grid(grid)

        side_a = list(x[:,0])
        side_b = list(x[0,1:-1])
        side_c = list(x[:,-1])
        side_d = list(x[-1,1:-1])

        yside_a = list(y[:,0])
        yside_b = list(y[0,1:-1])
        yside_c = list(y[:,-1])
        yside_d = list(y[-1,1:-1])

        na_coords = zip(reversed(side_a),reversed(yside_a))+zip(side_b,yside_b)+zip(side_c,yside_c)+zip(reversed(side_d),reversed(yside_d))
        na_polygon = shapely.geometry.Polygon(na_coords)
        na_polygons[grid] = na_polygon.simplify(0.1)

    for poly in geolayers:
        grid_to_use = 'MRMS'
        if poly['properties']['variableName'] in ['windspeed','visibility']:
            grid_to_use = 'HRRR'

        geom = poly['properties']['shape']
        new_geom = geom.difference(na_polygons[grid_to_use])

        if new_geom.area == 0:
            continue

        new_geolayer = geojson.dumps(new_geom)
        new_geolayer_dict = ast.literal_eval(new_geolayer)

        poly['type'] = new_geolayer_dict['type']
        poly['coordinates'] = new_geolayer_dict['coordinates']
        poly['properties']['shape'] = new_geom

        final_geolayers.append(poly)

    factors = [speed_mapping['low'],speed_mapping['medium'],speed_mapping['high']]

    for na_geolayer in na_geolayers:
        na_geolayer['properties']['riskLevelInt'] = na_geolayer['properties']['riskLevel']
        na_geolayer['properties']['forecast_time'] = 0
        na_geolayer['properties']['valid_time'] = geolayers[0]['properties']['valid_time']
        na_geolayer['properties']['speed_factor'] = factors[na_geolayer['properties']['riskLevel']-1]

    final_geolayers.extend(na_geolayers)

    return final_geolayers

def main():
    """ Downloads files and push geojson polygons """
    print 'Starting', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    #start_time = time.time()
    start_time = arrow.utcnow().datetime
    #postgres_functions.push_start_time(process_name,start_time)

    mongo_client = None
    observations = None
    geolayers = []
    forecasted_geolayers = []
    entities = []

    cameras = None

    na_geolayers = live_contours.main(intermediate=True)

    for variable_index in variable_indices+['gfs-hail','gfs-lightning']:
        process_file(variable_index, geolayers, mongo_client, observations, cameras, entities=entities)

    geolayers = combine_geolayers(na_geolayers,geolayers)

    for forecasted_time in forecasted_times:
        print 'Forecast: ', forecasted_time
        for variable_index in forecasted_variable_indices:
            process_file(variable_index, forecasted_geolayers, mongo_client, observations, cameras, entities=entities, forecasted=forecasted_time)

    print 'Processed, now pushing to DB'
    if using_postgres:
        postgres_functions.push_gfs_contours(geolayers+forecasted_geolayers)

    alarms_and_triggers.geoserver_trigger('worldwide_hydroplaning')
    alarms_and_triggers.geoserver_trigger('worldwide_windspeed')
    alarms_and_triggers.geoserver_trigger('worldwide_visibility')

    alarms_and_triggers.geoserver_trigger('posh')
    alarms_and_triggers.geoserver_trigger('windspeed')
    alarms_and_triggers.geoserver_trigger('lightning')
    alarms_and_triggers.geoserver_trigger('visibility')
    alarms_and_triggers.geoserver_trigger('hydroplaning')

    print 'DB updated'
    #end_time = time.time()
    end_time = arrow.utcnow().datetime
    #print 'Updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
    #postgres_functions.push_end_time(process_name,start_time,end_time)
    #print 'Done updating DB', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

    try:
        if socket.gethostname() == 'ubuntu-cv1':
            #current_socketIO = socketIO_client.SocketIO('192.168.5.122',4051)
            pass
        else:
            current_socketIO = socketIO_client.SocketIO('https://wx2-api.weathertelematics.com')

        current_socketIO.emit('contours_trigger', time.time())
        current_socketIO.emit('hrrr_trigger', time.time())
        current_socketIO.disconnect()

    except Exception as e:
        print e

    if using_geotab:
        geotab_functions.delete_old_zones()
        print 'Deleted', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())
        cleaned_up = geotab_functions.deal_with_holes(entities)
        geotab_functions.push_to_geotab(cleaned_up)

    print 'Done', time.strftime('%Y-%m-%d %H:%M:%S',time.gmtime())

if __name__ == '__main__':
    main()
