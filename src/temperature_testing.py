import json
import time

import requests
import arrow

import data_handler

base_url = 'http://130.211.154.46:5005'

def main():
    current_time = str(arrow.utcnow().replace(minutes=-20))

    #road_temp_array = data_handler.get_layer('road_temperatures','latest')

    resp = requests.get('{}/id_closest_to_date/{}'.format(base_url, current_time))

    first_id = resp.content

    resp = requests.get('{}/id_offset/{}'.format(base_url, first_id))
    
    decoded = json.loads(resp.content)

    observations = decoded['observations']
    for observation in observations:
        lat = observation['latitude']
        lon = observation['longitude']
        unit_id = observation['unit']
        readings = observation['readings']
        for reading in readings:
            if reading['equipment_id'] == 2:
                road_temperature = reading['value']
            if reading['equipment_id'] == 3:
                air_temperature = reading['value']

        print unit_id,lat,lon,road_temperature,air_temperature

    last_id = decoded['last_id']

if __name__ == '__main__':
    main()